(function ($) {
    $(document).ready(function () {
    });
	Vue.component('date-item', {
		template: '#date-line-item',
		data() {
			return {
				checked: false,
				allEvents:[],
				start_date_html:'',
				slots:[],
				selected_slots:[],
				start_date:''
			}
		},
        props:{
		    date:{},
            index:0
        },
        watch:{
			start_date:function (val) {
                this.$emit('change-date',{
                    index:this.index,
                    date:val
                })
			},
			selected_slots:function (val) {
                this.$emit('change-date',{
					index:this.index,
                    slots:val
                })
			},
        },
		mounted(){
			var me = this;
			/*$(".bravo_tour_book").sticky({
				topSpacing:30,
				bottomSpacing:$(document).height() - $('.end_tour_sticky').offset().top + 40
			});*/

			var options = {
				singleDatePicker: true,
				showCalendar: false,
				sameDate: true,
				autoApply           : true,
				disabledPast        : true,
				dateFormat          : takeaseat.date_format,
				enableLoading       : true,
				showEventTooltip    : true,
				classNotAvailable   : ['disabled', 'off'],
				disableHightLight: true,
				minDate:bravo_booking_data.minDate,
				opens:'left',
				isInvalidDate:function (date) {
					for(var k = 0 ; k < me.allEvents.length ; k++){
						var item = me.allEvents[k];
						if(item.start == date.format('YYYY-MM-DD')){
							return item.active == 1  ? false : true;
						}
					}
					return false;
				}
			};

			this.$nextTick(function () {
				$(this.$refs.start_date).daterangepicker(options).on('apply.daterangepicker',
					function (ev, picker) {
						me.start_date = picker.startDate.format('YYYY-MM-DD');
						me.start_date_html = picker.startDate.format(takeaseat.date_format);
						me.selected_slots=[];
						var startDate = new Date(me.start_date).getTime();
						for (var ix in me.allEvents) {
							var item = me.allEvents[ix];
							var cur_date = new Date(item.start).getTime();
							if (cur_date === startDate) {
								if (item.slots != null) {
									me.slots = Object.assign([], item.slots);
								} else {
									me.slots = []
								}
								me.$emit('change-date',{
								    index:me.index,
                                    price:parseFloat(item.price)
                                })
							}
						}
					})
					.on('update-calendar',function (e,obj) {
						me.fetchEvents(obj.leftCalendar.calendar[0][0], obj.leftCalendar.calendar[5][6])
					});

			});

		},
		methods: {
			openStartDate(){
				$(this.$refs.start_date).trigger('click');
			},
			fetchEvents(start,end){
				var me = this;
				var data = {
					start: start.format('YYYY-MM-DD'),
					end: end.format('YYYY-MM-DD'),
					id:bravo_booking_data.id,
					for_single:1
				};
				$.ajax({
					url: bravo_booking_i18n.load_dates_url,
					dataType:"json",
					type:'get',
					data:data,
					beforeSend: function() {
						$('.daterangepicker').addClass("loading");
					},
					success:function (json) {
						me.allEvents = json;
                         $('.daterangepicker').daterangepicker();
						// var drp = $(me.$refs.start_date).data('daterangepicker');
						// drp.renderCalendar('left');
						// if (!drp.singleDatePicker) {
						// 	drp.renderCalendar('right');
						// }
						$('.daterangepicker').removeClass("loading");
						console.log(json);
					},
					error:function (e) {
						console.log(e);
						console.log("Can not get availability");
					}
				});
			},
			addSlots(slot){
				var me=this;
				if(this.selected_slots.indexOf(slot) >=0){
					me.selected_slots.splice(this.selected_slots.indexOf(slot),1);
				}else {
					me.selected_slots.push(slot);
				}
			},
		}
	});
    new Vue({
        el:'#bravo_custom_tour_book_app',
        data:{
            id:'',
            term_id:'',
            extra_price:[],
            person_types:[],
            slots:[],
            first_booking:false,
            selected_slots:[],
            message:{
                content:'',
                type:false
            },
            html:'',
            onSubmit:false,
            start_date:'',
            start_date_html:'',
            step:1,
            guests:0,
            price:0,
            max_guests:1,
            start_date_obj:'',
            duration:0,
            allEvents:[],
            dates:[
                {
                    date:'2020-05-07',
                    slots:['1']
                }
            ]
        },
        watch:{
            extra_price:{
                handler:function f() {
                    this.step = 1;
                },
                deep:true
            },
            start_date(){
                this.step = 1;
            },
            guests(){
                this.step = 1;
            },
            person_types:{
                handler:function f() {
                    this.step = 1;
                },
                deep:true
            },
        },
        computed:{
            total_slots:function () {
                var t = 0;
				for(var i = 0; i < this.dates.length; i++) {
					var date = this.dates[i];
					if (!date.date) continue;
					if (!date.slots || !date.slots.length) continue;

					t += date.slots.length;
				}
				return t;
			},
			total_price: function () {
				var me = this;
				var total = 0;
				for(var i = 0; i < this.dates.length; i++){
				    var date = this.dates[i];
				    if(!date.date) continue;
				    if(!date.slots || !date.slots.length) continue;

					total += parseFloat(date.price) * date.slots.length;

                }

				if(me.first_booking){
					total = total/2
				}

				if(me.discount_for_10_slot && this.total_slots >=10){

				    total -= total * 0.1;

                }else{
					if(me.discount_for_4_slot && this.total_slots >= 4){
						total -= total * 0.05;
                    }
                }

                return total;

			},
            total_price_html:function () {
              return window.bravo_format_money(this.total_price);
			},
            daysOfWeekDisabled(){
                var res = [];

                for(var k in this.open_hours)
                {
                    if(typeof this.open_hours[k].enable == 'undefined' || this.open_hours[k].enable !=1 ){

                        if(k == 7){
                            res.push(0);
                        }else{
                            res.push(k);
                        }
                    }
                }

                return res;
            }
        },
        created:function(){
            for(var k in bravo_booking_data){
                this[k] = bravo_booking_data[k];
            }
            if(localStorage.getItem('tour_start_date') != null){
                this.start_date = localStorage.getItem('tour_start_date')
            }
            if(localStorage.getItem('tour_start_date_html') != null){
                this.start_date_html = localStorage.getItem('tour_start_date_html')
            }
            if(localStorage.getItem('tour_slots') != null){
                this.slots = localStorage.getItem('tour_slots').split (",");
            }
            if(localStorage.getItem('tour_selected_slots') != null){
                this.selected_slots = localStorage.getItem('tour_selected_slots').split (",").map(function(item) {
                    return parseInt(item, 10);
                });;
            }
            if(localStorage.getItem('tour_term_id') != null){
                this.term_id = localStorage.getItem('tour_term_id')
            }
        },
        mounted(){
            var me = this;
            /*$(".bravo_tour_book").sticky({
                topSpacing:30,
                bottomSpacing:$(document).height() - $('.end_tour_sticky').offset().top + 40
            });*/

            // var options = {
            //     singleDatePicker: true,
            //     showCalendar: false,
            //     sameDate: true,
            //     autoApply           : true,
            //     disabledPast        : true,
            //     dateFormat          : takeaseat.date_format,
            //     enableLoading       : true,
            //     showEventTooltip    : true,
            //     classNotAvailable   : ['disabled', 'off'],
            //     disableHightLight: true,
            //     minDate:this.minDate,
            //     opens:'left',
            //     isInvalidDate:function (date) {
            //         for(var k = 0 ; k < me.allEvents.length ; k++){
            //             var item = me.allEvents[k];
            //             if(item.start == date.format('YYYY-MM-DD')){
            //                 return item.active ? false : true;
            //             }
            //         }
            //         return false;
            //     }
            // };

            this.$nextTick(function () {
                // $(this.$refs.start_date).daterangepicker(options).on('apply.daterangepicker',
                //     function (ev, picker) {
                //         me.start_date = picker.startDate.format('YYYY-MM-DD');
                //         me.start_date_html = picker.startDate.format(takeaseat.date_format);
                //         me.selected_slots=[];
                //         me.handleTotalPrice();
                //         var startDate = new Date(me.start_date).getTime();
                //         for (var ix in me.allEvents) {
                //             var item = me.allEvents[ix];
                //             var cur_date = new Date(item.start).getTime();
                //             if (cur_date === startDate) {
                //                 if (item.slots != null) {
                //                     me.slots = Object.assign([], item.slots);
                //                 } else {
                //                     me.slots = []
                //                 }
                //             }
                //         }
                //     })
                //     .on('update-calendar',function (e,obj) {
                //         me.fetchEvents(obj.leftCalendar.calendar[0][0], obj.leftCalendar.calendar[5][6])
                //     });
				//

                $('.dungdt-select2-field').each(function () {
                    var configs = $(this).data('options');
                    $(this).select2(configs);
                }).on('change',function(){
                    me.term_id = $(this).val();
                });
            });

            var url = window.location.href;
            var myParam = url.split('action=')[1];
            if(myParam === "doAddToCart"){
                me.doSubmit();
            }
        },
        methods:{
			changeDateLine(res){
			    for(var k in res){
			        this.dates[res.index][k] = res[k];
                }
            },
			addDate:function(){
			    this.dates.push({
                    date:'',
                    slots:[]
                })
            },
			removeDate:function () {
              if(this.dates.length >1){
                  this.dates.pop();
              }
			},
            fetchEvents(start,end){
                var me = this;
                var data = {
                    start: start.format('YYYY-MM-DD'),
                    end: end.format('YYYY-MM-DD'),
                    id:bravo_booking_data.id,
                    for_single:1
                };
                $.ajax({
                    url: bravo_booking_i18n.load_dates_url,
                    dataType:"json",
                    type:'get',
                    data:data,
                    beforeSend: function() {
                        $('.daterangepicker').addClass("loading");
                    },
                    success:function (json) {
                        me.allEvents = json;
                        $('.daterangepicker').daterangepicker();
                        // var drp = $(me.$refs.start_date).data('daterangepicker');
                        // drp.renderCalendar('left');
                        // if (!drp.singleDatePicker) {
                        //     drp.renderCalendar('right');
                        // }
                        $('.daterangepicker').removeClass("loading");
                        console.log(json);
                    },
                    error:function (e) {
                        console.log(e);
                        console.log("Can not get availability");
                    }
                });
            },
            formatMoney: function (m) {
                return window.bravo_format_money(m);
            },
            validate(){
                if(!this.dates.length)
                {
                    this.message.status = false;
                    this.message.content = bravo_booking_i18n.no_date_array_select;
                    return false;
                }else{
                	for(var i = 0 ; i < this.dates.length ; i++){
                		if(!this.dates[i].date){

							this.message.status = false;
							this.message.content = bravo_booking_i18n.no_date_select;
							return false;
						}
                		if(!this.dates[i].slots || !this.dates[i].slots.length){
							this.message.status = false;
							this.message.content = bravo_booking_i18n.no_date_slot_select+' '+ moment(this.dates[i].date).format(takeaseat.date_format);
							return false;
						}
					}
				}

                return true;
            },
            addPersonType(type){
                type.number = parseInt(type.number);
                if(type.number < parseInt(type.max) || !type.max) type.number +=1;
                this.handleTotalPrice();
            },
            minusPersonType(type){
                type.number = parseInt(type.number);
                if(type.number > type.min) type.number -=1;
                this.handleTotalPrice();
            },
            addGuestsType(){
                var me = this;
                if(me.guests < parseInt(me.max_guests) || !me.max_guests) me.guests +=1;
                this.handleTotalPrice();
            },
            minusGuestsType(){
                var me = this;
                if(me.guests > 1) me.guests -=1;
                this.handleTotalPrice();
            },
            addSlots(slot){
                var me=this;
                if(this.selected_slots.indexOf(slot) >=0){
                    me.selected_slots.splice(this.selected_slots.indexOf(slot),1);
                }else {
                    me.selected_slots.push(slot);
                }
              this.handleTotalPrice()
            },

            doSubmit:function () {
                if(this.onSubmit) return false;

                //if(!this.validate()) return false;

                this.onSubmit = true;
                var me = this;
//console.log(me);
                this.message.content = '';

                if(this.step == 1){
                    this.html = '';
                }
                this.guests = 0;
                $.ajax({
                    url:takeaseat.url+'/booking/addToCart',
                    data:{
                        service_id:this.id,
                        service_type:'tour',
                        term_id:this.term_id,
                        start_date:'2020-05-07',
                        person_types:this.person_types,
                        extra_price:this.extra_price,
                        step:this.step,
                        guests:this.guests,
                        selected_slots:'1',
						dates:this.dates
                    },
                    dataType:'json',
                    type:'post',
                    success:function(res){

                        if ( typeof(Storage) !== 'undefined') {
                            localStorage.setItem('tour_selected_slots', "");
                            localStorage.setItem('tour_slots', "");
                            localStorage.setItem('tour_start_date',"");
                            localStorage.setItem('tour_start_date_html', "");
                            localStorage.setItem('tour_term_id',"");
                        }
                        if(!res.status){
                            me.onSubmit = false;
                        }
                        if(res.message)
                        {
                            me.message.content = res.message;
                            me.message.type = res.status;
                        }

                        if(res.step){
                            me.step = res.step;
                        }
                        if(res.html){
                            me.html = res.html
                        }

                        //if(res.url){
                         //   window.location.href = res.url
                            var arr = res.url.split('/');
                            //console.log(arr[4]);
                             $('#exampleModal').modal({backdrop: 'static', keyboard: false});
                             $(".modal-body #code").val( arr[4] );
                        //}

                        if(res.errors && typeof res.errors == 'object')
                        {
                            var html = '';
                            for(var i in res.errors){
                                html += res.errors[i]+'<br>';
                            }
                            me.message.content = html;
                        }

                    },
                    error:function (e) {
                        console.log(e);
                        me.onSubmit = false;

                        //bravo_handle_error_response(e);

                        if(e.status == 401){

                            $('.bravo_tour_book_wrap').modal('hide');

                            $('#login').modal('show');

                            var url = window.location.href + "?action=doAddToCart"
                            takeaseat.routes.login = takeaseat.routes.login + "?referer="+url;
                            if ( typeof(Storage) !== 'undefined') {
                                localStorage.setItem('tour_selected_slots', me.selected_slots);
                                localStorage.setItem('tour_slots', me.slots);
                                localStorage.setItem('tour_start_date', me.start_date);
                                localStorage.setItem('tour_start_date_html', me.start_date_html);
                                localStorage.setItem('tour_term_id', me.term_id);
                            }

                        }

                        if(e.status != 401 && e.responseJSON){
                            me.message.content = e.responseJSON.message ? e.responseJSON.message : 'Can not booking';
                            me.message.type = false;

                        }
                    }
                })
            },
            openStartDate(){
                $(this.$refs.start_date).trigger('click');
            }
        }

    });

    $('.bravo-video-popup').click(function() {
        let video_url = $(this).data( "src" );
        let target = $(this).data( "target" );
        $(target).find(".bravo_embed_video").attr('src',video_url + "?autoplay=0&amp;modestbranding=1&amp;showinfo=0" );
    });


    $(window).on("load", function () {
        var urlHash = window.location.href.split("#")[1];
        if (urlHash &&  $('.' + urlHash).length ){
            var offset_other = 70
            if(urlHash === "review-list"){
                offset_other = 330;
            }
            $('html,body').animate({
                scrollTop: $('.' + urlHash).offset().top - offset_other
            }, 1000);
        }
    });

    $(".bravo-button-book-mobile").click(function () {
        $('.bravo_tour_book_wrap').modal('show');
    });

    $(".bravo_detail_tour .g-faq .item .header").click(function () {
        $(this).parent().toggleClass("active");
    });

})(jQuery);
