(function ($) {

    function makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    // Form Configs
    $('.has-ckeditor-email').each(function () {
        var els  = $(this);

        // ClassicEditor
        //     .create( els[0],{
        //         extraPlugins: [ BookingCoreAdaterPlugin ],
        //     })
        //     .catch( error => {
        //         console.error( error );
        //     } );

        var id = $(this).attr('id');

        if(!id){
            id = makeid(10);
            $(this).attr('id',id);
        }
        var h  = els.data('height');
        if(!h && typeof h =='undefined') h = 300;

        // CKEDITOR.replace( id );
        tinymce.init({
            selector:'#'+id,
            plugins: 'preview searchreplace autolink fullscreen image link media codesample table charmap hr toc advlist lists wordcount imagetools textpattern help pagebreak hr',
            toolbar: 'formatselect | bold italic strikethrough forecolor backcolor permanentpen formatpainter | link image media | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | pagebreak codesample | removeformat',
            image_advtab: true,
            image_caption: true,
            toolbar_drawer: 'sliding',
            height:h,
            relative_urls : false,
            remove_script_host : false,
            file_picker_callback: function (callback, value, meta) {
                /* Provide file and text for the link dialog */
                if (meta.filetype === 'file') {
                    uploaderModal.show({
                        multiple:false,
                        file_type:'video',
                        onSelect:function (files) {
                            if(files.length)
                                callback(takeaseat.url+'/media/preview/'+files[0].id);
                        },
                    });
                }

                /* Provide image and alt text for the image dialog */
                if (meta.filetype === 'image') {
                    uploaderModal.show({
                        multiple:false,
                        file_type:'image',
                        onSelect:function (files) {
                            console.log(files);
                            if(files.length)
                                callback(files[0].full_size);
                        },
                    });
                }

                /* Provide alternative source and posted for the media dialog */
                if (meta.filetype === 'media') {
                    uploaderModal.show({
                        multiple:false,
                        file_type:'video',
                        onSelect:function (files) {
                            if(files.length)
                                callback(takeaseat.url+'/media/preview/'+files[0].id);
                        },
                    });
                }
            },
        });

    });

    $(document).on('click','.refresh',function(){

        //var image = $('.imgUrl').attr('src');
        var image = $('.attach-demo').find('img').attr('src'); 
        $('#imageCropper').attr("src", image);
        $('#cdn-browser-modal-cropper').modal('show');
        var $image = $('#imageCropper');
        var $download = $('#download');
        var $dataX = $('#dataX');
        var $dataY = $('#dataY');
        var $dataHeight = $('#dataHeight');
        var $dataWidth = $('#dataWidth');
        var $dataRotate = $('#dataRotate');
        var $dataScaleX = $('#dataScaleX');
        var $dataScaleY = $('#dataScaleY');
        var options = {
            aspectRatio: 1 / 1,
            preview: '.img-preview',
            crop: function (e) {
              $dataX.val(Math.round(e.detail.x));
              $dataY.val(Math.round(e.detail.y));
              $dataHeight.val(Math.round(e.detail.height));
              $dataWidth.val(Math.round(e.detail.width));
              $dataRotate.val(e.detail.rotate);
              $dataScaleX.val(e.detail.scaleX);
              $dataScaleY.val(e.detail.scaleY);
            }
        };

        var originalImageURL = $image.attr('src');
        var uploadedImageName = 'cropped.jpg';
        var uploadedImageType = 'image/jpeg';
        var uploadedImageURL;

        // Tooltip
        $('[data-toggle="tooltip"]').tooltip();

        $image.cropper({
            autoCrop: false,
            viewMode: 2,
            ready: function (e) {
                $('.white_background').hide();
                $('.spinner-border').hide();
                //$('.content').show();
            },
        });

        $('.docs-buttons').on('click', '[data-method]', function () {
            var $this = $(this);
            var data = $this.data();
            var cropper = $image.data('cropper');
            var cropped;
            var $target;
            var result;

            if ($this.prop('disabled') || $this.hasClass('disabled')) {
                return;
            }

            if (cropper && data.method) {
                data = $.extend({}, data); // Clone a new one
                if (typeof data.target !== 'undefined') {
                    $target = $(data.target);
                    if (typeof data.option === 'undefined') {
                        try {
                            data.option = JSON.parse($target.val());
                        } catch (e) {
                            console.log(e.message);
                        }
                    }
                }
                cropped = cropper.cropped;
                switch (data.method) {
                    case 'rotate':
                        if (cropped && options.viewMode > 0) {
                            $image.cropper('clear');
                        }
                        break;
                    case 'getCroppedCanvas':
                        if (uploadedImageType === 'image/jpeg') {
                            if (!data.option) {
                                data.option = {};
                            }
                            data.option.fillColor = '#fff';
                        }
                        break;
                }

                result = $image.cropper(data.method, data.option, data.secondOption);

                switch (data.method) {
                    case 'rotate':
                        if (cropped && options.viewMode > 0) {
                            $image.cropper('crop');
                        }
                        break;
                    case 'scaleX':
                    case 'scaleY':
                        $(this).data('option', -data.option);
                        break;
                    case 'getCroppedCanvas':
                        if (result) {
                            // Bootstrap's Modal
                            $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);

                            if (!$download.hasClass('disabled')) {
                                download.download = uploadedImageName;
                                $download.attr('href', result.toDataURL(uploadedImageType));
                            }
                        }
                        break;
                    case 'destroy':
                        if (uploadedImageURL) {
                            URL.revokeObjectURL(uploadedImageURL);
                            uploadedImageURL = '';
                            $image.attr('src', originalImageURL);
                        }

                        break;
                }
                if ($.isPlainObject(result) && $target) {
                    try {
                        $target.val(JSON.stringify(result));
                    } catch (e) {
                        console.log(e.message);
                    }
                }
            }
        });
    });

    $(document).on('click','#useFile',function(){
        
        $('#imageCropper').cropper('getCroppedCanvas').toBlob(function (blob) {
            var formData = new FormData();
            var file = 'test.png';
            var file = new File([blob], file);
            formData.append('type', 'image');
            formData.append('file', file);
            $.ajax({
                url: '/admin/module/media/store',
                method: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function (response) {
                    $('.attach-demo').find('img').attr("src", response.url);
                    $('.imageId').val(response.data.id);
                    $('#cdn-browser-modal-cropper').modal('hide');
                    $('#imageCropper').attr("src", '');
                },
            });
        });
    });

    $(document).on('click','.close',function(){
        $("#imageCropper").cropper("destroy");
    });

})(jQuery);
