jQuery(function ($) {
    $.fn.bravoAutocomplete_custom = function (options) {
        return this.each(function () {
            var $this = $(this);
            var main = $(this).closest(".smart-search");
            var textLoading = options.textLoading;
            main.append('<div class="bravo-autocomplete on-message"><div class="list-item"></div><div class="message">'+textLoading+'</div></div>');
            $(document).on("click.Bst", function(event){
                if (main.has(event.target).length === 0 && !main.is(event.target)) {
                    main.find('.bravo-autocomplete').removeClass('show');
                } else {
                    //main.find('.bravo-autocomplete').addClass('show');
                }
            });
            if (options.dataDefault.length > 0) {
                var items = '';
                for (var index in options.dataDefault) {
                    var item = options.dataDefault[index];
                    items += '<div class="item" data-id="' + item.id + '" data-text="' + item.title + '"> <i class="'+options.iconItem+'"></i> ' + item.title + ' </div>';
                }
                main.find('.bravo-autocomplete .list-item').html(items);
                main.find('.bravo-autocomplete').removeClass("on-message");
            }
            var requestTimeLimit;
            $this.keyup(function () {
                main.find('.bravo-autocomplete').addClass("on-message");
                main.find('.bravo-autocomplete .message').html(textLoading);
                main.find('.child_id').val("");
                var query = $(this).val();
                clearTimeout(requestTimeLimit);
                if(query.length === 0){
                    if (options.dataDefault.length > 0) {
                        var items = '';
                        for (var index in options.dataDefault) {
                            var item = options.dataDefault[index];
                            items += '<div class="item" data-id="' + item.id + '" data-text="' + item.title + '"> <i class="'+options.iconItem+'"></i> ' + item.title + ' </div>';
                        }
                        main.find('.bravo-autocomplete .list-item').html(items);
                        main.find('.bravo-autocomplete').removeClass("on-message");
                    }
                    main.find('.bravo-autocomplete').removeClass('show');
                    return;
                }
                requestTimeLimit = setTimeout(function () {
                    $.ajax({
                        url: options.url,
                        data: {
                            search: query,
                        },
                        dataType: 'json',
                        type: 'get',
                        beforeSend: function() {
                        },
                        success: function (res) {
                            if(res.status === 1){
                                var items = '';
                                for (var ix in res.data) {
                                    var item = res.data[ix];
                                    items += '<div class="item" data-id="' + item.id + '" data-text="' + item.title + '"> <i class="'+options.iconItem+'"></i> ' + get_highlight(item.title,query) + ' </div>';
                                }
                                main.find('.bravo-autocomplete .list-item').html(items);
                                main.find('.bravo-autocomplete').removeClass("on-message");
                            }
                            if(res.message.length > 0){
                                main.find('.bravo-autocomplete').addClass("on-message");
                                main.find('.bravo-autocomplete .message').html(res.message);
                            }
                        }
                    })
                }, 700);
                function get_highlight(text, val) {
                    return text.replace(
                        new RegExp(val + '(?!([^<]+)?>)', 'gi'),
                        '<span class="h-line">$&</span>'
                    );
                }
                main.find('.bravo-autocomplete').addClass('show');
            });
            main.find('.bravo-autocomplete').on('click','.item',function () {
                var id = $(this).attr('data-id'),
                    text = $(this).attr('data-text');
                if(id.length > 0 && text.length > 0){
                    main.find('.parent_text').val(text).trigger("change");
                    main.find('.child_id').val(id).trigger("change");
                }else{
                    console.log("Cannot select!")
                }
                setTimeout(function () {
                    main.find('.bravo-autocomplete').removeClass('show');
                },100);
            });
        });
    };
});
jQuery(function ($) {

    $('.bravo-list-locations.style2 .owl-carousel').owlCarousel({
        loop:false,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:6
            }
        }
    })
    $('.g-term .owl-carousel').owlCarousel({
        loop:false,
        margin:80,
        nav:true,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });

    $(".smart-search .smart-search-term").each(function () {
        var $this = $(this);
        var string_list = $this.attr('data-default');
        var default_list = [];
        if(string_list.length > 0){
            default_list = JSON.parse(string_list);
        }
        var options = {
            url: takeaseat.url+'/term/getForSelect2',
            dataDefault: default_list,
            textLoading: $this.attr("data-onLoad"),
            iconItem: "icofont-location-pin",
        };
        $this.bravoAutocomplete_custom(options);
    });

    $(".bravo-form-search-tour-2 .g-form-book .bravo_form .btn-search").click(function (event) {
        event.preventDefault();
        $(".bravo_form_book_select").fadeIn();
        $("body").css("overflow","hidden");
        return false
    });

    $(".bravo-form-search-tour-2 .g-form-book input[name=lng]").change(function (event) {
        event.preventDefault();
        $(".bravo_form_book_select").fadeIn();
        $("body").css("overflow","hidden");
        return false
    });

    $(".bravo_form_book_select .list-item .item").click(function () {
        var term_class = $(this).attr("data-term");
        $("."+term_class).prop('checked', true);
        $(".bravo-form-search-tour-2 .g-form-book .bravo_form").submit();
    });


    $(".bravo_form_book_select .g-form-book .child_id").change(function () {
        $(".bravo-form-search-tour-2 .g-form-book .child_id").val( $(this).val() );
        $(".bravo-form-search-tour-2 .g-form-book .bravo_form").submit();
    });

    $(".bravo_form_book_select .g-form-book .bravo_form .btn-search").click(function (event) {
        event.preventDefault();
        $(".bravo-form-search-tour-2 .g-form-book .child_id").val( $(this).val() );
        $(".bravo-form-search-tour-2 .g-form-book .bravo_form").submit();
        return false
    });

    $(".bravo-modal-ver2 .bravo-effect").click(function (event) {
        $(this).closest(".bravo-modal-ver2").fadeOut();
        $("body").attr("style","");
    });

    $(".vendor-about-text .go_to_faqs").click(function () {
        $('html,body').animate({
            scrollTop: $('.bravo-faq-lists').offset().top - 50
        }, 'slow');
    })

    $(".bravo_search_tour .bravo_filter .filter-title").each(function () {
        if($(window).width() <= 990){
            $(this).append('<i class="fa fa-angle-down" style="float: right;margin-right: 20px;font-size: 22px;"></i>');
            $(this).addClass("e-close");
            $(this).closest(".bravo_filter").addClass("hide-item");
        }
        $(this).click(function () {
            $(this).closest(".bravo_filter").toggleClass("hide-item");
        });
    });

});
window.initAutocomplete = function(){
    $('.input-search-google').each(function(){
        var autocomplete;
        var id  = $(this).attr('id');
        var parent = $(this).closest('.form-group');
        var input = $(this);
   
        autocomplete = new google.maps.places.Autocomplete(
            document.getElementById(id));
      
        // Avoid paying for data that you don't need by restricting the set of
        // place fields that are returned to just the address components.
        autocomplete.setFields(['address_component','geometry']);
      
        // When the user selects an address from the drop-down, populate the
        // address fields in the form.
        autocomplete.addListener('place_changed', function(){
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            
            if (place.geometry) {
                //parent.find
                $('[name=lat]').val(place.geometry.location.lat()).trigger("change");
                $('[name=lng]').val(place.geometry.location.lng()).trigger("change");
            }else{
                input.val('');
            }
        });
    })
}