@extends('layouts.app')

@section('content')



<!-- welcome seat -->
<div class="welcome">
  <div class="container">
    <div class="row">
      <div class="welcome-text clearfix">
        <h3>About Us</h3>
        <p>takeaseat.co.uk is an online community bringing together potential clients with counsellors, therapists and coaches. With ZERO cost to professionals. Our mission is to get people the help they need and let the professionals focus on the work
they love </p>

      <h6 class="blue-text font-weight-bold"> Our Story</h6>
        <p>After 5 years of education alongside a full-time job. Pam had fully qualified as a counsellor and was ready to commit to a
focussed career in something she loved and felt was her calling. One big problem, no clients </p>

   <p>At nearly 60 years old with an ever-changing digital landscape, social media options Pam never fully understood and
limited financial resources. She was in a chicken/egg scenario of no clients, no money. The road ahead seemed long and
difficult. Alongside this, on my journey, I had spent a decade building a business, while raising a family, had found many
complications while looking for support with the stresses that accompanied such a hectic lifestyle. Life coaches and
marriage counselling had been hard to find. People not talking about their own experiences or recommending anyone. It
felt like a gamble every time you were to meet a professional. Not to mention the expectation of opening up to someone
you had found on a google search or directory. </p>

<p> Together we decided something had to be done. Teaming up with an expert Marketeer, who had had similar frustration
on his journey and had given up on his search for support. Also, a social worker and family mediator who is passionate
about getting people and families the help they need. Take a seat was born</p>
</div>
    
    </div>

  </div>  

</div>
<!--<div class="mission">
<div class="container-fluid">
   <div class="row therapist-row text-center mt-5 mb-5">
     <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
      <div class="therapist theme-gradient theme-gradient-1 therapist-about">          
        <div class="logo-icon"></div>
        <h3 class="text-white mt-5 mb-3">
       Mission</h3>
       <p class="text-white">To build a trusted community for people and professionals to come together.
To help counsellors, coaches and therapists do the work they want to without
worrying about the financial implications of the early years.
To accelerate the number of people getting help.
To accelerate the number of professionals getting work.
To help counselling, therapy and coaching be viewed
as an essential personal investment.</p>
      </div>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
     <div class="therapist-photo">
      <img src="{{asset('public/images/Photo_Woman.jpg')}}" alt="woman" class="img-fluid">        
    </div>
  </div>
</div>
</div>
</div>

<div class="container">
  <div class="row">
  <div class="about-para">
     <h6 class="pink-text font-weight-bold"> How does it work?
 </h6>

 <p>Using online and social media marketing campaigns to help clients come to a place they can trust, revisit and
have contact with. They will have access through recommendations and searching functionality of our database
of pre-approved professionals.</p>

<p>The professionals will have full account management and support on building their digital profile, engaging with
service users through a completely free online booking and diary management software. All very simple and
straightforward to use. </p>

<h6 class="pink-text font-weight-bold mt-4"> The Pledge </h6>
<p>We aim to work with all registered mental health charities and provide 20,000 hours of Mental health services
per week free of charge. Ambitious we know but achievable and we intend to deliver on this</p>

<p>For more information or any questions you may have. Please contact <strong style="text-decoration:underline;"> support@takeaseat.co.uk </strong></p>

  </div>
</div>
</div>-->

</div>
<!-- end welcome -->


<section class="about-mission theme-gradient">
    <div class="container">
        <div class="row">
             <div class="col-md-12">
                <div class="mission-title text-center">
                   <img src="{{asset('custom/assets/images/Mission_Icon.png')}}" alt="mission" class="img-fluid">
                   <h3> Mission </h3>
                </div>
             </div>
        </div>
    </div>
    
    <div class="mission-icons">
      <div class="container">
        <div class="row">
             <div class="col-md">
                <div class="mission-item">
                   <img src="{{asset('custom/assets/images/Trust_Community_Icon.png')}}" alt="mission" class="img-fluid">
                   <p> To build a trusted community for people and professionals to come together.  </p>
                </div>
             </div>
             
             <div class="col-md">
                <div class="mission-item">
                   <img src="{{asset('custom/assets/images/Balance_Icon.png')}}" alt="mission" class="img-fluid">
                     <p> To help counsellors, coaches and therapists do the work they want to without worrying about the financial implications of the
early years.  </p>
                </div>
             </div>
             
             <div class="col-md">
                <div class="mission-item">
                   <img src="{{asset('custom/assets/images/Accelerate_Icon.png')}}" alt="mission" class="img-fluid">
                    <p>  To accelerate the number of people getting help.   </p>
                </div>
             </div>
             
             <div class="col-md">
                <div class="mission-item">
                   <img src="{{asset('custom/assets/images/Professional_ID_Icon.png')}}" alt="mission" class="img-fluid">
                    <p> To accelerate the number of professionals getting work.   </p>
                </div>
             </div>
             
             <div class="col-md">
                <div class="mission-item">
                   <img src="{{asset('custom/assets/images/Heart_Icon.png')}}" alt="mission" class="img-fluid">
                   <p> To help counselling, therapy and coaching be viewed as an essential personal investment. </p>
                </div>
             </div>
             
             
        </div>
    </div>
    
    </div>
    
    
</section>
<!--end mission-->


<section class="how-work">
    <div class="container">
        <div class="row">
            <div class="col-12">
               <div class="workcontent">
                   <h3>How does it work? </h3>
                   <p>Using online and social media marketing campaigns to help clients come to a place they can trust, revisit and
have contact with. They will have access through recommendations and searching functionality of our database
of pre-approved professionals.</p>

 <p>The professionals will have full account management and support on building their digital profile, engaging with
service users through a completely free online booking and diary management software. All very simple and
straightforward to use. </p>
<p class="blue-text font-weight-bold">Contact</p>
<p>
For more information or any questions you may have. Please contact <a class="blue-text" href="mailto:support@takeaseat.co.uk"><strong>support@takeaseat.co.uk</strong></a>
</p>
               </div>
            </div>
        </div>
    </div>
</section>
<!--end how work -->



<section class="pledge theme-gradient">
    <div class="container">
        <div class="row">
             <div class="col-md-12">
                <div class="mission-title text-center">
                   <img src="{{asset('custom/assets/images/Pledge_Icon.png')}}" alt="mission" class="img-fluid">
                   <h3 class="mb-5"> Take A Seat Pledge  </h3>
                   <p class="pledge-para">Here at Take A Seat we want to give something back, so we’re asking every therapist who becomes a member to
give 1 hour per week free of charge. We will then work with registered mental health charities to provide
thousands of hours of free mental health services per week free of charge.</p>

<p class="pledge-para">Ambitious we know but achievable and we intend to deliver. </p>
                </div>
             </div>
        </div>
    </div>
    
</section>
<!--end pledge-->








<div class="social-section text-center">
  <div class="container">
    <img src="{{asset('public/images/LogoIcon_Colour.png')}}" class="img-fluid mb-5">
    <h3 class="blue-text mb-3 text-center">Social</h3>
    <div class="row d-flex justify-content-center">
      <a href="#"> <img src="{{asset('public/images/YoutubeIcon.png')}}"></a>
      <a href="#"> <img src="{{asset('public/images/TwitterIcon.png')}}"></a>
      <a href="#"> <img src="{{asset('public/images/FacebookIcon.png')}}"></a>
    </div>
  </div>
</div>
<!-- end social-section -->

@endsection