@extends('layouts.app')

@section('content')



<!-- welcome seat -->
<div class="welcome">
  <div class="container">
    <div class="row">
      <div class="welcome-text cookies-policy clearfix">
        <h3 class="text-center"> Terms and Conditions     </h3>

        <h6 class="blue-text font-weight-bold"> IMPORTANT LEGAL NOTICE  </h6>

        <p>1. INTRODUCTION AND OUR ROLE
1.1. Company details: take a seat is a company registered in England and Wales with registered company number
12159548, whose registered office is at 27 Old Gloucester Street, London, United Kingdom, WC1N 3AX</p>

<p>1.2. VAT number: Our VAT number is GB 945 7192 91.</p>

<p>1.3. Product Orders: We provide a marketplace for clients to schedule a session with; counsellors, life coaches &
therapists (Professionals) displayed on our Website. </p>  

   <p>By accessing any part of the Website, you indicate that you accept these Website Terms. If you do not accept these
Website Terms, you should leave the Website immediately, and you will not be able to schedule appointments through
the Website</p>

 <p>The legal contract for the supply and purchase of counselling is between you,the client and the professional/s that you
schedule your appointment with and we will conclude the sale appointment on behalf as an agent for, the professionals
in all cases. </p>

<h6 class="blue-text font-weight-bold">2. WEBSITE ACCESS AND TERMS </h6>
<p>2.1. Website access: You may access some areas of the Website without making a booking for an appointment or for
registering your details with us. Most areas of the Website are open to everyone</p>

<p>2.2. Acceptance of terms: By accessing any part of the Website, you indicate that you accept our Website Terms. If you do
not accept our Website Terms, you should leave the Website immediately, and you will not be able to order any Products
through the Website</p>


<p>2.3. Revision of terms: We may revise these Website Terms at any time. You should check the Website regularly to review
the current Website Terms because they are binding on you. You will be subject to the policies and terms and conditions
in force at the time that you place an Order through us.</p>

<p>2.4. Responsibility: You are responsible for making all arrangements necessary for you to have access to the Website. You
are also responsible for ensuring that all persons who access the Website through your Internet connection are aware of
these Website Terms and that they comply with them.</p>

<h6 class="blue-text font-weight-bold"> 3. HOW TO BOOK AN APPOINTMENT AND HOW IT IS PROCESSED 
</h6>

<p>3.1. Compiling your Order: Once you have selected the Professional of your choice and provided the other required
information, you will be given the opportunity to submit your Order by clicking or selecting the "Book Now", "Book Date
& Time" or similar button. It is important that you check all the information that you enter and correct any errors before
clicking or selecting this button; once you do so you will be entering into a payment portal and errors cannot be corrected
(subject to paragraph 3.2. below). </p>

<p>3.1. Compiling your Order: Once you have selected the Professional of your choice and provided the other required
information, you will be given the opportunity to submit your Order by clicking or selecting the "Book Now", "Book Date
& Time" or similar button. It is important that you check all the information that you enter and correct any errors before
clicking or selecting this button; once you do so you will be entering into a payment portal and errors cannot be corrected
(subject to paragraph 3.2. below).</p>

<p>3.3. Payment authorisation: Where any payment you make is not authorised, your appointment will not be processed or
communicated to the relevant professional</p>


<h6 class="blue-text font-weight-bold">  4. PRICE AND PAYMENT
 </h6>

<p>4.1. VAT and processing costs: Prices will be as quoted on the Website. These prices include VAT but may exclude
processing costs. These will be added to the total amount due where applicable.</p>


<p>4.2. Incorrect pricing: This Website contains a large number of professionals and it is possible that some may include
incorrect prices. In such an event, neither we nor the relevant professional is under any obligation to ensure that the
appointment is provided to you at the incorrect lower price or to compensate you in respect of incorrect pricing..</p>


<p>4.3. Payment methods: Payment for appointments must be made by an accepted credit or debit card through the Website
only at the point of booking by, you.</p>

<p>4.4. Card payments: Please note that from time to time there may be delays with the processing of card payments and
transactions; this may result in payments taking up to sixty (60) days to be deducted from your bank account or charged
to your credit or debit card.
</p>


<h6 class="blue-text font-weight-bold"> 5. WEBSITE ACCESS  </h6>

<p>5.1. Website availability: While we try to ensure the Website is normally available twenty-four (24) hours a day, we do not
undertake any obligation to do so, and we will not be liable to you if the Website is unavailable at any time or for any
period.</p>


<p>5.2. Suspension of access: Access to the Website may be suspended temporarily at any time and without notice.</p>

<p>5.2. Suspension of access: Access to the Website may be suspended temporarily at any time and without notice.</p>

<p>5.3. Information security: The transmission of information via the Internet is not completely secure. Although we take the
steps required by law to protect your information, we cannot guarantee the security of your data transmitted to the
Website; any transmission is at your own risk.</p>

<h6 class="blue-text font-weight-bold"> 6. LIABILITY </h6>

<p>6.1. General: Nothing in these Website Terms excludes or limits our liability for death or personal injury arising from our
negligence, our liability for fraudulent misrepresentation, or any other liability which cannot be excluded or limited under
applicable law. Nothing in these Website Terms affects your statutory rights.</p>

<p>6.2. Exclusion of liability: Subject to clause 5.1, we will under no circumstances whatsoever be liable to you, whether in
contract, tort (including negligence), breach of statutory duty, or otherwise, even if foreseeable, arising under or in
connection with the Website (including the use, inability to use or the results of use of the Website) for:.</p>

<p>6.2.1. any loss of profits, sales, business, or revenue; .</p>

<p>6.2.2. loss or corruption of data, information or software;
three key areas</p>
<p>6.2.3. loss of business opportunity;</p>
<p>6.2.4. loss of anticipated savings;</p>
<p>6.2.5. loss of goodwill; or</p>

<p>6.2.6. any indirect or consequential loss.</p>

<p>6.3. Limitation of liability: Subject to clauses 6, 6.1 and 6.2, our total liability to you in respect of all other losses arising
under or in connection with the Website or your use of it, whether in contract, tort (including negligence), breach of
statutory duty, or otherwise, shall in no circumstances exceed twice the value of fee’s or £1000, whichever is lower, in
accordance with our Introductions & Ownership Policy.</p>

<p>6.4. Additional costs: You assume full and sole responsibility for any additional or associated costs that you may incur in
connection with or as a result of your use of the Website, including without limitation costs relating to the servicing,
software or data that you may own, lease, license or otherwise use.</p>


<h6 class="blue-text font-weight-bold"> 6. LIABILITY </h6>


<p>7.1. Grounds for termination: We may terminate or suspend (at our absolute discretion) your right to use the Website
immediately by notifying you in writing (including by email) if we believe in our sole discretion that:</p>

<p>7.1.1. you have used the website to acquire clients and breached TAS ‘introduction & ownership policy’);</p>

<p>7.1.2. you have posted fraudulent reviews or Visitor Material in order to enhance visibility on the platform.
</p>

<p>7.1.4. you have breached any other material terms of these Website Terms.</p>

<p>7.2. Obligations upon termination: Upon termination or suspension you must immediately destroy any downloaded or
printed extracts from the Website.</p>

<h6 class="blue-text font-weight-bold">8. WRITTEN COMMUNICATIONS
 </h6>

 <p>8.1. Applicable laws require that some of the information or communications we send to you should be in writing. When
using the Website or booking appointments via the Website, you accept that communication with us will be mainly
electronic. We will contact you by email or provide you with information by posting notices on the Website. For
contractual purposes, you agree to this electronic means of communication and you acknowledge that all contracts,
notices, information and other communications that we provide to you electronically comply with any legal requirement
that such communications be in writing. This condition does not affect your statutory rights.
</p>
<h6 class="blue-text font-weight-bold">9. EVENTS OUTSIDE OUR CONTROL  </h6>

<p>9.1. We will not be liable or responsible for any failure to perform, or delay in performance of, any of our obligations
under these Website Terms that is caused by events outside our reasonable control ("Force Majeure Event").
</p>

<p>9.2. A Force Majeure Event includes any act, event, non-occurrence, omission or accident beyond our reasonable control
and includes in particular (without limitation) the following:</p>

<p>9.2.1. strikes, lock-outs or other industrial action;</p>

<p>9.2.2. civil commotion, riot, invasion, terrorist attack or threat of terrorist attack, war (whether declared or not) or threat
or preparation for war;</p>

<p>9.2.3. fire, explosion, storm, flood, earthquake, subsidence, epidemic or other natural disaster;</p>

<p>9.2.4. impossibility of the use of railways, shipping, aircraft, motor transport or other means of public or private transport;
</p>

<p>9.2.5. impossibility of the use of public or private telecommunications networks; and</p>

<p>9.2.6. the acts, decrees, legislation, regulations or restrictions of any government. <br>
9.3. Our performance under these Website Terms is deemed to be suspended for the period that any Force Majeure Event
continues, and we will have an extension of time for performance for the duration of that period. We will use our
reasonable endeavours to bring any Force Majeure Event to a close or to find a solution by which our obligations under
these Website Terms may be performed despite the Force Majeure Event.</p>

<h6 class="blue-text font-weight-bold"> 10. INTELLECTUAL PROPERTY RIGHTS  </h6>

<p>10.1 EQ Group Ltd is the owner or the licensee of all intellectual property rights in the takeaseat.co.uk Site, and in the
material published on it. Those works are protected by copyright laws and treaties around the world. All such rights are
reserved.</p>

<p>10.2 You may print off one copy, and may download extracts, of any page(s) from our Site for your personal use and you
may draw the attention of others within your organisation to content posted on our Site.</p>

<p>10.3 You must not modify the paper or digital copies of any materials you have printed off or downloaded in any way, and
you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any
accompanying text</p>

<p>10.4 Our status (and that of any identified contributors) as the authors of content on our Site must always be
acknowledged.</p>

<p>10.5 You must not use any part of the content on our Site for commercial purposes without obtaining a licence to do so
from us or our licensors.</p>

<p>10.6 If you print off, copy or download any part of our Site in breach of these terms of use, your right to use our Site will
cease immediately and you must, at our option, return or destroy any copies of the materials you have made.</p>


<h6 class="blue-text font-weight-bold"> 11. ADDITIONAL TERMS  </h6>

<p> 11.1. Privacy Policy: We are committed to protecting your privacy and security. All personal data that we collect from you
will be processed in accordance with our Privacy Policy. You should review our Privacy Policy. </p>

<p>11.2. Other terms: You should also review our Introduction & Ownership Policy for information applicable to
‘professional’ agreement with ‘takeaseat’ regarding ‘client’ introductions & ownership. Cookies Policy for information
regarding how and why we use cookies to improve the quality of the Website and your use of it, our Professional Services
& Insurance for information regarding the terms applicable to ‘professionals’ responsibility & liability. All of these are
incorporated into these Website Terms by this reference.</p>

<h6 class="blue-text font-weight-bold"> 12. GOVERNING LAW & JURISDICTION  </h6>
<p>12.1. These Website Terms shall be governed by and construed in accordance with English law. Disputes or claims arising
in connection with these Website Terms (including non-contractual disputes or claims) shall be subject to the exclusive
jurisdiction of the English courts.</p>

<h6 class="blue-text font-weight-bold">13. CANCELLATION POLICY
 </h6>
 <p>STATUTORY 14 DAY “COOLING-OFF” PERIOD</p>
 <p>Where you purchase services online, you have a statutory right to cancel your order within fourteen (14) days after the
day on which the contract is entered into (“Cooling-Off Period”) without giving any reason</p>

<p>If you cancel during the Cooling-Off Period and you have not booked any Appointments with the 14 days of your
cancellation, then we will give you a full refund of the single appointment fee (as applicable) that you have paid.</p>

<p>If you cancel during the Cooling-Off Period and you have booked/received Appointments at the time of your cancellation,
then you will be required to pay for the appointment.</p>

<p>As such:</p>

<p>We will give you a full refund of the single appointment fee (as applicable) that you have paid, but you will be obliged to
pay for the Appointments you have booked within the 14 day period or those that exceed the “Cooling off-period” You
acknowledge and agree that we shall be entitled to take payment (from your nominated debit or credit card) for the total
amount of any outstanding fees which are due in respect of any Appointments under this paragraph </p>

<p>13.1<br>
Notice before appointment: 15 days or more
The opportunity to reschedule will be offered otherwise a refund will be given of 80% of the total amount of the
delegate fee</p>

<p>13.2<br>
Notice before Appointment: Between 1 and 14 days
No refund - full payment of the total delegate fee is required</p>

<p>13.3<br>
Failure to attend
No refund - full payment of the total delegate fee is required </p>

<h6 class="blue-text font-weight-bold">OTHER CANCELLATION RIGHTS  </h6>

<p>Subject to your statutory rights (as described above), you can cancel your appointment at any time, but (after the
Cooling-Off Period) you will not be entitled to a refund of the single appointment fee. </p>

<p>Nothing in this section affects your legal rights.
</p>

<p>Fair Usage:  </p>

<p>You will always be given the opportunity to re-schedule an appointment, the process is outlined below.</p>

<p>Rescheduling/cancellation of appointments: </p>

<p>While the takeaseat.co.uk site is in beta, the process will be to email support@takeaseat.co.uk to organise the
rescheduling or cancellation of appointments.</p>

<p>(Valid from November 2019)</p>




</div>
    
    </div>

  </div>  

</div>



</div>
<!-- end welcome -->



<div class="social-section text-center">
  <div class="container">
    <img src="{{asset('public/images/LogoIcon_Colour.png')}}" class="img-fluid mb-5">
    <h3 class="blue-text mb-3 text-center">Social</h3>
    <div class="row d-flex justify-content-center">
      <a href="#"> <img src="{{asset('public/images/YoutubeIcon.png')}}"></a>
      <a href="#"> <img src="{{asset('public/images/TwitterIcon.png')}}"></a>
      <a href="#"> <img src="{{asset('public/images/FacebookIcon.png')}}"></a>
    </div>
  </div>
</div>
<!-- end social-section -->

@endsection