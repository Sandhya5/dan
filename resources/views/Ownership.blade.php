@extends('layouts.app')

@section('content')

<div class="welcome">
  <div class="container">
    <div class="row">
      <div class="welcome-text cookies-policy clearfix">
        <h3 class="text-center"> Introduction and Ownership Policy  </h3>

        <p>Introduction &amp; Ownership Policy – Take a seat (TAS) introduces clients to professionals via its unique website introduction
facility, the takeaseat.co.uk booking system. Once registered the client agrees to TAS terms and conditions of usage.
Clients will be provided with professional services gained through appointments with a professional of their choice and
will thereafter be regarded as a “client “of the professional once an appointment has been booked via the TAS booking
system.TAS having provided the client connection, therefore, retains the rights of “ownership and exclusivity” in respect
of any such clients, even if a client was known previously to the professional, takeaseat.co.uk have given the ‘introduction’
and therefore reserve the rights of ‘ownership’ of the client for any further professional services between the connected
parties.</p>

        <p>Failure to comply with the ‘Introduction &amp; ownership policy’ gives TAS the right to enforce the following penalties: </p>



   <p>• Temporary or permanent suspension of a Professional or client from the takeaseat.co.uk platform and any of its partner
 networks.</p>

 <p>• £1000 fine payable by the Professional if a client is provided with any professional services outside of the TAS platform.</p>

<p> Any disputes arising from the failure to comply with the above terms and conditions shall be dealt with in accordance
with the TAS complaints procedure and it’s relevant associated provisions..</p>

<p>Failure to comply with the outcome of a dispute will result in TAS taking appropriate debt or legal proceedings against
you.</p>

<p>Any interpretation of “introduction”, “ownership” and “property” shall be considered giving due consideration to the fair
meaning of those terms. </p>

<p>No part of this agreement shall be used as the basis of a claim against TAS as TAS has drafted the agreement for its own
use in respect of professional or client breach of policy.</p>


</div>
    
    </div>

  </div>  

</div>



<div class="social-section text-center">
  <div class="container">
    <img src="{{asset('public/images/LogoIcon_Colour.png')}}" class="img-fluid mb-5">
    <h3 class="blue-text mb-3 text-center">Social</h3>
    <div class="row d-flex justify-content-center">
      <a href="#"> <img src="{{asset('public/images/YoutubeIcon.png')}}"></a>
      <a href="#"> <img src="{{asset('public/images/TwitterIcon.png')}}"></a>
      <a href="#"> <img src="{{asset('public/images/FacebookIcon.png')}}"></a>
    </div>
  </div>
</div>
<style>
.hero-section{ min-height:0px;  }
</style>
</style>
<!-- end social-section -->

@endsection