@extends('layouts.app')

@section('content')




<section class="contact">
  <div class="container">
    <div class="row">
      <div class="col-12">
                    <div>&nbsp;</div>
                    <div>&nbsp;</div>
                    <form method="post" action="{{url(app_get_locale().'/contact/store')}}"  class="bookcore-form cont-form">
							{{csrf_field()}}
							<div style="display: none;">
								<input type="hidden" name="g-recaptcha-response" value="">
							</div>
							<div class="contact-form">
								<div class="contact-header">
									<h2>{{ setting_item_with_lang("page_contact_title") }}</h3>
									<p class="mt-4">{{ setting_item_with_lang("page_contact_sub_title") }}</p>
                                </div>
								@include('admin.message')
								<div class="contact-form">
									<div class="form-group">
										<input type="text" value="" placeholder=" {{ __('Name') }} " name="name" class="form-control">
									</div>
									<div class="form-group">
										<input type="text" value="" placeholder="{{ __('Email') }}" name="email" class="form-control">
									</div>

									<div class="form-group">
										 <textarea name="message" cols="40" rows="10" class="form-control textarea" placeholder="{{ __('Message') }}"></textarea>
									</div>
									<div class="form-group">
										{{recaptcha_field('contact')}}
									</div>
									
									<p class="d-flex justify-content-center"><input type="submit" value="{{ __('SEND MESSAGE') }}" class=" btn btn-primary"></p></div></div>
									
							</form>
							
							<p class="text-center address">Take A Seat - EQ Group Ltd <br>
16 Alton Road, Wilmslow, SK9 5DX <br>
support@takeaseat.co.uk</p>
        </div>
    </div>
  </div>
</section>
<!-- end contact -->

<div class="social-section text-center">
  <div class="container">
    <img src="{{asset('public/images/LogoIcon_Colour.png')}}" class="img-fluid mb-5">
    <h3 class="blue-text mb-3 text-center">Social</h3>
    <div class="row d-flex justify-content-center">
      <a href="#"> <img src="{{asset('public/images/YoutubeIcon.png')}}"></a>
      <a href="#"> <img src="{{asset('public/images/TwitterIcon.png')}}"></a>
      <a href="#"> <img src="{{asset('public/images/FacebookIcon.png')}}"></a>
    </div>
  </div>
</div>
<!-- end social-section -->
@endsection