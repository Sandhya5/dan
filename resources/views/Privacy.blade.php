@extends('layouts.app')

@section('content')



<div class="welcome">
  <div class="container">
    <div class="row">
      <div class="welcome-text cookies-policy clearfix">
        <h3 class="text-center"> Privacy Policy   </h3>

        <p>takeaseat.co.uk is a service provided by EQ Group Ltd of 27 Old Gloucester Street, London, United Kingdom, WC1N 3AX.
This Privacy Policy provides our policies and procedures for collecting, using, and disclosing your information. Users can
access the takeaseat.co.uk service (the "Service") through our website www.takeaseat.co.uk through APIs, and through
third-parties. A "Device" is any computer used to access the takeaseat.co.uk Service, including without limitation a
desktop, laptop, mobile phone, tablet, or other consumer electronic device. This Privacy Policy governs your access to the
takeaseat.co.uk Service, regardless of how you access it, and by using our Services you consent to the collection, transfer,
processing, storage, disclosure and other uses described in this Privacy Policy. All of the different forms of data, content,
and information described below are collectively referred to as "information".</p>

        <h6 class="blue-text font-weight-bold"> 1. The Information We Collect And Store </h6>



   <p>We may collect and store the following information when running the takeaseat.co.uk Service:</p>

 <p>Information You Provide: When you register an account, we collect some personal information, such as your name, email
address, phone number, home and business postal addresses. If you are a mental health professional then we will also
collect information that request, and you can upload attachments such as images, video or documents to the service. You
may also provide us with your contacts' email addresses when sharing requests with them. If you are paying for part of
the takeaseat.co.uk service, then we will collect your billing details, including card details and billing address.</p>

<p> Log Data: When you use the Service, we automatically record information from your Device, its software, and your
activity using the Services. This may include the Device’s Internet Protocol ("IP") address, browser type, the web page
visited before you came to our website, information you search for on our website, locale preferences, identification
numbers associated with your Devices, your mobile carrier, date and time stamps associated with transactions, and other
interactions with the Service</p>

<p>Cookies: We also use "cookies" to collect information and improve our Services. A cookie is a small data file that we
transfer to your Device. We may use "persistent cookies" to save your registration ID and login password for future logins
to the Service. We may use “session ID cookies” to enable certain features of the Service, to better understand how you
interact with the Service and to monitor aggregate usage and web traffic routing on the Service. You can instruct your
browser, by changing its options, to stop accepting cookies or to prompt you before accepting a cookie from the websites
you visit. If you do not accept cookies, however, you may not be able to use all aspects of the Service</p>

<h6 class="blue-text font-weight-bold">  2. How We Use Personal Information</h6>

<p>Personal Information: In the course of using the Service, we may collect personal information that can be used to contact
or identify you ("Personal Information"). Personal Information is or may be used: (i) to provide and improve our Service,
(ii) to administer your use of the Service, (iii) to better understand your needs and interests, (iv) to personalize and
improve your experience, and (v) to provide updates and product announcements. </p>

<p>Geo-Location Information: When using the service we may use your Device’s Internet Protocol ("IP") address to identify
your approximate geographical location. Some photos you upload to Bark.com may also contain recorded location
information. We may use this information to optimize your experience.</p>


<h6 class="blue-text font-weight-bold">   3. Information Sharing and Disclosure </h6>

<p>Your Use: We will display your Personal Information in your profile page and elsewhere on the Service according to the
preferences you set in your account. Any information you choose to provide should reflect how much you want others to
know about you. Please consider carefully what information you disclose in your profile page and your desired level of
anonymity. You can review and revise your profile information at any time. We may also share or disclose your
information with your consent, for example, if you use a third-party application to access your account (see below).
Through certain features of the Service, you may also have the ability to make some of your information public. Public
information may be broadly and quickly disseminated.</p>


<p>Service Providers, Business Partners and Others: We may use certain trusted third party companies and individuals to
help us provide, analyze, and improve the Service (including but not limited to data storage, maintenance services,
database management, web analytics, payment processing, and improvement of the Service’s features). These third
parties may have access to your information only for purposes of performing these tasks on our behalf and under
obligations similar to those in this Privacy Policy.</p>


<p>Third-Party Applications: We may share your information with a third party application with your consent, for example
when you choose to access our Services through such an application. We are not responsible for what those parties do
with your information, so you should make sure you trust the application and that it has a privacy policy acceptable to
you.</p>

<p>Compliance with Laws and Law Enforcement Requests; Protection of EQ Group Limited's Rights: We may disclose to
parties outside takeaseat.co.uk content and information about you that we collect when we have a good faith belief that
disclosure is reasonably necessary to (a) comply with a law, regulation or compulsory legal request; (b) protect the safety
of any person from death or serious bodily injury; (c) prevent fraud or abuse of Bark.com or its users; or (d) to protect EQ
Groups Limited’s property rights.</p>

<p>Business Transfers: If we are involved in a merger, acquisition, or sale of all or a portion of our assets, your information
may be transferred as part of that transaction, but we will notify you (for example, via email and/or a prominent notice on
our website) of any change in control or use of your Personal Information or Files, or if either become subject to a
different Privacy Policy. We will also notify you of choices you may have regarding the information.</p>


<p>Non-private or Non-Personal Information: We may disclose your non-private, aggregated, or otherwise non-personal
information, such as usage statistics of our Service</p>



<h6 class="blue-text font-weight-bold">  4. Changing or Deleting Your Information </h6>

<p>If you are a registered user, you may review, update, correct or delete the Personal Information provided in your
registration or account profile by changing your "account settings". If your personally identifiable information changes, or
if you no longer desire our service, you may update or delete it by making the change on your account settings. In some
cases, we may retain copies of your information if required by law</p>

<h6 class="blue-text font-weight-bold">  5. Data Retention  </h6>
<p>We will retain your information for as long as your account is active or as needed to provide you services. We may retain
and use your information as necessary to comply with our legal obligations, resolve disputes, and enforce our
agreements. Consistent with these requirements, we will try to delete your information quickly upon request. Please
note, however, that there might be latency in deleting information from our servers and backed-up versions might exist
after deletion.</p>

<h6 class="blue-text font-weight-bold"> 6. Security </h6>

<p>The security of your information is important to us. Whenever you submit content using our service, we encrypt the
transmission of that information using secure socket layer technology (SSL). We follow generally accepted standards to
protect the information submitted to us, both during transmission and once we receive it. No method of electronic
transmission or storage is 100% secure, however. Therefore, we cannot guarantee its absolute security.</p>

<h6 class="blue-text font-weight-bold"> 7. GDPR Statement </h6>

<p>In terms of GDPR takeaseat.co.uk has been working towards being fully compliant in order to ensure that take a seat
customers can be certain that they are dealing with a fully compliant GDPR business. The work will conclude prior to the
May 2018 date for introduction of the regulation.</p>

<p>EQ Group Ltd will be happy to provide documentation to customers detailing as an organisation how we are fully
compliant with all aspects of GDPR.</p>

<h6 class="font-weight-bold"> Assessment </h6>
<p>EQ Group Ltd has assessed every paragraph of GDPR and matched its own activities against all of those paragraphs in
three key areas</p>
<p>1. A data controlled of its own data.</p>
<p>2. A data controller or processor of third party data such as activity relating to direct marketing.</p>
<p>3. A data processor or controller of client &amp; professional personal data</p>

<p>Documentation will be made available at the bottom of this commitment statement detailing the policies and activities
that EQ Group Ltd employs matched to the clauses of the GDPR should any customer have a detailed question in respect
of compliance</p>

<h6 class="font-weight-bold"> Activity </h6>

<p>EQ Group Ltd is amending its activities and associated policies and procedures as necessary in order to fully comply with
GDPR following a thorough assessment.</p>

<p>EQ Group Ltd is amending its customer and supplier contracts to ensure the GDPR reaches throughout the supply chain
for the provision of its services</p>

<p>EQ Group Ltd is reviewing all of its suppliers for compliance with GDPR paying very close attention to its marketing
activity suppliers.</p>

<p>EQ Group Ltd is carrying out Privacy Impact Assessments as necessary</p>

<p>EQ Group Ltd is organising a campaign whereby customers can be briefed further on EQ Group Ltd activity in respect of
GDPR.</p>

<p>The takeaseat.co.uk website and direct marketing activity is being updated so that customers have the assurance that
they will be contacted and treated in accordance with GDPR requirements. The website will contain TAS privacy policies
clearly identified.</p>

<p>EQ Group Ltd will be maintaining accreditations that demonstrate its commitment to information security, including
personal data.</p>


</div>
    
    </div>

  </div>  

</div>

@endsection