<?php


	namespace Custom\Booking\Models;


	use App\BaseModel;
    use Illuminate\Support\Facades\DB;

    class BookingSlot extends BaseModel
	{
		protected  $table  = 'bravo_booking_slot';
		protected $fillable =['slot','date','booking_id','slots','flag'];

		protected $casts = [
		    'slots'=>'array'
        ];
		public function booking(){
			return $this->belongsTo(Booking::class);
		}
		public function getSlotName($slot){
			return date('H:i',strtotime(date('d-m-Y').' '.$slot.':00:00'));
		}

        public function service()
        {
            $all = get_bookable_services();
            if ($this->object_model and !empty($all[$this->object_model])) {
                return $this->hasOne($all[$this->object_model], 'id', 'object_id');
            }
            return null;
        }

        public function getMeta($key, $default = '')
        {
            //if(isset($this->cachedMeta[$key])) return $this->cachedMeta[$key];
            $val = DB::table('bravo_booking_meta')->where([
                'booking_id' => $this->id,
                'name'       => $key
            ])->first();
            if (!empty($val)) {
                //$this->cachedMeta[$key]  = $val->val;
                return $val->val;
            }
            return $default;
        }

        public function getJsonMeta($key, $default = [])
        {
            $meta = $this->getMeta($key, $default);
            if(empty($meta)) return false;
            return json_decode($meta, true);
        }

        public function addMeta($key, $val, $multiple = false)
        {

            if (is_object($val) or is_array($val))
                $val = json_encode($val);
            if ($multiple) {
                return DB::table('bravo_booking_meta')->insert([
                    'name'       => $key,
                    'val'        => $val,
                    'booking_id' => $this->id
                ]);
            } else {
                $old = DB::table('bravo_booking_meta')->where([
                    'booking_id' => $this->id,
                    'name'       => $key
                ])->first();
                if ($old) {

                    return DB::table('bravo_booking_meta')->where('id', $old->id)->insert([
                        'val' => $val
                    ]);

                } else {
                    return DB::table('bravo_booking_meta')->insert([
                        'name'       => $key,
                        'val'        => $val,
                        'booking_id' => $this->id
                    ]);
                }
            }
        }
    }