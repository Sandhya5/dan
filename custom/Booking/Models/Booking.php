<?php
namespace Custom\Booking\Models;

use App\User;
use Custom\Tour\Models\Tour;

class Booking extends \Modules\Booking\Models\Booking
{
   public function slots(){
   	return $this->hasMany(BookingSlot::class)->orderBy('slot');
   }
	public static function getBookingInRangesNotGroup($object_id,$object_model,$from,$to){

		return parent::where([
			'object_id'=>$object_id,
			'object_model'=>$object_model,
		])->whereNotIn('status',static::$notAcceptedStatus)
			->where('end_date','>=',$from)
			->where('start_date','<=',$to)
			->take(200)
			->with('slots')
			->get();
	}


	public static function getAvgVendorPricePerHour(){
       $avg_price = 0;
        $total_data = parent::selectRaw('count(`id`) as total_books , sum( `total` ) AS total_earning ')->whereNotIn('status',static::$notAcceptedStatus)->first();

        if(!empty($total_data->total_books)){
            $avg_price = $total_data->total_earning / $total_data->total_books;
        }

        $avg_price = number_format($avg_price,2);

        if($avg_price < 0){
            return 0;
        }

        return $avg_price;
    }

    public static function getAvgBookingPerVendor(){

        $total_books = parent::whereNotIn('status',static::$notAcceptedStatus)->count('id');

        $total_vendor = Tour::where('status', 'publish')->count('id');

        if($total_vendor == 0 or $total_vendor == 0){
            return 0;
        }

        $avg_booking = number_format( $total_books / $total_vendor,2);

        return $avg_booking;
    }

    public static function getAvgBookingPerClient(){

        $total_books = parent::whereNotIn('status',static::$notAcceptedStatus)->count('id');

        $total_customer = User::role("customer")->where('status', 'publish')->count('id');

        $avg_booking = number_format($total_books / $total_customer , 2);

        return $avg_booking;
    }

    public static function getAllMarkerVendorForDashboard(){

        $model_Tour = Tour::select("bravo_tours.*");
        $model_Tour->where("bravo_tours.status", "publish");
        $list = $model_Tour->with(['location','hasWishList','translations'])->paginate(999);
        $markers = [];
        if (!empty($list)) {
            foreach ($list as $row) {
                $markers[] = [
                    "id" => $row->id,
                    "title" => $row->title,
                    "lat" => (float)$row->map_lat,
                    "lng" => (float)$row->map_lng,
                    "gallery" => $row->getGallery(true),
                    "infobox" => view('Tour::frontend.layouts.search.loop-gird', ['row' => $row, 'disable_lazyload' => 1, 'wrap_class' => 'infobox-item'])->render(),
                    'marker' => url('images/icons/png/pin.png'),
                ];
            }
        }
        return $markers;
    }
}
