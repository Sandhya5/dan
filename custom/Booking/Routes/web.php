<?php
// Booking
	Route::group(['prefix'=>config('booking.booking_route_prefix')],function(){
		Route::post('/addToCart','\Custom\Booking\Controllers\BookingController@addToCart')->middleware('auth');// Detail
		Route::post('/addToCart','\Custom\Booking\Controllers\BookingController@addToCart')->middleware('auth');// Detail

		Route::get('/{code}','\Custom\Booking\Controllers\BookingController@detail')->middleware('auth');// Detail
		Route::get('/{code}/checkout','\Custom\Booking\Controllers\BookingController@checkout')->middleware('auth');// Detail
		Route::get('/{code}/check-status','\Custom\Booking\Controllers\BookingController@checkStatusCheckout')->middleware('auth');// Detail
	});