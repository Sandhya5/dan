<?php
namespace Custom\Booking;

use Illuminate\Support\ServiceProvider;
use Custom\ModuleServiceProvider;
use Custom\Tour\Models\Tour;
use Modules\Booking\Models\Booking;

class ModuleProvider extends ModuleServiceProvider
{

    public function boot(){
        $this->loadMigrationsFrom(__DIR__ . '/Migrations');
    }
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouterServiceProvider::class);
    }

    public static function getBookableServices()
    {
        return [
            'booking'=>Booking::class,
        ];
    }
}