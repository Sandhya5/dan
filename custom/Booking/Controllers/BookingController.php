<?php
namespace Custom\Booking\Controllers;

use Custom\Booking\Models\Booking;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Mockery\Exception;
//use Modules\Booking\Events\VendorLogPayment;
use Modules\Tour\Models\TourDate;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\ReCaptchaEngine;

class BookingController extends \Modules\Booking\Controllers\BookingController
{
	protected $booking;
	public function __construct()
	{
		parent::__construct();
		$this->booking  = Booking::class;
	}

	public function checkout($code)
	{

		$booking = $this->booking::where('code', $code)->first();

		if (empty($booking)) {
			abort(404);
		}
		if ($booking->customer_id != Auth::id()) {
			abort(404);
		}
		if($booking->status != 'draft'){
			return redirect('/');
		}
		$data = [
			'page_title' => __('Checkout'),
			'booking'    => $booking,
			'service'    => $booking->service,
			'gateways'   => $this->getGateways(),
			'user'       => Auth::user()
		];
		return view('Booking::frontend/checkout', $data);
	}

	public function addToCart(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'service_id'   => 'required|integer',
            'service_type' => 'required'
        ]);
        if ($validator->fails()) {
            $this->sendError('', ['errors' => $validator->errors()]);
        }
        $service_type = $request->input('service_type');
        $service_id = $request->input('service_id');
        $allServices = get_bookable_services();
        if (empty($allServices[$service_type])) {
            $this->sendError(__('Service type not found'));
        }
        $module = $allServices[$service_type];
        $service = $module::find($service_id);
        if (empty($service) or !is_subclass_of($service, '\\Modules\\Booking\\Models\\Bookable')) {
            $this->sendError(__('Service not found'));
        }
        if (!$service->isBookable()) {
            $this->sendError(__('Service is not bookable'));
        }
        //        try{
        $service->addToCart($request);
        //
        //        }catch(\Exception $ex){
        //            $this->sendError($ex->getMessage(),['code'=>$ex->getCode()]);
        //        }
    }
	public function detail(Request $request, $code)
	{

		$booking = $this->booking::where('code', $code)->first();
		if (empty($booking)) {
			abort(404);
		}

		if ($booking->status == 'draft') {
			return redirect($booking->getCheckoutUrl());
		}
		if ($booking->customer_id != Auth::id()) {
			abort(404);
		}
		$data = [
			'page_title' => __('Booking Details'),
			'booking'    => $booking,
			'service'    => $booking->service,
		];
		if ($booking->gateway) {
			$data['gateway'] = get_payment_gateway_obj($booking->gateway);
		}
		return view('Booking::frontend/detail', $data);
	}

}
