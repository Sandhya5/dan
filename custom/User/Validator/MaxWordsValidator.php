<?php
namespace Custom\User\Validator;
class MaxWordsValidator
{
    public function validate($attribute, $value, $parameters, $validator) {
        $words = explode(' ', $value);
        $nbWords = count($words);
        return ($nbWords >=0 && $nbWords <= $parameters[0]);  // replace 5 by the correct $parameters value
    }
}
