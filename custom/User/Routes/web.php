<?php
use \Illuminate\Support\Facades\Route;
Route::group(['prefix'     => 'user', 'middleware' => ['auth']], function () {
    Route::get('/profile', 'UserController@profile')->name("vendor.profile.custom");
    Route::post('/profile', 'UserController@store')->name("vendor.profile.store.custom");
    Route::get('/calendar', 'UserController@calendar')->name("vendor.profile.calendar.custom");
	Route::get('/booking-history','UserController@bookingHistory')->name("vendor.booking_history");


    Route::get('/notes', 'NotesController@index')->name("vendor.profile.notes.custom");
    Route::post('/notes/store', 'NotesController@store')->name("vendor.profile.notes.store.custom");
    Route::get('/notes/loadDates', 'NotesController@loadDates')->name("vendor.profile.notes.loadDates.custom");
    Route::post('/notes/delete', 'NotesController@delete')->name("vendor.profile.notes.delete.custom");

    Route::post('/notes/store-calendar', 'UserController@storeCalendar')->name("vendor.profile.store.calendar.custom");

});


Route::group(['prefix'=>'user/booking/'],function(){
    Route::get('{code}/invoice','UserController@invoice')->name('user.booking.invoice');
});


Route::post('login','UserController@userLogin')->name('auth.login');
