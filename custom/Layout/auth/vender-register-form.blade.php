<form class="form bravo-form-register1" method="post" >
    @csrf
    <div class="">
        <input type="hidden" name="action" value="http://takeaseatstaging.com/en/vendor/register">
        <div class="text-left">
            <div class="">
                <input type="text" class="form-control mb-4" name="first_name" autocomplete="off" placeholder="{{__("First Name")}}">
                  <input type="hidden" class="form-control mb-4" name="nature" value="1">
                <span class="invalid-feedback error error-first_name"></span>
            </div>
        </div>
        <div class="text-left">
            <div class="">
                <input type="text" class="form-control mb-4" name="last_name" autocomplete="off" placeholder="{{__("Last Name")}}">
                <span class="invalid-feedback error error-last_name"></span>
            </div>
        </div>
    </div>
    <div class="text-left">
        <input type="email" class="form-control mb-4" name="email" autocomplete="off" placeholder="{{__('Email address')}}">
        <span class="invalid-feedback error error-email"></span>
    </div>
    <div class="text-left">
        <input type="password" class="form-control mb-4" name="password" autocomplete="off" placeholder="{{__('Password')}}">
        <span class="invalid-feedback error error-password"></span>
    </div>
   
    <div class="text-left">
        <label for="term">
            <input id="term" type="checkbox" name="term" class="mr5">
            {!! __("I have read and accept the <a href=':link' target='_blank'>Terms and Privacy Policy</a>",['link'=>get_page_url(setting_item('booking_term_conditions'))]) !!}
            <span class="checkmark fcheckbox"></span>
        </label>
        <div><span class="invalid-feedback error error-term"></span></div>
    </div>
    
    <br>
    
    @if(setting_item("user_enable_register_recaptcha"))
        <div class="">
            {{recaptcha_field($captcha_action ?? 'register')}}
        </div>
    @endif
    <div class="error message-error invalid-feedback"></div>
    <div class="text-center">
        <button id="vender_regform" type="submit" class="btn btn-primary form-submit">
            {{ __('Sign Up') }}
            <span class="spinner-grow spinner-grow-sm icon-loading" role="status" aria-hidden="true"></span>
        </button>
        
        <span id="vender_regformmsg"></span>
        
    </div>

</form>