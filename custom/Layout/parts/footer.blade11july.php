<style>
.selected {
  color: red;
  background-color: #54a79f;
}</style>
 <?php //$postcode = if(!empty($_GET['post_code']));?>
<footer class="footer theme-gradient">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="footer-content">
                  <a href="{{url(app_get_locale(false,'/'))}}" class="bravo-logo">
                    @if($logo_id = setting_item("logo_id"))
                        <?php $logo = get_file_url($logo_id,'full') ?>
                        <img class="img-fluid" src="{{$logo}}" alt="{{setting_item("site_title")}}">
                    @endif
                </a>
            <ul class="footer-info">
              <li>Call us 0161 312 8168</li>
              <li>Email us support@takeseat.co.uk</li>
            </ul>
            <p class="text-white copyright">Copyright &copy;  2020 ke a Seat:</p>
            
        </div>
      </div>
       <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
         <div class="row">
           <div class="col-sm-6">
            <div class="support-link">
              <h3 class="text-white">Support</h3>
            <ul>
              <li><a href="#">Your account</a></li>
               <li><a href="#">Contact us</a></li>
               <li><a href="#">About take a seat</a></li>
              
            </ul>
           </div>
           </div>
            <div class="col-sm-6">
            <div class="support-link">
              <h3 class="text-white">Policies</h3>
            <ul>
              <li><a href="#">Privacy Policy</a></li>
               <li><a href="#">Terms and conditions</a></li>
               <li><a href="#">Cookies Policy</a></li>
                <li><a href="#">Introduction & Ownership Policy</a></li>
              
            </ul>     

           </div>
           </div>
         </div>

         <div class="join-pro d-flex">
              <img src="{{asset('custom/assets/images/JoinIcon.png')}}" alt="join"><p>Join as a professional on take a seat</p>
        </div>
       </div>
    </div>
  </div>
</footer>

@include('Layout::parts.login-register-modal')
@include('Layout::parts.chat')
@if(Auth::id())
    @include('Media::browser')
@endif
<link rel="stylesheet" href="{{asset('libs/flags/css/flag-icon.min.css')}}" >

{!! \App\Helpers\Assets::css(true) !!}

{{--Lazy Load--}}
<script src="{{asset('libs/lazy-load/intersection-observer.js')}}"></script>
<script async src="{{asset('libs/lazy-load/lazyload.min.js')}}"></script>
<script>
    // Set the options to make LazyLoad self-initialize
    window.lazyLoadOptions = {
        elements_selector: ".lazy",
        // ... more custom settings?
    };

    // Listen to the initialization event and get the instance of LazyLoad
    window.addEventListener('LazyLoad::Initialized', function (event) {
        window.lazyLoadInstance = event.detail.instance;
    }, false);


</script>
<script src="{{ asset('libs/lodash.min.js') }}"></script>
<script src="{{ asset('libs/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('libs/vue/vue.js') }}"></script>
<script src="{{ asset('libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('libs/bootbox/bootbox.min.js') }}"></script>
@if(Auth::id())
    <script src="{{ asset('module/media/js/browser.js?_ver='.config('app.version')) }}"></script>
@endif
<script src="{{ asset('libs/carousel-2/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset("libs/daterange/moment.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("libs/daterange/daterangepicker.min.js") }}"></script>
<script src="{{ asset('libs/select2/js/select2.min.js') }}" ></script>
<script src="{{ asset('js/functions.js?_ver='.config('app.version')) }}"></script>
<script src="https://unpkg.com/cropperjs/dist/cropper.js"></script><!-- Cropper.js is required -->
<link rel="stylesheet" href="https://unpkg.com/cropperjs/dist/cropper.css" crossorigin="anonymous">
<script src="{{ asset('libs/cropper/dist/jquery-cropper.js') }}"></script>

@if(setting_item('inbox_enable'))
    <script src="{{ asset('module/core/js/chat-engine.js?_ver='.config('app.version')) }}"></script>
@endif
<script src="{{ asset('js/home.js?_ver='.config('app.version')) }}"></script>

@if(!empty($is_user_page))
    <script src="{{ asset('module/user/js/user.js?_ver='.config('app.version')) }}"></script>
@endif
{!! \App\Helpers\Assets::js(true) !!}
<script src="{{ asset('custom/js/app.custom.js') }}"></script>
{!! App\Helpers\MapEngine::scripts(['libraries'=>'places','callback'=>'initAutocomplete']) !!}

@yield('footer')

@php \App\Helpers\ReCaptchaEngine::scripts() @endphp
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" crossorigin="anonymous">

<script>
$(document).ready(function(){
	$(function() {                       
  $("#tourSubmit").click(() => $(".carousel").carousel("next"));
  
 // $("#findTherapist").click(() => $(".carousel").carousel("next"));
});
	$('#thCat').change(function() {
    var data = "";
	var base_url = window.location.origin;
	$('#ulbox2').empty();
    $.ajax({
        type:"GET",
        url : "ajax-call",
        data : "selectedvalue="+$(this).val(),
        async: false,
        success : function(response) {
            data = response;
			$('#ulbox2').empty();
			$('#ulbox2').append(data)
			//console.log(data);
            //return response;
        },
        error: function() {
            alert('Error occured');
        }
    });
	
});
$("#ulbox2").on("click", "li", function(event){

var postcode = $('#post_code').val();
var lat = $('#lat').val();
var lng = $('#lng').val();
//alert(postcode);
 //$(this).addClass('selected'); 
 event.preventDefault();
    $(this).toggleClass('selected');
    var l = [];
    $('.selected').each(function(){
		
        l.push($(this).attr('id'));
    });
     $('#colorvalues').val(l.join(','));
      var fval =$('#colorvalues').val();
	  //var arr = fval.split('@');
	  catId = 1;
	  //fval = arr[1];
 $('#findTherapist').click(function() {
    var data = "";
	
    $.ajax({
        type:"GET",
        url : "ajax-call-submit",
        data : "post_code="+postcode+"&selectedvalue="+fval+"&lat="+lat+"&lng="+lng,
        async: false,
        success : function(response) {
			//alert(response);
          window.location.href=response;
        },
        error: function() {
            alert('Error occured');
        }
    });
	
});



});



// $('input:checkbox[name=terms]').each(function() 
// {    
    // if($(this).is(':checked'))
      // alert($(this).val());
// });
});

// $('.check_class').on('click',function(){
			// var favorite = [];
            // $.each($(".check_class"), function(){
                // favorite.push($(this).val());
            // });
				
	// $.ajax({
        // type:"GET",
        // url : "ajax-call-getFilter",
        // data : "post_code="+<?php //echo $postcode?>+"&terms="+favorite,
        // async: false,
        // success : function(response) {
			// //alert(response);
          // window.location.href=response;
        // },
        // error: function() {
            // alert('Error occured');
        // }
    // });
// });
	$(document).ready(function(){
    $('.icon-loading').hide();
    $('.fa-spinner').hide();
$("#myform").validate({
        rules: {
            field1: "required"
        },
        messages: {
            field1: "Please Enter"

        }

    })

    $('#openmodal').click(function(){
      $('.icon-loading').show();
 if($("#myform").valid()){

     var uid =  $('#userId').val();
     //alert(uid);
      if(uid ==1){  
        $('#exampleModal').modal({backdrop: 'static', keyboard: false});
     }else{
      $('.bravo_tour_book_wrap').modal('hide');
      $('#login').modal('show');
     }
var selected = $('select[name="slots"]').val();
// var selected = $('select[name="slots[]"]').map(function(){
//     if ($(this).val())
//         return $(this).val();
// }).get();

var stdate = $("input[name='start_date']").val();
// var stdate = $("input[name='start_date[]']").map( function() { return $(this).val(); } ).get();
//console.log(stdate);


     var service_id= $('#service_id').val();
     var term_id= $("#term_id option:selected").text();
     var start_date=  stdate;
     var extra_price= $('#extra_price').val();
     var slots= selected;
     var person_types= 'guest';




     $(".modal-body #service_id").val( service_id );
      $(".modal-body #term_id").val( term_id );
       $(".modal-body [name='dates']").val( start_date );
        $(".modal-body #extra_price").val( extra_price );
         $(".modal-body [name='slots']").val( slots  );
         // $(".modal-body #sess_loc").val( sess_loc );

$('#nav-profile-tab').click(function(){
  $('.fa-spinner').show();
  $('.icon-loading').hide();
 var sess_loc= $('#sess_loc').val();
    // alert(sess_loc);
$.ajax({
                    url:takeaseat.url+'/booking/addToCart',
                    data:{
                        _token: "{{ csrf_token() }}",
                        service_id:service_id,
                        service_type:'tour',
                        term_id:term_id,
                        start_date:stdate,
                        person_types:person_types,
                        extra_price:extra_price,
                        sess_loc:sess_loc,
                        slots:[selected],
                        selected_slots:[selected],
                        dates:[
                            {
                                date:stdate,
                                slots:[selected],
                            }
                        ],
                        
                    },
                    dataType:'json',
                    type:'post',
                    success:function(res){
                      $('.fa-spinner').hide();
                       $('.icon-loading').hide();
                        //console.log(res.url);
              if(res.url){ 
                $('#nav-home').removeClass('active');
                //$('#exampleModal').modal('hide');
                         //   window.location.href = res.url
                            var arr = res.url.split('/');
                            //console.log(arr[4]);
                             //$('#nav-profile').modal('show');
                             $('.tab-pane a[href="#nav-profile"]').tab('show')
                             $('#nav-profile').addClass('show active');
                             $(".modal-body #code").val( arr[4] );
                        }
                       // console.log(res.url);

                       
}

 });
    });

}
});



        

  });



function doCheckout(){
                var me = this;
$('.fa-spinner').show();
                //if(this.onSubmit) return false;

                //if(!this.validate()) return false;

                //this.onSubmit = true;
//console.log($('.booking-form').find('input,textarea,select').serialize());
                $.ajax({
                    url:takeaseat.url+'/booking/doCheckout',
                    data:$('.booking-form').find('input,textarea,select').serialize(),
                    method:"post",
                    success:function (res) {
                      $('.fa-spinner').hide();
                      //  console.log(data);
                        if(!res.status && !res.url){
                            me.onSubmit = false;
                        }


                        if(res.elements){
                            for(var k in res.elements){
                                $(k).html(res.elements[k]);
                            }
                        }

                        if(res.message)
                        {
                            me.message.content = res.message;
                            me.message.type = res.status;
                        }

                        if(res.url){
                            window.location.href = res.url
                        }

                        if(res.errors && typeof res.errors == 'object')
                        {
                            var html = '';
                            for(var i in res.errors){
                                html += res.errors[i]+'<br>';
                            }
                            me.message.content = html;
                        }

                    },
                  
                });
            }

$(document).ready(function(){

 // Add new element
 $(".add").click(function(){

  // Finding total number of elements added
  var total_element = $(".element").length;
 
  // last <div> with element class id
  var lastid = $(".element:last").attr("id");
  var split_id = lastid.split("_");
  var nextindex = Number(split_id[1]) + 1;

  var max = 5;
  // Check total number elements
  if(total_element < max ){
   // Adding new div container after last occurance of element class
   $(".element:last").after("<div class='element' id='div_"+ nextindex +"'></div>");
 
   // Adding element to <div>
   $("#div_" + nextindex).append("<input type='date' placeholder='Enter' name='start_date' id='start_date_"+ nextindex +"'>&nbsp;<select required id='slots_"+ nextindex +"' name='slots'><option value='12:00'>12:00</option><option value='13:00'>13:00</option><option value='14:00'>14:00</option>      </select><span id='remove_" + nextindex + "' class='remove'>X</span>");
      
  }
 
 });

 // Remove element
 $('.container').on('click','.remove',function(){
 
  var id = this.id;
  var split_id = id.split("_");
  var deleteindex = split_id[1];

  // Remove <div> with id
  $("#div_" + deleteindex).remove();

 }); 
});
</script>
