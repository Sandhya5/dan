<style>
.selected {
  color: red;
  background-color: #54a79f;
}</style>
 <?php //$postcode = if(!empty($_GET['post_code']));?>
<footer class="footer theme-gradient">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="footer-content">
                  <a href="{{url(app_get_locale(false,'/'))}}" class="bravo-logo">
                    @if($logo_id = setting_item("logo_id"))
                        <?php $logo = get_file_url($logo_id,'full') ?>
                        <img class="img-fluid" src="{{$logo}}" alt="{{setting_item("site_title")}}">
                    @endif
                </a>
            <ul class="footer-info">
              <li>Call us 0161 312 8168</li>
              <li>Email us support@takeseat.co.uk</li>
            </ul>
            <p class="text-white copyright">Copyright &copy;  2020 Take a Seat:</p>
            
        </div>
      </div>
       <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
         <div class="row">
           <div class="col-sm-6">
            <div class="support-link">
              <h3 class="text-white">Support</h3>
            <ul>
              <li><a href="#">Your account</a></li>
               <li><a href="{{ url('en/contact-us') }}">Contact Us</a></li>
               <li><a href="{{ url('en/about-us') }}">About take a seat</a></li>
              
              
            </ul>
           </div>
           </div>
            <div class="col-sm-6">
            <div class="support-link">
              <h3 class="text-white">Policies</h3>
            <ul>
              <li><a href="{{ url('en/privacy-policy') }}">Privacy Policy</a></li>
               <li><a href="{{ url('en/terms-condition') }}">Terms and conditions</a></li>
               <li><a href="{{ url('en/cookies-policy') }}">Cookies Policy</a></li>
                <li><a href="{{ url('en/ownership-policy') }}">Introduction & Ownership Policy</a></li>
            </ul>     

           </div>
           </div>
         </div>

         <div class="join-pro d-flex">
              <img src="{{asset('custom/assets/images/JoinIcon.png')}}" alt="join"><p>Join as a professional on take a seat</p>
        </div>
       </div>
    </div>
  </div>
</footer>


@include('Layout::parts.login-register-modal')
@include('Layout::parts.chat')
@if(Auth::id())
    @include('Media::browser')
@endif
<link rel="stylesheet" href="{{asset('libs/flags/css/flag-icon.min.css')}}" >

{!! \App\Helpers\Assets::css(true) !!}

{{--Lazy Load--}}
<script src="{{asset('libs/lazy-load/intersection-observer.js')}}"></script>
<script async src="{{asset('libs/lazy-load/lazyload.min.js')}}"></script>
<script>
    // Set the options to make LazyLoad self-initialize
    window.lazyLoadOptions = {
        elements_selector: ".lazy",
        // ... more custom settings?
    };

    // Listen to the initialization event and get the instance of LazyLoad
    window.addEventListener('LazyLoad::Initialized', function (event) {
        window.lazyLoadInstance = event.detail.instance;
    }, false);


</script>
<script src="{{ asset('libs/lodash.min.js') }}"></script>
<script src="{{ asset('libs/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('libs/vue/vue.js') }}"></script>
<script src="{{ asset('libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('libs/bootbox/bootbox.min.js') }}"></script>
@if(Auth::id())
    <script src="{{ asset('module/media/js/browser.js?_ver='.config('app.version')) }}"></script>
@endif
<script src="{{ asset('libs/carousel-2/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset("libs/daterange/moment.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("libs/daterange/daterangepicker.min.js") }}"></script>
<script src="{{ asset('libs/select2/js/select2.min.js') }}" ></script>
<script src="{{ asset('js/functions.js?_ver='.config('app.version')) }}"></script>
<script src="https://unpkg.com/cropperjs/dist/cropper.js"></script><!-- Cropper.js is required -->
<link rel="stylesheet" href="https://unpkg.com/cropperjs/dist/cropper.css" crossorigin="anonymous">
<script src="{{ asset('libs/cropper/dist/jquery-cropper.js') }}"></script>

@if(setting_item('inbox_enable'))
    <script src="{{ asset('module/core/js/chat-engine.js?_ver='.config('app.version')) }}"></script>
@endif
<script src="{{ asset('js/home.js?_ver='.config('app.version')) }}"></script>

@if(!empty($is_user_page))
    <script src="{{ asset('module/user/js/user.js?_ver='.config('app.version')) }}"></script>
@endif
{!! \App\Helpers\Assets::js(true) !!}
<script src="{{ asset('custom/js/app.custom.js') }}"></script>
{!! App\Helpers\MapEngine::scripts(['libraries'=>'places','callback'=>'initAutocomplete']) !!}

@yield('footer')

@php \App\Helpers\ReCaptchaEngine::scripts() @endphp

<script>
$(document).ready(function(){
	$(function() {                       
  $("#tourSubmit").click(() => $(".carousel").carousel("next"));
  
 // $("#findTherapist").click(() => $(".carousel").carousel("next"));
});
	$('#thCat').change(function() {
    var data = "";
	var base_url = window.location.origin;
	$('#ulbox2').empty();
    $.ajax({
        type:"GET",
        url : "ajax-call",
        data : "selectedvalue="+$(this).val(),
        async: false,
        success : function(response) {
            data = response;
			$('#ulbox2').empty();
			$('#ulbox2').append(data)
			//console.log(data);
            //return response;
        },
        error: function() {
            alert('Error occured');
        }
    });
	
});
$("#ulbox2").on("click", "li", function(event){

var postcode = $('#post_code').val();
var lat = $('#lat').val();
var lng = $('#lng').val();
//alert(postcode);
 //$(this).addClass('selected'); 
 event.preventDefault();
    $(this).toggleClass('selected');
    var l = [];
    $('.selected').each(function(){
		
        l.push($(this).attr('id'));
    });
     $('#colorvalues').val(l.join(','));
      var fval =$('#colorvalues').val();
	  //var arr = fval.split('@');
	  catId = 1;
	  //fval = arr[1];
 $('#findTherapist').click(function() {
    var data = "";
	
    $.ajax({
        type:"GET",
        url : "ajax-call-submit",
        data : "post_code="+postcode+"&selectedvalue="+fval+"&lat="+lat+"&lng="+lng,
        async: false,
        success : function(response) {
			//alert(response);
          window.location.href=response;
        },
        error: function() {
            alert('Error occured');
        }
    });
	
});



});



$('.results_map  .col-sm-6 .col-md-6 .col-lg-6').removeClass('.col-sm-6 .col-md-6 .col-lg-6');
//$(.results_map .).removeClass('.col-sm-6 .col-md-6 .col-lg-6');
});

// $('.check_class').on('click',function(){
			// var favorite = [];
            // $.each($(".check_class"), function(){
                // favorite.push($(this).val());
            // });
				
	// $.ajax({
        // type:"GET",
        // url : "ajax-call-getFilter",
        // data : "post_code="+<?php //echo $postcode?>+"&terms="+favorite,
        // async: false,
        // success : function(response) {
			// //alert(response);
          // window.location.href=response;
        // },
        // error: function() {
            // alert('Error occured');
        // }
    // });
// });
	
</script>
