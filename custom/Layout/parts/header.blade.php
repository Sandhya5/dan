   <style>
   .modalcross{
           padding: 0 0 10px;
    border: none;
    position: absolute;
    z-index: 999;
    right: 17px;
    top: 0;
   }
    
    .modal .modal-content {
    position: relative;
}

.btn-venderhead {
    position: absolute;
    top: -36px;
}

.loggedinlogo{
    margin-top: 7px !important;
}
   </style>
   
   <script type="text/javascript">
   $("input").on("focus", function(e) {
  console.log('1');
  e.preventDefault();
  e.stopPropagation();
});

$("input").on("click", function(e) {
  console.log('1');
  e.preventDefault();
  e.stopPropagation();
  this.setSelectionRange(0, 9999);
});
 </script>
   
   
   
<?php

if (Route::getCurrentRoute()->uri() == 'en' || Route::getCurrentRoute()->uri()=='/')
{
     $logofile="http://takeaseatstaging.com/uploads/demo/space/featured-box/icon-space-box-3.png";

}
else{
    //echo 'bye';
    echo '<style>
	.minheadheight,.hero-section {
    min-height: auto;
}</style>';
      echo '</section>';
      
        $logofile="http://takeaseatstaging.com/custom/assets/images/white-logo.png";
     
}


?>


   
    <section id="sec-id" class="minheadheight cust-hero-section">
	<div>
	<div style="min-height:0px;" id="fronted-header" class="hometop-bar hero-section ">
     <div class="container">
  <div class="row topbar">
        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
               @if(Auth::id())
         <div class="logo loggedinlogo">
             @else
            
              <div class="logo">
                   @endif
                   
          <a href="{{url(app_get_locale(false,'/'))}}" class="bravo-logo">
                    @if($logo_id = setting_item("logo_id"))
                        <?php $logo = get_file_url($logo_id,'full');?>
                        <img src="{{$logofile}}" alt="{{setting_item("site_title")}}">
                    @endif
                </a></div>
      </div>
      <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="join">
               @if(Auth::id())
               
               @else
           <a href="https://www.takeaseat.co.uk/therapist/" target="_blank"  class="btn-join blue-text">Join as a therapist</a>
          <!--<a href="<?php // echo URL::to('/page/join-our-community');?>" class="btn-join blue-text">Join as a therapist</a>-->
    <!--<a href="http://www.takeaseat.co.uk/"   class="btn-join blue-text ">Join as a therapist</a>-->
    <!--  Vender Loign -->
@endif

<div class="modal fade login " id="Venderregister" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        
       <div class="modal-header modalcross">
      
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      
      <div class="modal-body relative">
        <div class="row">
          <div class="col-sm-12 col-md-12">
           <div class="login-form">
            <div class="d-flex justify-content-center">
              <a href="#" class="btn-personal btn-venderhead">{{__('Join as a Therapist')}}</a>
          
            </div>
            <div style="    margin-top: 29px;">
           @include('Layout::auth/vender-register-form')
           </div>
        <div>


</div>



          </div> 
         </div>

        
     
    </div>
  </div>
</div>



</div></div>


    <!-- Trigger the modal with a button -->

          <ul class="login-menu topbar-items">
                        @include('Language::frontend.switcher')
                        @if(!Auth::id())
                            <li class="login-item">
                                <a href="#login" data-toggle="modal" data-target="#login" class="login">{{__('Login')}}</a>
                            </li>
                            <li class="signup-item">
                                <a href="http://takeaseatstaging.com/news" class="signup">{{__('Info hub')}}</a>
                            </li>
                        @else
                            <li class="login-item dropdown">
                                <a href="#" data-toggle="dropdown" class="is_login">
                                    @if($avatar_url = Auth::user()->getAvatarUrl())
                                        <img class="avatar" src="{{$avatar_url}}" alt="{{ Auth::user()->getDisplayName()}}">
                                    @else
                                        <span class="avatar-text">{{ucfirst( Auth::user()->getDisplayName()[0])}}</span>
                                    @endif
                                    {{__("Hi, :Name",['name'=>Auth::user()->getDisplayName()])}}
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu text-left">
                                    <li><a href="{{url(app_get_locale().'/user/profile')}}"><i class="icon ion-md-construct"></i> {{__("My profile")}}</a></li>

                                    @if(Auth::user()->hasPermissionTo('dashboard_access'))
                                        <li class="menu-hr"><a href="{{url('/admin')}}"><i class="icon ion-ios-ribbon"></i> {{__("Dashboard")}}</a></li>
                                    @endif
                                    <li class="menu-hr">
                                        <a  href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> {{__('Logout')}}</a>
                                    </li>
                                </ul>
                                <form id="logout-form" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        @endif
                    </ul>
    
       </div>
     </div>
   </div>
   </div>
   
   </div>
   
</div>
