<div class="modal fade login" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center blue-text" id="exampleModalLabel">Login - Your Account
</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body relative">
        <div class="row">
          <div class="col-sm-6 col-md-6">
           <div class="login-form">
            <div class="d-flex justify-content-center">
            <a href="#" class="btn-personal">Personal Login</a>
          </div>
            <!-- Default form login -->
 
  <div>


</div>



          </div> 
          
          
         </div>

        <div class="col-sm-6 col-md-6">
           <div class="login-form">
             <div class="verticle-line"></div>
            <div class="d-flex justify-content-center">
            <a href="#" class="btn-personal">Therapistsss Login</a>
          </div>
            <!-- Default form login -->
<form class="bravo-form-login" method="POST" action="{{ route('login') }}">
    @csrf
    <div class="">
        <input type="text" class="form-control mb-4" name="email" autocomplete="off" placeholder="{{__('Email address')}}">
       <span class="invalid-feedback error error-email"></span>
    </div>
    <div class="">
        <input type="password" class="form-control  mb-4" name="password" autocomplete="off"  placeholder="{{__('Password')}}">
        <span class="invalid-feedback error error-password"></span>
    </div>
    <div class="">
        <div class="d-flex justify-content-between">
            <label for="remember-mee" class="mb0">
                <input type="checkbox" name="remember" id="remember-mee" value="1"> {{__('Remember me')}} <span class="checkmark fcheckbox"></span>
            </label>
            
        </div>
         </div>
         <a href="{{ route("password.request") }}">{{__('Forgot Password?')}}</a>
   
    @if(setting_item("user_enable_login_recaptcha"))
        <div class="form-group">
            {{recaptcha_field($captcha_action ?? 'login')}}
        </div>
    @endif
    <div class="error message-error invalid-feedback"></div>
    <div class="form-group">
        <button class="btn btn-primary form-submit" type="submit">
            {{ __('Login') }}
            <span class="spinner-grow spinner-grow-sm icon-loading" role="status" aria-hidden="true"></span>
        </button>
    </div>
    @if(setting_item('facebook_enable') or setting_item('google_enable') or setting_item('twitter_enable'))
        <div class="advanced">
            <p class="text-center f14 c-grey">{{__('or continue with')}}</p>
            <div class="row">
                @if(setting_item('facebook_enable'))
                    <div class="col-xs-12 col-sm-4">
                        <a href="{{url('/social-login/facebook')}}"class="btn btn_login_fb_link" data-channel="facebook">
                            <i class="input-icon fa fa-facebook"></i>
                            {{__('Facebook')}}
                        </a>
                    </div>
                @endif
                @if(setting_item('google_enable'))
                    <div class="col-xs-12 col-sm-4">
                        <a href="{{url('social-login/google')}}" class="btn btn_login_gg_link" data-channel="google">
                            <i class="input-icon fa fa-google"></i>
                            {{__('Google')}}
                        </a>
                    </div>
                @endif
                @if(setting_item('twitter_enable'))
                    <div class="col-xs-12 col-sm-4">
                        <a href="{{url('social-login/twitter')}}" class="btn btn_login_tw_link" data-channel="twitter">
                            <i class="input-icon fa fa-twitter"></i>
                            {{__('Twitter')}}
                        </a>
                    </div>
                @endif
            </div>
        </div>
    @endif
    <div class="c-grey font-medium f14 text-center"> {{__('Do not have an account?')}} <a href="" data-target="#Venderregister" data-toggle="modal">{{__('Sign Up')}}</a>
    </div>
</form>
<!-- Default form login -->
  <div>


</div>



          </div> 
         </div>
     
    </div>
  </div>
</div>



</div></div>


<div class="modal fade login " id="register" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center blue-text" id="exampleModalLabel">{{__('Sign Up')}} - Your Account
</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body relative">
        <div class="row">
          <div class="col-sm-12 col-md-12">
           <div class="login-form">
            <div class="d-flex justify-content-center">
              <a href="#" class="btn-personal">{{__('Sign Up')}}</a>
          
            </div>
           @include('Layout::auth/register-form')
        <div>


</div>



          </div> 
         </div>

        
     
    </div>
  </div>
</div>



</div></div>





<!--  Vender Loign -->



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        @include('Layout::auth/register-form')
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    
    
    
    
    
<div class="modal fade login " id="Venderregister" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center blue-text" id="exampleModalLabel">{{__('Sign Up')}} - Your Account
</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body relative">
        <div class="row">
          <div class="col-sm-12 col-md-12">
           <div class="login-form">
            <div class="d-flex justify-content-center">
              <a href="#" class="btn-personal">{{__('Sign Up')}}</a>
          
            </div>
           @include('Layout::auth/register-form')
        <div>


</div>



          </div> 
         </div>

        
     
    </div>
  </div>
</div>



</div></div>


