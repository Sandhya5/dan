    <section class="">
	<div>
	<div style="min-height:0px;"  class="hero-section minheadheight">
     <div class="container">
  <div class="row topbar">
        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
         <div class="logo">
          <a href="{{url(app_get_locale(false,'/'))}}" class="bravo-logo">
                    @if($logo_id = setting_item("logo_id"))
                        <?php $logo = get_file_url($logo_id,'full') ?>
                        <img src="{{$logo}}" alt="{{setting_item("site_title")}}">
                    @endif
                </a></div>
      </div>
      <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="join">
         <a href="<?php  echo URL::to('/page/become-a-vendor');?>" class="btn-join blue-text">Join as a therapist</a>
          <ul class="login-menu topbar-items">
                        @include('Language::frontend.switcher')
                        @if(!Auth::id())
                            <li class="login-item">
                                <a href="#login" data-toggle="modal" data-target="#login" class="login">{{__('Login')}}</a>
                            </li>
                            <li class="signup-item">
                                <a href="#register" data-toggle="modal" data-target="#register" class="signup">{{__('Info hub')}}</a>
                            </li>
                        @else
                            <li class="login-item dropdown">
                                <a href="#" data-toggle="dropdown" class="is_login">
                                    @if($avatar_url = Auth::user()->getAvatarUrl())
                                        <img class="avatar" src="{{$avatar_url}}" alt="{{ Auth::user()->getDisplayName()}}">
                                    @else
                                        <span class="avatar-text">{{ucfirst( Auth::user()->getDisplayName()[0])}}</span>
                                    @endif
                                    {{__("Hi, :Name",['name'=>Auth::user()->getDisplayName()])}}
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu text-left">
                                    <li><a href="{{url(app_get_locale().'/user/profile')}}"><i class="icon ion-md-construct"></i> {{__("My profile")}}</a></li>

                                    @if(Auth::user()->hasPermissionTo('dashboard_access'))
                                        <li class="menu-hr"><a href="{{url('/admin')}}"><i class="icon ion-ios-ribbon"></i> {{__("Dashboard")}}</a></li>
                                    @endif
                                    <li class="menu-hr">
                                        <a  href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> {{__('Logout')}}</a>
                                    </li>
                                </ul>
                                <form id="logout-form" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        @endif
                    </ul>
    
       </div>
     </div>
   </div>
   </div>
   
   </div>
   
</div>
<?php
//echo Route::getCurrentRoute()->uri();
if (Route::getCurrentRoute()->uri() == 'en' || Route::getCurrentRoute()->uri() == '/')
{
  
    // You're on the root route
}
else{
    //echo 'bye';
    echo '<style>.minheadheight{min-height:0px;}</style>';
      echo '</section>';
}
?>
