@extends('layouts.app')
@section('head')
    <link href="{{ asset('module/tour/css/tour.css?_ver='.config('app.version')) }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset("libs/ion_rangeslider/css/ion.rangeSlider.min.css") }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset("libs/fotorama/fotorama.css") }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset("listpagecss/css/booking_css.css") }}"/>

<!--<script src="{{ asset('module/booking/js/checkout.js') }}"></script>-->
 
    <style type="text/css">
        .modal .modal-content {
    border-radius: 0px;
    padding: 0px;
}
        .gmnoprint,.gm-control-active{
            display: none;
        }
        .bravo_banner_map:before{
            content: "";
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            position: absolute;
            z-index: 1;
        }
       .booking-page {
    margin-top: 0px;
}.date-btn {
 
    color: #fff;
}.btn:hover {
    color: #ffffff;
    text-decoration: none;
}.mt-5, .my-5 {
    margin-top: 1rem !important;
}
    </style>
@endsection
@section('content')
    <div class="bravo_detail_tour">


<section class="booking-page">
 <div class="container">
  <div class="row">
    <div class="col-12">
      <div class="back-btn">
        <a href="javascript:history.back()">
          <img src="{{ asset('listpagecss/images/back-arrow.png') }}" align="back arrow">
          <span>Back to search</span>
        </a>
      </div>

<?php //print_r($row->review_data);?>
    </div>
     @php $review_score = $row->review_data @endphp
    @include('Tour::frontend.layouts.details.tour-detail')
@include('Tour::frontend.layouts.details.tour-form-book')
</section>

       

@php
    $terms_ids = $row->tour_term->pluck('term_id');
    $attributes = \Modules\Core\Models\Terms::getTermsById($terms_ids);
@endphp
<?php   foreach($attributes as $attribute ){
   // print_r($attribute['parent']->slug);
        $list_active = ['anxiety','depression','stress'];
        if( in_array($attribute['parent']->slug, $list_active)){
             $translate_attribute = $attribute['parent']->translateOrOrigin(app()->getLocale());
                $terms = $attribute['child'] ;
                    foreach($terms as $term ){
                         $translate_term = $term->translateOrOrigin(app()->getLocale()) ;
                         $tnm[] = $translate_term->name;
                                $terms= implode(',',$tnm);
              }
        }
}

if($row->age==1){$ag= 'Adults';}elseif($row->age==2){$ag= 'Children (6-10)';}elseif($row->age==3){$ag= 'Elders (65+)';}elseif($row->age==4){$ag= 'Adolescents / Teenagers (14 to 19)';}elseif($row->age==5){$ag= 'Preteens / Tweens (11 to 13)';}elseif($row->age==6){$ag= 'Toddlers / Preschoolers (0 to 6)';}

$user = Auth::user();
//print_r($user);
?>

    @php
    $term_conditions = setting_item('booking_term_conditions');
    @endphp  
<!-- Modal -->
<div class="modal fade booking-information" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-body booking-form">
        <?php //print_r($row->age);?>
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>   
       <!--  <nav>
          <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Home</a>
            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Profile</a>
            <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</a>
          </div>
        </nav> -->
        <div class="tab-content" id="nav-tabContent">
          <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

            <div class="signup-form">
              <div class="d-flex justify-content-center">
                <a href="#" class="btn-personal">Booking Information</a>
              </div>
              <!-- Default form login -->
              <form class="text-center border border-light">
                @csrf 
                <input type="hidden" name="code" id="code" value=""/>
                <input type="hidden" name="dates" id="start_date" value=""/>
                <input type="hidden" name="service_id" id="service_id" value=""/>
                <input type="hidden" name="slots" id="slots" value=""/>
                <!-- <input type="hidden" name="_token" id="token" value="{{-- csrf_token() --}}"> 
                <input type="hidden" name="payment_gateway" value="offline_payment">-->
                <input type="hidden" name="payment_gateway" value="stripe">
                <input type="text" id="" value="{{$translation->title}}" class="form-control mb-4" placeholder="(Therapist) Jane Brown">


                <input type="text" id="location" class="form-control mb-4" placeholder="(Location) Bury, Manchester">

                <!--<input type="text" id="" value="{{$ag ?? ''}}" class="form-control mb-4" placeholder="(Session) Adult">-->

             
                <input type="text" id="term_id" name="term_id" value="" class="form-control mb-4" placeholder="(Issues) Despression, Aniety">

                <input type="date" id="start_date" value="<?php echo date('Y-m-d');?>" name="start_date" class="form-control mb-4" placeholder="(Date) 14/01/2020">

                <input type="text" id="extra_price"  name="extra_price" value=" " class="form-control mb-4" placeholder="(Price) £60">

<select name= "sess_loc" id="sess_loc" class="form-control">
      <option value=""> Select Session Location </option>
      <option value="facetoface"> Face to face </option>
      <option value="online"> Online Video</option>
      
</select>
                <div class="md-d-flex justify-content-md-center">
                 
                 <h3 class="number blue-text">1 of 2</h3>
                <!-- <a class="btn btn-primary btn-block my-4" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">CONFIRM YOUR DETAILS AND BOOK</a> -->
                  <a class="btn btn-primary btn-block my-4" id="nav-profile-tab">CONFIRM YOUR DETAILS AND BOOK
                    <i class="fa fa-spinner fa-spin"></i></a>
              </div>
         
              </form>
              <!-- Default form login -->
              <div>


              </div>



            </div> 

          </div>
          <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

          <div class="signup-form">
            <div class="d-flex justify-content-center">
            <a href="#" class="btn-personal">Booking Information</a>
          </div>
            <!-- Default form login -->
<form class="text-center border border-light">
    @csrf
    
                <input type="hidden" name="code" id="code" value=""/>
     <p class="blue-text pers-detail">Complete your personal details (all fields required) or <a href="#">Login</a> </p>

    <!-- first name -->
    <input type="text" id="" name="first_name" value="{{$user->first_name ?? ''}}" class="form-control mb-4" placeholder="First Name">


    <!-- first name -->
    <input type="text" id="" name="last_name" value="{{$user->last_name ?? ''}}" class="form-control mb-4" placeholder="Surname">

    <!-- Password -->
    <input type="email" id="" name="email"value="{{$user->email ?? ''}}" class="form-control mb-4" placeholder="Email address. 
">

    <!-- Confirm Password -->
    <input type="text" id="" name="phone" value="{{$user->phone ?? ''}}" class="form-control mb-4" placeholder="Contact number">

    <input type="text" name="address_line_1" value="{{$user->address ?? ''}}" id="" class="form-control mb-4" placeholder="House name or number">
    <input type="text" name="address_line_1" id="" value="{{$user->address2 ?? ''}}" class="form-control mb-4" placeholder="Address line
">
 <input type="text" id="" name="city" value="{{$user->city ?? ''}}" class="form-control mb-4" placeholder="Town or City
">
<input type="text" id="" name="zip_code"value="{{$user->zip_code ?? ''}}" class="form-control mb-4" placeholder="Postcode
">
    <div class="d-flex justify-content-center">
        <div>
            <!-- Remember me -->
            <div class="custom-control custom-checkbox">
<div class="">
       
            <input type="checkbox" name="term_conditions"> {{__('I have read and accept the')}} <label class="term-conditions-checkbox">  <a target="_blank" href="{{get_page_url($term_conditions)}}">{{__('terms and conditions')}}</a>
        </label>
    </div>
            </div>
        </div>
        
    </div>
    <div class="html_before_actions"></div>

     <p class="alert-text mt10" id="post" style="color: red;"></p>
<div class="md-d-flex justify-content-md-center">
     <!-- Sign in button -->
     <h3 class="number blue-text">2 of 2</h3>
    <!-- <button class="btn btn-primary btn-block my-4" type="submit">BOOK NOW</button> -->
   <div class="form-actions">
        <button type="button" class="btn btn-primary btn-block my-4" onclick="doCheckout(100)">{{__('BOOK NOW')}}
            <!-- <i class="fa fa-spin fa-spinner" v-show="onSubmit"></i> -->
        <i class="fa fa-spinner fa-spin"></i></button>
    </div>
</div>
   
   

</form>
<!-- Default form login -->
  <div>


</div>



          </div>  

          </div>

        </div>










      </div>
      
    </div>
  </div>
</div>










    </div>
@endsection

@section('footer')
    {!! App\Helpers\MapEngine::scripts() !!}
    <script>
        jQuery(function ($) {
            $('.jane-brown > p, ul').addClass('para mt-5');
            @if($row->map_lat && $row->map_lng)
            new BravoMapEngine('map_content', {
                disableScripts: true,
                fitBounds: true,
                center: [{{$row->map_lat}}, {{$row->map_lng}}],
                zoom: 9,
                map_options:{
                    disableDefaultUI: true,
                    zoomControl: false,
                    mapTypeControl: true,
                    scaleControl: false,
                    streetViewControl: false,
                    rotateControl: false,
                    fullscreenControl: false,
                    panControl: false,
                    styles: [
                        {
                            "featureType": "all",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#b5eee9"
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "gamma": 0.01
                                },
                                {
                                    "lightness": 20
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "saturation": -31
                                },
                                {
                                    "lightness": -33
                                },
                                {
                                    "weight": 2
                                },
                                {
                                    "gamma": 0.8
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "lightness": 30
                                },
                                {
                                    "saturation": 30
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "saturation": 20
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "lightness": 20
                                },
                                {
                                    "saturation": -20
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "lightness": 10
                                },
                                {
                                    "saturation": -30
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "saturation": 25
                                },
                                {
                                    "lightness": 25
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "lightness": -20
                                }
                            ]
                        }
                    ],
                },
            });
            @endif
        })
    </script>
    <script>
        var bravo_booking_data = {!! json_encode($booking_data) !!}
        var bravo_booking_i18n = {
                no_date_select:'{{__('Please select Start date')}}',
             //   no_slot_select:'{{__('Please select slot')}}',
                no_guest_select:'{{__('Please select at lease one guest')}}',
                load_dates_url:'{{route('tour.vendor.availability.loadDates')}}',
				no_date_array_select:'{{__('Please select date')}}',
				//no_date_slot_select:'{{__('No slots select for date ')}}',
            };
    </script>
    <script type="text/javascript" src="{{ asset("libs/ion_rangeslider/js/ion.rangeSlider.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("libs/fotorama/fotorama.js") }}"></script>
    <script type="text/javascript" src="{{ asset("libs/sticky/jquery.sticky.js") }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/single-tour-custom.js?_ver='.config('app.version')) }}"></script>

    <script src="{{ asset("listpagecss/js/jquery.min.js")}}"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="{{ asset("listpagecss/js/bootstrap.min.js")}}"></script>
    <script src="{{ asset("listpagecss/js/owl.carousel.min.js")}}"></script>
    <!-- <script src="{{ asset("listpagecss/js/main.js")}}"></script> -->
@endsection
