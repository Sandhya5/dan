<?php //print_r($row);?>

<div class="col-sm-12 col-md-12 col-lg-4">    
      <div id="custCarousel" class="carousel slide booking-carousel" data-ride="carousel" align="center">
        <!-- slides -->
          @if($row->getGallery())
          <div class="carousel-inner">
          
@foreach($row->getGallery() as $key=>$item)
              <?php if($key==0){$act ='active';}else{$act ='';}; ?>
          <div class="carousel-item <?php echo $act?>"> 

            <img src="{{$item['thumb']}}" alt="">
            <a href="{{$item['large']}}" class="video-link">{{$translation->title}}
            </a>
          </div>
          @endforeach
        </div> <!-- Left right --> 
        @else
 <div class="carousel-inner">
          
              <div class="carousel-item active"> 

            <img src="{{ asset("listpagecss/images/no-image.jpg") }}" alt="">
            <a href="{{ asset("listpagecss/images/no-image.jpg") }}" class="video-link">{{$translation->title}}<!-- <i class="fa fa-play" aria-hidden="true"></i>-->
            </a>

          </div>
        
        </div> <!-- Left right --> 

        @endif
        <a class="carousel-control-prev" href="#custCarousel" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a> <a class="carousel-control-next" href="#custCarousel" data-slide="next"> <span class="carousel-control-next-icon"></span> </a> <!-- Thumbnails -->

        <div class="dots">    
        @if($row->getGallery())         
          <ol class="carousel-indicators list-inline">
@foreach(array_slice($row->getGallery(), 0, 3) as $key=>$item)
<?php if($key==0){$act ='active';}else{$act ='';}; ?>
 <?php if($key==0){$slc ='selected';}else{$slc ='';}; ?>
            <li class="list-inline-item <?php echo $act?>"> <a id="carousel-selector-{{$key}}" class="<?php echo $slc?>" data-slide-to="{{$key}}" data-target="#custCarousel"> </a> </li>
           
            @endforeach                    
          </ol>
          @else

           <ol class="carousel-indicators list-inline">
            <li class="list-inline-item active"> <a id="carousel-selector-0" class="selected" data-slide-to="0" data-target="#custCarousel"> </a> </li>
           
                      
          </ol>
 @endif
        </div>



@if($row->getGallery())
        <ol class="carousel-indicators list-inline booking-thumb">
              
              @foreach(array_slice($row->getGallery(), 0, 3) as $key=>$item)
                   <li class="list-inline-item "> <a id="carousel-selector-{{ $key}}" class="" data-slide-to="{{ $key}}" data-target="#custCarousel"> <img src="{{$item['thumb']}}" class="img-fluid"> </a> </li>
          
              @endforeach
          
        </ol>

        @else

         <ol class="carousel-indicators list-inline booking-thumb">
              
                   <li class="list-inline-item "> <a id="carousel-selector-0" class="" data-slide-to="0" data-target="#custCarousel"> <img src="{{ asset("listpagecss/images/no-image.jpg") }}" class="img-fluid"> </a> </li>
          
        </ol>
 @endif


      </div>


      <div class="social-section share"> 
        <h3 class="blue-text mb-3 text-center">Share</h3>
        <div class="row d-flex justify-content-center">      
          <a class="facebook share-item" href="https://www.facebook.com/sharer/sharer.php?u={{$row->getDetailUrl()}}&amp;title={{$translation->title}}" target="_blank" original-title="{{__("Facebook")}}"><img src="{{ asset('listpagecss/images/YoutubeIcon.png') }}"></a>
    <a class="twitter share-item" href="https://twitter.com/share?url={{$row->getDetailUrl()}}&amp;title={{$translation->title}}" target="_blank" original-title="{{__("Twitter")}}"><img src="{{ asset('listpagecss/images/TwitterIcon.png') }}"></a>
    <a class="linkedin share-item" href="https://www.linkedin.com/sharing/share-offsite/?url={{$row->getDetailUrl()}}" target="_blank"><img src="{{ asset('listpagecss/images/FacebookIcon.png') }}"></a>
    <!-- <a class="reddit share-item" href="http://www.reddit.com/submit?url={{$row->getDetailUrl()}}" target="_blank"><i class="fa fa-reddit" aria-hidden="true"></a> -->
    <a class="email share-item" href="mailto:?subject={{$translation->title}}"
       title="{{__("Share by Email")}}"><img src="{{ asset('listpagecss/images/envelop.png') }}">
    </a>    
        </div>
      </div>





    </div>

    <div class="col-sm-12 col-md-12 col-lg-5">
      <div class="jane-brown">
       <div class="row">
         <div class="col-sm 12 col-md-6">
           <div class="jane-info">
             <h2 class="blue-text">{{$translation->title}}</h2>
             <address>
@if($row->country)
            <p class="address"><i class="fa fa-map-marker"></i>
                @if($row->city)
                    {{$row->city}},
                @endif
                @if($row->country)
                    {{$row->country}}
                @endif
            </p>
        @endif
         @if(!empty($distance_in_km = request()->input('distance_in_km')))
            <p><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__(':range km away',['range'=>number_format($distance_in_km,2)])}}</p>
        @endif
             </address>
           </div>
         </div>
         <div class="col-sm 12 col-md-6">
           <div class="award">
             <div class="award-img">                     
              <img src="{{ asset('listpagecss/images/award.png') }}">
              <img src="{{ asset('listpagecss/images/award.png') }}">
              <img src="{{ asset('listpagecss/images/award.png') }}">
            </div>


<div class="rating">
                            @for( $i = 0 ; $i < 5 ; $i++ )
                                @if($i < $review_score['score_total'])
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                @else
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                @endif
                            @endfor   
         </div>
         <p>{{__("from :number reviews",['number'=>$review_score['total_review']])}}</p>
      


         </div>
       </div>
     </div>

@if($translation->content)
     <p class="para mt-5"><?php echo $translation->content ?></p>
@endif
   @php
    $terms_ids = $row->tour_term->pluck('term_id');
    $attributes = \Modules\Core\Models\Terms::getTermsById($terms_ids);
@endphp
 <div class="issue mt-5">
    @foreach($attributes as $attribute )
            @php $translate_attribute = $attribute['parent']->translateOrOrigin(app()->getLocale()) @endphp
            <h4 class="blue-text">{{ $translate_attribute->name }}</h4>
                @php $terms = $attribute['child'] @endphp
                <div class="issue-link">
                    @foreach($terms as $term )
                        @php $translate_term = $term->translateOrOrigin(app()->getLocale()) @endphp
                        <a href="#" class="active">{{$translate_term->name}} </a>
                         @endforeach
                </div>
            
    @endforeach
     </div>


     
   

    
     @include('Tour::frontend.layouts.details.tour-review') 
</div>       
</div>














