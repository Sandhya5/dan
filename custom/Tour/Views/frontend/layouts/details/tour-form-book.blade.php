<style>
    .slotscls{
        background-color: #00b6ed;
        color: #fff;
    }
     .slotsclsanothr2{
        background-color: #00b6ed;
        color: #fff;
    }
    .slotsclsanothr3{
        background-color: #00b6ed;
        color: #fff;
    }
    .error {
      color: red;
      /*background-color: #acf;*/
   }
</style>
<div class="col-sm-12 col-md-12 col-lg-3">
  <div class="per-hour">
  <div id="bravo_custom_tour_book_app" v-cloak>     
  <form id="myform">  
  <input type="hidden" id="userId" name="userId" value="@if (Auth::check()) 1 @else 0 @endif">   
  
  <input type="hidden" id="location" name="location" value="{{$row->city}},{{$row->country}}">   
    <div class="price">

        <!-- <span class="onsale">{{ $row->display_sale_price }}</span> -->
        <span class="text-lg">{{ $row->display_price }}</span> <p>per hour</p>
    </div>
    <p class="text-center text-grey">Who is this session for?</p>

<?php
                    $terms_ids = $row->tour_term->pluck('term_id');
                    $query = \Custom\Core\Models\Terms::select('*');
                    if(!empty(setting_item('tour_attr_book_form_detail'))){
                        $query->where('attr_id',setting_item('tour_attr_book_form_detail'));
                    }
                    $query->whereIn('id',$terms_ids);
                    $res = $query->orderBy('id', 'desc')->limit(20)->get();
                    ?>
                    @if(!empty($res))
                        <select id="term_id"  name="term_id" v-model="term_id" class="form-control">
                            <option value="" disabled="">{{ __('-- Select --') }}</option>
                          
                            <option value="1">Adults</option>
                            <option value="2">Children (6-10)</option>
                            <option value="3">Elders (65+)</option>
                            <option value="4">Adolescents / Teenagers (14 to 19)</option>
                            <option value="5">Preteens / Tweens (11 to 13)</option>
                            <option value="6">Toddlers / Preschoolers (0 to 6)</option>
                           <!-- @foreach($res as $term)
                                <option value="{{ $term->id }}">{{ $term->name }}</option>
                            @endforeach-->
                            
                            
                        </select>
                    @endif

   
    <div class="select-date text-center">
       <!-- <date-item v-for="(date,index) in dates" :key="index" :index="index" :date="date" @change-date="changeDateLine" ></date-item> -->
      <div class="slot text-center element" id='div_1'>
         <p class="text-grey mb-1">Select Date
        <input class="dateclass" type="date"  id="start_date_1" min="{{ date('Y-m-d') }}" name="start_date[0]" value="">
      
      <p class="text-grey mb-1">Slots</p>
      <div class="slots-box">
        <input name="slots[]" id="slots_1" class="slots" type="text" value="12:00" readonly placeholder="12:00">
        <input name="slots[]" id="slots_1" class="slots" type="text" value="13:00" readonly placeholder="13:00">
        <input name="slots[]" id="slots_1" class="slots" type="text" value="14:00" readonly placeholder="14:00">
      </div>
      <div class="">
        <!-- <input type="time" class="slots form-control" ref="slots"  id="slots" name="slots" value ="" placeholder="12:00"> -->
      <!--<select required id="slots_1" name="slots[]">-->
      <!--  <option value="12:00">12:00</option>-->
      <!--  <option value="13:00">13:00</option>-->
      <!--  <option value="14:00">14:00</option>-->
      <!--</select>-->
        
      </div>
     
        </div>
      
                 
    </div> 
    <div class="text-center form-group attr-booking-form">
                        <button name=""  class="btn btn-large btn btn-danger date-btn add" id="addmoreDate">
                            {{__('BOOK ANOTHER SESSION')}}
                        </button>
         </div>
      <input type="hidden" id="service_id" name="service_id" value="{{ $row->id }}" class="form-control">    
      <input type="hidden" id="extra_price"  name="extra_price" value="{{ $row->display_price }}" class="form-control">
  


    <div class="total">
     <div class="d-flex justify-content-between">
       <p>Total</p>
       <p>{{ $row->display_price }}</p>
     </div>
     <!-- <button type="submit" class="btn btn-danger date-btn" data-toggle="modal" data-target="#exampleModal">Book Date & Time</button> -->
     
   </div>
<div v-html="html"></div>
            <div class="submit-group  text-center" style="color: #fff">
               <!-- <button type="submit" class="date-btn" data-toggle="modal" data-target="#exampleModal">Book Date &amp; Time</button> -->
               <!-- <a class="btn btn-large date-btn" @click="doSubmit($event)" :class="{'disabled':onSubmit,'btn-success':(step == 2),'btn-primary':step == 1}" name="submit"> -->
            <input type="submit" class="btn btn-large date-btn" id="openmodal" value="BOOK DATE & TIME" name="openmodal">
              <!-- <span class="spinner-grow spinner-grow-sm icon-loading" role="status" aria-hidden="true"></span> -->
            </button>
                

                 <div class="alert-text mt10" v-show="message.content" v-html="message.content" :class="{'danger':!message.type,'success':message.type}"></div>
            </div>
</form>
</div>
 </div>
</div>


<script  type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>  

<script type="text/javascript">
    $(function(){
        $(document).on('click','.slots',function(){ 
            // debugger;
            this.select(); 
          var selected = $(this).val();
          var plc = $(this).attr('placeholder');
          //alert(plc);
            if(plc=='12:00'){
             $('.slots').attr('value', '');  
                 $(this).attr('value', '12:00');
            }else if(plc=='13:00'){
                $('.slots').attr('value', '');  
                 $(this).attr('value', '13:00');
            }else if(plc=='14:00'){
             $('.slots').attr('value', '');  
                $(this).attr('value', '14:00');
            }else{
                
            }
             $(".slots").removeClass("slotscls");
         
            $(this).addClass("slotscls");
            // $(this).css();
        });
     
         $(document).on('click','.slotsanothr2',function(){ 
            
            this.select(); 
            var selected = $(this).val();
           var plc = $(this).attr('placeholder');
            if(plc=='12:00'){
             $('.slotsanothr2').attr('value', '');  
                 $(this).attr('value', '12:00');
            }else if(plc=='13:00'){
                $('.slotsanothr2').attr('value', '');  
                 $(this).attr('value', '13:00');
            }else if(plc=='14:00'){
             $('.slotsanothr2').attr('value', '');  
                $(this).attr('value', '14:00');
            }else{
                
            }
             $(".slotsanothr2").removeClass("slotsclsanothr2");
            $(this).addClass("slotsclsanothr2");
            // $(this).css();
        });
         $(document).on('click','.slotsanothr3',function(){ 
            
            this.select(); 
            var selected = $(this).val();
           var plc = $(this).attr('placeholder');
            if(plc=='12:00'){
             $('.slotsanothr3').attr('value', '');  
                 $(this).attr('value', '12:00');
            }else if(plc=='13:00'){
                $('.slotsanothr3').attr('value', '');  
                 $(this).attr('value', '13:00');
            }else if(plc=='14:00'){
             $('.slotsanothr3').attr('value', '');  
                $(this).attr('value', '14:00');
            }else{
                
            }
             $(".slotsanothr3").removeClass("slotsclsanothr3");
            $(this).addClass("slotsclsanothr3");
            // $(this).css();
        });
    });
</script>













