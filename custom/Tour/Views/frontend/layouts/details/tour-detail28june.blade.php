<div class="g-header">
    <div class="left">
        @if($row->image_url)
            <div class="thumb-image-detail" style="background-image: url({{$row->image_url}})">
                {{--            @if(!empty($disable_lazyload))--}}
                {{--                <img src="{{$row->image_url}}" class="img-fluid" alt="">--}}
                {{--            @else--}}
                {{--                {!! get_image_tag($row->image_id,'medium',['class'=>'img-fluid','alt'=>$row->title]) !!}--}}
                {{--            @endif--}}
            </div>
        @endif
        <h2>{{$translation->title}}</h2>
        @if($row->country)
            <p class="address"><i class="fa fa-map-marker"></i>
                @if($row->city)
                    {{$row->city}},
                @endif
                @if($row->country)
                    {{$row->country}}
                @endif
            </p>
        @endif
        @if(!empty($distance_in_km = request()->input('distance_in_km')))
            <p style="margin-top:-5px;"><i class="icofont-location-arrow"></i> {{__(':range km away',['range'=>number_format($distance_in_km,2)])}}</p>
        @endif
    </div>
    <div class="right">
        @if($review_score)
            <div class="review-score">
                <div class="head">
                    <div class="left">
                        <span class="head-rating">{{$review_score['score_text']}}</span>
                        <ul class="review-star">
                            @for( $i = 0 ; $i < 5 ; $i++ )
                                @if($i < $review_score['score_total'])
                                    <li><i class="fa fa-star"></i></li>
                                @else
                                    <li><i class="fa fa-star-o"></i></li>
                                @endif
                            @endfor
                        </ul>
                        <span class="text-rating">{{__("from :number reviews",['number'=>$review_score['total_review']])}}</span>
                    </div>

                    <div class="score d-none">
                        {{$review_score['score_total']}}<span>/5</span>
                    </div>
                </div>
                <div class="foot d-none">
                    {{__(":number% of guests recommend",['number'=>$row->recommend_percent])}}
                </div>
            </div>
        @endif
    </div>
</div>
<div class="g-tour-feature d-none">
    <div class="row">
        @if($row->duration)
            <div class="col-xs-6 col-lg-3 col-md-6">
                <div class="item">
                    <div class="icon">
                        <i class="icofont-wall-clock"></i>
                    </div>
                    <div class="info">
                        <h4 class="name">{{__("Duration")}}</h4>
                        <p class="value">
                            @if($row->duration > 1)
                                {{ __(":number hours",array('number'=>$row->duration)) }}
                            @else
                                {{ __(":number hour",array('number'=>$row->duration)) }}
                            @endif
                        </p>
                    </div>
                </div>
            </div>
        @endif
        @if(!empty($row->category_tour->name))
            @php $cat =  $row->category_tour->translateOrOrigin(app()->getLocale()) @endphp
            <div class="col-xs-6 col-lg-3 col-md-6">
                <div class="item">
                    <div class="icon">
                        <i class="icofont-beach"></i>
                    </div>
                    <div class="info">
                        <h4 class="name">{{__("Tour Type")}}</h4>
                        <p class="value">
                            {{$cat->name ?? ''}}
                        </p>
                    </div>
                </div>
            </div>
        @endif
        @if($row->max_people)
            <div class="col-xs-6 col-lg-3 col-md-6">
                <div class="item">
                    <div class="icon">
                        <i class="icofont-travelling"></i>
                    </div>
                    <div class="info">
                        <h4 class="name">{{__("Group Size")}}</h4>
                        <p class="value">
                            @if($row->max_people > 1)
                                {{ __(":number persons",array('number'=>$row->max_people)) }}
                            @else
                                {{ __(":number person",array('number'=>$row->max_people)) }}
                            @endif
                        </p>
                    </div>
                </div>
            </div>
        @endif
        @if(!empty($row->location->name))
            @php $location =  $row->location->translateOrOrigin(app()->getLocale()) @endphp
            <div class="col-xs-6 col-lg-3 col-md-6">
                <div class="item">
                    <div class="icon">
                        <i class="icofont-island-alt"></i>
                    </div>
                    <div class="info">
                        <h4 class="name">{{__("Location")}}</h4>
                        <p class="value">
                            {{$location->name ?? ''}}
                        </p>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
@if($row->getGallery())
    <div class="g-gallery">
        <div class="fotorama" data-width="100%" data-thumbwidth="135" data-thumbheight="135" data-thumbmargin="15" data-nav="thumbs" data-allowfullscreen="true">
            @foreach($row->getGallery() as $key=>$item)
                <a href="{{$item['large']}}" data-thumb="{{$item['thumb']}}"></a>
            @endforeach
        </div>
        <div class="social">
            <div class="service-wishlist {{$row->isWishList()}}" data-id="{{$row->id}}" data-type="{{$row->type}}">
                <i class="fa fa-heart-o"></i>
            </div>
        </div>
    </div>
@endif
@if($translation->content)
    <br>
    <div class="g-overview">
        <h3>{{__("Profile Overview")}}</h3>
        <div class="description">
            <?php echo $translation->content ?>
        </div>
    </div>
@endif

<div class="share">
       <span>
        {{__("Share")}}:
    </span>
    <a class="facebook share-item" href="https://www.facebook.com/sharer/sharer.php?u={{$row->getDetailUrl()}}&amp;title={{$translation->title}}" target="_blank" original-title="{{__("Facebook")}}"><i class="fa fa-facebook fa-lg"></i></a>
    <a class="twitter share-item" href="https://twitter.com/share?url={{$row->getDetailUrl()}}&amp;title={{$translation->title}}" target="_blank" original-title="{{__("Twitter")}}"><i class="fa fa-twitter fa-lg"></i></a>
    <a class="linkedin share-item" href="https://www.linkedin.com/sharing/share-offsite/?url={{$row->getDetailUrl()}}" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
    <a class="reddit share-item" href="http://www.reddit.com/submit?url={{$row->getDetailUrl()}}" target="_blank"><i class="fa fa-reddit" aria-hidden="true"></i></a>
    <a class="email share-item" href="mailto:?subject={{$translation->title}}"
       title="{{__("Share by Email")}}">
        <i class="fa fa-envelope" aria-hidden="true"></i>
    </a>
</div>
@include('Tour::frontend.layouts.details.tour-attributes')
@if($translation->highlights)
    @php if(!is_array($translation->highlights)) $translation->highlights = json_decode($translation->highlights); @endphp
    <div class="g-highlights">
        <h3> {{__("HIGHLIGHTS")}} </h3>
        <ul class="my-5">
            @foreach($translation->highlights as $item)
                <li>{!! clean($item['content']) !!}</li>
            @endforeach
        </ul>
    </div>
@endif

{{--@if($row->map_lat && $row->map_lng)--}}
{{--<div class="g-location">--}}

{{--    <h3>{{__("Tour Location")}}</h3>--}}
{{--    <div class="location-map">--}}
{{--        <div id="map_content"></div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--@endif--}}
@if(!empty($row->tour_term_detail) and count($row->tour_term_detail)>0)
    <div class="g-term" style="margin-top: 30px;">
       {{-- <h3>{{__($attrShowOnDetail->name)}}</h3>--}}
        <div class="owl-carousel">
            @foreach($row->tour_term_detail as $item=>$value)
                <div class="item">
                    @if($value->image_id)
                        <img src="{{ get_file_url($value->image_id , 'full') }}"  alt="{{$value->name}}">
                    @endif
                </div>
            @endforeach
        </div>
    </div>
@endif