<div class="col-sm-12 col-md-12 col-lg-3">
  <div class="per-hour">
  <div id="bravo_custom_tour_book_app" v-cloak>     
  <form id="myform">  
  <input type="hidden" id="userId" name="userId" value="@if (Auth::check()) 1 @else 0 @endif">       
    <div class="price">

        <!-- <span class="onsale">{{ $row->display_sale_price }}</span> -->
        <span class="text-lg">{{ $row->display_price }}</span> <p>per hour</p>
    </div>
    <p class="text-center text-grey">Who is this session for?</p>

<?php
                    $terms_ids = $row->tour_term->pluck('term_id');
                    $query = \Custom\Core\Models\Terms::select('*');
                    if(!empty(setting_item('tour_attr_book_form_detail'))){
                        $query->where('attr_id',setting_item('tour_attr_book_form_detail'));
                    }
                    $query->whereIn('id',$terms_ids);
                    $res = $query->orderBy('id', 'desc')->limit(20)->get();
                    ?>
                    @if(!empty($res))
                        <select id="term_id" required name="term_id" v-model="term_id" class="form-control">
                            <option value="" disabled="">{{ __('-- Select --') }}</option>
                            @foreach($res as $term)
                                <option value="{{ $term->id }}">{{ $term->name }}</option>
                            @endforeach
                        </select>
                    @endif

    
    <div class="select-date text-center">
      <p class="text-grey mb-1">Select Date</p>
       <!-- <date-item v-for="(date,index) in dates" :key="index" :index="index" :date="date" @change-date="changeDateLine" ></date-item> -->
      <input type="date" required id="start_date" min="{{ date('Y-m-d') }}" name="start_date" value="">
<div id="holder">
                    <div class="tour-date-actions">
                        <span class="tour-delete-date" @click="removeDate" v-if="dates.length >1">{{__('Remove')}}</span>
                    </div>

                    
        <input type="hidden" id="service_id" name="service_id" value="{{ $row->id }}" class="form-control">    
      <input type="hidden" id="extra_price"  name="extra_price" value="{{ $row->display_price }}" class="form-control">
    </div> 

  
<div class="slot text-center">
      <p class="text-grey mb-1">Slots</p>
      <div class="">
        
        <!-- <input type="time" class="slots form-control" ref="slots"  id="slots" name="slots" value ="" placeholder="12:00"> -->
      <select required id="slots" name="slots">
        <option value="1">12:00</option>
        <option value="2">13:00</option>
        <option value="3">14:00</option>
      </select>
        <!-- <input type="" class="slots form-control" ref="slots[]"  id="slots" name="slots"  placeholder="12:00">
         <input type="" class="slots  form-control" ref="slots" name="slots[]"  id="slots" name="slots" placeholder="13:00">
         <input type="" class="slots  form-control" ref="slots" name="slots[]" id="slots" name="slots" placeholder="14:00"> -->
      </div>
     <div class="text-center form-group attr-booking-form">
                        <button type="button" name="" class="btn btn-large btn btn-danger date-btn" id="addmoreDate">
                            {{__('BOOK ANOTHER SESSION')}}
                        </button>
                    </div>
                </div>

    <div class="total">
     <div class="d-flex justify-content-between">
       <p>Total</p>
       <p>{{ $row->display_price }}</p>
     </div>
     <!-- <button type="submit" class="btn btn-danger date-btn" data-toggle="modal" data-target="#exampleModal">Book Date & Time</button> -->
     
   </div>
<div v-html="html"></div>
            <div class="submit-group  text-center" style="color: #fff">
               <!-- <button type="submit" class="date-btn" data-toggle="modal" data-target="#exampleModal">Book Date &amp; Time</button> -->
               <!-- <a class="btn btn-large date-btn" @click="doSubmit($event)" :class="{'disabled':onSubmit,'btn-success':(step == 2),'btn-primary':step == 1}" name="submit"> -->
            <button type="button" class="btn btn-large date-btn" id="openmodal" name="openmodal">BOOK DATE & TIME</button>
                

                 <div class="alert-text mt10" v-show="message.content" v-html="message.content" :class="{'danger':!message.type,'success':message.type}"></div>
            </div>
</form>
</div>
 </div>
</div>

















