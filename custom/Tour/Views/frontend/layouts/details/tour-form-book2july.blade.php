<div class="col-sm-12 col-md-12 col-lg-3">
  <div class="per-hour">
  <div id="bravo_custom_tour_book_app" v-cloak>              
    <div class="price">

        <!-- <span class="onsale">{{ $row->display_sale_price }}</span> -->
        <span class="text-lg">{{ $row->display_price }}</span> <p>per hour</p>
    </div>
    <p class="text-center text-grey">Who is this session for?</p>

<?php
                    $terms_ids = $row->tour_term->pluck('term_id');
                    $query = \Custom\Core\Models\Terms::select('*');
                    if(!empty(setting_item('tour_attr_book_form_detail'))){
                        $query->where('attr_id',setting_item('tour_attr_book_form_detail'));
                    }
                    $query->whereIn('id',$terms_ids);
                    $res = $query->orderBy('id', 'desc')->limit(20)->get();
                    ?>
                    @if(!empty($res))
                        <select name="term_id" v-model="term_id" class="form-control">
                            <option value="" disabled="">{{ __('-- Select --') }}</option>
                            @foreach($res as $term)
                                <option value="{{ $term->id }}">{{ $term->name }}</option>
                            @endforeach
                        </select>
                    @endif

    
    <div class="select-date text-center">
     <!--  <p class="text-grey mb-1">Select Date</p> -->
       <date-item v-for="(date,index) in dates" :key="index" :index="index" :date="date" @change-date="changeDateLine" ></date-item>

                    <div class="tour-date-actions">
                        <span class="tour-delete-date" @click="removeDate" v-if="dates.length >1">{{__('Remove')}}</span>
                    </div>

                    
                
      <!-- <input type="text" placeholder="14/01/2020" class="form-control"> -->
    </div> 

  


    <div class="total">
     <div class="d-flex justify-content-between">
       <p>Total</p>
       <p>{{ $row->display_price }}</p>
     </div>
     <!-- <button type="submit" class="btn btn-danger date-btn" data-toggle="modal" data-target="#exampleModal">Book Date & Time</button> -->
     <div class="text-center form-group attr-booking-form">
                        <span class="btn btn-large btn-primary date-btn" @click="addDate">
                            {{__('BOOK ANOTHER SESSION')}}
                        </span>
                    </div>
   </div>
<div v-html="html"></div>
            <div class="submit-group  text-center" style="color: #fff">
                <a class="btn btn-large" @click="doSubmit($event)" :class="{'disabled':onSubmit,'btn-success':(step == 2),'date-btn':step == 1}" name="submit">
                    <span v-if="step == 1">{{__("BOOK DATE & TIME")}}</span>
                    <span v-if="step == 2">{{__("BOOK DATE & TIME")}}</span>
                    <i v-show="onSubmit" class="fa fa-spinner fa-spin"></i>
                </a>
                <div class="alert-text mt10" v-show="message.content" v-html="message.content" :class="{'danger':!message.type,'success':message.type}"></div>
            </div>

</div>
 </div>
</div>

<script type="text/x-template" id="date-line-item">
    <div>
        <div class="form-group form-date-field form-date-search clearfix " data-format="{{get_moment_date_format()}}">
            <div class="date-wrapper clearfix" @click="openStartDate">
                <div class="check-in-wrapper">
                    <label>{{__("Select Date")}}</label>
                    <div class="render check-in-render">@{{start_date_html}}</div>
                </div>
                <i class="fa fa-angle-down arrow"></i>
            </div>
            <input type="text" class="start_date" ref="start_date" style="height: 1px; visibility: hidden">
        </div>
        <div class="form-select-slots" v-if="slots.length">
            <label>{{__("Slots")}}</label>
            <div class="slots-wrapper">
                <div @click="addSlots(index)" :class="{active:selected_slots.indexOf(index) >=0}" v-for="(slot,index) in slots" class="badge">@{{slot}}</div>
            </div>

        </div>
    </div>
</script>















