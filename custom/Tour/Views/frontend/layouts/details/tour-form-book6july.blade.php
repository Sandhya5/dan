<div class="col-sm-12 col-md-12 col-lg-3">
  <div class="per-hour">
  <div id="bravo_custom_tour_book_app" v-cloak>              
    <div class="price">

        <!-- <span class="onsale">{{ $row->display_sale_price }}</span> -->
        <span class="text-lg">{{ $row->display_price }}</span> <p>per hour</p>
    </div>
    <p class="text-center text-grey">Who is this session for?</p>

<?php
                    $terms_ids = $row->tour_term->pluck('term_id');
                    $query = \Custom\Core\Models\Terms::select('*');
                    if(!empty(setting_item('tour_attr_book_form_detail'))){
                        $query->where('attr_id',setting_item('tour_attr_book_form_detail'));
                    }
                    $query->whereIn('id',$terms_ids);
                    $res = $query->orderBy('id', 'desc')->limit(20)->get();
                    ?>
                    @if(!empty($res))
                        <select name="term_id" v-model="term_id" class="form-control">
                            <option value="" disabled="">{{ __('-- Select --') }}</option>
                            @foreach($res as $term)
                                <option value="{{ $term->id }}">{{ $term->name }}</option>
                            @endforeach
                        </select>
                    @endif

    
    <div class="select-date text-center">
     <!--  <p class="text-grey mb-1">Select Date</p> -->
       <date-item v-for="(date,index) in dates" :key="index" :index="index" :date="date" @change-date="changeDateLine" ></date-item>

                    <div class="tour-date-actions">
                        <span class="tour-delete-date" @click="removeDate" v-if="dates.length >1">{{__('Remove')}}</span>
                    </div>

                    
                
      <!-- <input type="text" placeholder="14/01/2020" class="form-control"> -->
    </div> 

  
<div class="slot text-center">
      <p class="text-grey mb-1">Slots</p>
      <div class="slots-box">
        <input type="" placeholder="12:00">
        <input type="" placeholder="13:00">
        <input type="" placeholder="14:00">
      </div>
     <div class="text-center form-group attr-booking-form">
                        <span class="btn btn-large btn btn-danger date-btn" @click="addDate">
                            {{__('BOOK ANOTHER SESSION')}}
                        </span>
                    </div>
                </div>

    <div class="total">
     <div class="d-flex justify-content-between">
       <p>Total</p>
       <p>{{ $row->display_price }}</p>
     </div>
     <!-- <button type="submit" class="btn btn-danger date-btn" data-toggle="modal" data-target="#exampleModal">Book Date & Time</button> -->
     
   </div>
<div v-html="html"></div>
            <div class="submit-group  text-center" style="color: #fff">
               <button type="submit" class="date-btn" data-toggle="modal" data-target="#exampleModal">Book Date &amp; Time</button>
            </div>

</div>
 </div>
</div>

<script type="text/x-template" id="date-line-item">
    <div>
        <div class="form-group form-date-field form-date-search clearfix " data-format="{{get_moment_date_format()}}">
            <div class="date-wrapper clearfix" @click="openStartDate">
                <div class="check-in-wrapper">
                    <label>{{__("Select Date")}}</label>
                    <div class="render check-in-render">@{{start_date_html}}</div>
                </div>
                <i class="fa fa-angle-down arrow"></i>
            </div>
            <input type="text" class="start_date" ref="start_date" style="height: 1px; visibility: hidden">
        </div>
        <div class="form-select-slots" v-if="slots.length">
            <label>{{__("Slots")}}</label>
            <div class="slots-wrapper">
                <div @click="addSlots(index)" :class="{active:selected_slots.indexOf(index) >=0}" v-for="(slot,index) in slots" class="badge">@{{slot}}</div>
            </div>

        </div>
    </div>
</script>











@php
    $translation = $row->translateOrOrigin(app()->getLocale());
@endphp

            


@php
    $terms_ids = $row->tour_term->pluck('term_id');
    $attributes = \Modules\Core\Models\Terms::getTermsById($terms_ids);
@endphp
<?php   foreach($attributes as $attribute ){
    print_r($attribute['parent']->slug);
        $list_active = ['anxiety','depression','stress'];
        if( in_array($attribute['parent']->slug, $list_active)){
             $translate_attribute = $attribute['parent']->translateOrOrigin(app()->getLocale());
                $terms = $attribute['child'] ;
                    foreach($terms as $term ){
                         $translate_term = $term->translateOrOrigin(app()->getLocale()) ;
                         $tnm[] = $translate_term->name;
                                $terms= implode(',',$tnm);
              }
        }
}



if($row->age==1){$ag= 'Adults';}elseif($row->age==2){$ag= 'Children (6-10)';}elseif($row->age==3){$ag= 'Elders (65+)';}elseif($row->age==4){$ag= 'Adolescents / Teenagers (14 to 19)';}elseif($row->age==5){$ag= 'Preteens / Tweens (11 to 13)';}elseif($row->age==6){$ag= 'Toddlers / Preschoolers (0 to 6)';}
?>
  
<!-- Modal -->
<div class="modal fade booking-information" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-body">
        <?php //print_r($row->age);?>
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>   
       <!--  <nav>
          <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Home</a>
            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Profile</a>
            <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</a>
          </div>
        </nav> -->
        <div class="tab-content" id="nav-tabContent">
          <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

            <div class="signup-form">
              <div class="d-flex justify-content-center">
                <a href="#" class="btn-personal">Booking Information</a>
              </div>
              <!-- Default form login -->
              <form class="text-center border border-light" action="#">

    
                <input type="text" id="" value="{{$row->title}}" class="form-control mb-4" placeholder="(Therapist) Jane Brown
">


                <input type="text" id="" value="{{$location->name ?? ''}}" class="form-control mb-4" placeholder="(Location) Bury, Manchester
">

                <input type="text" id="" value="{{$ag ?? ''}}" class="form-control mb-4" placeholder="(Session) Adult
">

             
                <input type="text" id="" value="{{$terms?? ''}}" class="form-control mb-4" placeholder="(Issues) Despression, Aniety">

                <input type="date" id="" class="form-control mb-4" placeholder="(Date) 14/01/2020
">

                <input type="text" id="" value=" {{ $row->display_price ?? ''}}" class="form-control mb-4" placeholder="(Price) £60

">

<select class="form-control">
      <option> Select Session Location </option>
      <option> Face to face </option>
      <option> Online Video</option>
      
</select>
                <div class="md-d-flex justify-content-md-center">
                 
                 <h3 class="number blue-text">1 of 2</h3>
                <a class="btn btn-primary btn-block my-4" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">CONFIRM YOUR DETAILS AND BOOK</a>
              </div>
         
              </form>
              <!-- Default form login -->
              <div>


              </div>



            </div> 

          </div>
          <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

          <div class="signup-form">
            <div class="d-flex justify-content-center">
            <a href="#" class="btn-personal">Booking Information</a>
          </div>
            <!-- Default form login -->
<form class="text-center border border-light" action="#!">

    <p class="blue-text pers-detail">Complete your personal details (all fields required) or <a href="#">Login</a> </p>

    <!-- first name -->
    <input type="text" id="" class="form-control mb-4" placeholder="First Name">


    <!-- first name -->
    <input type="text" id="" class="form-control mb-4" placeholder="Surname">

    <!-- Password -->
    <input type="email" id="" class="form-control mb-4" placeholder="Email address. 
">

    <!-- Confirm Password -->
    <input type="text" id="" class="form-control mb-4" placeholder="Contact number">

    <input type="text" id="" class="form-control mb-4" placeholder="House name or number">
    <input type="text" id="" class="form-control mb-4" placeholder="Address line
">
 <input type="text" id="" class="form-control mb-4" placeholder="Town or City
">
<input type="text" id="" class="form-control mb-4" placeholder="Postcode
">
    <div class="d-flex justify-content-center">
        <div>
            <!-- Remember me -->
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="defaultLoginFormRemember">
                <label class="custom-control-label" for="defaultLoginFormRemember"> <strong>Please accept: </strong> <a href="#">Terms and conditions</a></label>

            </div>
        </div>
        
    </div>
<div class="md-d-flex justify-content-md-center">
     <!-- Sign in button -->
     <h3 class="number blue-text">2 of 2</h3>
    <button class="btn btn-primary btn-block my-4" type="submit">BOOK NOW</button>
   
</div>
   
   

</form>
<!-- Default form login -->
  <div>


</div>



          </div>  

          </div>

        </div>










      </div>
      
    </div>
  </div>
</div>













