@php
    $translation = $row->translateOrOrigin(app()->getLocale());
@endphp
<?php
//print_r($row);
    $reviewData = $row->getScoreReview();
    $score_total = $reviewData['score_total'];
    ?>
<style>.list-inline li {
    display: inline;
}

/*** 8-7-2020 ***/

@media screen and (min-device-width: 1920px) {
.list-scroll {
     height:unset !important; 
     overflow-y:unset !important; 
}

}

.bravo-list-item .col-sm-12.col-md-12.col-lg-12 {
     overflow-y: unset !important; 
     height:unset  !important;
}


/* Hide scrollbar for Chrome, Safari and Opera */
.has-search-map .bravo_search_map .results_item .listing_items::-webkit-scrollbar {
    display: none;
}

/* Hide scrollbar for IE, Edge and Firefox */
.has-search-map .bravo_search_map .results_item .listing_items {
  -ms-overflow-style: none;  /* IE and Edge */
  scrollbar-width: none;  /* Firefox */
  height:700px;
}

.has-search-map .bravo_search_map .results_item .text-paginate {
    margin-bottom: 15px;
}






</style>	
	<div class="row">
   <div class="listing-box">
  <div class="col-sm-6 col-md-6 col-lg-6">
   <!--Carousel Wrapper-->
<div id="carousel-example-1z{{$row->image_id}}" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
    <!--Indicators-->
    <ol class="carousel-indicators">
        <li data-target="#carousel-example-1z{{$row->image_id}}" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-1z{{$row->image_id}}" data-slide-to="1"></li>
      
    </ol>
    <!--/.Indicators-->
    <!--Slides-->
    <div class="carousel-inner" role="listbox">
        <!--First slide-->
		
		
        <div class="carousel-item active">         
           <a @if(!empty($blank)) target="_blank" @endif href="{{$row->getDetailUrl()}}">
            @if($row->image_url)
                @if(!empty($disable_lazyload))
                    <img src="{{$row->image_url}}" class="img-fluid" alt="">
                @else
                
                    {!! get_image_tag($row->image_id,'medium',['class'=>'img-fluid','alt'=>$row->title]) !!}
                @endif
                
                
                @else 
                <span><img src="{{ asset("listpagecss/images/no-image.jpg") }}"/></span>
            @endif
        </a>
        </div>
        <!--/First slide-->
        <!--Second slide-->
        <div class="carousel-item">
              <a @if(!empty($blank)) target="_blank" @endif href="{{$row->getDetailUrl()}}">
            @if($row->image_url)
                @if(!empty($disable_lazyload))
                    <img src="{{$row->image_url}}" class="img-fluid" alt="">
                @else
                    {!! get_image_tag($row->image_id,'medium',['class'=>'img-fluid','alt'=>$row->title]) !!}
                @endif
            @endif
        </a>
        </div>
        <!--/Second slide-->
        
    </div>
    <!--/.Slides-->
    <!--Controls-->
    <a class="carousel-control-prev" href="#carousel-example-1z{{$row->image_id}}" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carousel-example-1z{{$row->image_id}}" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    <!--/.Controls-->
</div>
<!--/.Carousel Wrapper--> 

  </div>
  <div class="col-sm-6 col-md-6 col-lg-6 pt-4">
      <div class="row">
        <div class="col-sm-6">
          <div class="therapist-detail">
                
				        <a @if(!empty($blank)) target="_blank" @endif href="{{$row->getDetailUrl()}}">
           <h4 class="blue-text">{{mb_strimwidth($translation->title,0,15,"...")}}</h4>
        </a>
				
                <address>
                 <p>
				 @if(!empty($row->location->name))
            @php $location =  $row->location->translateOrOrigin(app()->getLocale()) @endphp
            <i class="icofont-paper-plane"></i>
            {{$location->name ?? ''}}
        @endif</p>
        @if(request('lat') and request('lng'))
                 <p><i class="fa fa-paper-plane" aria-hidden="true"></i>{{round($row->distance_in_km)}} miles</p> 
               @endif
                </address>
             </div>
            	<?php
    $reviewData = $row->getScoreReview();
    $score_total = round($reviewData['score_total']);
    ?>
    <div class="service-review tour-review-{{$score_total}}">
        <div class="rating">
          @for( $i = 0 ; $i < 5 ; $i++ )
                                        @if($i < $reviewData['score_total'])
                                            <i class="fa fa-star"  aria-hidden="true"></i>
                                        @else
                                            <i class="fa fa-star-o"  aria-hidden="true"></i>
                                        @endif
                                    @endfor
        </div>
        <span class="review">
            @if($reviewData['total_review'] > 1)
                {{ __(":number Reviews",["number"=>$reviewData['total_review'] ]) }}
            @else
                {{ __(":number Review",["number"=>$reviewData['total_review'] ]) }}
            @endif
        </span>
    </div>
       

        </div>
		
		
	
		
        <div class="col-sm-6">
          <div class="therapist-price">
          <div class="price">{{ $row->display_price }} <p>per hour</p></div>
         
           <div class="view-profile">
             <a href="{{$row->getDetailUrl()}}" class="btn btn-primary">View profile</a>
             <a href="#" class="btn btn-primary book">Book</a>
           </div>

        </div>
        </div>


<div id="mapcardId" class="card mapcardId" style="width: 18rem;display :none" >
   <a @if(!empty($blank)) target="_blank" @endif href="{{$row->getDetailUrl()}}">
            @if($row->image_url)
                @if(!empty($disable_lazyload))
                    <img src="{{$row->image_url}}" class="card-img-top" alt="">
                @else
                    {!! get_image_tag($row->image_id,'medium',['class'=>'card-img-top','alt'=>$row->title]) !!}
                @endif
            @endif
        </a>
  <div class="card-body">
    <h5 class="card-title">{{mb_strimwidth($translation->title,0,15,"...")}}</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <div class="view-profile">
             <a href="{{$row->getDetailUrl()}}" class="btn btn-primary">View profile</a>
             <a href="#" class="btn btn-primary book">Book</a>
           </div>
  </div>
</div>


      </div>
</div>
     
</div>
  </div>
	
	
	
	