<div class="bravo_filter">
    <form action="{{url(app_get_locale(false,false,'/').config('tour.tour_route_prefix'))}}" class="bravo_form_filter">
        @if( !empty(Request::query('location_id')) )
            <input type="hidden" name="location_id" value="{{Request::query('location_id')}}">
        @endif
        @if( !empty(Request::query('start')) and !empty(Request::query('end')) )
            <input type="hidden" value="{{Request::query('start',date("d/m/Y",strtotime("today")))}}" name="start">
            <input type="hidden" value="{{Request::query('end',date("d/m/Y",strtotime("+1 day")))}}" name="end">
            <input type="hidden" name="date" value="{{Request::query('date')}}">
        @endif
        <input type="hidden" name="lat" value="{{Request::query('lat')}}">
        <input type="hidden" name="lng" value="{{Request::query('lng')}}">
        <input type="hidden" name="post_code" value="{{Request::query('post_code')}}">
        <div class="filter-title">
            {{__("FILTER BY")}}
        </div>
        @if(request('lat') and request('lng'))
            <div class="g-filter-item">
                <div class="item-title">
                    <h4>{{__("Filter Distance")}}</h4>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
                <div class="item-content">
                    <div class="bravo-filter-price">
                        <?php
                        // $price_min = $pri_from = $tour_min_max_price[0];
                        // $price_max = $pri_to = $tour_min_max_price[1];
                        if (!empty($price_range = Request::query('distance_range'))) {
                            $pri_from = explode(";", $price_range)[0];
                            $pri_to = explode(";", $price_range)[1];
                        }
                        $price_min = 0;
                        $price_max = 500;
                        $currency ='KM';// App\Currency::getCurrency(setting_item('currency_main'))
                        ?>
                        <input type="hidden" class="filter-price irs-hidden-input" name="distance_range"
                            data-symbol=" {{$currency['symbol'] ?? ''}}"
                            data-min="{{$price_min}}"
                            data-max="{{$price_max}}"
                            data-from="{{$pri_from ?? ''}}"
                            data-to="{{$pri_to ?? 500}}"
                            readonly="" value="{{$price_range}}">
                        <button type="submit" class="btn btn-link btn-apply-price-range">{{__("APPLY")}}</button>
                    </div>
                </div>
            </div>
        @endif
        @php
            $selected = (array) Request::query('terms');
        @endphp
        @foreach ($attributes as $item)
            @php
                $translate = $item->translateOrOrigin(app()->getLocale());
            @endphp
            <div class="g-filter-item">
                <div class="item-title">
                    <h4> {{$translate->name}} </h4>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </div>
                <div class="item-content">
                    <ul>
                        @foreach($item->terms as $key => $term)
                            @php $my_selected = $selected[$term->attr_id] ?? [] @endphp
                            @php $translate = $term->translateOrOrigin(app()->getLocale()); @endphp
                            <li @if($key > 2) class="hide" @endif>
                                <div class="bravo-checkbox">
                                    <label>
                                        <input @if(in_array($term->id,$my_selected)) checked @endif type="checkbox" name="terms[{{$term->attr_id}}][]" value="{{$term->id}}"> {!! $translate->name !!}
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    @if(count($item->terms) > 3)
                        <button type="button" class="btn btn-link btn-more-item">{{__("More")}} <i class="fa fa-caret-down"></i></button>
                    @endif
                </div>
            </div>
        @endforeach
    </form>
</div>


