@php
    $translation = $row->translateOrOrigin(app()->getLocale());
@endphp
<div class="item-tour  p-4 {{$wrap_class ?? ''}}  @if($row->is_featured == "1") is_featured @endif">
    @if($row->is_featured == "1")
        <div class="featured">
            {{__("Recommended")}}
        </div>
    @endif
    <div class="row">
        <div class="col-lg-3">
            <div class="thumb-image-flex">
                @if($row->discount_percent)
                    <div class="sale_info">{{$row->discount_percent}}</div>
                @endif
                <a @if(!empty($blank)) target="_blank" @endif href="{{$row->getDetailUrl()}}?distance_in_km={{$row->distance_in_km}}">

                    @if($row->image_url)
                        <div class="thumb-img-bg" style="background-image: url({{$row->image_url}})"></div>
{{--                        @if(!empty($disable_lazyload))--}}
{{--                            <img src="{{$row->image_url}}" class="img-fluid" alt="">--}}
{{--                        @else--}}
{{--                            {!! get_image_tag($row->image_id,'medium',['class'=>'img-fluid','alt'=>$row->title]) !!}--}}
{{--                        @endif--}}
                    @endif
                </a>
                <div class="service-wishlist d-none {{$row->isWishList()}}" data-id="{{$row->id}}" data-type="{{$row->type}}">
                    <i class="fa fa-heart"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            {{--        <div class="location">--}}
            {{--            @if(!empty($row->location->name))--}}
            {{--                @php $location =  $row->location->translateOrOrigin(app()->getLocale()) @endphp--}}
            {{--                <i class="icofont-paper-plane"></i>--}}
            {{--                {{$location->name ?? ''}}--}}
            {{--            @endif--}}
            {{--        </div>--}}
            <div class="item-title">
                <a @if(!empty($blank)) target="_blank" @endif href="{{$row->getDetailUrl()}}?distance_in_km={{$row->distance_in_km}}">
                    {{mb_strimwidth($translation->title,0,30,"...")}}
                </a>
            </div>
            @if($row->distance_in_km ?? '')
        <p style="padding:10px;"><i class="fa fa-map-marker"></i> {{__(':range miles away',['range'=>number_format($row->distance_in_km,2)])}}</p>
            @endif
            <div class="item-location">
                @if($row->city)
                    {{$row->city}},
                @endif
                @if($row->country)
                        {{$row->country}}
                @endif
            </div>
		    <?php
		    $reviewData = $row->getScoreReview();
		    $score_total = $reviewData['score_total'];
		    ?>
            <div class="service-review tour-review-{{$score_total}}">
                <div class="list-star">
                    <ul class="booking-item-rating-stars">
                        <li><i class="fa fa-star-o"></i></li>
                        <li><i class="fa fa-star-o"></i></li>
                        <li><i class="fa fa-star-o"></i></li>
                        <li><i class="fa fa-star-o"></i></li>
                        <li><i class="fa fa-star-o"></i></li>
                    </ul>
                    <div class="booking-item-rating-stars-active" style="width: {{  $score_total * 2 * 10 ?? 0  }}%">
                        <ul class="booking-item-rating-stars">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                        </ul>
                    </div>
                </div>
                <span class="review">
            @if($reviewData['total_review'] > 1)
                        {{ __(":number Reviews",["number"=>$reviewData['total_review'] ]) }}
                    @else
                        {{ __(":number Review",["number"=>$reviewData['total_review'] ]) }}
                    @endif
        </span>
            </div>
            @if(!empty($row->tour_term_detail))
                <div class="service-attr pb-0">
                       <?php $stt=0; ?>
                        @foreach($row->tour_term_detail as $item=>$value)
                            @if($stt < 6 )
                                <a href="#"><i class="field-icon icofont-check"></i> {{$value->name}}</a>
                            @endif
                            <?php $stt++ ?>
                        @endforeach
                </div>
            @endif
        </div>
        <div class="col-lg-3">
            <div class="info border rounded p-4 mb-3">
                <div class="g-price">
                    <div class="price">
                        <span class="onsale">{{ $row->display_sale_price }}</span>
                        <span class="text-price">{{ $row->display_price }}</span>
                    </div>
                </div>
                <div class="duration">
                    {{__("per hour")}}
                </div>
            </div>
            <div class="book-now">
                <a @if(!empty($blank)) target="_blank" @endif href="{{$row->getDetailUrl()}}?distance_in_km={{$row->distance_in_km}}" class="btn btn-large  btn-info-booking" >{{__('BOOK NOW')}}</a>
            </div>
        </div>
    </div>
</div>
