<!DOCTYPE html>
<html lang="en">
<head>
  <title>Take a Sheet</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{ asset("landingpage/css/bootstrap.min.css") }}">
  <link rel="stylesheet" href="{{ asset("landingpage/css/landing.css") }}"> 
  <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style>
/*** 17-07-2020 ***/
.seat-pledge {
  background:#e7e7e9;
  padding:50px 0;
  width:100%;
  float:left;
  border-top:8px solid #fac93e;
  border-bottom:8px solid #fac93e;
}

.seat-pledge p {
  color: #000;
  font-weight: 500;
  font-size: 17px;
}

.faq2{ 
  padding:80px 0;
  width:100%;
  float:left;
  clear:both;
  background:#fff; 
}
.grow-text {
padding-right: 30px;
}

.grow-text p {
   color: #000;
   font-weight: 500;
   font-size: 17px;
}

.faq2 p {
  color: #000;
  font-weight: 500;
  font-size: 17px;
}


.member-btn a {
    border-radius: 10px;
    background-color: #11b7eb;
    border-color: #11b7eb;
    padding:15px 30px;
    font-weight: 600;
    outline: none;
    color: #fff;
    text-decoration: none;
    font-size: 20px;
}

.tell-more {
  float:left;
  width:100%;
  background:#fff;
}

.footer {
  float:left;
  width:100%;
}

@media screen and (max-width: 767px) {
  .faq2 {
    padding-bottom:0;
}
}

@media screen and (min-width: 1441px) {
 .hero-banner {
    background-size:100% 100%;
}
}



.tell-text {
   width: 63% !important;  
}
.new-client-item h4 {  
   font-size: 1.3rem;
}


  </style>
</head>
<body>

  <section class="top-bar">
   <div class="container">
     <div class="row">
      <div class="col-6">
       <div class="logo">
        <a href="http://websitedemoonline.com/">
        <img src="{{ asset("landingpage/images/Take_A_Seat_Logo.png") }}" alt="logo" class="img-fluid"> </a>         
      </div><!-- end-->
    </div>

    <div class="col-6">
     <div class="register-btn d-flex d-flex justify-content-end">
       <a href="http://websitedemoonline.com/register">Register today! </a>                  
     </div><!-- end-->
   </div>    

 </div>
</div>
</section><!--end top-bar-->

<section class="hero-banner">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-lg-6">
        <div class="hero-content">
         <h2> Join our community </h2>
         <p class="font-weight-bold">Here at Take A Seat we understand how stressful your client lives
can be. They may think they're on their own and nobody can help
them, which is why we are building a best in class community
who are on hand to help at their time of need.</p>

<p class="font-weight-bold">We look forward to having you as part of our community and
helping as many people to feeling like themselves again.</p>
        @if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
    </div>
@endif
        <!--<form action="{{url(app_get_locale()."/addnewsletter")}}" method="POST">
             @csrf
        <div class="input-group">
          <input type="email" required class="form-control" name="email" placeholder="Enter your email address
          " aria-label="Recipient's username" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit">Submit</button>

          </div>

        </div>
</form>-->

      </div>
    </div>

  </div>
</div>
</section><!-- end-->

<section class="new-client">
  <div class="container">
    <div class="row">
     <div class="col-sm-12 col-md-4">
      <div class="new-client-item d-md-flex">
        <img src="{{ asset("landingpage/images/new-client-icon.png") }}" alt="new client">
        <h4>New Client <br> Referrals</h4>
      </div>
    </div>
    <div class="col-sm-12 col-md-4">
     <div class="new-client-item d-md-flex">
      <img src="{{ asset("landingpage/images/human-support-icon.png") }}" alt="human support">
      <h4>First 250 Members
are free for 
the first year </h4>
    </div>
  </div>
  <div class="col-sm-12 col-md-4">
    <div class="new-client-item d-md-flex">
      <img src="{{ asset("landingpage/images/booking-icon.png") }}" alt="booking">
      <h4>Immediate bookings and payments</h4>
    </div>
  </div>
</div>
</div>
</section><!-- new client-->

<section class="why-choose">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="text-center mb-5 blue-text">Why choose take a seat?</h2>
        <p>Here at take a seat we understand how stressful your clients lifes can be. They may think they're on their own
          and no one can help them. At times it can feel like the end of the world, and at the very least makes everyday
        things more difficult. Most of all it can affect how they are as a person; when something demands all their attention it’s hard to behave the way they always have with the people that they care most about.</p>

        <p>That’s why at take a seat we have a process that will help them tackle their worries and work towards getting
          them back to them again. We want to help them to work proactively with you, so they get to a point where
          they can focus on the things that really matter in their lives; on their future, not just their current state of
        mind.</p>

        <p>We appreciate making their first booking with you is only the first step. We’ll help them on every step of their
          journey to make sure that they are getting all the help, support and advice they need until together we get
        them back to feeling like You/Them Again. </p>
      </div>
    </div>
  </div>
</section><!-- end-->


<section class="membership">
  <div class="container">
    <div class="row">
      <div class="col-12">
       <div class="text-center">
        <img src="{{ asset("landingpage/images/Membership_Icon.png") }}" class="img-fluid">
        <h2 class="blue-text my-4 pb-3">Membership </h2>
      </div>
    </div>    
  </div>


  <!--<div class="row grow-business">
   <div class="most-popular"></div>
   <div class="col-md-4">
    <div class="business-card text-center">
      <img src="{{ asset("landingpage/images/Grow_You_Your_Business_Icon.png") }}" alt="grow your business" class="img-fluid">
      <div class="stars my-3">
        <i class="fa fa-star" aria-hidden="true"></i>
        <i class="fa fa-star" aria-hidden="true"></i>
        <i class="fa fa-star" aria-hidden="true"></i>
        <i class="fa fa-star" aria-hidden="true"></i>
        <i class="fa fa-star" aria-hidden="true"></i>
      </div>

      <div class="register my-5">
        <a href="#">Register</a>
      </div>




    </div>
  </div>
  <div class="col-md-8">
   <div class="grow-text">
     <h2 class="font-weight-bold">Grow You & Your Business</h2>
     <h5 class="my-4 pink-text font-weight-bold">Partner membership is FREE for the first 250 registrations
     and gets you access to the following:</h5>
     <ul>
      <li> - New client refferals </li>
      <li> - Online booking and payments </li>
      <li> - Client facing profile page </li>
      <li> - Dedicated support from a take a seat representitive </li>
      <li> - Access to corporate client work </li>
      <li> - Ability to offer video sessions  </li>
      <li> - Podcasts  </li>
      <li> - Plus full access to community membership   </li>
    </ul>
  </div>
</div>

</div>-->






<div class="row grow-business">

  <div class="col-md-4">
    <div class="business-card text-center">
      <img src="{{ asset("landingpage/images/Grow_You_Icon.png") }}" alt="grow your business" class="img-fluid">
      <div class="stars my-3">
        <i class="fa fa-star" aria-hidden="true"></i>
        <i class="fa fa-star" aria-hidden="true"></i>
        <i class="fa fa-star" aria-hidden="true"></i>
        <i class="fa fa-star" aria-hidden="true"></i>
        <i class="fa fa-star" aria-hidden="true"></i>

      </div>

      <div class="register my-5">
        <a href="#">Become a member</a>
      </div>

    </div>
  </div>
  <div class="col-md-8">
   <div class="grow-text">
     <h2 class="font-weight-bold">Founder Member</h2>
     <p> As a founding member of the Take A Seat community,
not only will you gain access to all member benefits of client
referrals, online booking and payments as well as your own
profile page, you will also get exclusive early acess to our
community portal when its released later this year.</p>

<ul>
 <li>Our plans for the community portal include:</li>
 <li>- CPD training and course</li>
 <li>- local and national events </li>
 <li>- share and contribute to papers </li>
 <li>- discuss all the latest topics in a community forum </li>
</ul>
<p>But what else would you like to see and do to make you and
your practice the best it can be? This is your opportunity to be
part of something fresh and new for the therapist community.</p>
<p>The first 250 members will get all the above for free for the
first year, to help us build a best in class therapist community. </p>
  </div>
</div>

</div>


</div>
</section><!--end-->


<!-- start seat-pledge-->
<section class="seat-pledge">
  <div class="container">
    <div class="row">
      <div class="col">
           <div class="pledge-content text-center">
             <img src="{{ asset("landingpage/images/pledge_Icon.png") }}" class="mb-4">
             <h2 class="blue-text mb-5">Take A Seat Pledge</h2>
             <p class="w-75 mx-auto">Here at Take A Seat we want to give something back, so we’re asking every therapist who becomes a member to
give 1 hour per week free of charge. We will then work with registered mental health charities to provide
thousands of hours of free mental health services per week free of charge.</p>
           </div>

           <p class="w-75 mx-auto text-center">Ambitious we know but achievable and we intend to deliver.</p>
           </div>
      </div>
    </div>
  </div>
</section><!--end-->

<section class="faq2">
  <div class="container">
    <div class="row">
      <div class="col">
       <div class="faq-item">
         <h2 class="blue-text text-center mb-5">FAQ’s</h2>
         <h5 class="blue-text font-weight-bold">Why should I trust Take A Seat? </h5>
         <p>Here at Take A Seat we are passionate about people having positive state of mind, which is why we are
dedicated to matching the right clients to the right therapists. Our focused digital marketing activity means you
will only receive bookings for those people looking for support. At no point will we buy or sell third party data,
we are only interested in those people looking for help.</p>

<h5 class="blue-text font-weight-bold">What's the cost? </h5>
         <p>The first 250 therapists who sign-up will get their first years subscription for free, after which there will be a £10
a month membership fee. At point of booking Clients will be charged a 10% booking fee that will be added onto
their session charge.</p>

<h5 class="blue-text font-weight-bold">I already have too many clients </h5>
         <p class="mb-0">That's great, but if you have too many clients, you still have options:</p>
         <div class="many-clients">
         <p class="mb-0">1. Pass any clients you can't help on to us and we can match them with one of our approved therapists.</p>
          <p class="mb-0">2. Still sign-up as a member so when you don't have capacity you are ready to receive new clients.</p>
         <p>3. Sign up as a community member and gain access to events, CPD and lots, lots more.</p>
       </div>

      <h5 class="blue-text font-weight-bold">I've already spent my marketing budget for the year </h5>
         <p>Don't worry, the first 250 therapists who sign-up to a full membership are free for the first year and even if you
miss out it only cost £10 a month to start receiving new clients.</p>

       </div>
      </div>
    </div>
  </div>
</section>



<section class="tell-more">
  <div class="container">
    <div class="row tell-text">
      <div class="col-12">
         <h2 class="text-center blue-text font-weight-bold">Sign me up</h2>
         <p class="text-center">Be one of the first 250 full members to get free access to new client referrals,
online booking payment system and a dedicated Take A Seat customer champion. 
</p>
  
   <div class="member-btn d-flex justify-content-center">
     <a href="#">Become a member</a>
    </div>
  </div>
</div>

   <div class="row">
     <div class="col-12">
         <div class="social text-center">
           <h2 class="font-weight-bold mt-5"> Social </h2>
            <div class="social-link mt-4">
              <a href="#"><img src="{{ asset("landingpage/images/YoutubeIcon.png") }}" alt="youtube"></a>
              <a href="#"><img src="{{ asset("landingpage/images/TwitterIcon.png") }}" alt="twitter"></a>     
              <a href="#"><img src="{{ asset("landingpage/images/FacebookIcon.png") }}" alt="facebook"></a>
            </div>
         </div><!--end-->
     </div>
   </div>



  </div>
</section><!-- end -->

<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-12">
          <div class="footer-item text-center">
            <a href="http://websitedemoonline.com">
            <img src="{{ asset("landingpage/images/Take_A_Seat_Logo.png") }}" alt="logo" class="img-fluid">
          </a>
            <p class="my-4">Email us <a href="mailto:support@takeaseat.co.uk">support@takeaseat.co.uk</a></p>

            <p class="copyright">Copyright © 2020 by Take a Seat</p>


          </div>
          


      </div>
    </div>
  </div>
</footer><!-- end -->










<script src="{{ asset("landingpage/js/jquery.min.js") }}"></script> 
<script src="{{ asset("landingpage/js/bootstrap.min.js") }}"></script>
<script>
  $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.show").each(function(){
          $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
        });
        
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
          $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
        }).on('hide.bs.collapse', function(){
          $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
        });
      });
    </script>




  </body>
  </html>

