<?php
use Custom\Core\Models\Terms;
;?>

@php $lang_local = app()->getLocale() @endphp
<div class="booking-review">
    <h4 class="booking-review-title">{{__("Your Booking")}}</h4>
    <div class="booking-review-content">
        <div class="review-section">
            <div class="service-info">
                <div>
                    @php
                        $service_translation = $service->translateOrOrigin($lang_local);
                    @endphp
                    <h3 class="service-name"><a href="{{$service->getDetailUrl()}}">{{$service_translation->title}}</a></h3>
                    @if($service_translation->address)
                        <p class="address"><i class="fa fa-map-marker"></i>
                            @if($service->city)
                                {{$service->city}},
                            @endif
                            @if($service->country)
                                {{$service->country}}
                            @endif
                        </p>
                    @endif
                </div>
                {{--<div>
                    @if($image_url = $service->getImageUrl())
                        <img src="{{$image_url}}" alt="{{$service->title}}">
                    @endif
                </div>--}}
            </div>
        </div>
        <div class="review-section">
            <ul class="review-list">
                 <?php $tmp_dates = $booking->getJsonMeta('tmp_dates'); 
                    if(!empty($tmp_dates)){
                   
                    ?>
                    <li>
                        <div>
                            <div class="font-weight-bold">Appointment Dates:</div>
                        </div>
                    </li>
            <?php foreach($tmp_dates as $info){
            
            
            ?>
                    <?php foreach(array_sort($info['date']) as $key=>$date) {
                     ?>
                     <li> <div class="label"> - <?php echo date('d/m/Y',strtotime($date)) ?>:
                  <?php   foreach(array_sort($info['slots']) as $slot) {
                      //print_r($slot);
                    ?>
             
                        <span class="badge active slot-active"><?php echo $slot[$key]?></span>
                   </div> 
                   </li>
                   <?php }}?>
                
            <?php }
        }
        ?>
                
               
                @php $person_types = $booking->getJsonMeta('person_types')@endphp
                @if(!empty($person_types))
                    @foreach($person_types as $type)
                        <li>
                            <div class="label">{{$type['name_'.$lang_local] ?? $type['name']}}:</div>
                            <div class="val">
                                {{$type['number']}}
                            </div>
                        </li>
                    @endforeach
                @else
                    @if(!empty($booking->total_guests))
                        <li>
                            <div class="label">{{__("Guests")}}:</div>
                            <div class="val">
                                {{$booking->total_guests}}
                            </div>
                        </li>
                    @endif
                @endif
                @if(!empty(setting_item('tour_attr_book_form_detail')) and $booking->getMeta('term_id'))
                    <?php
		                $term_meta = $booking->getMeta('term_id');
		                if(!empty($term_meta)){
		                	$term = Terms::find($term_meta);
                        }
		                ;?>
                        <li>
                            <div>
                                <p class="font-weight-bold">{{@$term->attr->name}}:</p>
                                <div>{{@$term->name}}</div>
                            </div>
                        </li>
                @endif
            </ul>
        </div>
        {{--@include('Booking::frontend/booking/checkout-coupon')--}}
        <div class="review-section total-review">
            <ul class="review-list">
                @php $person_types = $booking->getJsonMeta('person_types') @endphp
                @if(!empty($person_types))
                    @foreach($person_types as $type)
                        <li>
                            <div class="label">{{ $type['name_'.$lang_local] ?? $type['name']}}: {{$type['number']}} * {{format_money($type['price'])}}</div>
                            <div class="val">
                                {{format_money($type['price'] * $type['number'])}}
                            </div>
                        </li>
                    @endforeach
                @else
                    @if(!empty($booking->total_guests))
                        <li>
                            <div class="label">{{__("Guests")}}: {{$booking->total_guests}} * {{format_money($booking->getMeta('base_price'))}}</div>
                            <div class="val">
                                {{format_money($booking->getMeta('base_price') * $booking->total_guests)}}
                            </div>
                        </li>
                    @endif
                @endif

                {{--@if(!empty($booking->slots))--}}
                    {{--<li>--}}
                        {{--<div class="label">{{__("Slots")}}: {{count($booking->slots)}} * {{format_money($booking->getMeta('base_price'))}}</div>--}}
                        {{--<div class="val">--}}
                            {{--{{format_money($booking->getMeta('base_price') * count($booking->slots))}}--}}
                        {{--</div>--}}
                    {{--</li>--}}
                {{--@endif--}}

                <li>
                    <div class="label">{{__("Appointment")}}: </div>
                    <div class="val">
                        {{format_money($booking->total_before_fees)}}
                    </div>
                </li>

                @php $extra_price = $booking->getJsonMeta('extra_price') @endphp
                @if(!empty($extra_price))
                    <li>
                        <div class="label-title"><strong>{{__("Extra Prices:")}}</strong></div>
                    </li>
                    <li class="no-flex">
                        <ul>
                            @foreach($extra_price as $type)
                                <li>
                                    <div class="label">{{$type['name_'.$lang_local] ?? $type['name']}}:</div>
                                    <div class="val">
                                        {{format_money($type['total'] ?? 0)}}
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endif
                @php $discount_by_people = $booking->getJsonMeta('discount_by_people')@endphp
                @if(!empty($discount_by_people))
                    <li>
                        <div class="label-title"><strong>{{__("Discounts:")}}</strong></div>
                    </li>
                    <li class="no-flex">
                        <ul>
                            @foreach($discount_by_people as $type)
                                <li>
                                    <div class="label">
                                        @if(!$type['to'])
                                            {{__('from :from guests',['from'=>$type['from']])}}
                                        @else
                                            {{__(':from - :to guests',['from'=>$type['from'],'to'=>$type['to']])}}
                                        @endif
                                        :
                                    </div>
                                    <div class="val">
                                        - {{format_money($type['total'] ?? 0)}}
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endif
                @if(!empty($booking->buyer_fees))
					<?php
					$buyer_fees = json_decode($booking->buyer_fees, true);
					foreach ($buyer_fees as $buyer_fee){
					?>
                    <li>
                        <div class="label">
                            {{$buyer_fee['name_'.$lang_local] ?? $buyer_fee['name']}}
                            <i class="icofont-info-circle" data-toggle="tooltip" data-placement="top" title="{{ $buyer_fee['desc_'.$lang_local] ?? $buyer_fee['desc'] }}"></i>
                            @if(!empty($buyer_fee['per_person']) and $buyer_fee['per_person'] == "on")
                                : {{$booking->total_guests}} * {{format_money( $buyer_fee['price'] )}}
                            @endif
                        </div>
                        <div class="val">
                            @if(!empty($buyer_fee['per_person']) and $buyer_fee['per_person'] == "on")
                                {{ format_money( $buyer_fee['price'] * $booking->total_guests ) }}
                            @else
                                {{ format_money($buyer_fee['price']) }}
                            @endif
                        </div>
                    </li>
					<?php } ?>
                @endif
                @if(!empty($booking->first_booking))
                    <li>
                        <div class="label">
                            {{__('First Booking')}}
                        </div>
                        <div class="val">
                           -50%
                        </div>
                    </li>
                @endif
                @if($booking->getMeta('discount_for_10_slot'))
                    <li>
                        <div class="label">
                            {{__('Book more than 10 slots')}}
                        </div>
                        <div class="val">
                            -10%
                        </div>
                    </li>
                @endif
                @if($booking->getMeta('discount_for_4_slot'))
                    <li>
                        <div class="label">
                            {{__('Book more than 4 slots')}}
                        </div>
                        <div class="val">
                            -5%
                        </div>
                    </li>
                @endif
                <li class="final-total">
                    <div class="label">{{__("Total:")}}</div>
                    <div class="val">{{format_money($booking->total)}}</div>
                </li>
            </ul>
        </div>
    </div>
</div>
