<tr>
    <td class="booking-history-type d-none">
        <i class="fa fa-bolt"></i>
        <small>{{$booking->object_model}}</small>
    </td>
    <td>
        @if($service = $booking->service)
            @php
                $translation = $service->translateOrOrigin(app()->getLocale());
            @endphp
            <a target="_blank" href="{{$service->getDetailUrl()}}">
                {{$translation->title}}
            </a>
        @else
            {{__("[Deleted]")}}
        @endif
    </td>
    <td class="a-hidden">{{display_date($booking->created_at)}}</td>
    <td class="a-hidden">
        
        <?php $tmp_dates = $booking->getJsonMeta('tmp_dates'); 
        if(!empty($tmp_dates)){
       
        ?>
            <div class="font-weight-bold">Appointment Dates:</div>
            <?php foreach($tmp_dates as $info){
            
            
            ?>
                    <?php foreach(array_sort($info['date']) as $key=>$date) {
                     ?>
                     <div class="label"> - <?php echo date('d/m/Y',strtotime($date)) ?>:
                  <?php   foreach(array_sort($info['slots']) as $slot) {
                     // print_r($slot);
                    ?>
             
                        <span class="badge badge-success active slot-active"><?php echo $slot[$key]?></span>
                   </div> 
                   <?php }}?>
                
            <?php }
        }
        ?>
       
    </td>
    <td>{{format_money($booking->total)}}</td>
    <td class="{{$booking->status}} a-hidden">{{$booking->statusName}}</td>
    <td width="2%">
        @if($service = $booking->service)
            <a class="btn btn-xs btn-primary btn-info-booking" data-toggle="modal" data-target="#modal-booking-{{$booking->id}}">
                <i class="fa fa-info-circle"></i>{{__("Details")}}
            </a>
            @include ($service->checkout_booking_detail_modal_file ?? '')
        @endif
            <a href="{{route('user.booking.invoice',['code'=>$booking->code])}}" class="btn btn-xs btn-primary btn-info-booking open-new-window" onclick="window.open(this.href); return false;"  style="margin-top: 10px;">
                <i class="fa fa-print"></i>{{__("Invoice")}}
            </a>
    </td>
</tr>
