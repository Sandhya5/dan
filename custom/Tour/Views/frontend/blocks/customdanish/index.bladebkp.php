<?php //$tours = DB::table('bravo_tours')->distinct()->get();

$tours = DB::table('core_news')->orderBy('id','DESC')->take(8)->get();
//echo '<pre>';print_r($tours); 
//echo '</pre>';

?>  
   <section class="hero-section" style="
   
    min-height: 368px;
    padding-bottom: 15px;">
     <div class="container">
  
<form action="<?php  echo URL::to('/tour?tour=map');?>"  method="get">  
     
<div class="main-slider">
    <div class="carousel slide" id="main-carousel" data-ride="carousel" data-interval="false">
      <ol class="carousel-indicators">       
        <li data-target="#main-carousel" data-slide-to="0" class="active"></li>
        <li data-target="#main-carousel" data-slide-to="1"></li>
        <li data-target="#main-carousel" data-slide-to="2"></li>

       
      </ol><!-- /.carousel-indicators -->
      
      <div class="carousel-inner">
        <div class="carousel-item active">          
          <div class="carousel-caption d-md-block"	>
           <div class="coach">
    <h1>We'll help you find a local
    therapist, counsellor or coach </h1>   

   <div class="postcode">
<form action="#"  class="form bravo_form" method="get">
    <div class="input-group mb-3">
	<input type="hidden" name="lat" id="lat" value="">
    <input type="hidden" name="lng" id="lng" value="">
      <input type="text" class="post_code input-search-google form-control pac-target-input" name="post_code" autocomplete="off" id="post_code" placeholder="Enter your Location" aria-label="Recipient's username" aria-describedby="basic-addon2" value="">
      <div class="input-group-append">
        <button class="btn btn-outline-secondary" id="tourSubmit"  type="button">Go</button>

      </div>

    </div>
    <span>United Kingdom</span>    <h3 class="text-white take-seat">Browse > Book > Take a seat...</h3>

    <div class="find-btn">
      <a href="#"><i class="fa fa-angle-down"></i>
      </a>
      <p>Find out more</p>       
    </div> 
   
  </div>


</div>
          </div>
        </div>


        <div class="carousel-item">        
          <div class="carousel-caption d-md-block">
            <div class="therapy-cat">
             <h2>Select Key issues that you would like help with...</h2>
             <p>Select a categorty or choose as many keywords that are relevant to you</p>


             <select class="form-control" id="thCat">

				 <option value="" >Therapy Category</option>
                @foreach($rows as $row)
                  <option value="{{ $row->id }}" >{{ $row->name }}</option>
               
				@endforeach
             </select>

             <div class="tag">
			 <input type="hidden" id="colorvalues" name="colorvalues" value="" >
               <ul id="ulbox2"class="art-vmenu">
			
</ul>
             </div>
             
            <div class="start-search">
 <button type="button" id="findTherapist" class="btn btn-primary find-therapist">Find Your Therapist</button>
</div>

           </div>
        </div>
      </div>

        <div class="carousel-item">          
          <div class="carousel-caption d-md-block">
            <h3>Mountain</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci esse vitae exercitationem fugit, numquam minus!</p>
          </div>
        </div>
       
      </div><!-- /.carousel-inner -->
      
     <!--  <a href="#main-carousel" class="carousel-control-prev" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
        <span class="sr-only" aria-hidden="true">Prev</span>
      </a>
      <a href="#main-carousel" class="carousel-control-next" data-slide="next">
        <span class="carousel-control-next-icon"></span>
        <span class="sr-only" aria-hidden="true">Next</span>
      </a> -->
    </div><!-- /.carousel -->
 

</div>

</form>


</div>
</section>
<!-- end banner-section -->


<!-- welcome seat -->
<div class="welcome">
  <div class="container">
    <div class="row">
      <div class="welcome-text clearfix">
        <h3>Welcome to take a seat</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in </p>

        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in </p>
      </div>

      <div class="divider mt-2 mb-5">
        <img src="{{ asset('custom/assets/images/Separator.jpg') }}">
      </div>
    </div>

  </div>

  <div class="container-fluid">

    <div class="row therapist-row text-center">
     <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
      <div class="therapist theme-gradient theme-gradient-1">          
        <div class="logo-icon"></div>
        <h3 class="text-white mt-5 mb-3">Hundreds of qualified therapists at the click of a button.</h3>
        <h3 class="text-white mt-3">Star your journey.</h3>
      </div>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
     <div class="therapist-photo">
      <img src="{{ asset('custom/assets/images/Photo_Woman.jpg') }}" alt="woman" class="img-fluid">        
    </div>
  </div>

  <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
   <div class="therapist-photo-alt">
    <img src="{{ asset('custom/assets/images/Photo_Man.jpg') }}" alt="woman" class="img-fluid">        
  </div>
</div>

<div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
  <div class="therapist-alt theme-gradient">
    <img src="{{ asset('custom/assets/images/LogoIcon_White.png') }}" alt="logo icon">
    <div id="demo" class="carousel slide therapist-slide carousel-fade" data-ride="carousel">
      <ul class="carousel-indicators">
        <li data-target="#demo" data-slide-to="0" class="active"></li>
        <li data-target="#demo" data-slide-to="1"></li>
        <li data-target="#demo" data-slide-to="2"></li>
      </ul>
      <div class="carousel-inner">
        <div class="carousel-item active">

          <div class="carousel-caption">
            <h3> Take a seat</h3>       
          </div>   
        </div>
        <div class="carousel-item">

          <div class="carousel-caption">
            <h3>How it works for you</h3>       
          </div>   
        </div>
        <div class="carousel-item">

          <div class="carousel-caption">
            <h3>browse our therapists</h3>       
          </div>   
        </div>
      </div>

    </div>
  </div>
</div>

</div>
</div>

<div class="start-search">
 <a href="#" class="btn btn-primary">Start your search</a>
</div>

</div>

<!-- end welcome -->

<section class="resource-center">
  <div class="container">
    <img src="{{ asset('custom/assets/images/LogoIcon_Colour.png') }}">
    <h3>Our Resources Centre</h3>
    <div class="row">
	@foreach($tours as $row)
      <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
        <div class="card">
          <div class="resourcebgimg"> {!! get_image_tag($row->image_id,'medium',['class'=>'img-fluid','alt'=>$row->title]) !!}</div>
          <div class="card-text">
           <h5 class="text-white rphead"><a class="text-white" href="news/<?php echo $row->slug; ?>">{{mb_strimwidth($row->title,0,30,"...")}}</a></h5>
           <p class="text-white rpcontent">{{mb_strimwidth(strip_tags($row->content),0,130,"...")}} </p>

           <a href="news/<?php echo $row->slug; ?>" class="read-more  text-white"> Read > </a>

         </div>
       </div>
     </div>
	 @endforeach
      

   </div>

   <div class="viewall">
   <a href="http://websitedemoonline.com/news" class="btn btn-primary">View all</a>
  </div>

 


 </div>
</section>
<!-- end resource-center -->

<div class="social-section">
  <div class="container">
    <div class="row d-flex justify-content-center">      
      <a href="#"> <img src="{{ asset('custom/assets/images/YoutubeIcon.png') }}"></a>
      <a href="#"> <img src="{{ asset('custom/assets/images/TwitterIcon.png') }}"></a>
      <a href="#"> <img src="{{ asset('custom/assets/images/FacebookIcon.png') }}"></a>
    </div>
  </div>
</div>
<!-- end social-section -->


