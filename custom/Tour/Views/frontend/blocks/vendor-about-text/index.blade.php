<div class="vendor-about-text">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="title">
                    {!! $title !!}
                </div>
                <div class="desc">
                    {!! $content !!}
                </div>
                <span class="go_to_faqs">{{ __("View FAQ's") }}</span>
            </div>
            <div class="col-lg-6">
                <img class="img-responsive" src="{{ get_file_url($image , 'full') }}"  alt="{{$title}}">
            </div>
        </div>
    </div>
</div>