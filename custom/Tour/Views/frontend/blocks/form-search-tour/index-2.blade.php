<div class="bravo-header-custom">
    <div class="bravo_header">
        <div class="container">
            <div class="content">
                <div class="header-left">
                    <a href="{{url(app_get_locale(false,'/'))}}" class="bravo-logo">
                        @if($logo_id = setting_item("logo_id"))
                            <?php $logo = get_file_url($logo_id,'full') ?>
                            <img src="{{$logo}}" alt="{{setting_item("site_title")}}">
                        @endif
                    </a>
                </div>
                <div class="header-right">
                    @if(!Auth::id())
                        <a href="#login" data-toggle="modal" data-target="#login" class="btn btn-professional">{{__('Login')}}</a>
                    @else
                        <a class="btn btn-professional" href="{{url(app_get_locale().'/user/calendar')}}">
                            {{__("Hi, :Name",['name'=>Auth::user()->getDisplayName()])}}
                        </a>
                    @endif
                    <?php $page_vendor = setting_item("custom_vendor_page_id") ? get_page_url(setting_item("custom_vendor_page_id")) : "";  ?>
                    <a href="{{$page_vendor}}" class="btn btn-professional">
                        {{__("Join as a Professional")}}
                    </a>
                    <a href="{{ url("/contact") }}"  class="btn btn-professional">{{__('Support')}}</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bravo-form-search-tour-2" style="background-image: url('{{$bg_image_url}}') !important">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="g-form-control">
                    <h1 class="text-heading">{!! $title !!}</h1>
                    <div class="sub-heading">{{$sub_title}}</div>

                </div>
            </div>
        </div>
        <div class="g-form-book">
            <div class="title">
                {{__("Enter your postcode to find local professionals")}}
            </div>
            @include('Tour::frontend.layouts.search.form-search-2')
        </div>
    </div>
</div>
<div class="bravo-modal-ver2 bravo_form_book_select">
    <div class="bravo-modal-content">
        <div class="context">
            <div class="title">
                {{__("What do you hope to achieve?")}}
            </div>
            <div class="list-item">
                @php $custom_attribute_page_intro = setting_item("custom_attribute_page_intro") @endphp
                @if(!empty($custom_attribute_page_intro))
                    <?php $custom_attribute_page_intro =  json_decode($custom_attribute_page_intro,true) ?>
                    @foreach($custom_attribute_page_intro as $item)
                        <div class="col">
                            <div class="item" data-term="{{$item['title']}}">
                                <div class="img">
                                    <img src="{{ get_file_url($item['image_id']) }}">
                                </div>
                                <div class="text">
                                    {{$item['title']}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="title2">
                {{__("Or alternatively use the search bar below")}}
            </div>
            <div class="g-form-book">
                @include('Tour::frontend.layouts.search.form-search')
            </div>
        </div>
        <div class="bravo-effect"></div>
    </div>

</div>
<style type="text/css">
    .bravo_wrap .bravo_header {
        background: transparent;
        position: relative;
        z-index: 199;
    }
    .bravo_wrap .bravo_header .content {
        background: transparent;
        margin: 0 -80px;
    }
    @media(max-width: 1120px){
        .bravo_wrap .bravo_header .content {
            margin: 0;
        }
    }
    .bravo_wrap .bravo_header .content .header-right .btn-professional {
        background: #2c2e36;
    }
    .bravo_wrap .bravo_footer .custom-form-search {
        background: #fff;
    }
    .bravo_wrap .bravo_footer .custom-form-search .bravo_form .g-button-submit button {
        width: 101%;
    }
    @media(max-width: 1023px){
        .bravo_wrap .bravo_footer .custom-form-search .bravo_form .g-button-submit button {
            width: auto;
        }
    }
    .bravo_wrap .bravo_topbar {
        display: none;
    }
    .bravo_wrap > .bravo_header {
        display: none;
    }
    @media(max-width: 768px){
        .bravo_wrap > .bravo_header {
            display: block;
        }
    }
</style>