<div class="why-use">
    <div class="container">
        <div class="title">
            {{$title}}
        </div>
        <div class="list-item">
            @if(!empty($list_item))
                @foreach($list_item as $key=>$row)
                    <div class="item item-{{$key+1}}">
                        <div class="left">
                            <div class="text-1">
                                {{$row['title']}}
                            </div>
                            <div class="text-2">
                                {!!  $row['sub_title'] !!}
                            </div>
                        </div>
                        <div class="right">
                            @if($row['icon_image'])
                                <img src="{{ get_file_url($row['icon_image'] , 'full') }}"  alt="{{$row['title']}}">
                            @endif
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>