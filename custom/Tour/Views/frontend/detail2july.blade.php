@extends('layouts.app')
@section('head')
    <link href="{{ asset('module/tour/css/tour.css?_ver='.config('app.version')) }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset("libs/ion_rangeslider/css/ion.rangeSlider.min.css") }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset("libs/fotorama/fotorama.css") }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset("listpagecss/css/booking_css.css") }}"/>

    <style type="text/css">
        .gmnoprint,.gm-control-active{
            display: none;
        }
        .bravo_banner_map:before{
            content: "";
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            position: absolute;
            z-index: 1;
        }
       .booking-page {
    margin-top: 0px;
}.date-btn {
 
    color: #fff;
}.btn:hover {
    color: #ffffff;
    text-decoration: none;
}.mt-5, .my-5 {
    margin-top: 1rem !important;
}
    </style>
@endsection
@section('content')
    <div class="bravo_detail_tour">


<section class="booking-page">
 <div class="container">
  <div class="row">
    <div class="col-12">
      <div class="back-btn">
        <a href="javascript:history.back()">
          <img src="{{ asset('listpagecss/images/back-arrow.png') }}" align="back arrow">
          <span>Back to search</span>
        </a>
      </div>

<?php //print_r($row->review_data);?>
    </div>
     @php $review_score = $row->review_data @endphp
    @include('Tour::frontend.layouts.details.tour-detail')
@include('Tour::frontend.layouts.details.tour-form-book')
</section>

<!-- Modal -->
<div class="modal fade booking-information" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-body">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>   
       <!--  <nav>
          <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Home</a>
            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Profile</a>
            <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</a>
          </div>
        </nav> -->
        <div class="tab-content" id="nav-tabContent">
          <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

            <div class="signup-form">
              <div class="d-flex justify-content-center">
                <a href="#" class="btn-personal">Booking Information</a>
              </div>
              <!-- Default form login -->
              <form class="text-center border border-light" action="#">

    
                <input type="text" id="" class="form-control mb-4" placeholder="(Therapist) Jane Brown
">


                <input type="text" id="" class="form-control mb-4" placeholder="(Location) Bury, Manchester
">

                <input type="text" id="" class="form-control mb-4" placeholder="(Session) Adult
">

             
                <input type="text" id="" class="form-control mb-4" placeholder="(Issues) Despression, Aniety">

                <input type="text" id="" class="form-control mb-4" placeholder="(Date) 14/01/2020
">

                <input type="text" id="" class="form-control mb-4" placeholder="(Price) £60

">

<select class="form-control">
      <option> Select Session Location </option>
      <option> Face to face </option>
      <option> Online Video</option>
      
</select>
                <div class="md-d-flex justify-content-md-center">
                 
                 <h3 class="number blue-text">1 of 2</h3>
                <a class="btn btn-primary btn-block my-4" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">CONFIRM YOUR DETAILS AND BOOK</a>
              </div>
         
              </form>
              <!-- Default form login -->
              <div>


              </div>



            </div> 

          </div>
          <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

          <div class="signup-form">
            <div class="d-flex justify-content-center">
            <a href="#" class="btn-personal">Booking Information</a>
          </div>
            <!-- Default form login -->
<form class="text-center border border-light" action="#!">

    <p class="blue-text pers-detail">Complete your personal details (all fields required) or <a href="#">Login</a> </p>

    <!-- first name -->
    <input type="text" id="" class="form-control mb-4" placeholder="First Name">


    <!-- first name -->
    <input type="text" id="" class="form-control mb-4" placeholder="Surname">

    <!-- Password -->
    <input type="email" id="" class="form-control mb-4" placeholder="Email address. 
">

    <!-- Confirm Password -->
    <input type="text" id="" class="form-control mb-4" placeholder="Contact number">

    <input type="text" id="" class="form-control mb-4" placeholder="House name or number">
    <input type="text" id="" class="form-control mb-4" placeholder="Address line
">
 <input type="text" id="" class="form-control mb-4" placeholder="Town or City
">
<input type="text" id="" class="form-control mb-4" placeholder="Postcode
">
    <div class="d-flex justify-content-center">
        <div>
            <!-- Remember me -->
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="defaultLoginFormRemember">
                <label class="custom-control-label" for="defaultLoginFormRemember"> <strong>Please accept: </strong> <a href="#">Terms and conditions</a></label>

            </div>
        </div>
        
    </div>
<div class="md-d-flex justify-content-md-center">
     <!-- Sign in button -->
     <h3 class="number blue-text">2 of 2</h3>
    <button class="btn btn-primary btn-block my-4" type="submit">BOOK NOW</button>
   
</div>
   
   

</form>
<!-- Default form login -->
  <div>


</div>



          </div>  

          </div>

        </div>










      </div>
      
    </div>
  </div>
</div>










    </div>
@endsection

@section('footer')
    {!! App\Helpers\MapEngine::scripts() !!}
    <script>
        jQuery(function ($) {
            $('.jane-brown > p, ul').addClass('para mt-5');
            @if($row->map_lat && $row->map_lng)
            new BravoMapEngine('map_content', {
                disableScripts: true,
                fitBounds: true,
                center: [{{$row->map_lat}}, {{$row->map_lng}}],
                zoom: 9,
                map_options:{
                    disableDefaultUI: true,
                    zoomControl: false,
                    mapTypeControl: true,
                    scaleControl: false,
                    streetViewControl: false,
                    rotateControl: false,
                    fullscreenControl: false,
                    panControl: false,
                    styles: [
                        {
                            "featureType": "all",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#b5eee9"
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "gamma": 0.01
                                },
                                {
                                    "lightness": 20
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "saturation": -31
                                },
                                {
                                    "lightness": -33
                                },
                                {
                                    "weight": 2
                                },
                                {
                                    "gamma": 0.8
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "lightness": 30
                                },
                                {
                                    "saturation": 30
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "saturation": 20
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "lightness": 20
                                },
                                {
                                    "saturation": -20
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "lightness": 10
                                },
                                {
                                    "saturation": -30
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "saturation": 25
                                },
                                {
                                    "lightness": 25
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "lightness": -20
                                }
                            ]
                        }
                    ],
                },
            });
            @endif
        })
    </script>
    <script>
        var bravo_booking_data = {!! json_encode($booking_data) !!}
        var bravo_booking_i18n = {
                no_date_select:'{{__('Please select Start date')}}',
                no_slot_select:'{{__('Please select slot')}}',
                no_guest_select:'{{__('Please select at lease one guest')}}',
                load_dates_url:'{{route('tour.vendor.availability.loadDates')}}',
				no_date_array_select:'{{__('Please select date')}}',
				no_date_slot_select:'{{__('No slots select for date ')}}',
            };
    </script>
    <script type="text/javascript" src="{{ asset("libs/ion_rangeslider/js/ion.rangeSlider.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("libs/fotorama/fotorama.js") }}"></script>
    <script type="text/javascript" src="{{ asset("libs/sticky/jquery.sticky.js") }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/single-tour-custom.js?_ver='.config('app.version')) }}"></script>

    <script src="{{ asset("listpagecss/js/jquery.min.js")}}"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="{{ asset("listpagecss/js/bootstrap.min.js")}}"></script>
    <script src="{{ asset("listpagecss/js/owl.carousel.min.js")}}"></script>
    <!-- <script src="{{ asset("listpagecss/js/main.js")}}"></script> -->
@endsection
