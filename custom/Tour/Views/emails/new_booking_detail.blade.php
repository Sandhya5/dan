<?php
use Custom\Core\Models\Terms;
$translation = $service->translateOrOrigin(app()->getLocale());
$lang_local = app()->getLocale();
?>
<style>
.slot-list{

}
    .slot-list .slot-active{
        margin: 2px;
        font-size: 12px;
        background-color: #dadada;
        display: inline-block;
        padding: 0.25em 0.4em;
        font-weight: 700;
        line-height: 1;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
        border-radius: 0.25rem;
        transition: color 0.15s
    }
.slot-list .slot-active.active{
    background-color: #38C172;
    color: #ffffff;
}
</style>
<div class="b-panel-title">{{__('Booking information')}}</div>
<div class="b-table-wrap">
    <table class="b-table" cellspacing="0" cellpadding="0">
        <tr>
            <td class="label">{{__('Booking Number')}}</td>
            <td class="val">#{{$booking->id}}</td>
        </tr>
        <tr>
            <td class="label">{{__('Booking Status')}}</td>
            <td class="val">{{$booking->statusName}}</td>
        </tr>
        @if($booking->gatewayObj)
            <tr>
                <td class="label">{{__('Payment method')}}</td>
                <td class="val">{{$booking->gatewayObj->getOption('name')}}</td>
            </tr>
        @endif
        @if($to = "vendor")
            <tr>
                <td class="label">{{__("Therapist's name")}}</td>
                <td class="val">
                    <a href="{{$service->getDetailUrl()}}">{{$translation->title}}</a>
                </td>

            </tr>
            <tr>
                @if($translation->address)
                    <td class="label">{{__("Therapist's Address")}}</td>
                    <td class="val">
                        {{$translation->address}}
                    </td>
                @endif
            </tr>
        @endif
        @php $tmp_dates = $booking->getJsonMeta('tmp_dates'); @endphp
        @if(!empty($tmp_dates))
            <tr>
                <td class="label">{{__('Appointment dates')}}:</td>
                <td class="val">
                    <!--@foreach($tmp_dates as $date=>$info)-->
                    <!--    <div class="label" style="text-align: left"> - <strong>{{display_date($date)}}</strong>:-->
                    <!--        @foreach(array_sort($info['slots']) as $slot)-->
                    <!--            <span class="badge badge-success active slot-active"><i>{{sprintf("%02d:00", $slot)}}</i></span>,-->
                    <!--        @endforeach-->
                    <!--    </div>-->
                    <!--@endforeach-->
                    
                     <?php foreach($tmp_dates as $info){
            
            
            ?>
                    <?php foreach(array_sort($info['date']) as $key=>$date) {
                     ?>
                     <div class="label" style="text-align: left"> - <?php echo date('d/m/Y',strtotime($date)) ?>:
                  <?php   foreach(array_sort($info['slots']) as $slot) {
                      //print_r($slot);
                    ?>
             
                        <span class="badge badge-success active slot-active"><?php echo $slot[$key]?></span>
                   </div> 
                   
                   <?php }}?>
                
            <?php }
        
        ?>
                </td>
            </tr>
        @endif

        @php $person_types = $booking->getJsonMeta('person_types')
        @endphp

        @if(!empty($person_types))
            @foreach($person_types as $type)
                <tr>
                    <td class="label">{{$type['name']}}:</td>
                    <td class="val">
                        <strong>{{$type['number']}}</strong>
                    </td>
                </tr>
            @endforeach
        @else
            @if(!empty($booking->total_guest))
            <tr>
                <td class="label">{{__("Guests")}}:</td>
                <td class="val">
                    <strong>{{$booking->total_guests}}</strong>
                </td>
            </tr>
                @endif
        @endif
        @if(!empty(setting_item('tour_attr_book_form_detail')) and $booking->getMeta('term_id'))
		    <?php
		    $term_meta = $booking->getMeta('term_id');
		    if(!empty($term_meta)){
			    $term = Terms::find($term_meta);
		    }
		    ;?>
                <tr>
                    <td colspan="2">
                        <p style="font-weight: 500">{{setting_item_with_lang('tour_attr_book_form_detail_label',request()->query('lang'))}}:</p>
                        <p>{{@$term->name}}</p>
                    </td>
                </tr>
        @endif
        <tr>
            <td class="label">{{__('Pricing')}}</td>
            <td class="val no-r-padding">
                <table class="pricing-list" width="100%">
                    @php $person_types = $booking->getJsonMeta('person_types')
                    @endphp

                    @if(!empty($person_types))
                        @foreach($person_types as $type)
                            <tr>
                                <td class="label">{{$type['name']}}: {{$type['number']}} * {{format_money($type['price'])}}</td>
                                <td class="val no-r-padding">
                                    <strong>{{format_money($type['price'] * $type['number'])}}</strong>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        @if(!empty($booking->total_guests))
                        <tr>
                            <td class="label">{{__("Guests")}}: {{$booking->total_guests}} {{format_money($booking->getMeta('base_price'))}}</td>
                            <td class="val no-r-padding">
                                <strong>{{format_money($booking->getMeta('base_price') * $booking->total_guests)}}</strong>
                            </td>
                        </tr>
                            @endif
                    @endif
                    <tr>
                        <td class="label">{{__("Appointment")}}: </td>
                        <td class="val no-r-padding">
                            <strong>{{format_money($booking->total_before_fees)}}</strong>
                        </td>
                    </tr>

                    @php $extra_price = $booking->getJsonMeta('extra_price')@endphp

                    @if(!empty($extra_price))
                        <tr>
                            <td colspan="2" class="label-title"><strong>{{__("Extra Prices:")}}</strong></td>
                        </tr>
                        <tr class="">
                            <td colspan="2" class="no-r-padding no-b-border">
                                <table width="100%">
                                @foreach($extra_price as $type)
                                    <tr>
                                        <td class="label">{{$type['name']}}:</td>
                                        <td class="val no-r-padding">
                                            <strong>{{format_money($type['total'] ?? 0)}}</strong>
                                        </td>
                                    </tr>
                                @endforeach
                                </table>
                            </td>
                        </tr>

                    @endif

                    @php $discount_by_people = $booking->getJsonMeta('discount_by_people')
                    @endphp
                    @if(!empty($discount_by_people))
                        <tr>
                            <td colspan="2" class="label-title"><strong>{{__("Discounts:")}}</strong></td>
                        </tr>
                        <tr class="">
                            <td colspan="2" class="no-r-padding no-b-border">
                                <table width="100%">
                                @foreach($discount_by_people as $type)
                                    <tr>
                                        <td class="label">
                                            @if(!$type['to'])
                                                {{__('from :from guests',['from'=>$type['from']])}}
                                            @else
                                                {{__(':from - :to guests',['from'=>$type['from'],'to'=>$type['to']])}}
                                            @endif
                                            :
                                        </td>
                                        <td class="val no-r-padding">
                                            <strong>- {{format_money($type['total'] ?? 0)}}</strong>
                                        </td>
                                    </tr>
                                @endforeach
                                </table>
                            </td>
                        </tr>
                    @endif
                    @if(!empty($booking->buyer_fees))
                        <?php
                        $buyer_fees = json_decode($booking->buyer_fees , true);
                        foreach ($buyer_fees as $buyer_fee){
                        ?>
                        <tr>
                            <td class="label">
                                {{$buyer_fee['name_'.$lang_local] ?? $buyer_fee['name']}}
                                <i class="icofont-info-circle" data-toggle="tooltip" data-placement="top" title="{{ $buyer_fee['desc_'.$lang_local] ?? $buyer_fee['desc'] }}"></i>
                                @if(!empty($buyer_fee['per_person']) and $buyer_fee['per_person'] == "on")
                                    : {{$booking->total_guests}} * {{format_money( $buyer_fee['price'] )}}
                                @endif
                            </td>
                            <td class="val">
                                @if(!empty($buyer_fee['per_person']) and $buyer_fee['per_person'] == "on")
                                    {{ format_money( $buyer_fee['price'] * $booking->total_guests ) }}
                                @else
                                    {{ format_money($buyer_fee['price']) }}
                                @endif
                            </td>
                        </tr>
                        <?php } ?>
                    @endif
                </table>
            </td>
        </tr>
        <tr>
            <td class="label fsz21">{{__('Total')}}</td>
            <td class="val fsz21"><strong style="color: #FA5636">{{format_money($booking->total)}}</strong></td>
        </tr>
    </table>
</div>
<div class="text-center mt20">
    @if($to == "customer")
        <a href="{{url('user/booking-history')}}" target="_blank" class="btn btn-primary manage-booking-btn">{{__('Manage Bookings')}}</a>
    @endif
</div>
