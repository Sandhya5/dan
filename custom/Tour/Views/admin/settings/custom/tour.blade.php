<?php
use Modules\Core\Models\Attributes;
use Modules\Core\Models\Terms;
;?>
<div class="row">
    <div class="col-sm-4">
        <h3 class="form-group-title">{{__("Form Search")}}</h3>
        <p class="form-group-desc">{{__('Config form search of your website')}}</p>
    </div>
    <div class="col-sm-8">
        <div class="panel">
            <div class="panel-body">
                @if(is_default_lang())
                    <div class="form-group">
                        <label class="" >{{__("Choose type on form search")}}</label>
                        <div class="form-controls">
                            <div class="form-controls">
		                        <?php
                                $attr = !empty(setting_item('tour_attr_on_form_search')) ? json_decode(setting_item('tour_attr_on_form_search')) : [];
                                $query = \Modules\Core\Models\Attributes::select('*')->where("service","tour");
                                $res = $query->orderBy('id', 'desc')->limit(1000)->get();
		                        ?>
                                @if($res)
                                    @foreach($res as $item)
                                        <lable>
                                            <input type="checkbox" @if(in_array($item->id , $attr)) checked @endif name="tour_attr_on_form_search[]" value="{{$item->id}}"> {{$item->name}}
                                        </lable> <br>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-sm-4">
        <h3 class="form-group-title">{{__("Page Search")}}</h3>
        <p class="form-group-desc">{{__('Config page search of your website')}}</p>
    </div>
    <div class="col-sm-8">
        <div class="panel">
            <div class="panel-body">
                @if(is_default_lang())
                    <div class="form-group">
                        <label class="" >{{__("Choose type show on page search")}}</label>
                        <div class="form-controls">
                            <div class="form-controls">
		                        <?php
	                            $attr = !empty(setting_item('tour_attr_on_page_search')) ? Attributes::find(setting_item('tour_attr_on_page_search')) : false;

		                        \App\Helpers\AdminForm::select2('tour_attr_on_page_search', [
			                        'configs' => [
				                        'ajax' => [
					                        'url'      => route('tour.admin.attribute.getTypeForSelect2'),
					                        'dataType' => 'json'
				                        ]
			                        ]
		                        ],
			                        !empty($attr->id) ? [$attr->id, $attr->name] : false
		                        )
		                        ?>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-sm-4">
        <h3 class="form-group-title">{{__("Page Detail")}}</h3>
        <p class="form-group-desc">{{__('Config page detail of your website')}}</p>
    </div>
    <div class="col-sm-8">
        <div class="panel">
            <div class="panel-body">
                @if(is_default_lang())
                    <div class="form-group">
                        <label class="" >{{__("Choose type show on page detail")}}</label>
                        <div class="form-controls">
                            <div class="form-controls">
								<?php
								$attr = !empty(setting_item('tour_attr_on_page_detail')) ? Attributes::find(setting_item('tour_attr_on_page_detail')) : false;
								\App\Helpers\AdminForm::select2('tour_attr_on_page_detail', [
									'configs' => [
										'ajax' => [
											'url'      => route('tour.admin.attribute.getTypeForSelect2'),
											'dataType' => 'json'
										]
									]
								],
									!empty($attr->id) ? [$attr->id, $attr->name] : false
								)
								?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="" >{{__("Choose type in booking form")}}</label>
                        <div class="form-controls">
                            <div class="form-controls">
								<?php
								$attr = !empty(setting_item('tour_attr_book_form_detail')) ? Attributes::find(setting_item('tour_attr_book_form_detail')) : false;
								\App\Helpers\AdminForm::select2('tour_attr_book_form_detail', [
									'configs' => [
										'ajax' => [
											'url'      => route('tour.admin.attribute.getTypeForSelect2'),
											'dataType' => 'json'
										]
									]
								],
									!empty($attr->id) ? [$attr->id, $attr->name] : false
								)
								?>
                            </div>
                        </div>
                    </div>
                @endif
                    <div class="form-group">
                        <label class="" >{{__("Label type in booking form")}}</label>
                        <div class="form-controls">
                            <div class="form-controls">
                                <input type="text" name="tour_attr_book_form_detail_label" value="{{ setting_item('tour_attr_book_form_detail_label') }}" class="form-control">
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-sm-4">
        <h3 class="form-group-title">{{__('Vendor Page')}}</h3>
        <p class="form-group-desc">{{__('Change your vendor page')}}</p>
    </div>
    <div class="col-sm-8">
        <div class="panel">
            <div class="panel-body">
                <div class="form-group">
                    <label>{{__("Page for Homepage")}}</label>
                    <div class="form-controls">
                        <?php
                        $template = !empty($settings['custom_vendor_page_id']) ? \Modules\Page\Models\Page::find($settings['custom_vendor_page_id']) : false;

                        \App\Helpers\AdminForm::select2('custom_vendor_page_id', [
                            'configs' => [
                                'ajax' => [
                                    'url'      => url('/admin/module/page/getForSelect2'),
                                    'dataType' => 'json'
                                ]
                            ]
                        ],
                            !empty($template->id) ? [$template->id, $template->title] : false
                        )
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-sm-4">
        <h3 class="form-group-title">{{__("Config for Intro Page")}}</h3>
        <p class="form-group-desc">{{__('Config for Intro Page')}}</p>
    </div>
    <div class="col-sm-8">
        <div class="panel">
            <div class="panel-body">
                @if(is_default_lang())
                    <div class="form-group">
                        <label class="">{{__("Form Search In Intro Page")}}</label>
                        <div class="form-controls">
                            <div class="form-group-item">
                                <div class="g-items-header">
                                    <div class="row">
                                        <div class="col-md-3">{{__("Icon")}}</div>
                                        <div class="col-md-4">{{__("Title")}}</div>
                                        <div class="col-md-1"></div>
                                    </div>
                                </div>
                                <div class="g-items">
                                    <?php
                                    if(!empty($settings['custom_attribute_page_intro'])){
                                    $custom_attribute_page_intro = json_decode($settings['custom_attribute_page_intro']);
                                    ?>
                                    @foreach($custom_attribute_page_intro as $key=>$item)
                                        <div class="item" data-number="{{$key}}">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    {!! \Modules\Media\Helpers\FileHelper::fieldUpload('custom_attribute_page_intro['.$key.'][image_id]',$item->image_id ?? false) !!}
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" name="custom_attribute_page_intro[{{$key}}][title]" class="form-control" value="{{$item->title}}" placeholder="{{__('Eg: Service')}}">
                                                </div>
                                                <div class="col-md-4">
                                                    @php $configs = [
                                                        'ajax' => [
                                                            'url'      => route('tour.admin.attribute.term.getForSelect2'),
                                                            'dataType' => 'json'
                                                        ]
                                                    ] ;
                                                    @endphp
                                                    <select class="form-control dungdt-select2-field" multiple="true" data-options='<?php echo json_encode($configs) ?>' name="custom_attribute_page_intro[{{$key}}][val][]">
                                                        @if(!empty($item->val) and is_array($item->val))
                                                            @foreach( $item->val as $option)
                                                                @php  $attr = Terms::find($option) ?? false; @endphp
                                                                <option selected value="{{$option}}"> {{$attr->name ?? ""}}</option>
                                                            @endforeach
                                                        @elseif(!empty($item->val) and is_string($item->val))
                                                            @php  $attr = Terms::find($item->val) ?? false;  @endphp
                                                            <option selected value="{{$item->val}}"> {{$attr->name ?? ""}}</option>
                                                        @endif
                                                    </select>

                                                </div>
                                                <div class="col-md-1">
                                                    <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    <?php } ?>
                                </div>
                                <div class="text-right">
                                    <span class="btn btn-info btn-sm btn-add-item"><i class="icon ion-ios-add-circle-outline"></i> {{__('Add item')}}</span>
                                </div>
                                <div class="g-more hide">
                                    <div class="item" data-number="__number__">
                                        <div class="row">
                                            <div class="col-md-3">
                                                {!! \Modules\Media\Helpers\FileHelper::fieldUpload('custom_attribute_page_intro[__number__][image_id]','','__name__') !!}
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" __name__="custom_attribute_page_intro[__number__][title]" class="form-control" value="" placeholder="{{__('Eg: title')}}">
                                            </div>
                                            <div class="col-md-4">
                                                @php $configs = [
                                                    'ajax' => [
                                                        'url'      => route('tour.admin.attribute.term.getForSelect2'),
                                                        'dataType' => 'json'
                                                    ]
                                                ] @endphp
                                                <select class="form-control dungdt-select2-field-append" multiple="true" data-options='<?php echo json_encode($configs) ?>' __name__="custom_attribute_page_intro[__number__][val]"></select>
                                            </div>
                                            <div class="col-md-1">
                                                <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

<hr>
<div class="row">
    <div class="col-sm-4">
        <h3 class="form-group-title">{{__("Other Page Configs")}}</h3>
        <p class="form-group-desc">{{__('Other Page Configs')}}</p>
    </div>
    <div class="col-sm-8">
        <div class="panel">
            <div class="panel-body">
                @if(is_default_lang())
                    <div class="form-group">
                        <label>{{__("Introductions & Ownership policy")}}</label>
                        <div class="form-controls">
                            <?php
                            $template = !empty($settings['custom_page_introductions_ownership_policy']) ? \Modules\Page\Models\Page::find($settings['custom_page_introductions_ownership_policy']) : false;

                            \App\Helpers\AdminForm::select2('custom_page_introductions_ownership_policy', [
                                'configs' => [
                                    'ajax' => [
                                        'url'      => url('/admin/module/page/getForSelect2'),
                                        'dataType' => 'json'
                                    ]
                                ]
                            ],
                                !empty($template->id) ? [$template->id, $template->title] : false
                            )
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>{{__("Thanks you page when after register vendor")}}</label>
                        <div class="form-controls">
                            <input type="text" name="custom_page_thanks_after_register_vendor" value="{{ setting_item('custom_page_thanks_after_register_vendor') }}" class="form-control">
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
