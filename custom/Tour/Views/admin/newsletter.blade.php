@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="d-flex justify-content-between mb20">
            <h1 class="title-bar">{{__("All Newsletter")}}</h1>
            
        </div>
        
        <div class="panel">
            <div class="panel-body">
                <form action="" class="bravo-form-item">
                    <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <!--<th width="60px"><input type="checkbox" class="check-all"></th>-->
                            <th> {{ __('Email')}}</th>
                            <th width="100px"> {{ __('Date')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($rows)> 0)
                            @foreach($rows as $row)
                                <tr class="">
                                    <!--<td><input type="checkbox" name="ids[]" class="check-item" value="{{$row->id}}">-->
                                    <!--</td>-->
                                    <td class="title">
                                        <a href="">{{$row->email}}</a>
                                    </td>
                                    <td>{{ display_date($row->post_date)}}</td>
                                    
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7">{{__("No data")}}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
