<div class="panel">
    <div class="panel-title"><strong>{{__("Age")}}</strong></div>
    <div class="panel-body">
        @if(is_default_lang())
             <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="control-label">{{__("Define Your Age")}}</label>
                         <select name="age" class="form-control">
										<option value="">{{__("Select Age")}}</option>
                                        <option @if($row->age ==  '1') selected @endif value="1">{{__("Adults")}}</option>
                                        <option @if($row->age ==  '2') selected @endif value="2">{{__("Children (6-10)")}}</option>
										<option @if($row->age ==  '3') selected @endif value="3">{{__("Elders (65+)")}}</option>
                                        <option @if($row->age ==  '4') selected @endif  value="4">{{__("Adolescents / Teenagers (14 to 19)")}}</option>
										<option @if($row->age ==  '5') selected @endif value="5">{{__("Preteens / Tweens (11 to 13)")}}</option>
                                        <option @if($row->age ==  '6') selected @endif value="6">{{__("Toddlers / Preschoolers (0 to 6)")}}</option>
                                    </select>  </div>
                </div>
           
                
            </div>
            <hr>
        @endif
 </div>

    </div>