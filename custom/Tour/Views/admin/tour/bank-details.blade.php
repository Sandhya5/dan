<div class="panel">
    <div class="panel-title"><strong>{{__("Bank Detail")}}</strong></div>
    <div class="panel-body">
        @if(is_default_lang())
            <?php $row->bank_details = old("bank_details",$row->bank_details) ?>
            <?php if(!empty($row->bank_details) and !is_array($row->bank_details)) $row->bank_details = json_decode($row->bank_details,true);?>
            <div class="row">
                <div class="col-lg-7">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="control-label">{{__("First name")}}</label>
                                <input type="text" name="bank_details[first_name]" class="form-control" value="{{ $row->bank_details['first_name'] ??  "" }}" placeholder="{{__("First name")}}">
                                <span style="font-size: 12px;font-style: italic;color: #950a00;margin-top: 5px;">{{ __("Required field (*)") }}</span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="control-label">{{__("Last name")}}</label>
                                <input type="text" name="bank_details[last_name]" class="form-control" value="{{ $row->bank_details['last_name'] ??  "" }}" placeholder="{{__("Last name")}}">
                                <span style="font-size: 12px;font-style: italic;color: #950a00;margin-top: 5px;">{{ __("Required field (*)") }}</span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="control-label">{{__("Account number")}}</label>
                                <input type="text" name="bank_details[account_number]" class="form-control" value="{{ $row->bank_details['account_number'] ??  "" }}" placeholder="{{__("Account number")}}">
                                <span style="font-size: 12px;font-style: italic;color: #950a00;margin-top: 5px;">{{ __("Required field (*)") }}</span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="control-label">{{__("Sort code")}}</label>
                                <input type="text" name="bank_details[short_code]" class="form-control" value="{{ $row->bank_details['short_code'] ??  "" }}" placeholder="{{__("Sort code")}}">
                                <span style="font-size: 12px;font-style: italic;color: #950a00;margin-top: 5px;">{{ __("Required field (*)") }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        @endif
    </div>
</div>