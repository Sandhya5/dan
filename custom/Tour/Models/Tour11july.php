<?php
namespace Custom\Tour\Models;

use Custom\Booking\Models\BookingSlot;
use Custom\Core\Models\Terms;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Modules\Booking\Models\Bookable;
use Modules\Booking\Models\Booking;
use Modules\Location\Models\Location;
use Modules\Review\Models\Review;
use Modules\Template\Models\Template;
use Modules\Tour\Models\TourCategory;
use Modules\Tour\Models\TourTerm;
use Modules\Media\Helpers\FileHelper;
use Illuminate\Support\Facades\Cache;
use PHPUnit\Framework\Constraint\Attribute;
use Validator;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\SEO;

use Session;
use Modules\Tour\Models\TourMeta;
use Modules\User\Models\User;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\DB;
use Modules\Media\Models\MediaFile;

use Modules\Core\Models\Settings;
use Modules\Core\Models\Menu;
use Modules\Core\Models\Attributes;
use Modules\Page\Models\Page;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Tour extends \Modules\Tour\Models\Tour
{

    use PaginationWithHaving;

    protected $casts = [
        'faqs' => 'array',
        'highlights' => 'array',
        'bank_details' => 'array',
        'faqs2' => 'array',
    ];

    /**
     * @var $bookingClass Booking
     */
	protected $bookingClass;
    /**
     * @var $bookingSlotClass BookingSlot
     */
	protected $bookingSlotClass;

	protected $tmp_price;
	protected $tmp_dates;
	protected $tmp_slots;

	public function __construct(array $attributes = [])
	{
		parent::__construct($attributes);
		$this->bookingClass = \Custom\Booking\Models\Booking::class;
		$this->bookingSlotClass = \Custom\Booking\Models\BookingSlot::class;
		$this->tourDateClass = TourDate::class;
	}

	public function getBookingData()
	{
		$booking_data = [
			'id'           => $this->id,
			'person_types' => [],
			'max'          => 0,
			'open_hours'   => [],
			'extra_price'  => [],
			'minDate'      => date('m/d/Y'),
			'duration'     => $this->duration,
		];
		$meta = $this->meta ?? false;
		$lang = app()->getLocale();
		if ($meta) {
			if ($meta->enable_person_types) {
				$booking_data['person_types'] = $meta->person_types;
				if(!empty($booking_data['person_types'])) {
					foreach ($booking_data['person_types'] as $k => &$type) {
						if (!empty($lang) and !empty($type['name_' . $lang])) {
							$type['name'] = $type['name_' . $lang];
							$type['desc'] = $type['desc_' . $lang];
						}
						$type['min'] = (int)$type['min'];
						$type['max'] = (int)$type['max'];
						$type['number'] = $type['min'];
						$type['display_price'] = format_money($type['price']);
					}

					$booking_data['person_types'] = array_values((array)$booking_data['person_types']);
				}else{
					$booking_data['person_types'] = [];
				}
			}
			if ($meta->enable_extra_price) {
				$booking_data['extra_price'] = $meta->extra_price;
				if (!empty($booking_data['extra_price'])) {
					foreach ($booking_data['extra_price'] as $k => &$type) {
						if (!empty($lang) and !empty($type['name_' . $lang])) {
							$type['name'] = $type['name_' . $lang];
						}
						$type['number'] = 0;
						$type['enable'] = 0;
						$type['price_html'] = format_money($type['price']);
						$type['price_type'] = '';
						switch ($type['type']) {
							case "per_day":
								$type['price_type'] .= '/' . __('day');
								break;
							case "per_hour":
								$type['price_type'] .= '/' . __('hour');
								break;
						}
						if (!empty($type['per_person'])) {
							$type['price_type'] .= '/' . __('guest');
						}
					}
				}

				$booking_data['extra_price'] = array_values((array)$booking_data['extra_price']);
			}
			if ($meta->enable_open_hours) {
				$booking_data['open_hours'] = $meta->open_hours;
			}
		}
		$booking_data['first_booking']=$this->isCustomFirstBooking();
		$booking_data['discount_for_10_slot'] = $this->discount_for_10_slot;
		$booking_data['discount_for_4_slot'] = $this->discount_for_4_slot;
		return $booking_data;
	}
	public function isCustomFirstBooking(){
		$status=false;
		if($this->enable_sale_of_first_booking==1){
			if(Auth::check()){
				$a = $this->bookingClass::where('object_id',$this->id)->where('object_model','tour')->whereNotIn('status',Booking::$notAcceptedStatus)->count();
				if(!empty($a)){
					$status=false;
				}else{
					$status=true;
				}
			}
		}
		return $status;
	}
	public function addToCartValidate(Request $request)
	{
		$meta = $this->meta;
		$rules = [
		    'dates'=>'required',
			'dates.*.date' => 'required|date_format:Y-m-d',
			'dates.*.slots' => 'required'
		];
		if ($meta) {

			// Percent Types
			if ($meta->enable_person_types) {
				unset($rules['guests']);
				$rules['person_types'] = 'required';
				$person_types_configs = $meta->person_types;
				if (!empty($person_types_configs) and is_array($person_types_configs)) {
					foreach ($person_types_configs as $k => $person_type) {
						$ruleStr = 'integer';
						if ($person_type['min']) {
							$ruleStr .= '|min:' . $person_type['min'];
						}
						if ($person_type['max']) {
							$ruleStr .= '|max:' . $person_type['max'];
						}
						if ($ruleStr) {
							$rules['person_types.' . $k . '.number'] = $ruleStr;
						}
					}
				}
			}
		}

		// Validation
		if (!empty($rules)) {
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				$this->sendError('', ['errors' => $validator->errors()]);
			}
		}
//		if ($meta) {
//			// Open Hours
//			if ($meta->enable_open_hours) {
//				$open_hours = $meta->open_hours;
//				$nDate = date('N', strtotime($start_date));
//				if (!isset($open_hours[$nDate]) or empty($open_hours[$nDate]['enable'])) {
//					$this->sendError(__("This tour is not open on your selected day"));
//				}
//			}
//		}

		$dates = collect($request->input('dates'));

		return $this->isAvailableAt($dates);

	}
	public function getSlotBookedDate($start_date,$slots){
		$slotAvilable = $this->bookingClass::where('bravo_bookings.object_id', $this->id)->where('bravo_bookings.start_date', $start_date)->whereNotIn('status', $this->bookingClass::$notAcceptedStatus)->join('bravo_booking_slot',function ($join) use($slots){
			$join->on('bravo_bookings.id','=','bravo_booking_slot.booking_id')->whereIn('bravo_booking_slot.slot',$slots);
		})->get();
		$slotActived = [];
		if(!empty($slotAvilable)){
			foreach ($slotAvilable as $slot){
				if(empty($slotActived[$slot->slot])){
					$slotActived[$slot->slot]= (new BookingSlot())->getSlotName($slot->slot);
				}
			}
		}
		asort($slotActived);
		return $slotActived;
	}

	public function addToCart(Request $request)
	{
		$this->addToCartValidate($request);

		// Add Booking
		$total = $this->tmp_price;
		$total_guests = 0;
		$discount = 0;

        $total_before_fees = $total;

		//Buyer Fees
		$list_fees = setting_item('tour_booking_buyer_fees');
		if (!empty($list_fees)) {
			$lists = json_decode($list_fees, true);
			foreach ($lists as $item) {
				if (!empty($item['per_person']) and $item['per_person'] == "on") {
					$total += $item['price'] * $total_guests;
				} else {
					$total += $item['price'];
				}
			}
		}

		$discount_for_10_slot = false;
        $discount_for_4_slot = false;

		$first_booking = $this->isCustomFirstBooking();
		if($first_booking){
			$total= $total/2;
            $total_before_fees= $total_before_fees/2;
		}

		if($this->discount_for_10_slot && count($this->tmp_slots)>=10){
		    $total -= $total*0.1;
            $total_before_fees -= $total_before_fees*0.1;
            $discount_for_10_slot = true;
        }else{
		    if($this->discount_for_4_slot and count($this->tmp_slots) >=4 ){
		        $total -= $total*0.05;
                $total_before_fees -= $total_before_fees*0.05;
                $discount_for_4_slot = true;
            }
        }


        /**
         * @var $booking \Custom\Booking\Models\Booking
         */
		$booking = new $this->bookingClass();
		$booking->status = 'draft';
		$booking->object_id = $request->input('service_id');
		$booking->object_model = $request->input('service_type');
		$booking->vendor_id = $this->create_user;
		$booking->customer_id = Auth::id();
		$booking->total = $total;
		$booking->first_booking = $first_booking;
		$booking->total_guests = $total_guests;
		$booking->start_date = '';
		$booking->end_date = '';
		$booking->buyer_fees = $list_fees ?? '';
		$booking->total_before_fees = $total_before_fees;
		$booking->sess_loc = $request->input('sess_loc');
		$booking->calculateCommission();

		$check = $booking->save();
		if ($check) {

			$this->bookingClass::clearDraftBookings();

//			$booking->addMeta('duration', $this->duration);
//			$booking->addMeta('base_price', $this->price);
//			$booking->addMeta('guests', max($total_guests, $request->input('guests')));
//			$booking->addMeta('extra_price', []);
//			$booking->addMeta('person_types', []);
//			$booking->addMeta('discount_by_people', []);
			$booking->addMeta('term_id', $request->term_id);
			$booking->addMeta('tmp_dates', $this->tmp_dates);
			$booking->addMeta('tmp_slots', $this->tmp_slots);
			$booking->addMeta('selected_dates',$request->input('dates'));
			if($discount_for_10_slot) $booking->addMeta('discount_for_10_slot',1);
			if($discount_for_4_slot) $booking->addMeta('discount_for_4_slot',1);

			//	update booking slots
			if(!empty($this->tmp_dates)){
				foreach ($this->tmp_dates as $date=>$dateData){
					$booking->slots()->save(new BookingSlot(['slots'=>$dateData['slots'],'date'=>$date]));
				}
			}

			// $this->sendSuccess([
			// 	'url' => $booking->getCheckoutUrl()
			// ]);
		return $this->sendSuccess([
			 	'url' => $booking->getCheckoutUrl()
			 ]);

		}
		$this->sendError(__("Can not check availability"));
	}

	public function isAvailableAt($dates){

        $newDates = [];

        $tourDatesTmp = $this->tourDateClass::where('target_id', $this->id)
            ->whereIn('start_date', $dates->pluck('date') )
            ->groupBy(['start_date'])
            ->get();
        $bookingDataTmp = $this->bookingSlotClass::query()->where([
            'object_id'=>$this->id,
        ])
            ->whereNotIn('status',$this->bookingClass::$notAcceptedStatus)
            ->join('bravo_bookings','bravo_booking_slot.booking_id','=','bravo_bookings.id')
            ->whereIn('bravo_booking_slot.date',$dates->pluck('date'))
            ->get();

        $tourDates = [];
        foreach ($tourDatesTmp as $date){
            $tourDates[$date['start_date']] = $date;
        }
        $bookingData = [];
        foreach ($bookingDataTmp as $item){
            $bookingData[$item['date']][] = $item;
        }

        $blocked = [];
        $allSlots = [];

        $meta = $this->meta;

        foreach ($dates as $date){
            if(empty($date['slots']) or !is_array($date['slots']))
            {
                continue;
            }
            if(array_key_exists($date['date'],$newDates)){
                $newDates[$date['date']]['slots'] = array_unique(array_merge($newDates[$date['date']]['slots'],$date['slots']));
            }else{
                $newDates[$date['date']] = $date;
            }

            if ($meta) {
                // Open Hours
                if ($meta->enable_open_hours) {
                    $open_hours = $meta->open_hours;
                    $nDate = date('N', strtotime($date['date']));

                    if (!isset($open_hours[$nDate]) or empty($open_hours[$nDate]['enable'])) {
                        $blocked [$date['date']] = $date['slots'];
                        continue;

                    }else{
                        $item_open_hours = $open_hours[$nDate];
                        foreach ($date['slots'] as $slot)
                        {
                            $str_time = strtotime($date['date'].' '.$slot.':00:00');
                            $from_time = strtotime($date['date'].' '.$item_open_hours['from'].':00');
                            $to_time = strtotime($date['date'].' '.$item_open_hours['to'].':00');
                            if($str_time < $from_time or $str_time > $to_time){
                                if(empty($blocked [$date['date']])) $blocked [$date['date']] = [];

                                $blocked[$date['date']][] = $slot;
                            }

                        }
                    }
                }
            }

            foreach ($date['slots'] as $slot)
            {
                $allSlots[$date['date'].' '.$slot.':00:00'] = [
                    'price'=>(float) $this->price
                ];
            }

        }

        if(empty($blocked)) {
            foreach ($newDates as $date => $data) {

                if (!empty($tourDates[$date]->slots)) {
                    foreach ($tourDates[$date]->slots as $dateSlot) {
                        $key = $date . ' ' . $dateSlot['slot'] . ':00:00';

                        if (array_key_exists($key, $allSlots)) {
                            if (empty($dateSlot['active']) or $dateSlot['active'] != 1 or !empty($dateSlot['price'])) {
//                            $this->sendError(__(":date has been booked",['date'=>display_datetime(strtotime($date['date'].' '.$dateSlot['slot'].':00:00'))]));
                                $blocked[$date][] = $dateSlot['slot'];
                                continue;
                            }

                            $allSlots[$key]['price'] = (float)$dateSlot['price'];
                        }
                    }
                    foreach ($bookingData[$date]->slots as $slot) {
                        $key = $date . ' ' . $slot . ':00:00';

                        if (array_key_exists($key, $allSlots)) {
                            $blocked[$date][] = $slot;
                            continue;
                        }
                    }
                    if (!empty($blocked[$date])) {
                        $blocked[$date] = array_unique($blocked[$date]);
                    }
                }
            }
        }
        if(!empty($blocked))
        {
            $html = [];
            foreach ($blocked as $date=>$slots)
            {
                $slotHtml = [];
                foreach ($slots as $slot){
                    $slotHtml[]=$slot.":00";
                }
                $html[] = __(':date at :slots',['date'=>display_date(strtotime($date)),'slots'=>implode(', ',$slotHtml)]);
            }

            $this->sendError(__(":date are not available",['date'=>implode(", ",$html)]));
        }


        $this->tmp_dates = $newDates;
        $this->tmp_price = array_sum(array_column($allSlots,'price'));
        $this->tmp_slots = $allSlots;
//var_dump($this->tmp_dates);
//var_dump($this->tmp_price);
//var_dump($this->tmp_slots);
//
//        die;
        return true;
    }

    /**
     * @param Request $request
     * @param $booking \Custom\Booking\Models\Booking
     */
	public function beforeCheckout(Request $request, $booking)
	{
	    $tmp_dates = collect($booking->getJsonMeta('selected_dates'));

		return $this->isAvailableAt($tmp_dates);
	}

	public function tour_term_detail()
	{
		return $this->hasManyThrough(Terms::class, $this->tourTermClass,'tour_id','id','id','term_id');
	}

    public function tour_term_in_list()
    {
        $attr_id = setting_item("tour_attr_on_page_search");
        if(!empty($attr_id)){
            $tour_id= $this->id;
            $list = Terms::where("attr_id",$attr_id)
            ->join('bravo_tour_term', function ($join) use ($tour_id) {
                $join->on('bravo_terms.id', '=', 'bravo_tour_term.term_id')
                    ->where('bravo_tour_term.tour_id', '=', $tour_id);
            })->orderByRaw("RAND()")->first();
            return $list;
        }
        return false;
    }

	static function updateSeedForJob(){

        Schema::table('bravo_tours', function (Blueprint $table) {
            if (!Schema::hasColumn('bravo_tours', 'country')) {
                $table->string('country',255)->nullable();
            }
            if (!Schema::hasColumn('bravo_tours', 'city')) {
                $table->string('city',255)->nullable();
            }
            if (!Schema::hasColumn('bravo_tours', 'discount_for_4_slot')) {
                $table->smallInteger('discount_for_4_slot')->nullable();
            }
            if (!Schema::hasColumn('bravo_tours', 'discount_for_10_slot')) {
                $table->smallInteger('discount_for_10_slot')->nullable();
            }
            if (!Schema::hasColumn('bravo_tours', 'bank_details')) {
                $table->text('bank_details')->nullable();
            }
            if (!Schema::hasColumn('bravo_tours', 'introductions_ownership_policy')) {
                $table->smallInteger('introductions_ownership_policy')->nullable();
            }
            if (!Schema::hasColumn('bravo_tours', 'faqs2')) {
                $table->text('faqs2')->nullable();
            }
        });
        Schema::table('bravo_tour_dates', function (Blueprint $table) {
            if (!Schema::hasColumn('bravo_tour_dates', 'slots')) {
                $table->text('slots')->nullable();
            }
        });
        Schema::table('bravo_booking_slot', function (Blueprint $table) {
            if (!Schema::hasColumn('bravo_booking_slot', 'slots')) {
                $table->text('slots')->nullable();
            }
        });
        if(!Schema::hasTable('user_notes')){
            Schema::create('user_notes', function (Blueprint $table) {
                if (!Schema::hasColumn('user_notes', 'id')) {
                    $table->bigIncrements('id');
                    $table->text('content');
                    $table->bigInteger('user_id');
                    $table->timestamp('date');

                    $table->integer('create_user')->nullable();
                    $table->integer('update_user')->nullable();
                    $table->timestamps();
                }
            });
        }
        Schema::table('bravo_booking_slot', function (Blueprint $table) {
            if (Schema::hasColumn('bravo_booking_slot', 'slot')) {
                $table->dropColumn('slot');
            }
        });
        Schema::table('user_notes', function (Blueprint $table) {
            if (!Schema::hasColumn('user_notes', 'client_name')) {
                $table->string('client_name',255)->nullable();
            }
            if (Schema::hasColumn('user_notes', 'date')) {
                $table->dropColumn('date');
            }
            if (!Schema::hasColumn('user_notes', 'start_date')) {
                $table->date('start_date')->nullable();
            }
        });


        if(setting_item('update_to_job')){
            return "Updated Up Job";
        }
        // Settings
        $setting = Settings::where('name', "tour_page_search_title")->first();
        $setting->val = 'Well done for making it this far!';
        $setting->save();

        $setting = Settings::where('name', "footer_text_left")->first();
        $setting->val = 'Copyright © 2019 by Booking';
        $setting->save();

        $setting = Settings::where('name', "topbar_left_text")->first();
        $setting->val = '<div class="socials"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-linkedin"></i></a><a href="#"><i class="fa fa-google-plus"></i></a></div><span class="line"></span><a href="mailto:contact@takeaseat.com">contact@takeaseat.co.uk</a>';
        $setting->save();

        // Settings
        $setting = new Settings();
        $setting->val = '#54a79f';
        $setting->name = 'style_main_color';
        $setting->save();

        // Menu
        $menu = Menu::where('id', "1")->first();
        $menu_items = array(
            array(
                'name'       => 'Home',
                'url'        => '/',
                'item_model' => 'custom',
                'model_name' => 'Custom',
                'children'   => array(),
            ),
            array(
                'name'       => 'Therapists',
                'url'        => '/therapist',
                'item_model' => 'custom',
                'model_name' => 'Custom',
            ),
            array(
                'name'       => 'Blog',
                'url'        => '/news',
                'item_model' => 'custom',
                'model_name' => 'Custom',
                'children'   => array(),
            ),
            array(
                'name'       => 'Contact',
                'url'        => '/contact',
                'item_model' => 'custom',
                'model_name' => 'Custom',
                'children'   => array(),
            ),
        );
        $menu->items = json_encode($menu_items);
        $menu->save();


        //Home Page


        // Tour
        $sql = "DELETE from bravo_tours";
        DB::statement($sql);

        $sql = "DELETE from bravo_attrs where service = 'tour'";
        DB::statement($sql);
        $sql = "DELETE from bravo_terms";
        DB::statement($sql);
        $sql = "DELETE from bravo_tour_term";
        DB::statement($sql);

        $a = new \Modules\Core\Models\Attributes();
        $a->name = 'Therapy Type';
        $a->service = 'tour';
        $a->save();
        $attr_page_search[] = $a->id;



        foreach (
            [
                'Mindfulness Based',
                'Eclectic',
                'AEDP',
                'Gestalt',
                'Humanistic',
                'Hypnotherapy'
            ] as $i => $term
        ) {
            $t = new \Modules\Core\Models\Terms();
            $t->name = $term;
            $t->attr_id = $a->id;
            $t->save();
            $term_ids[] = $t->id;
        }

        $a = new \Modules\Core\Models\Attributes();
        $a->name = 'Issues';
        $a->service = 'tour';
        $a->save();
        $attr_page_search[] = $a->id;
//        seed setting page search
		$setting = Settings::where('name', "tour_attr_on_page_search")->first();
		if(empty($setting)){
			$setting  = new Settings();
		}
		$setting->name = 'tour_attr_on_page_search';
		$setting->val = $a->id;
		$setting->save();

		DB::table('media_files')->insert([
			['file_name' => 'type-1', 'file_path' => 'demo/job/type-1.png', 'file_type' => 'image/png', 'file_extension' => 'png'],
			['file_name' => 'type-2', 'file_path' => 'demo/job/type-2.png', 'file_type' => 'image/png', 'file_extension' => 'png'],
			['file_name' => 'type-3', 'file_path' => 'demo/job/type-3.png', 'file_type' => 'image/png', 'file_extension' => 'png'],
			['file_name' => 'type-4', 'file_path' => 'demo/job/type-4.png', 'file_type' => 'image/png', 'file_extension' => 'png'],
			['file_name' => 'type-5', 'file_path' => 'demo/job/type-5.png', 'file_type' => 'image/png', 'file_extension' => 'png'],
			['file_name' => 'type-6', 'file_path' => 'demo/job/type-5.png', 'file_type' => 'image/png', 'file_extension' => 'png'],
		]);

		$list_issue_id = [];
        foreach (
            [
                'Anxiety',
                'Depression',
                'Stress',
                'Relationships',
                'ADHD',
                'Autism'
            ] as $i => $term
        ) {
            $t = new \Modules\Core\Models\Terms();
            $t->name = $term;
            $t->attr_id = $a->id;
	        $t->image_id = MediaFile::findMediaByName("type-".($i+1))->id ?? "";
	        $t->save();
            $list_issue_id[] = $t->id;
        }

		DB::table('media_files')->insert([
			['file_name' => 'accreditations-1', 'file_path' => 'demo/job/accreditations-1.jpg', 'file_type' => 'image/jpeg', 'file_extension' => 'jpg'],
			['file_name' => 'accreditations-2', 'file_path' => 'demo/job/accreditations-2.jpg', 'file_type' => 'image/jpeg', 'file_extension' => 'jpg'],
			['file_name' => 'accreditations-3', 'file_path' => 'demo/job/accreditations-3.jpg', 'file_type' => 'image/jpeg', 'file_extension' => 'jpg'],
			['file_name' => 'accreditations-4', 'file_path' => 'demo/job/accreditations-4.jpg', 'file_type' => 'image/jpeg', 'file_extension' => 'jpg'],
			['file_name' => 'accreditations-5', 'file_path' => 'demo/job/accreditations-5.jpg', 'file_type' => 'image/jpeg', 'file_extension' => 'jpg'],
			['file_name' => 'accreditations-6', 'file_path' => 'demo/job/accreditations-6.jpg', 'file_type' => 'image/jpeg', 'file_extension' => 'jpg'],
		]);

        $a = new \Modules\Core\Models\Attributes();
        $a->name = 'Accreditations';
        $a->service = 'tour';
        $a->save();

		//        seed setting page search
		$setting = Settings::where('name', "tour_attr_on_page_detail")->first();
		if(empty($setting)){
			$setting  = new Settings();
		}
		$setting->name = 'tour_attr_on_page_detail';
		$setting->val = $a->id;
		$setting->save();

		$setting = Settings::where('name', "site_enable_multi_lang")->first();
		$setting->val = 0;
		$setting->save();

		$setting = Settings::where('name', "tour_page_search_banner")->first();
		$setting->val = "";
		$setting->save();

        $setting  = new Settings();
		$setting->name = "custom_vendor_page_id";
		$setting->val = "3";
		$setting->save();
        $setting = false;

		$setting  = new Settings();
		$setting->name = "tour_attr_on_form_search";
		$setting->val = json_encode($attr_page_search);
		$setting->save();
        $setting = false;

        $setting  = new Settings();
        $setting->name = "tour_attr_book_form_detail_label";
        $setting->val = "Who is this session for?";
        $setting->save();

        foreach (
            [
                'Federation of Holistic Therapists (FHT)',
                'The Association of Cognitive Analytic Therapy (ACAT)',
                'Association of Christian Counsellors (ACC)',
                'Association of Child Psychotherapists (ACP)',
                'Association for Dance Movement Psychotherapy (ADMP)',
                'British Association for Behavioural and Cognitive Psychotherapies (BABCP)',
            ] as $i => $term
        ) {
            $t = new \Modules\Core\Models\Terms();
            $t->name = $term;
            $t->attr_id = $a->id;
            $t->image_id = MediaFile::findMediaByName("accreditations-".($i+1))->id ?? "";
            $t->save();
        }

        $a = new \Modules\Core\Models\Attributes();
        $a->name = 'Age';
        $a->service = 'tour';
        $a->save();
//        seed setting page search
		$setting = Settings::where('name', "tour_attr_book_form_detail")->first();
		if(empty($setting)){
			$setting  = new Settings();
		}
		$setting->name = 'tour_attr_book_form_detail';
		$setting->val = $a->id;
		$setting->save();


        foreach (
            [
                'Adolescents / Teenagers (14 to 19)',
                'Adults',
                'Children (6 to 10)',
                'Elders (65+)',
                'Preteens / Tweens (11 to 13)',
                'Toddlers / Preschoolers (0 to 6)',
            ] as $term
        ) {
            $t = new \Modules\Core\Models\Terms();
            $t->name = $term;
            $t->attr_id = $a->id;
            $t->save();
        }
        $a = new \Modules\Core\Models\Attributes();
        $a->name = 'Sexuality';
        $a->service = 'tour';
        $a->save();
        foreach (
            [
                'Bisexual',
                'Gay',
                'Lesbian',
            ] as $term
        ) {
            $t = new \Modules\Core\Models\Terms();
            $t->name = $term;
            $t->attr_id = $a->id;
            $t->save();
        }
        $a = new \Modules\Core\Models\Attributes();
        $a->name = 'Gender';
        $a->service = 'tour';
        $a->save();
        foreach (
            [
                'Men',
                'Non-Binary',
                'Women'
            ] as $term
        ) {
            $t = new \Modules\Core\Models\Terms();
            $t->name = $term;
            $t->attr_id = $a->id;
            $t->save();
        }


        //Tour
        DB::table('media_files')->insert([
            ['file_name' => 'work-1', 'file_path' => 'demo/job/work-1.jpg', 'file_type' => 'image/jpeg', 'file_extension' => 'jpg'],
            ['file_name' => 'work-2', 'file_path' => 'demo/job/work-2.jpg', 'file_type' => 'image/jpeg', 'file_extension' => 'jpg'],
            ['file_name' => 'work-3', 'file_path' => 'demo/job/work-3.jpg', 'file_type' => 'image/jpeg', 'file_extension' => 'jpg'],
            ['file_name' => 'icon-work-1', 'file_path' => 'demo/job/icon-1.png', 'file_type' => 'image/png', 'file_extension' => 'png'],
            ['file_name' => 'icon-work-2', 'file_path' => 'demo/job/icon-2.png', 'file_type' => 'image/png', 'file_extension' => 'png'],
            ['file_name' => 'icon-work-3', 'file_path' => 'demo/job/icon-3.png', 'file_type' => 'image/png', 'file_extension' => 'png'],
        ]);


        //Home page
        $dataHome = array(
            array(
                'type'         => 'form_search_tour',
                'name'         => 'Profile: Form Search',
                'model'        => array(
                    'title'     => 'Find the perfect <br> therapist for you',
                    'sub_title' => 'Book with a professional in seconds',
                ),
                'component'    => 'RegularBlock',
                'open'         => true,
                'is_container' => false,
            ),
            array(
                'type'         => 'tour_list_terms',
                'name'         => 'Profile: List Terms',
                'model'        => array(
                    'title'     => 'We\'ll match you to right therapist for you...',
                    'terms'     => '32',
                    'attribute' => Attributes::where("name","Issues")->first()->id ?? "5",
                    'number'    => 5,
                    'style'     => 'normal',
                ),
                'component'    => 'RegularBlock',
                'open'         => true,
                'is_container' => false,
            ),
            array(
                'type'         => 'text',
                'name'         => 'Text',
                'model'        => array(
                    'content' => '<p><span style="color: #2c2e36; font-size: 24pt;">How it works</span></p>',
                    'class'   => 'bravo_custom_text_1',
                ),
                'component'    => 'RegularBlock',
                'open'         => true,
                'is_container' => false,
            ),
            array(
                'type'         => 'list_featured_item',
                'name'         => 'List Featured Item',
                'model'        => array(
                    'list_item' => array(
                        array(
                            '_active'    => true,
                            'title'      => 'Browse Therapists',
                            'sub_title'  => 'Search through hundreds of experienced, qualified therapists to find the right one for you. <br><br>
Choose from time, cost, specialism and session type to find the best therapist for you in minutes',
                            'icon_image' => MediaFile::findMediaByName("work-1")->id,
                            'order'      => NULL,
                        ),
                        array(
                            '_active'    => true,
                            'title'      => 'Book and Pay',
                            'sub_title'  => 'In person sessions are available 7 days  week. You can find a therapist at a time and price that suits you, wherever you are.  <br> <br>
If you’re not satisfied with your first meeting, we’ll arrange a session with an alternative therapist totally free of charge.',
                            'icon_image' => MediaFile::findMediaByName("work-2")->id,
                            'order'      => NULL,
                        ),
                        array(
                            '_active'    => true,
                            'title'      => 'Connect with your Therapist',
                            'sub_title'  => 'Once booked, your therapist will contact you to confirm your session.  <br><br>
You will be able to view your upcoming  and previous sessions from your personal dashboard and you will also be emailed reminders.',
                            'icon_image' => MediaFile::findMediaByName("work-3")->id,
                            'order'      => NULL,
                        ),
                    ),
                    'style'     => 'style3',
                ),
                'component'    => 'RegularBlock',
                'open'         => true,
                'is_container' => false,
            ),
            array(
                'type'         => 'list_featured_item',
                'name'         => 'List Featured Item',
                'model'        => array(
                    'list_item' => array(
                        array(
                            '_active'    => false,
                            'title'      => '1000+ therapists',
                            'sub_title'  => 'We are confident that you will find a therapist perfect for you.',
                            'icon_image' => MediaFile::findMediaByName("icon-work-1")->id,
                            'order'      => NULL,
                        ),
                        array(
                            '_active'    => true,
                            'title'      => 'Accredited professionals',
                            'sub_title'  => 'All profiles show accreditations so you can book with confidence.',
                            'icon_image' => MediaFile::findMediaByName("icon-work-2")->id,
                            'order'      => NULL,
                        ),
                        array(
                            '_active'    => true,
                            'title'      => 'Face to face consultations',
                            'sub_title'  => 'Our location based system allows you to visit therapists for a more personal consultation.',
                            'icon_image' => MediaFile::findMediaByName("icon-work-3")->id,
                            'order'      => NULL,
                        ),
                    ),
                    'style'     => 'normal',
                ),
                'component'    => 'RegularBlock',
                'open'         => true,
                'is_container' => false,
            ),
            array(
                'type'         => 'list_tours',
                'name'         => 'Profile: List Items',
                'model'        => array(
                    'title'       => 'Featured Therapists',
                    'number'      => 6,
                    'style'       => 'carousel',
                    'category_id' => '',
                    'location_id' => '',
                    'order'       => '',
                    'order_by'    => '',
                    'is_featured' => '',
                ),
                'component'    => 'RegularBlock',
                'open'         => true,
                'is_container' => false,
            ),
            array(
                'type'         => 'tour_list_terms',
                'name'         => 'Profile: List Terms',
                'model'        => array(
                    'title'     => 'Accreditations',
                    'attribute' => Attributes::where("name","Accreditations")->first()->id ?? "4",
                    'number'    => 6,
                    'style'     => 'style2',
                ),
                'component'    => 'RegularBlock',
                'open'         => true,
                'is_container' => false,
            ),
            array(
                'type'      => 'call_to_action',
                'name'      => 'Call To Action',
                'model'     => array(
                    'title'      => 'Are you a therapist?',
                    'sub_title'  => 'Join our website and start helping people today.',
                    'link_title' => 'Join as a professional',
                    'link_more'  => '/page/become-a-vendor',
                ),
                'component' => 'RegularBlock',
                'open'      => true,
            ),
            array(
                'type'      => 'testimonial',
                'name'      => 'List Testimonial',
                'model'     => array(
                    'title'     => 'Recent reviews',
                    'list_item' => array(
                        array(
                            '_active'     => true,
                            'name'        => 'Jamie Wright',
                            'desc'        => 'Faucibus tristique felis potenti ultrices ornare rhoncus semper hac facilisi Rutrum tellus lorem sem velit nisi non pharetra in dui.',
                            'number_star' => 5,
                            'avatar'      => MediaFile::findMediaByName("avatar")->id,
                        ),
                        array(
                            '_active'     => true,
                            'name'        => 'Jamie Wright',
                            'desc'        => 'Faucibus tristique felis potenti ultrices ornare rhoncus semper hac facilisi Rutrum tellus lorem sem velit nisi non pharetra in dui.',
                            'number_star' => 5,
                            'avatar'      => MediaFile::findMediaByName("avatar-2")->id,
                        ),
                        array(
                            '_active'     => true,
                            'name'        => 'Jamie Wright',
                            'desc'        => 'Faucibus tristique felis potenti ultrices ornare rhoncus semper hac facilisi Rutrum tellus lorem sem velit nisi non pharetra in dui.',
                            'number_star' => 5,
                            'avatar'      => MediaFile::findMediaByName("avatar-3")->id,
                        ),
                    ),
                ),
                'component' => 'RegularBlock',
                'open'      => true,
            ),
        );

        $template = new Template();
        $template->title = "Home Profiles";
        $template->content = json_encode($dataHome);
        $template->save();

        $page = Page::where("title","Home Page")->first();
        $page->template_id = $template->id;
        $page->save();


        //Tour
        DB::table('media_files')->insert([
            ['file_name' => 'profile-1', 'file_path' => 'demo/job/profile-1.jpg', 'file_type' => 'image/jpeg', 'file_extension' => 'jpg'],
            ['file_name' => 'profile-2', 'file_path' => 'demo/job/profile-2.jpg', 'file_type' => 'image/jpeg', 'file_extension' => 'jpg'],
            ['file_name' => 'profile-3', 'file_path' => 'demo/job/profile-3.jpg', 'file_type' => 'image/jpeg', 'file_extension' => 'jpg'],
            ['file_name' => 'profile-4', 'file_path' => 'demo/job/profile-4.jpg', 'file_type' => 'image/jpeg', 'file_extension' => 'jpg'],
        ]);
        $listUsers = \App\User::get();
        foreach ($listUsers as $user){
            if( $user->hasPermissionTo('dashboard_vendor_access') ) {
                $customInforUser = Tour::where("create_user",$user->id)->first();
                if(empty($customInforUser)){
                    $tourClass = new Tour([
                        'title'=>$user->first_name." ".$user->last_name,
                        'status'=> 'publish',
                        'content'=> '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla orci odio, blandit at rhoncus sed, accumsan sit amet quam. Ut ut lectus risus. Suspendisse lacinia arcu ut justo scelerisque, id viverra ex luctus. Phasellus molestie nisi nec massa laoreet, eget iaculis mi scelerisque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed non diam mi. Aliquam nec bibendum nibh. Etiam id purus volutpat, eleifend ante quis, hendrerit felis. Quisque vestibulum, ante id mollis dignissim, lectus nibh scelerisque leo, id lacinia urna nunc nec quam. Phasellus a diam at neque aliquam pretium. Ut augue erat, ultricies vel porta sit amet, pretium id dui. Suspendisse vel ligula et dui laoreet efficitur. Pellentesque bibendum dignissim fermentum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>

<p>Sed scelerisque orci lectus, vel tristique lorem ornare sed. Mauris vitae ligula ut leo ullamcorper fringilla a eu lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Quisque sagittis ullamcorper sem, quis mollis mauris scelerisque tempus. Sed orci tortor, laoreet in iaculis tempor, hendrerit sit amet arcu. Nulla quis tortor varius ante dapibus molestie sit amet quis felis. Integer tortor augue, mattis eu fermentum in, pulvinar in felis. Curabitur accumsan libero eget orci venenatis, ac cursus tortor accumsan. Suspendisse massa purus, lacinia venenatis consequat eu, maximus at urna. Donec porta nibh eros, blandit vulputate ipsum vestibulum id. Etiam nec varius nunc, ut porttitor sapien. Sed at commodo est. In condimentum suscipit ex id aliquam. Vivamus et luctus turpis, sed fringilla quam. Etiam tellus odio, sodales sed pharetra nec, luctus ut massa.</p>',
                        'faqs' => '[{"title":"When and where does the tour end?","content":"Your tour will conclude in San Francisco on Day 8 of the trip. There are no activities planned for this day so you free to depart at any time. We highly recommend booking post-accommodation to give yourself time to fully experience the wonders of this iconic city!"},{"title":"When and where does the tour start?","content":"Day 1 of this tour is an arrivals day, which gives you a chance to settle into your hotel and explore Los Angeles. The only planned activity for this day is an evening welcome meeting at 7pm, where you can get to know your guides and fellow travellers. Please be aware that the meeting point is subject to change until your final documents are released."},{"title":"Do you arrange airport transfers?","content":"Airport transfers are not included in the price of this tour, however you can book for an arrival transfer in advance. In this case a tour operator representative will be at the airport to greet you. To arrange this please contact our customer service team once you have a confirmed booking."},{"title":"What is the age range","content":"This tour has an age range of 12-70 years old, this means children under the age of 12 will not be eligible to participate in this tour. However, if you are over 70 years please contact us as you may be eligible to join the tour if you fill out G Adventures self-assessment form."}]',
                        'image_id'=> MediaFile::findMediaByName("profile-".rand(1,4))->id,
                        'price'=> rand(100,200),
                        'address'=> "New York, United States",
                        'map_lat'=> "51.449136670955866",
                        'map_lng'=> "-0.28020080566409433",
                        'map_lng'=> "10",
                    ]);
                    $tourClass->create_user = $user->id;
                    $tourClass->save();

                    foreach ($list_issue_id as $term_id) {
                        TourTerm::firstOrCreate([
                            'term_id' => $term_id,
                            'tour_id' => $tourClass->id
                        ]);
                    }

                    foreach ($term_ids as $term_id) {
                        TourTerm::firstOrCreate([
                            'term_id' => $term_id,
                            'tour_id' => $tourClass->id
                        ]);
                    }

                }
            }
        }

        Settings::store('update_to_job',true);
        return "Migrate Up Job";
    }


}
