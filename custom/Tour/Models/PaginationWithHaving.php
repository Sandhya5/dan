<?php
/**
 * Created by PhpStorm.
 * User: h2 gaming
 * Date: 10/7/2019
 * Time: 8:32 PM
 */
namespace Custom\Tour\Models;

trait PaginationWithHaving
{
    protected function newBaseQueryBuilder()
    {
        $connection = $this->getConnection();
        return new PaginationHelper(
            $connection,
            $connection->getQueryGrammar(),
            $connection->getPostProcessor()
        );
    }
}