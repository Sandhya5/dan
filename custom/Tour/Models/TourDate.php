<?php
namespace Custom\Tour\Models;

use App\BaseModel;

class TourDate extends \Modules\Tour\Models\TourDate
{

    protected $casts = [
        'person_types'=>'array',
        'slots'=>'array'
    ];

}
