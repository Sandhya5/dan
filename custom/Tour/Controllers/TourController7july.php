<?php

namespace Custom\Tour\Controllers;

use App\Http\Controllers\Controller;

use Custom\Booking\Models\Booking;
use Custom\Core\Models\Terms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Tour\Models\TourCategory;
use Modules\Location\Models\Location;
use Modules\Review\Models\Review;
use Modules\Review\Controllers\ReviewController;
use Modules\Core\Models\Attributes;
use App\Currency;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\HomeController;


use Custom\Tour\Models\Tour;
use Illuminate\Support\Facades\Artisan;

class TourController extends \Modules\Tour\Controllers\TourController
{
	public function __construct()
	{
		parent::__construct();
		$this->tourClass  = Tour::class;
	}

	function updateMigrate(){
        $controller = new HomeController();
        $controller->updateMigrate();
        Artisan::call('cache:clear');
        echo "<br>";
        echo Tour::updateSeedForJob();
        Artisan::call('cache:clear');
        die();
    }
	public function index(Request $request)
	{
		$is_ajax = $request->query('_ajax');
		$model_Tour = $this->tourClass::select("bravo_tours.*");
		
		$model_Tour->where("bravo_tours.status", "publish");
		if (!empty($location_id = $request->query('post_code'))) {
			$location = $this->locationClass::where('id', $location_id)->where("status", "publish")->first();
			//print_r($location);die;
			if (!empty($location)) {
				$model_Tour->join('bravo_locations', function ($join) use ($location) {
					$join->on('bravo_locations.id', '=', 'bravo_tours.location_id')
						->where('bravo_locations._lft', '>=', $location->_lft)
						->where('bravo_locations._rgt', '<=', $location->_rgt);
				});
			}
		}
		
		if (!empty($price_range = $request->query('price_range'))) {
			$pri_from = (int)explode(";", $price_range)[0];
			$pri_to = (int)explode(";", $price_range)[1];
			$raw_sql_min_max = "( (bravo_tours.sale_price > 0 and bravo_tours.sale_price >= ? ) OR (bravo_tours.sale_price <= 0 and bravo_tours.price >= ?) )
								AND ( (bravo_tours.sale_price > 0 and bravo_tours.sale_price <= ? ) OR (bravo_tours.sale_price <= 0 and bravo_tours.price <= ?) )";
			$raw_sql_min_max = " bravo_tours.price >= ?  AND  bravo_tours.price <= ?";
			$model_Tour->WhereRaw($raw_sql_min_max,[$pri_from,$pri_to]);
			
			// $sql_where_term = "";
            // $k=0;
            // foreach ($price_range as $attr_id => $term_ids){
                // $id_join = $attr_id;
                // $tmp = "";
                // if($k == 0){
                    // $tmp .= "(";
                // }else{
                    // $tmp .= " OR (";
                // }
                // $clear = false;
                // if(empty($term_ids)){
                    // continue;
                // }
                // foreach ($term_ids as $i => $term_id){
                    // if(empty($term_id)){
                        // $clear = true;
                    // }
                    // if(count(term_ids == 0){
                         // $tmp .= "bravo_tours.sale_price = ".$term_id;
                    // }else{
                        // $tmp .= " OR tt_{$id_join}.term_id = ".$term_id;
                    // }
                // }
                // $tmp .= ")";
                // if($clear){
                    // $k = -1;
                // }else{
                    // //$model_Tour->join('bravo_tour.price as tt_'.$id_join, 'tt_'.$id_join.'.tour_id', "bravo_tours.id");
                    // $sql_where_term .= $tmp;
                // }
                // $k++;
            // } $sql_where_term;
            // if($sql_where_term != ''){
            	 // $model_Tour->whereRaw($sql_where_term);
				 
				 // //print_r($model_Tour);
            // }
			
			
			
			
		}
		//print_r($request->query('term_ids'));die;
		if (!empty($category_ids = $request->query('cat_id'))) {
			if(!is_array($category_ids)) $category_ids = [$category_ids];
			$list_cat = $this->tourCategoryClass::whereIn('id', $category_ids)->where("status","publish")->get();
			if(!empty($list_cat)){
				$where_left_right = [];
				foreach ($list_cat as $cat){
					$where_left_right[] = " ( bravo_tour_category._lft >= {$cat->_lft} AND bravo_tour_category._rgt <= {$cat->_rgt} ) ";
				}
				$sql_where_join = " ( " . implode("OR", $where_left_right) . " )  ";
				$model_Tour
					->join('bravo_tour_category', function ($join) use ($sql_where_join) {
						$join->on('bravo_tour_category.id', '=', 'bravo_tours.category_id')
							->WhereRaw($sql_where_join);
					});
			}

		}
		//print_r( $request->query('age'));
if (!empty($age = $request->query('age'))) {
			$sql_where_term = "";
            $k=0;
            foreach ($age as $attr_id => $term_ids){
                $id_join = $attr_id;
                $tmp = "";
                if($k == 0){
                    $tmp .= "(";
                }else{
                    $tmp .= " OR (";
                }
                $clear = false;
                if(empty($term_ids)){
                    continue;
                }
               
                         $tmp .= "bravo_tours.age = ".$term_ids;
                   
                $tmp .= ")";
                if($clear){
                    $k = -1;
                }else{
                     $sql_where_term .= $tmp;
                }
                $k++;
            } $sql_where_term;
            if($sql_where_term != ''){
            	 $model_Tour->whereRaw($sql_where_term);
				// print_r($sql_where_term);
            }	
		}
		$term_ids_for_search_name = [];
		if(!empty($term_name = $request->query('term_name'))){
            $query = Terms::select('*');
            $query->where('name', 'like', '%' . $term_name . '%');
            $res = $query->get();

            if(!empty($res)){
                foreach ($res as $item){
                    $term_ids_for_search_name[$item->attr_id][] = $item->id;
                }
            }
        }
		$terms = $request->query('terms');
		//echo $term_ids[] = $request->query('term_ids');die;
		if(!empty($term_ids_for_search_name)){
            $terms  =array_merge($terms,$term_ids_for_search_name);
        }
  //       echo "<pre>";;
		// print_r($terms);
		// exit;
		if (is_array($terms) && !empty($terms)) {
            $sql_where_term = "";
            $k=0;
            foreach ($terms as $attr_id => $term_ids){
                $id_join = $attr_id;
                $tmp = "";
                if($k == 0){
                    $tmp .= "(";
                }else{
                    $tmp .= " AND (";
                }
                $clear = false;
                if(empty($term_ids)){
                    continue;
                }
//print_r($_GET);die;
                 foreach ($term_ids as $i => $term_id){
                    if(empty($term_id)){
                        $clear = true;
                    }
                    if($i == 0){
                        $tmp .= "tt_{$id_join}.term_id = ".$term_id;
                    }else{
                         $tmp .= " OR tt_{$id_join}.term_id = ".$term_id;
                    }
                 }
				
                $tmp .= ")";
                if($clear){
                    $k = -1;
                }else{
                    $model_Tour->join('bravo_tour_term as tt_'.$id_join, 'tt_'.$id_join.'.tour_id', "bravo_tours.id");
                    $sql_where_term .= $tmp;
                }
                $k++;
            }
            if($sql_where_term != ''){
            	$model_Tour->whereRaw($sql_where_term);
            }
		}
		//$model_Tour->groupBy("bravo_tours.id");
//print_r($request->query());
		if(request('lat') and request('lng') and !(request('age')))
		{
			$model_Tour->selectRaw("
				111.111 *
				DEGREES(ACOS(LEAST(1.0, COS(RADIANS(?))
				* COS(RADIANS(     CAST(bravo_tours.map_lat AS DECIMAL(9,6))               ))
				* COS(RADIANS(? -  CAST(bravo_tours.map_lng AS DECIMAL(9,6))    ))
				+ SIN(RADIANS(?))
				* SIN(RADIANS(    CAST(bravo_tours.map_lat AS DECIMAL(9,6))            ))))) AS distance_in_km

			",[(float)request('lat'),(float)request('lng'),(float)request('lat')]);
			//$model_Tour->setBindings([request('lat'),request('lng'),request('lat')]);
			$model_Tour->orderBy("distance_in_km", "ASC");

			$distance_range = request('distance_range');
			if($distance_range and strpos($distance_range,';') !== false ){
				$pri_from = (int)explode(";", $distance_range)[0];
				$pri_to = (int)explode(";", $distance_range)[1];
				$model_Tour->having('distance_in_km','>=',$pri_from);
				$model_Tour->having('distance_in_km','<=',$pri_to);
			}else{
                $model_Tour->having('distance_in_km','>',0);
				$model_Tour->having('distance_in_km','<=',500);
            }
		}


		$model_Tour->orderBy("is_featured", "desc");

		$list = $model_Tour->with(['location','hasWishList','translations','tour_term_detail'=>function($query){
			$query->where('attr_id',setting_item('tour_attr_on_page_search'));
		}])->paginate(9);
		//print_r($list);
		$markers = [];
		if (!empty($list)) {
			foreach ($list as $row) {
				$markers[] = [
					"id" => $row->id,
					"title" => $row->title,
					"lat" => (float)$row->map_lat,
					"lng" => (float)$row->map_lng,
					"gallery" => $row->getGallery(true),
					"infobox" => view('Tour::frontend.layouts.search.loop-gird', ['row' => $row, 'disable_lazyload' => 1, 'wrap_class' => 'infobox-item'])->render(),
					'marker' => url('images/icons/png/pin.png'),
					//                    'marker'=>'http://travelhotel.wpengine.com/wp-content/uploads/2018/11/ico_mapker_hotel.png'
				];
			}
		}
		$limit_location = 15;
		if( empty(setting_item("space_location_search_style")) or setting_item("space_location_search_style") == "normal" ){
			$limit_location = 1000;
		}
		$data = [
			'rows' => $list,
			'tour_category' => $this->tourCategoryClass::where('status', 'publish')->with(['translations'])->get()->toTree(),
			'tour_location' => $this->locationClass::where('status', 'publish')->with(['translations'])->limit($limit_location)->get()->toTree(),
			'tour_min_max_price' => $this->tourClass::getMinMaxPrice(),
			'markers' => $markers,
			"blank" => 0,
			"seo_meta" => $this->tourClass::getSeoMetaForPageList()
		];
		$layout = setting_item("tour_layout_search", 'normal');
		if ($request->query('_layout')) {
			$layout = $request->query('_layout');
		}
		if ($is_ajax) {
			$this->sendSuccess([
				'html' => view('Tour::frontend.layouts.search-map.list-item', $data)->render(),
				"markers" => $data['markers']
			]);
		}
		$data['attributes'] = $this->attributesClass::where('service', 'tour')->with(['terms','translations'])->get();
		if ($layout == "map") {
			$data['body_class'] = 'has-search-map';
			$data['html_class'] = 'full-page';
			return view('Tour::frontend.search-map', $data);
		}
		return view('Tour::frontend.search', $data);
	}

	public function detail(Request $request, $slug)
	{
		$attrShowOnDetail = $this->attributesClass::find(setting_item('tour_attr_on_page_detail'));

		$row = $this->tourClass::where('slug', $slug)->where("status", "publish")->with(['location','translations','hasWishList','tour_term_detail'=>function($query) use($attrShowOnDetail){
			if(!empty($attrShowOnDetail)){
				$query->where('attr_id',$attrShowOnDetail->id);
			}
		}])->first();
		if (empty($row)) {
			return redirect('/');
		}
		$translation = $row->translateOrOrigin(app()->getLocale());
		$tour_related = [];
		$location_id = $row->location_id;
		if (!empty($location_id)) {
			$tour_related = $this->tourClass::where('location_id', $location_id)->where("status","publish")->take(4)->whereNotIn('id', [$row->id])->with(['location','translations','hasWishList'])->get();
		}
		$review_list = Review::where('object_id', $row->id)
			->where('object_model', 'tour')
			->where("status", "approved")
			->orderBy("id", "desc")
			->with('author')
			->paginate(setting_item('tour_review_number_per_page', 5));

		$data = [
			'row' => $row,
			'translation' => $translation,
			'tour_related' => $tour_related,
			'booking_data' => $row->getBookingData(),
			'review_list' => $review_list,
			'attrShowOnDetail' => $attrShowOnDetail,
			'seo_meta' => $row->getSeoMetaWithTranslation(app()->getLocale(), $translation),
			'body_class'=>'is_single'
		];
		$this->setActiveMenu($row);
		return view('Tour::frontend.detail', $data);
	}

	public function getTypeForSelect2(Request $request){
	    dd($request->input());
		$q = $request->query('q');
		$query = Terms::select('id', 'name as text');
		if(!empty(setting_item('tour_attr_book_form_detail'))){
			$query->where('attr_id',setting_item('tour_attr_book_form_detail'));
		}
		if ($q) {
			$query->where('title', 'like', '%' . $q . '%');
		}
		$res = $query->orderBy('id', 'desc')->limit(20)->get();
		return response()->json([
			'results' => $res
		]);
	}
	
	public function getTerms(Request $request){
			   // dd($request->input());
		 $q = $request->query('selectedvalue');
		$list = DB::table('bravo_terms')->where('attr_id',$q )->get();
			if (!empty($list)) {
				 $opt='';
			foreach ($list as $row) {
				 $opt.= '<li id="'.$row->id.'"><a href="#">'.$row->name.'</a></li>';
			}
		}
		return($opt);
		//return view('Tour::frontend.blocks.customdanish.index', $data);
	}
	
	 public function submitTerms(Request $request){
			   //dd($request->input());
			   // echo url('/');die;
			   //print_r($request->query());die;
		  $post_code = $request->query('post_code');
		  $lat = $request->query('lat');
		  $lng = $request->query('lng');
		  
		  //$category = $request->query('category');
		  $q = $request->query('selectedvalue');
		 $link = url('/').'/tour?_layout=map&lat='.$lat.'&lng='.$lng.'&post_code='.$post_code.'&term_ids='.$q;
		 return($link);
	 } 
	 
	
	 public function getFilterTerms(Request $request){
			   //dd($request->input());
			   // echo url('/');die;
		  $post_code = $request->query('post_code');
		  //$category = $request->query('category');
		  $q = $request->query('terms');
		 $exp =explode(',',$q);
		 //print_r($exp);die;
		 $term = "";
		 foreach($exp as $tid){
			$term = $term.'&terms%5B1%5D%5B%5D='.$tid;
			
		 }
		 //print($term);
		 
		 $link = url('/').'/tour?_layout=map&post_code='.$post_code.$term;
		 return($link);
	 }
}
