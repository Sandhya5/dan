<?php
namespace Custom\Tour\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Models\Attributes;

use Custom\Core\Models\Terms;
use Modules\FrontendController;

class AttributeController extends FrontendController
{
    protected $attributesClass;
    protected $termsClass;
    public function __construct()
    {
        $this->setActiveMenu('admin/module/tour');
        parent::__construct();
        $this->attributesClass = Attributes::class;
        $this->termsClass = Terms::class;
    }

    public function getForSelect2(Request $request)
    {
        $q = $request->query('search');
        $query = $this->termsClass::select('*');
        if ($q) {
            $query->where('name', 'like', '%' . $q . '%');
        }

        $attr = !empty(setting_item('tour_attr_on_form_search')) ? json_decode(setting_item('tour_attr_on_form_search')) : false;
        if(!empty($attr)){
            $query->whereIn('attr_id',$attr);
        }
        $res = $query->orderBy('id', 'desc')->limit(20)->get();

        if(!empty($res) and count($res)){
            $list_json = [];
            foreach ($res as $term) {
                $translate = $term->translateOrOrigin(app()->getLocale());
                $list_json[] = [
                    'id' => $term->id,
                    'title' => " ".$translate->name,
                ];
            }

            $this->sendSuccess(['data'=>$list_json]);
        }
        return $this->sendError(__("Not found"));
    }
}
