<?php
namespace Custom\Tour\Admin;

use Illuminate\Http\Request;
use Modules\AdminController;
use Modules\Core\Models\Attributes;
use Modules\Core\Models\AttributesTranslation;
use Modules\Core\Models\TermsTranslation;


use Custom\Core\Models\Terms;

class AttributeController extends \Modules\Tour\Admin\AttributeController
{
    protected $attributesClass;
    protected $termsClass;
    public function __construct()
    {
        $this->setActiveMenu('admin/module/tour');
        parent::__construct();
        $this->attributesClass = Attributes::class;
        $this->termsClass = Terms::class;
    }
	public function getTypeForSelect2(Request $request){
		$q = $request->query('q');
		$query = $this->attributesClass::select('id', 'name as text')->where('service','tour');
		if ($q) {
			$query->where('name', 'like', '%' . $q . '%');
		}
		$res = $query->orderBy('id', 'desc')->limit(999)->get();
		return response()->json([
			'results' => $res
		]);
	}

	public function getTermForSelect2(Request $request){
        $q = $request->query('q');
        $query = $this->termsClass::select('id', 'name as text');
        if ($q) {
            $query->where('name', 'like', '%' . $q . '%');
        }
        $res = $query->orderBy('id', 'desc')->limit(999)->get();
        return response()->json([
            'results' => $res
        ]);
    }

    public function getForSelect2(Request $request)
    {
        $pre_selected = $request->query('pre_selected');
        $selected = $request->query('selected');

        if($pre_selected && $selected){
            $item = $this->attributesClass::find($selected);
            if(empty($item)){
                return response()->json([
                    'text'=>''
                ]);
            }else{
                return response()->json([
                    'text'=>$item->name
                ]);
            }
        }
        $q = $request->query('q');
        $query = $this->attributesClass::select('id', 'name as text')->where("service","tour");
        if ($q) {
            $query->where('name', 'like', '%' . $q . '%');
        }
        $res = $query->orderBy('id', 'desc')->limit(20)->get();
        return response()->json([
            'results' => $res
        ]);
    }
}
