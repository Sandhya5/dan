<?php
namespace Custom\Tour\Blocks;

use Modules\Template\Blocks\BaseBlock;
use Custom\Core\Models\Terms;

class VendorWhyUse extends BaseBlock
{
    function __construct()
    {
        $this->setOptions([
            'settings' => [
                [
                    'id'        => 'title',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Title')
                ],
                [
                    'id'          => 'list_item',
                    'type'        => 'listItem',
                    'label'       => __('List Item(s)'),
                    'title_field' => 'title',
                    'settings'    => [
                        [
                            'id'        => 'title',
                            'type'      => 'input',
                            'inputType' => 'text',
                            'label'     => __('Title')
                        ],
                        [
                            'id'        => 'sub_title',
                            'type'      => 'textArea',
                            'inputType' => 'textArea',
                            'label'     => __('Sub Title')
                        ],
                        [
                            'id'    => 'icon_image',
                            'type'  => 'uploader',
                            'label' => __('Image Uploader')
                        ]
                    ]
                ],
            ]
        ]);
    }

    public function getName()
    {
        return __('VendorPage: Why Use');
    }

    public function content($model = [])
    {
        return view('Tour::frontend.blocks.why-use.index', $model);
    }
}
