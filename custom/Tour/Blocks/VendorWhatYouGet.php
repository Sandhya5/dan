<?php
namespace Custom\Tour\Blocks;

use Modules\Template\Blocks\BaseBlock;
use Custom\Core\Models\Terms;

class VendorWhatYouGet extends BaseBlock
{
    function __construct()
    {
        $this->setOptions([
            'settings' => [
                [
                    'id'        => 'title',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Title')
                ],
                [
                    'id'          => 'list_item',
                    'type'        => 'listItem',
                    'label'       => __('List Item(s)'),
                    'title_field' => 'title',
                    'settings'    => [
                        [
                            'id'        => 'title',
                            'type'      => 'input',
                            'inputType' => 'text',
                            'label'     => __('Title')
                        ],
                    ]
                ],
            ]
        ]);
    }

    public function getName()
    {
        return __('VendorPage: What you get');
    }

    public function content($model = [])
    {
        return view('Tour::frontend.blocks.what-you-get.index', $model);
    }
}
