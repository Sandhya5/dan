<?php
namespace Custom\Tour\Blocks;

use Modules\Template\Blocks\BaseBlock;

class VendorRegisterForm2 extends BaseBlock
{
    function __construct()
    {
        $this->setOptions([
            'settings' => [
                [
                    'id'        => 'title',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Title')
                ],
            ]
        ]);
    }

    public function getName()
    {
        return __('VendorPage: Register Form');
    }

    public function content($model = [])
    {
        return view('Tour::frontend.blocks.form-register-2.index', $model);
    }
}
