<?php
namespace Custom\Tour\Blocks;

use Modules\Template\Blocks\BaseBlock;
use Custom\Core\Models\Terms;

class VendorStepTo extends BaseBlock
{
    function __construct()
    {
        $this->setOptions([
            'settings' => [
                [
                    'id'        => 'title',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Title')
                ],
                [
                    'id'          => 'list_item',
                    'type'        => 'listItem',
                    'label'       => __('List Item(s)'),
                    'title_field' => 'title',
                    'settings'    => [
                        [
                            'id'        => 'sub_title',
                            'type'      => 'textArea',
                            'inputType' => 'textArea',
                            'label'     => __('Desc')
                        ],
                        [
                            'id'    => 'icon_image',
                            'type'  => 'uploader',
                            'label' => __('Image Uploader')
                        ]
                    ]
                ],
                [
                    'id'        => 'link',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Link Learn more')
                ],
            ]
        ]);
    }

    public function getName()
    {
        return __('VendorPage: Steps to get setup');
    }

    public function content($model = [])
    {
        return view('Tour::frontend.blocks.steps-to.index', $model);
    }
}
