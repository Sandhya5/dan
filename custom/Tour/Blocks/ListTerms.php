<?php
namespace Custom\Tour\Blocks;

use Modules\Template\Blocks\BaseBlock;
use Custom\Core\Models\Terms;

class ListTerms extends BaseBlock
{
    function __construct()
    {
        $this->setOptions([
            'settings' => [
                [
                    'id'        => 'title',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Title')
                ],
                [
                    'id'           => 'attribute',
                    'type'         => 'select2',
                    'label'        => __('Select Term'),
                    'select2'      => [
                        'ajax'     => [
                            'url'      => route('tour.admin.attribute.getForSelect2'),
                            'dataType' => 'json'
                        ],
                        'width'    => '100%',
                    ],
                    'pre_selected' => route('tour.admin.attribute.getForSelect2', [
                        'type'         => 'space',
                        'pre_selected' => 1
                    ])
                ],
                [
                    'id'        => 'number',
                    'type'      => 'input',
                    'inputType' => 'number',
                    'label'     => __('Number Item')
                ],
                [
                    'id'            => 'style',
                    'type'          => 'radios',
                    'label'         => __('Style'),
                    'values'        => [
                        [
                            'value'   => 'normal',
                            'name' => __("Style 1")
                        ],
                        [
                            'value'   => 'style2',
                            'name' => __("Style 2")
                        ],
                        [
                            'value'   => 'style3',
                            'name' => __("Style 3")
                        ]
                    ]
                ],
            ]
        ]);
    }

    public function getName()
    {
        return __('Profile: List Terms');
    }

    public function content($model = [])
    {
        if (empty($term_space = $model['attribute'])) {
            return "";
        }
        $list_term = Terms::where('attr_id',$model['attribute'])->limit($model['number'] ?? 5)->get();
        $model['list_term'] = $list_term;
        return view('Tour::frontend.blocks.list-terms.index', $model);
    }
}
