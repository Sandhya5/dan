<?php
namespace Custom\Vendor\Controllers;

use App\Helpers\ReCaptchaEngine;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Matrix\Exception;
use Modules\FrontendController;
use Modules\User\Events\NewVendorRegistered;
use Modules\User\Events\SendMailUserRegistered;
use Modules\Vendor\Models\VendorRequest;

use Custom\Tour\Models\Tour;

class VendorController extends FrontendController
{
    public function register(Request $request)
    {
        $rules = [
           
            'email'      => [
                'required',
                'string',
                'email',
                'max:255',
                'unique:users'
            ],
            'password'   => [
                'required',
                'string'
            ],
            'term'       => ['required'],
        ];
        $messages = [
            'email.required'      => __('Email is required field'),
            'email.email'         => __('Email invalidate'),
            'password.required'   => __('Password is required field'),
           // 'first_name.required' => __('The first name is required field'),
            //'last_name.required'  => __('The last name is required field'),
           // 'full_name.required'  => __('The name is required field'),
            'term.required'       => __('The terms and conditions field is required'),
        ];
        if (ReCaptchaEngine::isEnable() and setting_item("user_enable_register_recaptcha")) {
            $codeCapcha = $request->input('g-recaptcha-response');
            if (!$codeCapcha or !ReCaptchaEngine::verify($codeCapcha)) {
                $errors = new MessageBag(['message_error' => __('Please verify the captcha')]);
                return response()->json([
                    'error'    => true,
                    'messages' => $errors
                ], 200);
            }
        }
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json([
                'error'    => true,
                'messages' => $validator->errors()
            ], 200);
        } else {

            $user = new User();
            //$user->name = $request->input('full_name');
            $user->first_name = $request->input('first_name');
          // $user->last_name = $request->input('full_name');
//            $user->phone = $request->input('phone');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->status = 'publish';
            $check = $user->save();
            if (empty($check)) {
                $this->sendError(__("Can not register"));
            }
            //                check vendor auto approved
            $vendorAutoApproved = setting_item('vendor_auto_approved');
            $dataVendor['role_request'] = setting_item('vendor_role');
            if ($vendorAutoApproved) {
                if ($dataVendor['role_request']) {
                    $user->assignRole($dataVendor['role_request']);
                }
                $dataVendor['status'] = 'approved';
                $dataVendor['approved_time'] = now();
            } else {
                $dataVendor['status'] = 'pending';
                $user->assignRole('Vendor');
            }

            $this->createUserInfo($user , $vendorAutoApproved);

            $vendorRequestData = $user->vendorRequest()->save(new VendorRequest($dataVendor));
            Auth::loginUsingId($user->id);
            try {
                event(new NewVendorRegistered($user, $vendorRequestData));
            } catch (Exception $exception) {
                Log::warning("NewVendorRegistered: " . $exception->getMessage());
            }

            $page_thankyou = setting_item("custom_page_thanks_after_register_vendor");

            if(!empty($page_thankyou)){
                $this->sendSuccess([
                    'redirect' => $page_thankyou,
                ]);
            }else{
                if ($vendorAutoApproved) {
                    $this->sendSuccess([
                        'redirect' => url(app_get_locale(false, '/')),
                    ]);
                } else {
                    $this->sendSuccess([
                        'redirect' => url(app_get_locale(false, '/')),
                    ], __("Register success. Please wait for admin approval"));
                }
            }
        }
    }

    protected function createUserInfo($user , $vendorAutoApproved)
    {
        $tourClass = new Tour([
            'title'=> $user->name,
            'status'=> 'draft',
        ]);
        $tourClass->create_user  = $user->id;
        $tourClass->save();
    }
}
