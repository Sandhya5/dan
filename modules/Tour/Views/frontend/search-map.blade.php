@extends('layouts.app',['container_class'=>'container-fluid','header_right_menu'=>true])
@section('head')
    <link href="{{ asset('module/tour/css/tour.css?_ver='.config('app.version')) }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset("libs/ion_rangeslider/css/ion.rangeSlider.min.css") }}"/>
    <style type="text/css">
        .bravo_topbar, .bravo_footer {
            display: none
        }.therapist-info h5 {
    border-bottom: 1px solid #00B6ED;
    width: 100% !important;
    margin: 15px 0;
}.has-search-map .bravo_wrap .bravo_search_tour .bravo_form_search_map {
    border-top: 1px solid #e0e0e0;
    flex-shrink: 0;
    padding: 0px !important;}
    .has-search-map .bravo_search_map .results_map {
    flex-shrink: 0;
    width: 100% !important;
    left: 0;
    border-right: 1px solid #e0e0e0;
    position: relative;
/*}.gm-style img {*/
/*    max-width: 250px;*/
/*}*/
.therapist-listing .listing-box {
    margin: 0 0px !important;
    width: 300px;
}
    </style>
    
 
	<link rel="stylesheet" href="{{ asset('listpagecss/css/bootstrap.min.css') }}">
	 <link rel="stylesheet" href="{{ asset('listpagecss/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('listpagecss/css/responsive.css') }}">
 
  
@endsection
@section('content')


<section class="therapist-listing">
   <div class="bravo_search_tour"> 
   <div class="container-fluid">
    

   <div class="row">     
    
        <div class="bravo_search_map">
            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">

            <div class="results_item">
			<div class="bravo_form_search_map">
            @include('Tour::frontend.layouts.search-map.form-search-map')
        </div>
                @include('Tour::frontend.layouts.search-map.advance-filter')
                <div class="listing_items">
                    @include('Tour::frontend.layouts.search-map.list-item')
                </div>
            </div>
			</div>
			
			<div class="results_map">
                <div class="map-loading d-none">
                    <div class="st-loader"></div>
                </div>
                <div id="bravo_results_map" class="results_map_inner"></div>
            </div>
        </div>
		</div>
    </div>
    </div>
	</section>

<!-- end therapis-listing -->



<div class="social-section text-center">
  <div class="container">
    <img src="{{ asset('listpagecss/images/LogoIcon_Colour.png') }}" class="img-fluid mb-5">
    <h3 class="blue-text mb-3 text-center">Social</h3>
    <div class="row d-flex justify-content-center">
      <a href="#"> <img src="{{ asset('listpagecss/images/YoutubeIcon.png') }}"></a>
      <a href="#"> <img src="{{ asset('listpagecss/images/TwitterIcon.png') }}"></a>
      <a href="#"> <img src="{{ asset('listpagecss/images/FacebookIcon.png') }}"></a>
    </div>
  </div>
</div>
<!-- end social-section -->
@endsection

@section('footer')
    {!! App\Helpers\MapEngine::scripts() !!}
    <script>
        var bravo_map_data = {
            markers:{!! json_encode($markers) !!}
        };
    </script>
    <script type="text/javascript" src="{{ asset("libs/ion_rangeslider/js/ion.rangeSlider.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset('module/tour/js/tour-map.js?_ver='.config('app.version')) }}"></script>
@endsection



