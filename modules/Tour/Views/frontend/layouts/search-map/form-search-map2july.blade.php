<?php 
$therapylist = DB::table('bravo_terms')->where('attr_id','4')->get();
$therapyisue = DB::table('bravo_terms')->where('attr_id','6')->get();
$tours = DB::table('bravo_tours')->distinct()->get();
//print_r($tours);
$Accrlist = DB::table('bravo_terms')->where('attr_id','5')->get();

         
?> 
<div class="therapist-info">
 
              <h5 class="blue-text">POST CODE- : {{ request()->get('post_code') }}</h5>
			  
              <div class="dropdown show">

                 <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
   ISSUE
  </a>

              <div class="dropdown-menu" aria-labelledby="dropdownMenuLink1">                

              <form action="" >
			  @foreach($therapyisue as $row)
    <div class="custom-control custom-checkbox">
		
                  
               <input type="checkbox" class="custom-control-input btn-apply-advances" name="terms[1][]"  value="{{ $row->id }}" id="customCheck{{ $row->id }}">
				<label class="custom-control-label" for="customCheck{{ $row->id }}">{{ $row->name }}</label>	<br>
				
    </div>
@endforeach
     

  </form>


              </div>
            </div>


            <div class="dropdown show">

                 <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
   Distance
  </a>

              <div class="dropdown-menu" aria-labelledby="dropdownMenuLink2">                
 <form action="" >
              @if(request('lat') and request('lng'))
                
                    <div class="bravo-filter-price">
                        <?php
                        // $price_min = $pri_from = $tour_min_max_price[0];
                        // $price_max = $pri_to = $tour_min_max_price[1];
                        if (!empty($price_range = Request::query('distance_range'))) {
                            $pri_from = explode(";", $price_range)[0];
                            $pri_to = explode(";", $price_range)[1];
                        }
                        $price_min = 0;
                        $price_max = 500;
                        $currency ='KM';// App\Currency::getCurrency(setting_item('currency_main'))
                        ?>
						
						<input type="hidden" name="lat" id="lat" value="{{request('lat')}}">
						<input type="hidden" name="lng" id="lng" value="{{request('lng')}}">
                        <input type="hidden" class="filter-price irs-hidden-input" name="distance_range"
                            data-symbol=" {{$currency['symbol'] ?? ''}}"
                            data-min="{{$price_min}}"
                            data-max="{{$price_max}}"
                            data-from="{{$pri_from ?? ''}}"
                            data-to="{{$pri_to ?? 500}}"
                            readonly="" value="{{$price_range}}">
                        <button type="button" class="btn btn-link btn-apply-price-range btn-apply-advances">{{__("APPLY")}}</button>
                    </div>
                
            
        @endif

</form>
              </div>
            </div>

             <div class="dropdown show">

                 <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
   Therapy Type
  </a>

              <div class="dropdown-menu" aria-labelledby="dropdownMenuLink3">                

              <form action="">
     @foreach($therapylist as $row)
    <div class="custom-control custom-checkbox">
		
                  
               <input type="checkbox" class="custom-control-input btn-apply-advances" name="terms[1][]"  value="{{ $row->id }}" id="customCheck{{ $row->id }}">
				<label class="custom-control-label" for="customCheck{{ $row->id }}">{{ $row->name }}</label>	<br>
				
    </div>
@endforeach



  </form>


              </div>
            </div>


            <div class="dropdown show">

                 <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
   Age
  </a>

              <div class="dropdown-menu age" aria-labelledby="dropdownMenuLink4">                

              <form action="">
    <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input btn-apply-advances" name="age[]"  value="1" id="customCheck17">
        <label class="custom-control-label" for="customCheck17"> Adults
 </label>
    </div>

    <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input btn-apply-advances" name="age[]"  value="2"  id="customCheck18">
        <label class="custom-control-label" for="customCheck18"> Children (6-10)  </label>
    </div>

    <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input btn-apply-advances" name="age[]"  value="3"  id="customCheck19">
        <label class="custom-control-label" for="customCheck19"> Elders (65+)  </label>
    </div>
    <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input btn-apply-advances" name="age[]"  value="4"  id="customCheck20">
        <label class="custom-control-label" for="customCheck20"> Adolescents / Teenagers (14 to 19) </label>
    </div>

     <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input btn-apply-advances" name="age[]"  value="5"  id="customCheck21">
        <label class="custom-control-label" for="customCheck21"> Preteens / Tweens (11 to 13)
  </label>
    </div>

     <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input btn-apply-advances" name="age[]"  value="6"  id="customCheck22">
        <label class="custom-control-label" for="customCheck22"> Toddlers / Preschoolers (0 to 6)  </label>
    </div>



  </form>


              </div>
            </div>


            <div class="dropdown show">

                 <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
ACCREDITATIONS
  </a>

              <div class="dropdown-menu accre" aria-labelledby="dropdownMenuLink5">                

              <form action="">
   @foreach($Accrlist as $row)
    <div class="custom-control custom-checkbox">
               <input type="checkbox" class="custom-control-input btn-apply-advances" name="terms[1][]"  value="{{ $row->id }}" id="customCheck{{ $row->id }}">
				<label class="custom-control-label" for="customCheck{{ $row->id }}">{{ $row->name }}</label>	<br>
				
    </div>
@endforeach

  </form>


              </div>
            </div>


      <div class="dropdown show">

                 <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
FEE
  </a>

              <div class="dropdown-menu" aria-labelledby="dropdownMenuLink6">                

              <form action="">
			<div class="bravo-filter-price">
                <?php
                $price_min = $pri_from = $tour_min_max_price[0];
                $price_max = $pri_to = $tour_min_max_price[1];
                if (!empty($price_range = Request::query('price_range'))) {
                    $pri_from = explode(";", $price_range)[0];
                    $pri_to = explode(";", $price_range)[1];
                }
                $currency = App\Currency::getCurrency(setting_item('currency_main'))
                ?>
                <input type="hidden" class="filter-price " name="price_range"
                       data-symbol=" {{$currency['symbol'] ?? ''}}"
                       data-min="{{$price_min}}"
                       data-max="{{$price_max}}"
                       data-from="{{$pri_from}}"
                       data-to="{{$pri_to}}"
                       readonly="" value="{{$price_range}}">
                <div class="text-right">
                    <br>
                    <button type="button" class="btn btn-link btn-apply-price-range btn-apply-advances">{{__("APPLY")}}</button>
                   
					
                </div>
            </div>
   
  </form>


              </div>
            </div>


  <!--<div class="dropdown show">

                 <div class="filter-item filter-simple btn btn-secondary">
        <div class="form-group" style="margin: 8px;">
            <a href="#" class="filter-title toggle-advance-filter" data-target="#advance_filters">{{__('More filters')}} </spaan>
        </div>
    </div>
            </div>-->


          </div>



 