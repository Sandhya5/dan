@if(setting_item("tour_enable_review"))

 <div class="booking-reviews mt-4">
      <h4 class="blue-text">Reviews</h4>
      <div class="review-box">
         @if($review_list)
                @foreach($review_list as $item)
                    @php $userInfo = $item->author; @endphp
        <div class="d-flex justify-content-between">
          <h5>{{$userInfo->getDisplayName()}}</h5></br><div class="date">{{display_datetime($item->created_at)}}</div>
                                
          <div class="rating">
                            @if($item->rate_number)
                                    @for( $i = 0 ; $i < 5 ; $i++ )
                                        @if($i < $item->rate_number)
                                            <i class="fa fa-star"></i>
                                        @else
                                            <i class="fa fa-star-o"></i>
                                        @endif
                                    @endfor
                                
                            @endif
         </div>
       </div>

       <p class="mt-4"> {{$item->content}}</p>
@endforeach
            @endif
       

   </div>

 </div>








@endif










