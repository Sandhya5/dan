<?php
namespace Modules\Tour\Controllers;

use Modules\FrontendController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Tour\Models\Tour;
use Modules\Tour\Models\TourCategory;
use Modules\Tour\Models\TourTranslation;
use Modules\Location\Models\Location;
use Modules\Core\Models\Attributes;
use Modules\Tour\Models\TourTerm;
use Modules\Booking\Models\Booking;

class ManageTourController extends FrontendController
{
    protected $tour;
    protected $tour_translation;
    protected $tour_category;
    protected $tour_term;
    protected $attributes;
    protected $location;
    protected $booking;

    public function __construct()
    {
        $this->tour = Tour::class;
        $this->tour_translation = TourTranslation::class;
        $this->tour_category = TourCategory::class;
        $this->tour_term = TourTerm::class;
        $this->attributes = Attributes::class;
        $this->location = Location::class;
        $this->booking = Booking::class;
    }
    public function manageTour(Request $request)
    {
        $this->checkPermission('tour_view');
        $user_id = Auth::id();
        $list_tour = $this->tour::where("create_user", $user_id)->orderBy('id', 'desc');
        $data = [
            'rows' => $list_tour->paginate(5),
            'breadcrumbs'        => [
                [
                    'name' => __('Manage Tours'),
                    'url'   => route('tour.vendor.index'),
                ],
                [
                    'name'  => __('All'),
                    'class' => 'active'
                ],
            ],
            'page_title'         => __("Manage Tours"),
        ];
        return view('Tour::frontend.manageTour.index', $data);
    }

    public function createTour(Request $request)
    {
        $this->checkPermission('tour_create');
        $row = new $this->tour();
        $data = [
            'row'           => $row,
            'translation' => new $this->tour_translation(),
            'tour_category' => $this->tour_category::get()->toTree(),
            'tour_location' => $this->location ::get()->toTree(),
            'attributes'    => $this->attributes::where('service', 'tour')->get(),
            'breadcrumbs'        => [
                [
                    'name' => __('Manage Tours'),
                    'url'   => route('tour.vendor.index'),
                ],
                [
                    'name'  => __('Create'),
                    'class' => 'active'
                ],
            ],
            'page_title'         => __("Create Tours"),
        ];
        return view('Tour::frontend.manageTour.detail', $data);
    }

    public function editTour(Request $request, $id)
    {
        $this->checkPermission('tour_update');
        $user_id = Auth::id();
        $row = $this->tour::where("create_user", $user_id);
        $row = $row->find($id);
        if (empty($row)) {
            return redirect(route('tour.vendor.index'))->with('warning', __('Tour not found!'));
        }
        $translation = $row->translateOrOrigin($request->query('lang'));
        $data = [
            'translation'    => $translation,
            'row'           => $row,
            'tour_category' => $this->tour_category::get()->toTree(),
            'tour_location' => $this->location ::get()->toTree(),
            'attributes'    => $this->attributes::where('service', 'tour')->get(),
            "selected_terms" => $row->tour_term->pluck('term_id'),
            'breadcrumbs'        => [
                [
                    'name' => __('Manage Tours'),
                    'url'   => route('tour.vendor.index'),
                ],
                [
                    'name'  => __('Edit'),
                    'class' => 'active'
                ],
            ],
            'page_title'         => __("Edit Tours"),
        ];
        return view('Tour::frontend.manageTour.detail', $data);
    }

    public function store( Request $request, $id ){
		
        if($id>0){
            $this->checkPermission('tour_update');
            $row = $this->tour::find($id);
            if (empty($row)) {
                return redirect(route('tour.vendor.edit',['id'=>$row->id]));
            }

            if($row->create_user != Auth::id() and !$this->hasPermission('tour_manage_others'))
            {
                return redirect(route('tour.vendor.edit',['id'=>$row->id]));
            }

        }else{
            $this->checkPermission('tour_create');
            $row = new $this->tour();
            $row->status = "draft";
            if(setting_item("tour_vendor_create_service_must_approved_by_admin", 0)){
                $row->status = "publish";
            }
        }

        $row->fillByAttr([
            'title',
            'content',
            'image_id',
            'banner_image_id',
            'short_desc',
            'category_id',
            'location_id',
            'address',
            'map_lat',
            'map_lng',
            'map_zoom',
            'gallery',
            'video',
            'price',
            'sale_price',
            'duration',
            'max_people',
            'min_people',
            'faqs'
        ], $request->input());

        $res = $row->saveOriginOrTranslation($request->input('lang'),true);
        if ($res) {
            if(!$request->input('lang') or is_default_lang($request->input('lang'))) {
                $this->saveTerms($row, $request);
            }
            $row->saveMeta($request);
            if($id > 0 ){
                return back()->with('success',  __('Tour updated') );
            }else{
                return redirect(route('tour.vendor.edit',['id'=>$row->id]))->with('success', __('Tour created') );
            }
        }
    }

    public function saveTerms($row, $request)
    {
        if (empty($request->input('terms'))) {
            $this->tour_term::where('tour_id', $row->id)->delete();
        } else {
            $term_ids = $request->input('terms');
            foreach ($term_ids as $term_id) {
                $this->tour_term::firstOrCreate([
                    'term_id' => $term_id,
                    'tour_id' => $row->id
                ]);
            }
            $this->tour_term::where('tour_id', $row->id)->whereNotIn('term_id', $term_ids)->delete();
        }
    }

    public function deleteTour($id)
    {
        $this->checkPermission('tour_delete');
        $user_id = Auth::id();
        $query = $this->tour::where("create_user", $user_id)->where("id", $id)->first();
        if(!empty($query)){
            $query->delete();
        }
        return redirect(route('tour.vendor.index'))->with('success', __('Delete tour success!'));
    }

    public function bookingReport(Request $request)
    {
        $data = [
            'bookings' => $this->booking::getBookingHistory($request->input('status'), false ,Auth::id() , 'tour'),
            'statues'  => config('booking.statuses'),
            'breadcrumbs'        => [
                [
                    'name' => __('Manage Tours'),
                    'url'   => route('tour.vendor.index'),
                ],
                [
                    'name'  => __('Booking Report'),
                    'class' => 'active'
                ],
            ],
            'page_title'         => __("Booking Report"),
        ];
        return view('Tour::frontend.manageTour.bookingReport', $data);
    }
}
