<?php
namespace Modules\Vendor;

use Illuminate\Support\ServiceProvider;
use Modules\ModuleServiceProvider;

class ModuleProvider extends ModuleServiceProvider
{

    public function boot(){
        $this->loadMigrationsFrom(__DIR__ . '/Migrations');
    }
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouterServiceProvider::class);
    }

    public static function getAdminMenu()
    {
        return [
            //    [
            //        "position"=>120,
            //        'url'        => 'admin/module/vendor/plan',
            //        'title'      => __('Vendor'),
            //        'icon'       => 'icon ion-ios-paper',
            //        'permission' => 'report_view',
            //        'children'   => [
            //            [
            //                'url'        => 'admin/module/vendor/plan',
            //                'title'      => __('Vendor Plans'),
            //                'icon'       => 'icon ion-ios-paper',
            ////                'permission' => 'vendor_plan_view',
            //            ],
            //        ]
            //    ],
        ];
    }
    public static function getTemplateBlocks(){
        return [
            'vendor_register_form'=>"\\Modules\\Vendor\\Blocks\\VendorRegisterForm"
        ];
    }
}
