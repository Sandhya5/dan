
 <section class="blog-article">
   <div class="container">
     <div class="row">
       <div class="col-12">
          <a href="{{ url('/news') }}" class="back pink-text">< BACK </a>
		  
		   @php $category = $row->getCategory; @endphp
                @if(!empty($category))
                    @php $t = $category->translateOrOrigin(app()->getLocale()); @endphp
				
				 <h3 class="blue-text text-center">For   <a href="{{$category->getDetailUrl(app()->getLocale())}}"> {{$t->name ?? ''}}</a>: {{$translation->title}} 
                   
                @endif
         
</h3>
   <div class="d-flex justify-content-center my-4">
    <img src="{{ asset('module/news/images/LogoIcon_Colour.png') }}" class="img-fluid">
    </div>
    <p class="text-center w-75 mx-auto">{!! $translation->content !!}</p>

<div class="d-flex justify-content-center my-5">


 @if($image_url = get_file_url($row->image_id, 'full'))
        <img src="{{ $image_url  }}" alt="{{$translation->title}}" class="img-fluid">
        @endif
		
</div>

<div class="article-slide">
   <h3 class="text-center">Related Articles</h3>
 <div class="owl-carousel owl-theme">
    <div class="item">
     <div class="card">
          <img src="{{ asset('module/news/images/LogoIcon_Colour.png') }}" class="img-fluid">
          <div class="card-text">
           <h5 class="text-white">For parents:How to navigate your child's anxiety</h5>
           <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor  </p>

           <a href="#" class="read-more"> Read > </a>

         </div>
       </div>
    </div>
    <div class="item"><div class="card">
          <img src="{{ asset('module/news/images/LogoIcon_Colour.png') }}" class="img-fluid">
          <div class="card-text">
           <h5 class="text-white">For parents:How to navigate your child's anxiety</h5>
           <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor  </p>

           <a href="#" class="read-more"> Read > </a>

         </div>
       </div></div>
    <div class="item"><div class="card">
          <img src="{{ asset('module/news/images/LogoIcon_Colour.png') }}" class="img-fluid">
          <div class="card-text">
           <h5 class="text-white">For parents:How to navigate your child's anxiety</h5>
           <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor  </p>

           <a href="#" class="read-more"> Read > </a>

         </div>
       </div></div>
    <div class="item"><div class="card">
          <img src="assets/images/resource-img-1.png" class="img-fluid">
          <div class="card-text">
           <h5 class="text-white">For parents:How to navigate your child's anxiety</h5>
           <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor  </p>

           <a href="#" class="read-more"> Read > </a>

         </div>
       </div></div>
    <div class="item"><div class="card">
          <img src="assets/images/resource-img-1.png" class="img-fluid">
          <div class="card-text">
           <h5 class="text-white">For parents:How to navigate your child's anxiety</h5>
           <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor  </p>

           <a href="#" class="read-more"> Read > </a>

         </div>
       </div></div>
    <div class="item"><div class="card">
          <img src="assets/images/resource-img-1.png" class="img-fluid">
          <div class="card-text">
           <h5 class="text-white">For parents:How to navigate your child's anxiety</h5>
           <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor  </p>

           <a href="#" class="read-more"> Read > </a>

         </div>
       </div></div>
    <div class="item"><div class="card">
          <img src="assets/images/resource-img-1.png" class="img-fluid">
          <div class="card-text">
           <h5 class="text-white">For parents:How to navigate your child's anxiety</h5>
           <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor  </p>

           <a href="#" class="read-more"> Read > </a>

         </div>
       </div></div>
    <div class="item"><div class="card">
          <img src="assets/images/resource-img-1.png" class="img-fluid">
          <div class="card-text">
           <h5 class="text-white">For parents:How to navigate your child's anxiety</h5>
           <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor  </p>

           <a href="#" class="read-more"> Read > </a>

         </div>
       </div></div>
    <div class="item"><div class="card">
          <img src="assets/images/resource-img-1.png" class="img-fluid">
          <div class="card-text">
           <h5 class="text-white">For parents:How to navigate your child's anxiety</h5>
           <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor  </p>

           <a href="#" class="read-more"> Read > </a>

         </div>
       </div></div>
    <div class="item"><div class="card">
          <img src="assets/images/resource-img-1.png" class="img-fluid">
          <div class="card-text">
           <h5 class="text-white">For parents:How to navigate your child's anxiety</h5>
           <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor  </p>

           <a href="#" class="read-more"> Read > </a>

         </div>
       </div></div>
    <div class="item"><div class="card">
          <img src="assets/images/resource-img-1.png" class="img-fluid">
          <div class="card-text">
           <h5 class="text-white">For parents:How to navigate your child's anxiety</h5>
           <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor  </p>

           <a href="#" class="read-more"> Read > </a>

         </div>
       </div></div>
    <div class="item"><div class="card">
          <img src="assets/images/resource-img-1.png" class="img-fluid">
          <div class="card-text">
           <h5 class="text-white">For parents:How to navigate your child's anxiety</h5>
           <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor  </p>

           <a href="#" class="read-more"> Read > </a>

         </div>
       </div></div>
</div>
</div>
<!--article slide-->

<div class="start-search">
 <a href="{{ url('/news') }}" class="btn btn-primary find-therapist">Start Your Search</a>
</div>
   </div>
     </div>
   </div>
  
 </section>
 <!-- end blog -->





<!--

<div class="article">
    <div class="header">
        @if($image_url = get_file_url($row->image_id, 'full'))
            <header class="post-header">
                <img src="{{ $image_url  }}" alt="{{$translation->title}}">
            </header>
            <div class="cate">
                @php $category = $row->getCategory; @endphp
                @if(!empty($category))
                    @php $t = $category->translateOrOrigin(app()->getLocale()); @endphp
                    <ul>
                        <li>
                            <a href="{{$category->getDetailUrl(app()->getLocale())}}">
                                {{$t->name ?? ''}}
                            </a>
                        </li>
                    </ul>
                @endif
            </div>
        @endif
    </div>
    <h2 class="title">{{$translation->title}}</h2>
    <div class="post-info">
        <ul>
            @if(!empty($row->getAuthor))
                <li>
                    <span> {{ __('BY ')}} </span>
                    {{$row->getAuthor->getDisplayName() ?? ''}}
                </li>
            @endif
            <li> {{__('DATE ')}}  {{ display_date($row->updated_at)}}  </li>
        </ul>
    </div>
    <div class="post-content"> {!! $translation->content !!}</div>
    <div class="space-between">
        @if (!empty($tags = $row->getTags()) and count($tags) > 0)
            <div class="tags">
                {{__("Tags:")}}
                @foreach($tags as $tag)
                    @php $t = $tag->translateOrOrigin(app()->getLocale()); @endphp
                    <a href="{{ $tag->getDetailUrl(app()->getLocale()) }}" class="tag-item"> {{$t->name ?? ''}} </a>
                @endforeach
            </div>
        @endif
        <div class="share"> {{__("Share")}}
            <a class="facebook share-item" href="https://www.facebook.com/sharer/sharer.php?u={{$row->getDetailUrl()}}&amp;title={{$translation->title}}" target="_blank" original-title="{{__("Facebook")}}"><i class="fa fa-facebook fa-lg"></i></a>
            <a class="twitter share-item" href="https://twitter.com/share?url={{$row->getDetailUrl()}}&amp;title={{$translation->title}}" target="_blank" original-title="{{__("Twitter")}}"><i class="fa fa-twitter fa-lg"></i></a>
        </div>
    </div>
</div>
-->
 
