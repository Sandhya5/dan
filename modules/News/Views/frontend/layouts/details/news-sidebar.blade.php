<aside class="sidebar-right filter">
    @php
        $list_sidebars = setting_item_with_lang("news_sidebar");
    @endphp
    @if($list_sidebars)
		
				@include('News::frontend.layouts.sidebars.category')
	@include('News::frontend.layouts.sidebars.recent_news')
	
	
        @php
            $list_sidebars = json_decode($list_sidebars);
        @endphp
 <!--       @foreach($list_sidebars as $item)
            @include('News::frontend.layouts.sidebars.'.$item->type)
        @endforeach-->
    @endif
</aside>