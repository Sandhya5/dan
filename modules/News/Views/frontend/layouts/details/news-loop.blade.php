
@foreach($rows as $row)
    @php
        $translation = $row->translateOrOrigin(app()->getLocale()); @endphp
		
		
		 <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
      <div class="card">
	  
	              @if($image_tag = get_image_tag($row->image_id,'full'))
              <a href="{{$row->getDetailUrl()}}" class="rescntrimg w-100">
                        {!! $image_tag !!}
                    </a>
             
            @endif
	
        <div class="card-text">
         <h5 class="text-white">   <a class="text-darken" href="{{$row->getDetailUrl()}}"> {{$translation->title}}</a></h5>
         <p class="text-white"> {!! get_exceprt($translation->content) !!} </p>
  <a class="btn-readmore read-more" href="{{$row->getDetailUrl()}}">{{ __('Read More')}}</a>
      

       </div>
     </div>
   </div>
   
@endforeach
