@php
    $list_category = $model_category->with('translations')->get()->toTree();
@endphp
@if(!empty($list_category))
<div class="sidebar-widget widget_category">
<div class="accordion" id="accordion2">
         <?php
        $traverse = function ($categories, $prefix = '') use (&$traverse) {
			
			$abc=1;;
            foreach ($categories as $category) {
                $translation = $category->translateOrOrigin(app()->getLocale());
                ?>
                    
              
  
          <div class="accordion-group">
            <div class="accordion-heading">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?php echo $abc; ?>">
               {{--  {{$prefix}} {{$translation->name}}  --}} <a href="{{ $category->getDetailUrl() }}">{{$prefix}} {{$translation->name}}</a>
              </a>
            </div>
            <div id="collapse<?php echo $abc; ?>" class="accordion-body collapse">
              <div class="accordion-inner">
               <ul class="noflag">
                   <?php
                $traverse($category->children, $prefix . '-');
				
				$abc++;
          ?>
               </ul>
             </div>
           </div>
         </div>
		 
		 <?php
  }
        };
        $traverse($list_category);
		
        ?>
		
   </div>

</div>
@endif