<div class="sidebar-widget widget_bloglist">
    
    <div class="latest mt-4">
        @php $list_blog = $model_news->with(['getCategory','translations'])->orderBy('id','desc')->paginate(5) @endphp
        @if($list_blog)
            @foreach($list_blog as $blog)
                @php $translation = $blog->translateOrOrigin(app()->getLocale()) @endphp
				 <div class="latest mt-4">
    <h5 class="blue-text">Latest</h5>
	
    <a href="#" class="title"><a href="{{ $blog->getDetailUrl(app()->getLocale()) }}">For  @php $translation_cat = $blog->getCategory->translateOrOrigin(app()->getLocale()); @endphp
                                    {{$translation_cat->name ?? ''}}  : {{$translation->title}}</a></a>
    <p class="mt-2">{{ str_limit(get_exceprt($translation->content), $limit = 100, $end = '...') }}</p>
     <a href="{{ $blog->getDetailUrl(app()->getLocale()) }}" class="more">Read ></a>
  </div>
  

               
            @endforeach
        @endif
    </ul>
</div>
