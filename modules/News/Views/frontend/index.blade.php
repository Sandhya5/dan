@extends('layouts.app')
@section('head')
    <link href="{{ asset('module/news/css/news.css?_ver='.config('app.version')) }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset("libs/daterange/daterangepicker.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("libs/ion_rangeslider/css/ion.rangeSlider.min.css") }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset("libs/fotorama/fotorama.css") }}"/>
@endsection
@section('content')
 
        <!--@include('News::frontend.layouts.details.news-breadcrumb')-->
		
 <!-- <section class="innder-header theme-gradient">
   <div class="container">
   	<div class="row topbar">
      <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
       <div class="logo">
        <a href="#"><img src="assets/images/logo.png" align="logo" class="img-fluid"></a>
      </div>
    </div>
    <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
      <div class="join">
       <a href="#" class="btn-join blue-text">Join as a therapist</a>
       <ul class="login-menu">
         <li><a href="#">Login</a></li>
         <li><a href="#">Info hub</a></li>
       </ul>
     </div>
   </div>
 </div> 


</div>
</section>
<!-- end banner-section -->


        <div class="bravo_content">
        
			
		
<section class="info-hub">
  <div class="container">

    <div class="row">
      <div class="col-12 text-center">
        <div class="infohub-title my-5">
       <h3 class="blue-text">Info Hub </h3>
      <img src="{{ asset('module/news/images/LogoIcon_Colour.png') }}" class="img-fluid">
       </div>
      </div>
    </div>



    <div class="row">
      <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 ">
	    @include('News::frontend.layouts.details.news-sidebar')
	  
       
</div>


<div class="col-sm-9 col-md-9 col-lg-9 col-xl-9 resource-center bg-white infohub-content">

      @if($rows->count() > 0)
                            <div class="list-news">
						<div class="row">
                                @include('News::frontend.layouts.details.news-loop')
                                </div>
								<hr>
                                <div class="bravo-pagination">
                                    {{$rows->appends(request()->query())->links()}}
                                    <span class="count-string">{{ __("Showing :from - :to of :total posts",["from"=>$rows->firstItem(),"to"=>$rows->lastItem(),"total"=>$rows->total()]) }}</span>
                                </div>
                            </div>
                        @else
                            <div class="alert alert-danger">
                                {{__("Sorry, but nothing matched your search terms. Please try again with some different keywords.")}}
                            </div>
                        @endif

</div>
</div>
</div>
</section>
    
    </div>
@endsection

 
  