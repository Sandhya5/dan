<?php

namespace Modules\User\Listeners;

use Illuminate\Support\Facades\Mail;
use Modules\User\Emails\RegisteredEmail;
use Modules\User\Emails\VendorApprovedEmail;
use Modules\User\Events\SendMailUserRegistered;
use Modules\User\Events\VendorApproved;
use Modules\User\Models\User;
use Modules\Vendor\Models\VendorRequest;

class SendVendorApprovedMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $user;
    public $vendorRequest;

    const CODE = [
        'first_name' => '[first_name]',
        'last_name'  => '[last_name]',
        'name'       => '[name]',
        'email'      => '[email]',
    ];

    public function __construct(User $user, VendorRequest $vendorRequest)
    {
        $this->user = $user;
        $this->vendorRequest = $vendorRequest;
        //
    }

    /**
     * Handle the event.
     *
     * @param Event $event
     * @return void
     */
    public function handle(VendorApproved $event)
    {
        if($event->user->locale){
            $old = app()->getLocale();
            app()->setLocale($event->user->locale);
        }
        if (!empty(setting_item('vendor_content_email_auto_approved'))) {
            $subject = setting_item('vendor_subject_email_auto_approved',__("Vendor Registration Approved"));
            $body = $this->replaceContentEmail($event, setting_item_with_lang('vendor_content_email_auto_approved', app_get_locale()));
            Mail::to($event->user->email)->send(new VendorApprovedEmail($event->user,$body,$subject));
        }
        if(!empty($old)){
            app()->setLocale($old);
        }

    }
    public function replaceContentEmail($event, $content)
    {
        if (!empty($content)) {
            foreach (self::CODE as $item => $value) {
                $content = str_replace($value, @$event->user->$item, $content);
            }
        }
        return $content;
    }

}