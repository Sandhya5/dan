<?php

    namespace Modules\User\Emails;

    use App\User;
    use Illuminate\Bus\Queueable;
    use Illuminate\Mail\Mailable;
    use Illuminate\Queue\SerializesModels;

    class RegisteredEmail extends Mailable
    {
        use  SerializesModels;

        public $user;
        public $content;
        public $to_address;

        public function __construct(User $user, $content, $to_address)
        {
            $this->user = $user;
            $this->content = $content;
            $this->to_address = $to_address;
        }

        public function build()
        {
            if($this->to_address == 'admin') $subject = setting_item('admin_enable_mail_user_registered_subject');
            else $subject = setting_item('user_content_email_registered_subject');

            if(empty($subject)) $subject = 'New registration';
            return $this->subject($subject)->view('User::emails.registered')->with([
                'user'    => $this->user,
                'content' => $this->content,
                'to'      => $this->to_address,
            ]);
        }
    }
