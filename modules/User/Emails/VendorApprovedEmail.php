<?php

namespace Modules\User\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\User\Events\VendorApproved;

class VendorApprovedEmail extends Mailable
{
    use  SerializesModels;

    public $token;
    const CODE = [
        'buttonReset' => '[button_reset_password]',
    ];
    public $user;
    public $body;
    public $subject;

    public function __construct($user,$body, $subject)
    {
        $this->user = $user;
        $this->body= $body;
        $this->subject= $subject;
    }

    public function build()
    {
        return $this->subject($this->subject)->view('User::emails.vendor-approved')->with([
            'user'    => $this->user,
            'content' => $this->body,
        ]);
    }


}
