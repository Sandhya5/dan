<?php
namespace Modules\Sms\Events;

use Modules\Booking\Models\Booking;
use Illuminate\Queue\SerializesModels;

class SmsBeforeAppointmentEvent
{
    use SerializesModels;
	public $to;
	public $message;
    public function __construct($to,$message)
    {
        $this->to = $to;
        $this->message = $message;
    }
}