<?php

namespace Modules\Sms\Jobs;

use Custom\Booking\Models\BookingSlot;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Modules\Booking\Models\Booking;
use Modules\Sms\Core\Facade\Sms;
use Propaganistas\LaravelPhone\PhoneNumber;

class SmsBeforeAppointmentJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 3;
    public $timeout = 5;
    public $booking;
    public $bookingSlot;
    public $message;
    public function __construct(Booking $booking,$message,BookingSlot $bookingSlot)
    {
    	$this->booking = $booking;
    	$this->bookingSlot = $bookingSlot;
    	$this->message = $message;
        //
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

    	if(!empty($this->booking->phone) and !empty($this->booking->country)){
    		try{
    			$booking = Booking::find($this->booking->id);
    			if(!empty($booking) and !in_array($booking->status,Booking::$notAcceptedStatus)){
				    $to = (string)PhoneNumber::make($this->booking->phone)->ofCountry($this->booking->country);
				    Sms::to($to)->content($this->message)->send();
				    $this->bookingSlot->flag= 'processed';
				    $this->bookingSlot->save();
			    }


		    }catch (\Exception $exception){
    			Log::error($exception);
		    }
	    }else{
    		Log::error('Has errors');
	    }

    }
}
