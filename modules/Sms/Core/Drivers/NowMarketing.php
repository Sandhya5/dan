<?php

	namespace Modules\Sms\Core\Drivers;
	use Modules\Sms\Core\Exceptions\SmsException;

	class NowMarketing extends Driver
	{

		protected $config;

		public function __construct($config)
		{
			$this->config = $config;
		}

		public function send()
		{
			$data = [
				'destination'=>(int)$this->recipient,
				'originator'=>$this->config['from'],
				'message'=>$this->message
			];
			$url = $this->config['url'];
			$curl = $this->curl($url,$data);
			$result = json_decode($curl,true);
			if(!empty($result['status_code'])){
				throw new SmsException($result['raw_response']);
			}else{
				throw new SmsException($result['description']);
			}

			return $result['description'];
		}

		public function curl($url,$data){
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch, CURLOPT_USERPWD, $this->config['userId'].':'.$this->config['apiKey']);
			curl_setopt($ch, CURLOPT_POST, count($data));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			$data = curl_exec($ch);
			curl_close($ch);
			return $data;

		}


	}