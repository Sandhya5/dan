<?php
	return[
		'default' => env('SMS_DRIVER', ''),
		'nexmo'=>[
			'url'=>'https://rest.nexmo.com/sms/json',
			'from'=>env('SMS_NEXMO_FROM','Take a Seat'),
			'key'=>env('SMS_NEXMO_KEY',''),
			'secret'=>env('SMS_NEXMO_SECRET',''),
		],
		'twilio'=>[
			'url'=>'https://api.twilio.com',
			'from'=>env('SMS_TWILIO_FROM','+12019480710'),
			'sid'=>env('SMS_TWILIO_ACCOUNTSID',''),
			'token'=>env('SMS_TWILIO_TOKEN',''),
		],
		'nowMarketing'=>[
			'url'=>'https://api.nowmarketing.co.uk/1.0/sms/send.json',
			'from'=>env('SMS_NOWMARKETING_FROM','TakeASeat'),
			'userId'=>env('SMS_NOWMARKETING_USERID',''),
			'apiKey'=>env('SMS_NOWMARKETING_APIKEY',''),
		],
	];