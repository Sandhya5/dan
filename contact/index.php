<html><head>
    <title>Contact Form Tutorial by Bootstrapious.com</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" type="text/css">
</head>

<body>

    <div class="container">

        <div class="row">

            <div class="col-xl-8 offset-xl-2 py-5">
			<div class="make-heading" align="center">
             <h1>Contact Form</h1>
			 </div>
<form method="post" action="html_form_send.php">
 <div class="col-md-12">
 <div class="form-group">
  <label for="first_name">First Name *</label>
  <input type="text" class="form-control" name="first_name">
</div>
</div>

 <div class="col-md-12">
 <div class="form-group">
  <label for="last_name">Last Name *</label>
  <input type="text" class="form-control" name="last_name">
  </div>
</div>

 <div class="col-md-12">
 <div class="form-group">
  <label for="email">Email Address *</label>
  <input type="text" class="form-control" name="email">
</div>
</div>

 <div class="col-md-12">
 <div class="form-group">
  <label for="telephone">Telephone Number</label>
  <input type="text" class="form-control" name="telephone">
</div>
</div>

 <div class="col-md-12">
 <div class="form-group">
  <label for="comments">Comments *</label>

  <textarea class="form-control" name="comments" rows="6"></textarea>
</div>
</div>

 <div class="col-md-12">
 <div class="form-group">
  <input type="submit" value="Submit">



</div>
</div></form>
</div>

</div></div></body></html>