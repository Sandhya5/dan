<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InfoController extends Controller
{
    public function about()
    {
        return view('About');
    }
    
    public function contact()
    {
        return view('Contact');
    }
    
     public function privacy()
    {
        return view('Privacy');
    }
     public function terms()
    {
        return view('Terms');
    }
    public function cookies()
    {
        return view('Cookies');
    }
     public function ownership()
    {
        return view('Ownership');
    }
}
