<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class ChangeLocaleFromSettings
{

    /**
     * This function checks if language to set is an allowed lang of config.
     *
     * @param string $locale
     **/
    private function setLocale($locale)
    {

    }

    public function setDefaultLocale()
    {
        if(strpos(request()->path(),'install') === false){
            $locale = setting_item('site_locale');

            if ($locale) {
                $this->setLocale($locale);
            }
        }

    }
    public function setUserLocale()
    {
        $user = auth()->user();
        if ($user->locale) {
            $this->setLocale($user->locale);
        } else {
            $this->setDefaultLocale();
        }
    }
    public function setSystemLocale($request)
    {
        if ($request->session()->has('locale')) {
            $this->setLocale(session('locale'));
        } else {
            $this->setDefaultLocale();
        }
    }
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

    }
}
