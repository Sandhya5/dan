<?php

namespace App\Console\Commands;

use Custom\Booking\Models\Booking;
use Illuminate\Console\Command;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Arr;
use Modules\Sms\Jobs\SmsBeforeAppointmentJob;

class SmsBeforeAppointment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sms:beforeAppointment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Find booking status Accepted send sms to customer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	if(!empty(setting_item('sms_before_appointment_enable')) and !empty(setting_item('sms_before_appointment_message'))){
    		$message = setting_item('sms_before_appointment_message');
		    $bookings = Booking::whereNotin('status',Booking::$notAcceptedStatus)->whereDoesntHave('slots',function ($query){
		    	$query->whereIn('flag',['processed','processing']);
		    })->with('slots')->get();

		    $beforeMinutes = setting_item('sms_before_appointment_minutes',15);
		    if(!empty($bookings)){
		    	try{
				    foreach ($bookings as $item=>$booking){
					    $bookingSlots = $booking->slots;
					    if(!empty($bookingSlots)){
						    foreach ($bookingSlots as $slot){
							    $times = $this->getSlotTime($slot->slots);
							    if(!empty($times)){
								    foreach ($times as $hours){
									    $delay = new \DateTime($slot->date);
//									    $delay->modify('+'.$hours.' seconds');
									    $delay->modify('+ '.$hours.'hours');
									    $delay= $delay->modify('-'.$beforeMinutes.' minutes');
									    dispatch(new SmsBeforeAppointmentJob($booking,$message,$slot))->delay($delay);
								    }
							    }
						    $slot->flag='processing';
						    $slot->save();
						    }
					    }
				    }

			    }catch (\Exception $exception){
		    		$this->info($exception->getMessage());
			    }
		    }else{
		    	$this->info(__('No results were found'));
		    }
	    }else{
    		$this->info(__('You must enable function in sms setting'));
	    }

        //
    }
    public function getSlotTime(array $slots){
    	$return =[];
	    ksort($slots);
	    $return[]=Arr::first($slots);
	    foreach ($slots as $i=>$v){
	    	if(isset($slots[$i-1]) and $i!=0){
			    if($v-1 != $slots[$i-1]){
			    	$return[]= $v;
			    }
		    }
	    }
	    return $return;
    }
}
