<?php
namespace Custom\Tour\Controllers;

use Custom\Booking\Models\BookingSlot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Booking\Models\Booking;
use Modules\FrontendController;
use Modules\Tour\Models\Tour;
use Modules\Tour\Models\TourDate;

class AvailabilityController extends \Modules\Tour\Controllers\AvailabilityController {

    protected $tourClass;
    /**
     * @var TourDate
     */
    protected $tourDateClass;
    /**
     * @var Booking
     */
    protected $bookingClass;


    public function __construct()
    {
    	parent::__construct();
    	$this->tourClass  = \Custom\Tour\Models\Tour::class;
    	$this->bookingClass  = \Custom\Booking\Models\Booking::class;
    	$this->tourDateClass = \Custom\Tour\Models\TourDate::class;
    }


	public function index(Request $request){
        $this->checkPermission('tour_create');

        $q = $this->tourClass::query();

        if($request->query('s')){
            $q->where('title','like','%'.$request->query('s').'%');
        }

        if(!$this->hasPermission('tour_manage_others')){
            $q->where('create_user',$this->currentUser()->id);
        }

        $q->orderBy('bravo_tours.id','desc');

        $rows = $q->paginate(15);

        $current_month = strtotime(date('Y-m-01',time()));

        if($request->query('month')){
            $date = date_create_from_format('m-Y',$request->query('month'));
            if(!$date){
                $current_month = time();
            }else{
                $current_month = $date->getTimestamp();
            }
        }
        $breadcrumbs = [
            [
                'name' => __('Tours'),
                'url'  => 'admin/module/tour'
            ],
            [
                'name'  => __('Availability'),
                'class' => 'active'
            ],
        ];
        $page_title = __('Tours Availability');

        return view($this->indexView,compact('rows','breadcrumbs','current_month','page_title','request'));
    }

    public function loadDates(Request $request){

        $request->validate([
            'id'=>'required',
            'start'=>'required',
            'end'=>'required',
        ]);

        $tour = $this->tourClass::find($request->query('id'));
        if(empty($tour)){
            $this->sendError(__('Tour not found'));
        }
        $for_single = $request->query('for_single');

        $allDates = $slots = [];
	    for($i=0;$i<=23;$i++){
		    $slots[$i]=(new BookingSlot())->getSlotName($i);
	    }
        $dates = collect();
        for($i = strtotime($request->query('start')); $i <= strtotime($request->query('end')); $i+= DAY_IN_SECONDS)
        {
            if($for_single){
                if(strtotime('today') > $i){
                    continue;
                }
            }
            $dates->add([
                'date'=>date('Y-m-d',$i)
            ]);
        }

        $tourDatesTmp = $this->tourDateClass::where('target_id', $tour->id)
            ->whereIn('start_date', $dates->pluck('date') )
            ->groupBy(['start_date'])
            ->get();
        $bookingDataTmp = BookingSlot::query()->where([
            'object_id'=>$tour->id,
        ])
            ->whereNotIn('status',$this->bookingClass::$notAcceptedStatus)
            ->join('bravo_bookings','bravo_booking_slot.booking_id','=','bravo_bookings.id')
            ->whereIn('bravo_booking_slot.date',$dates->pluck('date'))
            ->with('booking')
            ->where("bravo_bookings.deleted_at",null)
            ->get();

        $bookingData = [];
        foreach ($bookingDataTmp as $item){
            $bookingData[$item['date']][] = $item;
        }
        $allSlots = [];
        $meta = $tour->meta;

        $start_date = strtotime(date('Y-m-d 00:00:00',strtotime($request->query('start'))));
        $end_date = strtotime(date('Y-m-d 23:59:59',strtotime($request->query('end'))));
        $open_hours = [];
        $enable_open_hours = false;
        if ($meta) {
            // Open Hours
            $enable_open_hours = true;
            $open_hours = $meta->open_hours;
        }
	    for($i = $start_date; $i <= $end_date; $i+= 60*60)
        {

            $date = [
                'id'=>rand(0,999),
                'active'=>0,
                'price'=>$tour->price,
                'is_default'=>true,
                'slots'=>[],
                'textColor'=>'black',
                'backgroundColor' => '#83e5e6',
                'slot'=>date('G',$i),
                'start_date'=>date('Y-m-d',$i),
                'open_hour'=>1
            ];
            $textStr = "<br>".date('G',$i).' - '.date('Ga',$i + 60*60);

            $date['price_html'] = format_money($date['price']);
            $date['max_guests'] = $tour->max_people;
            $date['title'] = $date['event']  = __("Available - :money",["money"=>format_money($tour->price)]).$textStr;
            $date['start'] = $date['end'] = date('Y-m-d H:i:s',$i);
            if($for_single){
                $date['start'] = $date['end'] = date('Y-m-d',$i);
            }
            $date['active'] = 1;
            // Open Hours
            if ($enable_open_hours) {
                $nDate = date('N', $i);
                if (!isset($open_hours[$nDate]) or empty($open_hours[$nDate]['enable'])) {
                    $date['active'] = 0;
                    $date['open_hour'] = 0;
                    $date['event']= __("Unavailable").$textStr;
                    $date['title']= __("Unavailable").$textStr;
                    $date['backgroundColor']= '#d7e0e5';

                }else{
                    $item_open_hours = $open_hours[$nDate];
                    $from_time = strtotime(date('Y-m-d',$i).' '.$item_open_hours['from'].':00');
                    $to_time = strtotime(date('Y-m-d',$i).' '.$item_open_hours['to'].':00 -1 minutes');
                    if($i < $from_time or $i > $to_time){
                        $date['open_hour'] = 0;
                        $date['active'] = 0;
                        $date['event']= __("Unavailable").$textStr;
                        $date['title']= __("Unavailable").$textStr;
                        $date['backgroundColor']= '#d7e0e5';
                    }
                }
            }
            $allSlots[date('Y-m-d H',$i)] = $date;
        }
        foreach ($tourDatesTmp as $tourDate){
            $d_str = date('Y-m-d',strtotime($tourDate['start_date']));
            if(!empty($tourDate->slots) and is_array($tourDate->slots)){
                foreach ($tourDate->slots as $slot){
                    $time = sprintf("%02d", $slot['slot']);

                    $textStr = "<br>".($slot['slot'])." - ".($slot['slot'] + 1).($slot['slot'] > 11 ? "pm":"am");

                    if(array_key_exists($d_str.' '.$time,$allSlots) and empty($allSlots[$d_str.' '.$time]['open_hour'])){
                        continue;
                    }
                    if(!empty($slot['active']) and $slot['active'] == 1){

                        $allSlots[$d_str.' '.$time] = array_merge($allSlots[$d_str.' '.$time],[
                            'active'=>1,
                            'price'=>$slot['price'] ?? 0,
                            'event'=>__("Available - :money",["money"=>format_money($slot['price'] ?? 0)]).$textStr,
                            'title'=>__("Available - :money",["money"=>format_money($slot['price'] ?? 0)]).$textStr
                        ]);
                    }else{
                        $allSlots[$d_str.' '.$time]['active']= 0;
                        $allSlots[$d_str.' '.$time]['price']= $slot['price'] ?? 0;
                        $allSlots[$d_str.' '.$time]['event']= __("Unavailable").$textStr;
                        $allSlots[$d_str.' '.$time]['title']= __("Unavailable").$textStr;
                        $allSlots[$d_str.' '.$time]['backgroundColor']= '#d7e0e5';

                        if($slot['active'] == 2){
                            $allSlots[$d_str.' '.$time]['event'] = $allSlots[$d_str.' '.$time]['title'] = __("Booking with :name",['name'=>$slot['first_name'] ?? ""]).$textStr;
                            $allSlots[$d_str.' '.$time]['active'] = 2;
                            $allSlots[$d_str.' '.$time]['notes'] = $slot['notes'] ?? '';
                            $allSlots[$d_str.' '.$time]['first_name'] = $slot['first_name'] ?? '';
                            $allSlots[$d_str.' '.$time]['backgroundColor'] ='#cc932c';

                        }
                    }
                }
            }
        }
        foreach ($bookingDataTmp as $bookingData){
            $d_str = date('Y-m-d',strtotime($bookingData['date']));

            $data_detail = "";
            if(!$for_single){
                $data_detail = !empty($service = $bookingData->service) ? view('Tour::frontend/booking/detail-modal',['booking'=>$bookingData,'service'=>$service])->render() : "";
            }

            if(!empty($bookingData->slots)){
                foreach ($bookingData->slots as $slot){
                    $time = sprintf("%02d", $slot);

                    $textStr = "<br>".($slot)." - ".($slot + 1).($slot > 11 ? "pm":"am");

                    $allSlots[$d_str.' '.$time] = array_merge($allSlots[$d_str.' '.$time],[
                        'active'=>0,
                        'event'=>__("Booking with :name",['name'=>$bookingData->booking->first_name]).$textStr,
                        'title'=>__("Booking with :name",['name'=>$bookingData->booking->first_name]).$textStr,
                        'eventBackgroundColor' => '#ffa2b4',
                        'booking_online'=>1,
                        'booking_detail' =>$data_detail,
                        'booking_id' =>$bookingData->booking_id
                    ]);
                }
            }
        }
        if(!$for_single){
            $data = array_values($allSlots);
        }else{
            $data  = [];
            foreach ($allSlots as $slotData){
                if(empty($data[$slotData['start_date']])) $data[$slotData['start_date']] = $slotData;
                if($slotData['active'] == 1){
                    $data[$slotData['start_date']]['slots'][$slotData['slot']] = sprintf("%02d:00", $slotData['slot']);
                }
            }
            foreach ($data as $k=>$item){
                if(empty($item['slots'])){
                    $data[$k]['active'] = 0;
                }else{
                    $data[$k]['active'] = 1;
                }
            }
            $data = array_values($data);
        }


        return response()->json($data);
    }
	public function store(Request $request){

		$request->validate([
			'target_id'=>'required',
			'start_date'=>'required',
            'slot'=>'required'
		]);

		$tour = $this->tourClass::find($request->input('target_id'));
		$target_id = $request->input('target_id');

		if(empty($tour)){
			$this->sendError(__('Tour not found'));
		}

		if(!$this->hasPermission('tour_manage_others')){
			if($tour->create_user != Auth::id()){
				$this->sendError("You do not have permission to access it");
			}
		}

		$date = strtotime($request->input('start_date'));

		$tourDate = \Custom\Tour\Models\TourDate::where([
		    'target_id'=>$request->input('target_id'),
            'start_date'=>date('Y-m-d',$date)
        ])->first();
        $slot = $request->input('slot');
		if(empty($tourDate))
        {
            $tourDate = new \Custom\Tour\Models\TourDate();
            $tourDate->start_date = date('Y-m-d',$date);
            $tourDate->target_id = $target_id;
            $tourDate->slots = [
                [
                    'slot'=>$slot,
                    'active'=>$request->input('active'),
                    'first_name'=>$request->input('first_name'),
                    'notes'=>$request->input('notes'),
                    'price'=>$request->input('price'),
                ]
            ];
        }else{
		    $check = false;
		    $tmpSlots =  $tourDate->slots;
		    foreach ( $tmpSlots as $k=>$slotArr){
		        if($slotArr['slot'] == $slot){

                    $tmpSlots[$k] = [
                        'slot'=>$slot,
                        'active'=>$request->input('active'),
                        'first_name'=>$request->input('first_name'),
                        'notes'=>$request->input('notes'),
                        'price'=>$request->input('price'),
                    ];
                    $check = true;
                }
            }
            if(!$check){
                $tmpSlots[] = [
                    'slot'=>$slot,
                    'active'=>$request->input('active'),
                    'first_name'=>$request->input('first_name'),
                    'notes'=>$request->input('notes'),
                    'price'=>$request->input('price'),
                ];
            }
            $tourDate->slots = $tmpSlots;
        }

		$tourDate->save();

		$this->sendSuccess([],__("Update Success"));
	}

}
