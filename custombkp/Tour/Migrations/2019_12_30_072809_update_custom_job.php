<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCustomJob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('bravo_tours', function (Blueprint $table) {
            $table->tinyInteger('enable_sale_of_first_booking')->default(0)->nullable();
	        $table->text('highlights')->nullable();

        });
        Schema::table('bravo_tour_translations', function (Blueprint $table) {
	        $table->text('highlights')->nullable();
        });
	    Schema::table('bravo_bookings', function (Blueprint $table) {
		    $table->boolean('first_booking')->nullable();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
