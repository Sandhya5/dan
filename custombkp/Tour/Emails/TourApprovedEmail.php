<?php

namespace Custom\Tour\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\User\Events\VendorApproved;

class TourApprovedEmail extends TourSubmitedEmail
{
    public function build()
    {
        $subject = setting_item('tour_approved_email_subject');
        $body = setting_item('tour_approved_email_body');
        if(!$subject or !$body) return;

        $body = $this->replaceContentEmail($body);

        return $this->subject($subject)->view('Tour::emails.default')->with([
            'content' => $body,
        ]);
    }


}
