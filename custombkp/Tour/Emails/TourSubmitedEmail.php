<?php

namespace Custom\Tour\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\User\Events\VendorApproved;

class TourSubmitedEmail extends Mailable
{
    use  SerializesModels;

    public $token;

    const CODE = [
        'profile_title' => '[profile_title]',
        'email' => '[email]',
        'slug'=>'[slug]'
    ];
    public $tour;
    public $body;
    public $subject;

    public function __construct($tour)
    {
        $this->tour = $tour;
    }

    public function replaceContentEmail($content)
    {
        if (!empty($content)) {
            foreach (self::CODE as $item => $value) {
                switch ($item){
                    case "full_name":
                    case "profile_title":
                        $replace =  $this->tour->title;
                        break;
                    case "first_name":
                        $replace =  $this->tour->author->first_name;
                        break;
                    case "last_name":
                        $replace =  $this->tour->author->last_name;
                        break;
                    case "email":
                        $replace =  $this->tour->author->email;
                        break;
                    case "slug":
                        $replace =  $this->tour->slug;
                        break;
                    default:
                        $replace = '';
                        break;
                }
                $content = str_replace($value, $replace, $content);
            }
        }
        return $content;
    }

    public function build()
    {
        $subject = setting_item('tour_submited_email_subject');
        $body = setting_item('tour_submited_email_body');
        if(!$subject or !$body) return;

        $body = $this->replaceContentEmail($body);

        return $this->subject($subject)->view('Tour::emails.default')->with([
            'content' => $body,
        ]);
    }


}
