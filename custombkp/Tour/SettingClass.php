<?php
namespace  Custom\Tour;

use Modules\Core\Abstracts\BaseSettingsClass;

class SettingClass extends BaseSettingsClass
{
    public static function getSettingPages()
    {
	    return [
		    [
			    'id'   => 'custom_tour',
			    'title' => __("Custom Profile Settings"),
			    'position'=>211,
			    'view'=>"Tour::admin.settings.custom.tour",
			    "keys"=>[
                    'tour_attr_on_form_search',
				    'tour_attr_on_page_detail',
				    'tour_attr_on_page_search',
				    'tour_attr_book_form_detail',
				    'tour_attr_book_form_detail_label',

				    'custom_vendor_page_id',
				    'custom_attribute_page_intro',

				    'custom_page_introductions_ownership_policy',
				    'custom_page_thanks_after_register_vendor',
			    ],
			    'html_keys'=>[
			    ]
		    ]
	    ];

    }
}
