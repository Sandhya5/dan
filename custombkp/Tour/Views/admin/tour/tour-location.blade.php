<div class="panel">
    <div class="panel-title"><strong>{{__("Locations")}}</strong></div>
    <div class="panel-body">
        <div class="form-group">
            <label class="control-label">{{__("Enter your address")}}</label>
            <input type="text" name="address" class="form-control bravo_searchbox" placeholder="{{__("Enter your address")}}" value="{{old("address",$translation->address)}}">
            <span style="font-size: 12px;font-style: italic;color: #950a00;margin-top: 5px;">{{ __("Required field (*)") }}</span>
        </div>
        <div class="row">
            <div class="col-md-6">
                @include('Tour::admin/tour/country')
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">{{__("Enter your city")}}</label>
                    <input type="text" name="city" class="form-control" placeholder="{{__("Enter your city")}}" value="{{old("city",$row->city)}}">
                    <span style="font-size: 12px;font-style: italic;color: #950a00;margin-top: 5px;">{{ __("Required field (*)") }}</span>
                </div>
            </div>
        </div>
        @if(is_default_lang())
            <div class="form-group">
                <label class="control-label">{{__("Map Engine")}}</label>
                <div class="control-map-group">
                    <div id="map_content" style="margin: 0"></div>
                    <div class="g-control d-none">
                        <div class="form-group">
                            <label>{{__("Map Lat")}}:</label>
                            <input type="text" name="map_lat" class="form-control" value="{{old("map_lat",$row->map_lat)}}" >
                        </div>
                        <div class="form-group">
                            <label>{{__("Map Lng")}}:</label>
                            <input type="text" name="map_lng" class="form-control" value="{{old("map_lng",$row->map_lng)}}" >
                        </div>
                        <div class="form-group">
                            <label>{{__("Map Zoom")}}:</label>
                            <input type="text" name="map_zoom" class="form-control" value="{{old("map_zoom",$row->map_zoom ?? "8")}}" >
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
