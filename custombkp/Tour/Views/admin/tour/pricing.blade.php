<div class="panel">
    <div class="panel-title"><strong>{{__("Pricing")}}</strong></div>
    <div class="panel-body">
        @if(is_default_lang())
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="control-label">{{__("Price per hour")}}</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" style="padding: 4px 15px;">
                                    <?php  $currency = App\Currency::getCurrency(setting_item('currency_main')) ?>
                                    {{$currency['symbol'] ?? ''}}
                                </span>
                            </div>
                            <input type="number" min="0" name="price" class="form-control" value="{{old("price",$row->price)}}" placeholder="{{__("Price per hour")}}">
                        </div>
                        <span style="font-size: 12px;font-style: italic;color: #950a00;margin-top: 5px;">{{ __("Required field (*)") }}</span>
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="checkbox" name="enable_sale_of_first_booking" @if(!empty(old("enable_sale_of_first_booking",$row->enable_sale_of_first_booking))) checked @endif value="1">
                            {{__('50% off first booking')}}
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="checkbox" name="discount_for_4_slot" @if(!empty(old("discount_for_4_slot",$row->discount_for_4_slot))) checked @endif value="1">
                            {{__('5% off for booking more than 4 slots')}}
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="checkbox" name="discount_for_10_slot" @if(!empty(old("discount_for_10_slot",$row->discount_for_10_slot))) checked @endif value="1">
                            {{__('10% off for booking more than 10 slots')}}
                        </label>
                    </div>

                </div>
            </div>
            <hr>
        @endif
    </div>
</div>