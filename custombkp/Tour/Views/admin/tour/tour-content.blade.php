<div class="panel">
    <input type="hidden" name="min_people" value="0">
    <input type="hidden" name="max_people" value="0">
    <div class="panel-title"><strong>{{__("Edit Your Profile Here")}}</strong></div>
    <div class="panel-body">
        @if(is_default_lang())
        <div class="form-group">
            <label class="control-label">{{__("Profile Picture")}}</label>
            <div class="form-group-image">
                {!! \Modules\Media\Helpers\FileHelper::fieldUpload('image_id', old("image_id",$row->image_id)) !!}
            </div>
        </div>
        @endif
        <div class="form-group">
            <label>{{__("Full name")}}</label>
            <input type="text" value="{{ old("title",$translation->title) }}" placeholder="{{__("Full name")}}" name="title" class="form-control">
            <span style="font-size: 13px;font-style: italic;color: #950a00">{{ __("Required field(*)") }}</span>
        </div>
        <div class="form-group">
            <label class="control-label">{{__("Profile overview")}}</label> <br>
            <small>{{ __("Max 300 words") }}</small>
            <div class="">
                <textarea name="content" class="d-none has-ckeditor" cols="30" rows="10">{{old("content",$translation->content)}}</textarea>
            </div>
            <span style="font-size: 13px;font-style: italic;color: #950a00">{{ __("Required field(*)") }}</span>
        </div>
        <div class="form-group-item">
            <label class="control-label">{{__('HIGHLIGHTS')}}</label>
            <div class="g-items-header">
                <div class="row">
                    <div class="col-md-10">{{__('Content')}}</div>
                    <div class="col-md-2"></div>
                </div>
            </div>
            <div class="g-items mb-3">
                @php $translation->highlights = old("highlights",$translation->highlights); @endphp
                @if(!empty($translation->highlights))
                    @php if(!is_array($translation->highlights)) $translation->highlights = json_decode($translation->highlights); @endphp
                    @foreach($translation->highlights as $key=>$highlight)
                        <div class="item" data-number="{{$key}}">
                            <div class="row">
                                <div class="col-md-10">
                                    <textarea name="highlights[{{$key}}][content]" class="form-control" placeholder="...">{{$highlight['content']}}</textarea>
                                </div>
                                <div class="col-md-2">
                                    @if(is_default_lang())
                                        <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="text-right">
                @if(is_default_lang())
                    <span class="btn btn-info btn-sm btn-add-item"><i class="icon ion-ios-add-circle-outline"></i> {{__('Add item')}}</span>
                @endif
            </div>
            <div class="g-more d-none">
                <div class="item" data-number="__number__">
                    <div class="row">
                        <div class="col-md-10">
                            <textarea __name__="highlights[__number__][content]" class="form-control" placeholder="..."></textarea>
                        </div>
                        <div class="col-md-2">
                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
