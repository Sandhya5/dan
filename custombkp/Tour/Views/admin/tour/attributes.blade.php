@foreach ($attributes as $attribute)
    <div class="panel">
        <div class="panel-title"><strong>{{__(':name',['name'=>$attribute->name])}}</strong></div>
        <div class="panel-body">
            <div class="terms-scrollable">
                @php
                    $selected = old("terms", $selected_terms->toArray() );
                @endphp
                @foreach($attribute->terms as $term)
                    <label class="term-item">
                        <input @if(!empty($selected) and in_array($term->id,$selected)) checked @endif type="checkbox" name="terms[]" value="{{$term->id}}">
                        <span class="term-name">{{$term->name}}</span>
                    </label>
                @endforeach
            </div>
        </div>
    </div>
@endforeach