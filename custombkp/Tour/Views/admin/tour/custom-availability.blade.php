<div class="panel">
    <div class="panel-title"><strong>{{__("Availability")}}</strong></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-12 col-lg-3">
                <div id="months-calendar" class="months-calendar"></div>
                <hr>
                <h6 class="mb-3">{{__('Key')}}</h6>
                <p><span class="badge d-inline text-white availability-key available" ><i class="fa fa-check"></i></span> {{__('Available')}}</p>
                <p><span class="badge d-inline text-white availability-key" ><i class="fa fa-check"></i></span> Unavailable</p>
                <p><span class="badge d-inline text-white availability-key takeaseat" ><i class="fa fa-check"></i></span> Take a Seat Bookings</p>
                <p><span class="badge d-inline text-white availability-key other" ><i class="fa fa-check"></i></span> Other Bookings</p>
            </div>
            <div class="col-12 col-lg-9">
                <div id="dates-calendar" class="dates-calendar"></div>
            </div>
        </div>
        <div id="bravo_modal_calendar_old" class="modal fade">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{__('Date Information')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row form_modal_calendar form-horizontal" novalidate onsubmit="return false">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{__('Date Ranges')}}</label>
                                    <input readonly type="text" class="form-control has-daterangepicker">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>{{__('Start Time')}}</label>
                                    <select v-model="form.start_time" class="form-control">
                                        <option v-for="n in 23" :value="n">@{{ n }}:00</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>{{__('End Time')}}</label>
                                    <select v-model="form.end_time" class="form-control">
                                        <option v-for="n in 23" :value="n">@{{ n }}:00</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{__('Status')}}</label>
                                    <br>
                                    <label><input true-value=1 false-value=0 type="checkbox" v-model="form.active"> {{__('Available for booking?')}}</label>
                                </div>
                            </div>
                            <div class="col-md-6" v-if="person_types">
                                <div class="col-md-12" v-for="(type,index) in person_types">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>{{__("Name")}}</label>
                                                <input type="text" readonly class="form-control" v-model="person_types[index].name">
                                            </div>
                                            <div class="col-md-4">
                                                <label>{{__("Min")}}</label>
                                                <input type="text" v-model="person_types[index].min" class="form-control">
                                            </div>
                                            <div class="col-md-3">
                                                <label>{{__("Max")}}</label>
                                                <input type="text" v-model="person_types[index].max" class="form-control">
                                            </div>
                                            <div class="col-md-3">
                                                <label>{{__("Price")}}</label>
                                                <input type="text" v-model="person_types[index].price" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" v-else>
                                <div class="form-group">
                                    <label>{{__('Price')}}</label>
                                    <input type="text" v-model="form.price" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div v-if="lastResponse.message">
                            <br>
                            <div class="alert" :class="!lastResponse.status ? 'alert-danger':'alert-success'">@{{ lastResponse.message }}</div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="button" class="btn btn-primary" @click="saveForm">{{__('Save changes')}}</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="bravo_modal_calendar" class="modal fade">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{__('Slot Information')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row" novalidate onsubmit="return false">
                            <div class="col-12">
                                <p>Please click on the correct status for this time slot:</p>
                            </div>
                            <div class="col-12 ">
                                <div class="form-group d-flex justify-content-around">
                                    <span :class="{active:form.active == 0}" class="btn rounded text-white availability-key" @click="form.active=0" >Unavailable</span>
                                    <span :class="{active:form.active == 1}" class="btn rounded text-white availability-key available" @click="form.active=1" >Available</span>
                                    <span :class="{active:form.active == 2}" class="btn rounded text-white availability-key other" @click="form.active=2" >Other Booking</span>
                                </div>
                            </div>
                            <div class="col-12" v-show="form.active==2">
                                <div class="form-group">
                                    <label>{{__('Customer Name:')}}</label>
                                    <input type="text" v-model="form.first_name" class="form-control" placeholder="{{__("Enter customer name here...")}}">
                                </div>
                                <div class="form-group">
                                    <label>{{__('Additional Notes:')}}</label>
                                    <textarea class="form-control" v-model="form.notes" rows="15"  style="min-height: 150px;"></textarea>
                                </div>
                            </div>
                            <div class="col-12" v-show="form.active==1">
                                <div class="form-group">
                                    <label>{{__('Price:')}}</label>
                                    <input type="text" v-model="form.price" class="form-control" placeholder="{{__("Enter custom price")}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-if="lastResponse.message">
                        <br>
                        <div class="alert" :class="!lastResponse.status ? 'alert-danger':'alert-success'">@{{ lastResponse.message }}</div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="button" class="btn btn-primary" @click="saveForm">{{__('Save changes')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

