<div class="text-block-featured">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="title">
                    {{ $title }}
                </div>
                <div class="context">
                    {!! $content !!}
                </div>
            </div>
            <div class="col-lg-6">
                @if(!empty($list_item))
                    <div class="list-item">
                        @foreach($list_item as $key=>$row)
                            <div class="col">
                                <div class="item">
                                    @if($row['icon_image'])
                                        <img src="{{ get_file_url($row['icon_image'] , 'full') }}"  alt="{{$title}}">
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>