<?php
$style = $style ?? "normal";
?>

<div class="container">
    <div class="bravo-list-locations {{ $style }}">
        <div class="title">
            {{$title}}
        </div>
        <div class="list-item">
            @if($style == "normal")
                <div class="row">
                    @foreach($list_term as $key=>$row)
                        <?php
                        $size_col = 4;
                        if($key == 0){
                            $size_col = 8;
                        }
                        ?>
                        <div class="col-lg-{{$size_col}}">
                            @include('Tour::frontend.blocks.list-terms.loop',['row'=>$row])
                        </div>
                    @endforeach
                </div>
            @endif
            @if($style == "style2")
                <div class="owl-carousel">
                    @foreach($list_term as $key=>$row)
                        <div class="item">
                            @if($row->image_id)
                                <img src="{{ get_file_url($row->image_id , 'full') }}"  alt="{{$row->name}}">
                            @endif
                        </div>
                    @endforeach
                </div>
            @endif
            @if($style == "style3")
                @foreach($list_term as $key=>$row)
                    @php
                        $translation = $row->translateOrOrigin(app()->getLocale());
                    @endphp
                    <a href="{{ $row->getLinkForPageSearchTour($row->id) }}">{{$translation->name}}</a>
                @endforeach
            @endif
        </div>
    </div>
</div>