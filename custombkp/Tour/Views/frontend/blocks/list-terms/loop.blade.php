@php
    $translation = $row->translateOrOrigin(app()->getLocale());
@endphp
<div class="destination-item @if(!$row->image_id) no-image  @endif">
    <a href="{{ $row->getLinkForPageSearchTour($row->id) }}">
        <div class="image" @if($row->image_id) style="background: url({{ get_file_url($row->image_id , 'full') }})" @endif >
            <div class="effect"></div>
            <div class="content">
                <h4 class="title">{{$translation->name}}</h4>
                <div class="desc">{{$row->getDisplayNumberTourInTerm($row->id)}}</div>
            </div>
        </div>
    </a>
</div>
