<div class="about-featured" style="background-image: url('{{get_file_url( $bg_image , 'full')}}') !important">
    <div class="container">
        <div class="title">
            {{$title}}
        </div>
        <div class="sub-title">
            {{$sub_title}}
        </div>
        <div class="list-item">
            @if(!empty($list_item))
                @foreach($list_item as $key=>$row)
                    <div class="item">
                        <div class="icon">
                            <img src="{{get_file_url( $row['icon_image'] , 'full')}}">
                        </div>
                        <div class="text">
                            <div class="text-1">
                                {{$row['title']}}
                            </div>
                            <div class="text-2">
                                {{$row['sub_title']}}
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>