<div class="bravo-form-search-tour" style="background: #fff">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="g-form-control">
                    <h1 class="text-heading">{!! $title !!}</h1>
                    <div class="sub-heading">{{$sub_title}}</div>
                    @include('Tour::frontend.layouts.search.form-search')
                </div>
            </div>
        </div>
    </div>
</div>