@section('head')
    <link href="{{ asset('module/vendor/css/vendor-register.css?_ver='.config('app.version')) }}" rel="stylesheet">
@endsection

<div class="bravo-vendor-form-register ver-2">
    <div class="container">
        <form class="form bravo-form-register-vendor" method="post" action="{{route('vendor.register')}}">
            @csrf
            <div class="row">
                <div class="col-lg-3">
                    <h2>{{$title}}</h2>
                </div>
                <div class="col-lg-7">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>
                                    {{__("Your name")}}
                                </label>
                                <input type="text" class="form-control" name="full_name" autocomplete="off">
                                <span class="invalid-feedback error error-full_name"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>
                                    {{__("Your email")}}
                                </label>
                                <input type="email" class="form-control" name="email" autocomplete="off">
                                <span class="invalid-feedback error error-email"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>
                                    {{__("Your phone number")}}
                                </label>
                                <input type="text" class="form-control" name="phone" autocomplete="off">
                                <span class="invalid-feedback error error-phone"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>
                                    {{__("Your password")}}
                                </label>
                                <input type="password" class="form-control" name="password" autocomplete="off">
                                <span class="invalid-feedback error error-password"></span>
                            </div>
                        </div>
                    </div>
                    <div class="message-error"></div>
                </div>
                <div class="col-lg-2 f-submit">
                    <input id="term" type="checkbox" name="term" checked class="d-none">
                    <button type="submit" class="btn btn-primary form-submit">
                        {{ __('Sign Up') }}
                        <span class="spinner-grow spinner-grow-sm icon-loading" role="status" aria-hidden="true" style="display: none"></span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@section('footer')
    <script type="text/javascript" src="{{ asset("/module/vendor/js/vendor-register.js?_ver=".config('app.version')) }}"></script>
@endsection
