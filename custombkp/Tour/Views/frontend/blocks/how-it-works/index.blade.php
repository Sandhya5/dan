<div class="how-it-works">
    <div class="container">
        <div class="title">
            {{$title}}
        </div>
        <div class="list-item">
            @if(!empty($list_item))
                @foreach($list_item as $key=>$row)
                    <div class="item item-{{$key+1}}">
                        <div class="left">
                            @if($row['icon_image'])
                                <img src="{{ get_file_url($row['icon_image'] , 'full') }}"  alt="{{$row['title']}}">
                            @endif
                        </div>
                        <div class="right">
                            <div class="number">
                                {{$key+1}}.
                            </div>
                            <div class="text-1">
                                {{$row['title']}}
                            </div>
                            <div class="text-2">
                                {!!  $row['sub_title'] !!}
                            </div>
                        </div>
                        @if(!empty($row['line']))
                            <div class="line" style="background-image: url('{{ get_file_url( $row['line'] , 'full') }}');"></div>
                        @endif
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>