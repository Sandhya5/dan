<div class="what-you-get">
    <div class="container">
        <div class="title">
            {{$title}}
        </div>
        <div class="list-item">
            @if(!empty($list_item))
                @foreach($list_item as $key=>$row)
                    <div class="item item-{{$key+1}}">
                        <div class="text-2">
                            <span class="dot"></span> {!! $row['title'] !!}
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>