<div class="bravo_tour_book_wrap @if(setting_item('tour_enable_inbox')) has-vendor-box @endif">
    <div class="bravo_tour_book">
        <div id="bravo_custom_tour_book_app" v-cloak>
            @if($row->discount_percent)
                <div class="tour-sale-box">
                    <span class="sale_class box_sale sale_small">{{$row->discount_percent}}</span>
                </div>
            @endif
            <div class="form-head">
                <div class="price">
                    <span class="value">
                        <span class="onsale">{{ $row->display_sale_price }}</span>
                        <span class="text-lg">{{ $row->display_price }}</span>
                    </span>
                    <span class="label">
                        {{__("per hour")}}
                    </span>
                </div>
            </div>
            <div class="form-content">
                @if(!empty(setting_item('tour_attr_book_form_detail')))
                <div class="form-group attr-booking-form">
                    <label>{{setting_item_with_lang('tour_attr_book_form_detail_label',request()->query('lang'))}}</label>
                    <?php
                    $terms_ids = $row->tour_term->pluck('term_id');
                    $query = \Custom\Core\Models\Terms::select('*');
                    if(!empty(setting_item('tour_attr_book_form_detail'))){
                        $query->where('attr_id',setting_item('tour_attr_book_form_detail'));
                    }
                    $query->whereIn('id',$terms_ids);
                    $res = $query->orderBy('id', 'desc')->limit(20)->get();
                    ?>
                    @if(!empty($res))
                        <select name="term_id" v-model="term_id" class="form-control">
                            <option value="" disabled="">{{ __('-- Select --') }}</option>
                            @foreach($res as $term)
                                <option value="{{ $term->id }}">{{ $term->name }}</option>
                            @endforeach
                        </select>
                    @endif
                </div>
                @endif
                <date-item v-for="(date,index) in dates" :key="index" :index="index" :date="date" @change-date="changeDateLine" ></date-item>

                    <div class="tour-date-actions">
                        <span class="tour-delete-date" @click="removeDate" v-if="dates.length >1">{{__('Remove')}}</span>
                    </div>

                    <div class="text-center form-group attr-booking-form">
                        <span class="btn btn-large btn-primary" @click="addDate">
                            {{__('BOOK ANOTHER SESSION')}}
                        </span>
                    </div>
                <div class="" v-if="person_types">
                    <div class="form-group form-guest-search" v-for="(type,index) in person_types">
                        <div class="guest-wrapper d-flex justify-content-between align-items-center">
                            <div class="flex-grow-1">
                                <label>@{{type.name}}</label>
                                <div class="render check-in-render">@{{type.desc}}</div>
                                <div class="render check-in-render">@{{type.display_price}} {{__("per person")}}</div>
                            </div>
                            <div class="flex-shrink-0">
                                <div class="input-number-group">
                                    <i class="icon ion-ios-remove-circle-outline" @click="minusPersonType(type)"></i>
                                    {{--<input type="number" class="form-control" v-model="person_types[index].number" :min="type.min" :max="type.max" readonly>--}}
                                    <span class="input">@{{type.number}}</span>
                                    <i class="icon ion-ios-add-circle-outline" @click="addPersonType(type)"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group form-guest-search" v-else>
                    <div class="guest-wrapper d-flex justify-content-between align-items-center">
                        <div class="flex-grow-1">
                            <label>{{__("Guests")}}</label>
                        </div>
                        <div class="flex-shrink-0">
                            <div class="input-number-group">
                                <i class="icon ion-ios-remove-circle-outline" @click="minusGuestsType()"></i>
                                <span class="input">@{{guests}}</span>
                                <i class="icon ion-ios-add-circle-outline" @click="addGuestsType()"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-section-group" v-if="extra_price.length">
                    <h4 class="form-section-title">{{__('Extra prices:')}}</h4>
                    <div class="form-group" v-for="(type,index) in extra_price">
                        <div class="extra-price-wrap d-flex justify-content-between">
                            <div class="flex-grow-1">
                                <label><input type="checkbox" v-model="type.enable" @change="handleTotalPrice()"> @{{type.name}}</label>
                                <div class="render" v-if="type.price_type">(@{{type.price_type}})</div>
                            </div>
                            <div class="flex-shrink-0">@{{type.price_html}}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-section-total" v-if="total_price > 0">
                <label>{{__("Total")}}</label>
                <span class="price">@{{total_price_html}}</span>
            </div>
            <div v-html="html"></div>
            <div class="submit-group">
                <a class="btn btn-large" @click="doSubmit($event)" :class="{'disabled':onSubmit,'btn-success':(step == 2),'btn-primary':step == 1}" name="submit">
                    <span v-if="step == 1">{{__("BOOK DATE & TIME")}}</span>
                    <span v-if="step == 2">{{__("BOOK DATE & TIME")}}</span>
                    <i v-show="onSubmit" class="fa fa-spinner fa-spin"></i>
                </a>
                <div class="alert-text mt10" v-show="message.content" v-html="message.content" :class="{'danger':!message.type,'success':message.type}"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/x-template" id="date-line-item">
    <div>
        <div class="form-group form-date-field form-date-search clearfix " data-format="{{get_moment_date_format()}}">
            <div class="date-wrapper clearfix" @click="openStartDate">
                <div class="check-in-wrapper">
                    <label>{{__("Select Date")}}</label>
                    <div class="render check-in-render">@{{start_date_html}}</div>
                </div>
                <i class="fa fa-angle-down arrow"></i>
            </div>
            <input type="text" class="start_date" ref="start_date" style="height: 1px; visibility: hidden">
        </div>
        <div class="form-select-slots" v-if="slots.length">
            <label>{{__("Slots")}}</label>
            <div class="slots-wrapper">
                <div @click="addSlots(index)" :class="{active:selected_slots.indexOf(index) >=0}" v-for="(slot,index) in slots" class="badge">@{{slot}}</div>
            </div>

        </div>
    </div>
</script>
