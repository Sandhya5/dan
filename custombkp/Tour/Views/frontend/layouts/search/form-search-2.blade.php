<form action="{{url(app_get_locale(false,false,'/').config('tour.tour_route_prefix'))}}" class="form bravo_form" method="get">
    <div class="g-field-search">
        <div class="row">
            <div class="col-md-6 border-right">
                <div class="form-group">
                    <div class="form-content">
                        <?php
                        $location_name = Request::query('term_name');
                        $list_json = [];
                        ?>
                        <div class="smart-search">
                            <input type="text" class="smart-search-term parent_text form-control" name="term_name" placeholder="{{__("How can we help you today?")}}" value="{{ $location_name }}" data-onLoad="{{__("Loading...")}}"
                            data-default="{{ json_encode($list_json) }}">
                            <input type="hidden" class="child_id" name="terms[][]">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 border-right">
                <div class="form-group">
                    <i class="field-icon icofont-location-pin"></i>
                    <input type="hidden" name="lat" value="{{request('lng')}}">
                    <input type="hidden" name="lng" value="{{request('lng')}}">
                    <div class="form-content">
                        <div class="form-content">
                            <input type="text" id="gmap_autocomplete_{{rand(0,999999)}}" class="post_code input-search-google form-control" name="post_code" placeholder="{{ __("e.g. M16 0TH") }}" value="{{(request('lng') and request('lat'))  ? Request::query('post_code') : ''}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="g-button-submit">
        <button class="btn btn-primary btn-search" type="submit">{{__("Search")}}</button>
    </div>
    @php $custom_attribute_page_intro = setting_item("custom_attribute_page_intro") @endphp
    @if(!empty($custom_attribute_page_intro))
        <?php $custom_attribute_page_intro =  json_decode($custom_attribute_page_intro,true) ?>

        @foreach($custom_attribute_page_intro as $item)
            <?php
            if(!empty($item['val'])){
                $term_ids = [];
                foreach ($item['val'] as $term_id){
                    $term_ids[] = $term_id;
                    $res = Custom\Core\Models\Terms::select('*')->whereIn('id',$term_ids)->get();
                    if(!empty($res)){
                        foreach ($res as $item_term){
                            echo '<input class="d-none '.$item['title'].'" name="terms['.$item_term->attr_id.'][]" type="checkbox" value="'.$item_term->id.'">';
                        }
                    }
                }
            }
            ?>
        @endforeach

    @endif
</form>