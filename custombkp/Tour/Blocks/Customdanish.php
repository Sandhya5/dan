<?php
namespace Custom\Tour\Blocks;

use Modules\Template\Blocks\BaseBlock;
use Custom\Core\Models\Terms;
use Custom\Tour\Models\Tour;
use Modules\Tour\Models\TourCategory;
use Illuminate\Support\Facades\DB;

class Customdanish extends BaseBlock
{
    function __construct()
    {
        $this->setOptions([
            'settings' => [
                [
                    'id'        => 'title',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Title')
                ],
                [
                    'id'          => 'list_item',
                    'type'        => 'listItem',
                    'label'       => __('List Item(s)'),
                    'title_field' => 'title',
                    'settings'    => [
                        [
                            'id'        => 'title',
                            'type'      => 'input',
                            'inputType' => 'text',
                            'label'     => __('Title')
                        ],
                        [
                            'id'        => 'sub_title',
                            'type'      => 'textArea',
                            'inputType' => 'textArea',
                            'label'     => __('Sub Title')
                        ],
                        [
                            'id'    => 'icon_image',
                            'type'  => 'uploader',
                            'label' => __('Image Uploader')
                        ],
                        [
                            'id'        => 'line',
                            'type'      => 'uploader',
                            'label'     => __('Line break')
                        ],
                    ]
                ],
            ]
        ]);
    }

    public function getName()
    {
        return __('Homepage: Customdanish');
    }

    public function content($model = [])
    {
		$list = DB::table('bravo_attrs')->get();
		$data = [
            'rows'       => $list,
            
        ];
//print_r($data['rows'][0]->id);
		
        return view('Tour::frontend.blocks.customdanish.index', $data);
       // return view('Tour::frontend.blocks.customdanish.index', $model);
    }
}
