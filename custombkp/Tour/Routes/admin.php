<?php
use Illuminate\Support\Facades\Route;

Route::get('/','TourController@index')->name('tour.admin.index');

Route::post('/bulkEdit','TourController@bulkEdit')->name('tour.admin.bulkEdit');

Route::get('attribute/getTypeForSelect2','AttributeController@getTypeForSelect2')->name('tour.admin.attribute.getTypeForSelect2');
Route::get('attribute/term/getForSelect2','AttributeController@getTermForSelect2')->name('tour.admin.attribute.term.getForSelect2');
Route::get('attribute/getForSelect2','AttributeController@getForSelect2')->name('tour.admin.attribute.getForSelect2');

Route::post('attribute/term_store/{id}','AttributeController@term_store')->name('tour.admin.attribute.term.store');
Route::post('/store/{id}','TourController@store')->name('tour.admin.store');
Route::group(['prefix'=>'availability'],function(){
	Route::get('/','AvailabilityController@index')->name('tour.admin.availability.index');
	Route::get('/loadDates','AvailabilityController@loadDates')->name('tour.admin.availability.loadDates');
	Route::get('/store','AvailabilityController@store')->name('tour.admin.availability.store');
});

Route::group(['prefix'=>'profile_request'],function(){
    Route::get('/','ProfileRequestController@index')->name('tour.profile.request');
});
