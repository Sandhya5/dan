<?php

	use Illuminate\Support\Facades\Route;

	Route::get('/update', 'TourController@updateMigrate');
// Tour
	Route::get('getTypeForSelect2', 'TourController@getTypeForSelect2')->name('tour.book.form.getTypeForSelect2');

	Route::group(['prefix' => config('tour.tour_route_prefix')], function () {
		Route::get('/', 'TourController@index')->name('tour.search'); // Search
		Route::get('/{slug}', 'TourController@detail')->name('tour.detail');// Detail
	});
	Route::get('term/getForSelect2', 'AttributeController@getForSelect2')->name('tour.term.getForSelect2');
	Route::group(['prefix' => 'user/' . config('tour.tour_route_prefix')], function () {
		Route::group(['prefix' => 'availability'], function () {
			Route::get('/loadDates', 'AvailabilityController@loadDates')->name('tour.vendor.availability.loadDates');
			Route::match(['post'],'/store','AvailabilityController@store')->name('tour.vendor.availability.store');
		});
	});


  Route::get('user/profile-booking-report','ManageTourController@bookingReport')->name("tour.vendor.booking_report");
  
  Route::get('ajax-call','TourController@getTerms');// Detail
  Route::get('ajax-call-submit','TourController@submitTerms');// Detail