<?php
namespace Custom\Tour;

use Illuminate\Support\ServiceProvider;
use Custom\ModuleServiceProvider;
use Custom\Tour\Models\Tour;

class ModuleProvider extends ModuleServiceProvider
{

    public function boot(){
        $this->loadMigrationsFrom(__DIR__ . '/Migrations');
    }
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouterServiceProvider::class);
    }

    public static function getBookableServices()
    {
        return [
            'tour'=>Tour::class,
        ];
    }

    public static function getAdminMenu()
    {
        $count_request = Tour::query()->where('status','!=','publish')->count(['id']);
        return [
            'tour'=>[
                "position"=>40,
                'url'        => 'admin/module/tour',
                'title'      => __("Profile :count",['count'=>sprintf("<span class='badge badge-warning'>%d</span>",$count_request)]),
                'icon'       => 'icon ion-ios-contacts',
                'permission' => 'tour_view',
                'children'   => [
                    'tour_view'=>[
                        'url'        => 'admin/module/tour',
                        'title'      => __('All Tours'),
                        'permission' => 'tour_view',
                    ],
                    'tour_view_draft'=>[
                        'url'        => 'admin/module/tour?status=draft',
                        'title'      => __('Pending Profiles :count',['count'=>sprintf("<span class='badge badge-warning'>%d</span>",$count_request)]),
                        'permission' => 'tour_view',
                    ],
                    'tour_create'=>[
                        'url'        => 'admin/module/tour/create',
                        'title'      => __("Add Tour"),
                        'permission' => 'tour_create',
                    ],
                    'tour_category'=>[
                        'url'        => 'admin/module/tour/category',
                        'title'      => __('Categories'),
                        'permission' => 'tour_manage_others',
                    ],
                    'tour_attribute'=>[
                        'url'        => 'admin/module/tour/attribute',
                        'title'      => __('Attributes'),
                        'permission' => 'tour_manage_attributes',
                    ],
                    'tour_availability'=>[
                        'url'        => 'admin/module/tour/availability',
                        'title'      => __('Availability'),
                        'permission' => 'tour_create',
                    ],
                    'tour_booking'=>[
                        'url'        => 'admin/module/tour/booking',
                        'title'      => __('Booking Calendar'),
                        'permission' => 'tour_create',
                    ],
                ]
            ],
        ];
    }

    public static function getTemplateBlocks(){
        return [
            'tour_list_terms'=>"\\Custom\\Tour\\Blocks\\ListTerms",
            'form_search_tour'=>"\\Custom\\Tour\\Blocks\\FormSearchTour",
            'list_featured_item'=>"\\Custom\\Tour\\Blocks\\ListFeaturedItem",
            'list_tours'=>"\\Custom\\Tour\\Blocks\\ListTours",
            'text_block_featured'=>"\\Custom\\Tour\\Blocks\\TextBlockFeatured",
            'how_it_works'=>"\\Custom\\Tour\\Blocks\\HowItWorks",
            'about_featured'=>"\\Custom\\Tour\\Blocks\\AboutFeatured",
            'testimonial'=>"\\Custom\\Tour\\Blocks\\Testimonial",
            'vendor_about_text'=>"\\Custom\\Tour\\Blocks\\VendorAboutText",
            'vendor_register_form_2'=>"\\Custom\\Tour\\Blocks\\VendorRegisterForm2",
            'Vendor_why_use'=>"\\Custom\\Tour\\Blocks\\VendorWhyUse",
            'Vendor_step_to'=>"\\Custom\\Tour\\Blocks\\VendorStepTo",
            'Vendor_what_you_get'=>"\\Custom\\Tour\\Blocks\\VendorWhatYouGet",
            'Vendor_FAQs'=>"\\Custom\\Tour\\Blocks\\VendorFAQs",
			'customdanish'=>"\\Custom\\Tour\\Blocks\\Customdanish",
        ];
    }
}
