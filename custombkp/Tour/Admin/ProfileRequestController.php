<?php

namespace Custom\Tour\Admin;
use Custom\Tour\Models\Tour;
use Plugins\AdminController;

class ProfileRequestController extends AdminController{
    public function __construct()
    {
        parent::__construct();
        $this->setActiveMenu('admin/module/tour/profile_request');
    }

    public function index(){

        $data = [
            'rows'=>Tour::query()->where('status','!=','publish')->paginate(20)
        ];

        return view('Tour::admin.profile_request.index',$data);
    }
}
