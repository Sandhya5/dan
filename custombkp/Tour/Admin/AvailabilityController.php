<?php


	namespace Custom\Tour\Admin;

	class AvailabilityController extends \Custom\Tour\Controllers\AvailabilityController
	{
		public function __construct()
		{
			parent::__construct();
			$this->indexView = 'Tour::admin.availability';
		}
	}