<?php
namespace Custom\Tour\Admin;

use Custom\Tour\Emails\TourApprovedEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Modules\AdminController;
use Modules\Core\Models\Attributes;
use Modules\Tour\Models\TourTerm;

use Modules\Tour\Models\TourCategory;
use Modules\Tour\Models\TourTranslation;
use Modules\Location\Models\Location;

use Custom\Tour\Models\Tour;

class TourController extends \Modules\Tour\Admin\TourController
{
    protected $tourClass;

    public function __construct()
    {
        parent::__construct();
        $this->tourClass = Tour::class;
    }
    public function index(Request $request)
    {
        $this->checkPermission('tour_view');
        $query = $this->tourClass::query() ;
        $query->orderBy('id', 'desc');
        if (!empty($tour_name = $request->input('s'))) {
            $query->where('title', 'LIKE', '%' . $tour_name . '%');
            $query->orderBy('title', 'asc');
        }
        if (!empty($cate = $request->input('cate_id'))) {
            $query->where('category_id', $cate);
        }
        if ($this->hasPermission('tour_manage_others')) {
            if (!empty($author = $request->input('vendor_id'))) {
                $query->where('create_user', $author);
            }
        } else {
            $query->where('create_user', Auth::id());
        }
        if($status = $request->query('status')){
            $query->where('status',$status);
        }
        $data = [
            'rows'               => $query->with(['getAuthor','category_tour'])->paginate(20),
            'tour_categories'    => $this->tourCategoryClass::where('status', 'publish')->get()->toTree(),
            'tour_manage_others' => $this->hasPermission('tour_manage_others'),
            'page_title'=>__("Tour Management"),
            'breadcrumbs'        => [
                [
                    'name' => __('Tours'),
                    'url'  => 'admin/module/tour'
                ],
                [
                    'name'  => __('All'),
                    'class' => 'active'
                ],
            ]
        ];
        return view('Tour::admin.index', $data);
    }


    public function bulkEdit(Request $request)
    {
        $ids = $request->input('ids');
        $action = $request->input('action');
        if (empty($ids) or !is_array($ids)) {
            return redirect()->back()->with('error', __('No items selected!'));
        }
        if (empty($action)) {
            return redirect()->back()->with('error', __('Please select an action!'));
        }

        switch ($action){
            case "delete":
                foreach ($ids as $id) {
                    $query = $this->tourClass::where("id", $id);
                    if (!$this->hasPermission('tour_manage_others')) {
                        $query->where("create_user", Auth::id());
                        $this->checkPermission('tour_delete');
                    }
                    $query->first();
                    if(!empty($query)){
                        $query->delete();
                    }
                }
                return redirect()->back()->with('success', __('Deleted success!'));
                break;
            case "clone":
                $this->checkPermission('tour_create');
                foreach ($ids as $id) {
                    (new $this->tourClass())->saveCloneByID($id);
                }
                return redirect()->back()->with('success', __('Clone success!'));
                break;
            default:
                // Change status
                foreach ($ids as $id) {
                    $query = $this->tourClass::where("id", $id);
                    if (!$this->hasPermission('tour_manage_others')) {
                        $query->where("create_user", Auth::id());
                        $this->checkPermission('tour_update');
                    }
                    $tour = $query->first();
                    $tour->status = $action;
                    $tour->save();

                    if($action == "publish"){
                        Mail::to($tour->author->email)->send(new TourApprovedEmail($tour));
                    }
                }
                return redirect()->back()->with('success', __('Update success!'));
                break;
        }
    }

    public function store( Request $request, $id ){

		if($id>0){
			$this->checkPermission('tour_update');
			$row = $this->tourClass::find($id);
			if (empty($row)) {
				return redirect(route('tour.admin.index'));
			}
			if($row->create_user != Auth::id() and !$this->hasPermission('tour_manage_others'))
			{
				return redirect(route('space.admin.index'));
			}

		}else{
			$this->checkPermission('tour_create');
			$row = new $this->tourClass();
			$row->status = "publish";
		}
		$row->fill($request->input());
		$row->create_user = $request->input('create_user');
		$row->default_state = $request->input('default_state',1);
		$row->enable_sale_of_first_booking = $request->input('enable_sale_of_first_booking',0);
		$row->highlights = $request->input('highlights');
		$row->country = $request->input('country');
		$row->city = $request->input('city');
		$row->bank_details = $request->input('bank_details');
		$row->faqs2 = $request->input('faqs2');
		$res = $row->saveOriginOrTranslation($request->input('lang'),true);
		if ($res) {
			if(!$request->input('lang') or is_default_lang($request->input('lang'))) {
				$this->saveTerms($row, $request);
			}
			$row->saveMeta($request);
			if($id > 0 ){
				return back()->with('success',  __('Tour updated') );
			}else{
				return redirect(route('tour.admin.edit',$row->id))->with('success', __('Tour created') );
			}
		}
	}

}
