@php $main_color =setting_item('style_main_color','{{$main_color}}');
$style_typo = json_decode(setting_item_with_lang('style_typo',false,"{}"),true);
@endphp
<style id="custom-css">
    body{
        @if(!empty($style_typo) && is_array($style_typo))
            @foreach($style_typo as $k=>$v)
                    @if($v)
                        {{str_replace('_','-',$k)}}:{!! $v !!};
                    @endif
            @endforeach
        @endif
    }
    a,
    .bravo-news .btn-readmore,
    .bravo_wrap .bravo_header .content .header-left .bravo-menu ul li:hover > a,
    .bravo_wrap .bravo_search_tour .bravo_form_search .bravo_form .field-icon,
    .bravo_wrap .bravo_search_tour .bravo_form_search .bravo_form .render,
    .bravo_wrap .bravo_search_tour .bravo_form_search .bravo_form .field-detination #dropdown-destination .form-control,
    .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .btn-apply-price-range,
    .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .btn-more-item,
    .input-number-group i,
    .bravo_wrap .page-template-content .bravo-form-search-tour .bravo_form_search_tour .field-icon,
    .bravo_wrap .page-template-content .bravo-form-search-tour .bravo_form_search_tour .field-detination #dropdown-destination .form-control,
    .bravo_wrap .page-template-content .bravo-form-search-tour .bravo_form_search_tour .render
    {
        color:{{$main_color}}
    }
    a:hover {
        color: {{$main_color}};
    }
    .bravo-pagination ul li.active a, .bravo-pagination ul li.active span
    {
        color:{{$main_color}}!important;
    }
    .bravo-news .widget_category ul li span,
    .bravo_wrap .bravo_search_tour .bravo_form_search .bravo_form .g-button-submit button,
    .bravo_wrap .bravo_search_tour .bravo_filter .filter-title:before,
    .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-bar,
    .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-from, .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-to, .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-single,
    .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-handle>i:first-child,
    .bravo-news .header .cate ul li,
    .bravo_wrap .page-template-content .bravo-form-search-tour .bravo_form_search_tour .g-button-submit button,
    .bravo_wrap .page-template-content .bravo-list-locations .list-item .destination-item .image .content .desc
    {
        background: {{$main_color}};
    }
    .bravo-pagination ul li.active a, .bravo-pagination ul li.active span
    {
        border-color:{{$main_color}}!important;
    }
    .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-from:before, .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-to:before, .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-single:before,
    .bravo-reviews .review-form .form-wrapper,
    .bravo_wrap .bravo_detail_tour .bravo_content .bravo_tour_book
    {
        border-top-color:{{$main_color}};
    }

    .bravo_wrap .bravo_footer .main-footer .nav-footer .context .contact{
        border-left-color:{{$main_color}};
    }

    .bravo_wrap .bravo_form .g-button-submit button {
        background: {{$main_color}};
    }
    .btn.btn-primary,
    .btn-primary:not(:disabled):not(.disabled):active, .btn-primary:not(:disabled):not(.disabled).active, .show > .btn-primary.dropdown-toggle {
        background: {{$main_color}};
    }
    .bravo_wrap .bravo_form .form-content .render {
        color: {{$main_color}};
    }
    .bravo_wrap .bravo_form .field-icon {
        color: {{$main_color}};
    }
    .bravo_wrap .bravo_form .smart-search .parent_text {
        color: {{$main_color}};
    }

    .bravo_wrap .bravo_form .smart-search .parent_text::-webkit-input-placeholder {
        color: {{$main_color}};
    }

    .bravo_wrap .bravo_form .smart-search .parent_text::-moz-placeholder {
        color: {{$main_color}};
    }

    .bravo_wrap .bravo_form .smart-search .parent_text:-ms-input-placeholder {
        color: {{$main_color}};
    }

    .bravo_wrap .bravo_form .smart-search .parent_text:-moz-placeholder {
        color: {{$main_color}};
    }

    .bravo_wrap .bravo_form .smart-search .parent_text::placeholder {
        color: {{$main_color}};
    }
    .bravo_wrap .bravo_form .smart-search:after {
        color: {{$main_color}};
    }
    .bravo_wrap .bravo_form .dropdown-toggle:after {
        color: {{$main_color}};
    }

    .bravo_wrap .page-template-content .bravo-list-space .item-loop .service-review .rate {
        color: {{$main_color}};
    }

    .bravo_wrap .page-template-content .bravo-list-locations.style_2 .list-item .destination-item:hover .title {
        background: {{$main_color}};
    }
    .bravo_wrap .page-template-content .bravo-list-space .item-loop .sale_info {
        background-color: {{$main_color}};
    }
    .bravo_wrap .bravo_search_space .bravo-list-item .list-item .item-loop .sale_info {
        background-color: {{$main_color}};
    }
    .bravo_wrap .bravo_search_space .bravo-list-item .list-item .item-loop .service-review .rate {
        color: {{$main_color}};
    }
    .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .btn-apply-price-range {
        color: {{$main_color}};
    }
    .bravo_wrap .bravo_search_space .bravo_filter .filter-title:before {
        background: {{$main_color}};
    }

    .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-bar {
        background-color: {{$main_color}};
    }
    .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-from, .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-to, .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-single {
        background-color: {{$main_color}};
    }
    .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-from, .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-to, .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-single {
        background-color: {{$main_color}};
    }
    .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-handle>i:first-child {
        background-color: {{$main_color}};
    }
    .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-from:before, .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-to:before, .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-single:before {
        border-top-color: {{$main_color}};
    }
    .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .btn-more-item {
        color: {{$main_color}};
    }
    .bravo_wrap .bravo_detail_space .bravo_content .g-header .review-score .head .score {
        background: {{$main_color}};
    }
    .bravo_wrap .bravo_detail_space .bravo_content .g-header .review-score .head .left .text-rating {
        color: {{$main_color}};
    }

    .bravo_wrap .bravo_detail_space .bravo_content .g-overview ul li:before {
        border: 1px solid {{$main_color}};
    }

    .bravo-reviews .review-box .review-box-score .review-score {
        color: {{$main_color}};
    }

    .bravo-reviews .review-box .review-box-score .review-score-base span {
        color: {{$main_color}};
    }

    .bravo-reviews .review-form .btn {
        background: {{$main_color}};
    }

    .bravo_wrap .bravo_detail_space .bravo-list-space-related .item-loop .sale_info {
        background-color: {{$main_color}};
    }

    .bravo_wrap .bravo_detail_space .bravo_content .g-header .review-score .head .score::after {
        border-bottom: 25px solid {{$main_color}};
    }

    .bravo_wrap .bravo_detail_space .bravo_content .bravo_space_book {
        border-top: 5px solid {{$main_color}};
    }

    body .daterangepicker.loading:after {
        color: {{$main_color}};
    }

    body .daterangepicker .drp-calendar .calendar-table tbody tr td.end-date {
        border-right: solid 2px {{$main_color}};
    }
    body .daterangepicker .drp-calendar .calendar-table tbody tr td.start-date {
        border-left: solid 2px {{$main_color}};
    }
    .bravo_wrap .bravo_detail_space .bravo-list-space-related .item-loop .service-review .rate {
        color: {{$main_color}};
    }

    .has-search-map .bravo-filter-price .irs--flat .irs-bar {
        background-color: {{$main_color}};
    }
    .has-search-map .bravo-filter-price .irs--flat .irs-handle>i:first-child {
        background-color: {{$main_color}};
    }
    .has-search-map .bravo-filter-price .irs--flat .irs-from, .has-search-map .bravo-filter-price .irs--flat .irs-to, .has-search-map .bravo-filter-price .irs--flat .irs-single {
        background-color: {{$main_color}};
    }

    .has-search-map .bravo-filter-price .irs--flat .irs-from:before, .has-search-map .bravo-filter-price .irs--flat .irs-to:before, .has-search-map .bravo-filter-price .irs--flat .irs-single:before {
        border-top-color: {{$main_color}};
    }

    .bravo_wrap .bravo_detail_tour .bravo_content .g-header .review-score .head .score {
        background: {{$main_color}};
    }
    .bravo_wrap .bravo_detail_tour .bravo_content .g-header .review-score .head .score::after {
        border-bottom: 25px solid {{$main_color}};
    }

    .bravo_wrap .bravo_detail_tour .bravo_content .g-header .review-score .head .left .text-rating {
        color: {{$main_color}};
    }

    .bravo_wrap .bravo_detail_tour .bravo_content .g-overview ul li:before {
        border: 1px solid {{$main_color}};
    }

    .bravo_wrap .bravo_detail_location .bravo_content .g-location-module .location-module-nav li a.active {
        border-bottom: 1px solid {{$main_color}};
        color: {{$main_color}};
    }

    .bravo_wrap .bravo_detail_location .bravo_content .g-location-module .item-loop .sale_info {
        background-color: {{$main_color}};
    }
    .bravo_wrap .bravo_detail_location .bravo_content .g-location-module .item-loop .service-review .rate {
        color: {{$main_color}};
    }

    .bravo_wrap .bravo_detail_location .bravo_content .g-trip-ideas .trip-idea .trip-idea-category {
        color: {{$main_color}};
    }

    .bravo_wrap .page-template-content .bravo-featured-item.style2 .number-circle {
        border: 2px solid {{$main_color}};
        color: {{$main_color}};
    }
    .bravo_wrap .page-template-content .bravo-featured-item.style3 .featured-item:hover {
        border-color: {{$main_color}};
    }
    .bravo_wrap .bravo_footer .main-footer .nav-footer .context ul li a:hover {
        color: {{$main_color}};
    }

    .booking-success-notice .booking-info-detail {
        border-left: 3px solid {{$main_color}};
    }

    .bravo_wrap .bravo_user_profile .booking-history-manager .tabbable .ht-nav-tabs li.active a {
        color: {{$main_color}};
        border-bottom: 3px solid {{$main_color}};
    }

    .bravo_wrap .bravo_user_profile .form-add-service .nav-tabs {
        background: {{$main_color}};
    }

    .modal .modal-content .modal-body .form-submit {
        background: {{$main_color}};
    }
    {!! setting_item_with_lang('style_custom_css') !!}
</style>
