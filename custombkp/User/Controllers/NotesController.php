<?php
namespace Custom\User\Controllers;

use Custom\User\Models\UserNote;
use Illuminate\Support\Str;
use Modules\FrontendController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class NotesController extends FrontendController
{
    use AuthenticatesUsers;
    protected $bookingClass;
    public function __construct()
    {
    	parent::__construct();
    }


    public function index(Request $request){

        $user = Auth::user();
        $this->setActiveMenu(app_get_locale().'/user/notes');
        $data = [
            'dataUser' => $user,
            'page_title'         => __("Therapist Notes"),
            'breadcrumbs'        => [
                [
                    'name' => __('Edit Profile'),
                    'class' => 'active'
                ]
            ]
        ];
        return view('User::frontend.profile-notes', $data);
    }
    public function loadDates(){

        $user = Auth::user();
        $query = UserNote::query();
        $query->where('user_id',$user->id);
        $query->whereBetween('start_date',[\request()->query('start'),\request()->query('end')]);
        $rows = $query->take(50)->get();
        $res = [];
        foreach ($rows as $item) {
            $item->event = $item->title =   Str::limit(($item->client_name ? $item->client_name."<br> " :'').$item->content,20);
            $item->date = date('Y-m-d',strtotime($item->start_date));
            $res[] = $item;
        }

        return response()->json($res);
    }

    public function store(Request $request)
    {
        $request->validate([
            'client_name'=>'required|max:255',
            'content' => 'required|max:6500',
            'date'=>'required|date_format:Y-m-d'
        ]);

        $user = Auth::user();

        $date = $request->input('date');
        $note = UserNote::query()->where([
            'start_date'=>$date,
            'user_id'=>$user->id
        ])->first();

        if(!$note){
            $note = new UserNote();
            $note->user_id = $user->id;
            $note->start_date = $request->input('date');
        }

        $note->client_name = $request->input('client_name');
        $note->content = $request->input('content');
        $note->save();

        $this->sendSuccess([],__("Saved"));
    }
    public function delete(Request $request)
    {
        $request->validate([
            'date'=>'required|date_format:Y-m-d'
        ]);

        $user = Auth::user();

        $date = $request->input('date');
        $note = UserNote::query()->where([
            'start_date'=>$date,
            'user_id'=>$user->id
        ])->first();

        if($note){
            $note->delete();
        }

        $this->sendSuccess([],__("Deleted"));
    }
}
