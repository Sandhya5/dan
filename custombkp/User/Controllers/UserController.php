<?php
namespace Custom\User\Controllers;

use Custom\Tour\Emails\TourSubmitedEmail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Matrix\Exception;
use Modules\FrontendController;
use Modules\Tour\Models\TourTerm;
use Modules\User\Events\SendMailUserRegistered;

use Modules\User\Models\Subscriber;
use Modules\User\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use Validator;
use Modules\Booking\Models\Booking;
use App\Helpers\ReCaptchaEngine;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Custom\Tour\Models\Tour;
use Modules\Core\Models\Attributes;

class UserController extends \Modules\User\Controllers\UserController
{
    use AuthenticatesUsers;
    protected $bookingClass;
    public function __construct()
    {
    	parent::__construct();
    	$this->bookingClass = \Custom\Booking\Models\Booking::class;
    }

	public function profile(Request $request)
    {
        $user = Auth::user();
        if($user->hasPermissionTo('dashboard_vendor_access')){
            //Get custom info user
            $customInforUser = Tour::where("create_user",$user->id)->first();
            if(empty($customInforUser)){
                $tourClass = new Tour([
                    'title'=> $user->name ??  $user->first_name." ".$user->last_name,
                    'create_user'=>$user->id,
                    'status'=> setting_item('vendor_auto_approved') ? 'publish' : 'draft',
                ]);
                $tourClass->create_user = $user->id;
                $tourClass->save();
                $customInforUser = $tourClass;
            }

            $data = [
                'dataUser' => $user,
                'row' => $customInforUser,
                'translation' => $customInforUser->translateOrOrigin($request->query('lang')),
                'attributes'    => Attributes::where('service', 'tour')->get(),
                'selected_terms' => $customInforUser->tour_term->pluck('term_id'),
                'page_title'         => __("Profile"),
                'breadcrumbs'        => [
                    [
                        'name' => __('Edit Profile'),
                        'class' => 'active'
                    ]
                ]
            ];
            return view('User::frontend.profile-vendor', $data);
        }
        return parent::profile($request);
    }

    public function calendar(Request $request){
        $user = Auth::user();
        $this->setActiveMenu(app_get_locale().'/user/calendar');

        $customInforUser = Tour::where("create_user",$user->id)->first();
        if(empty($customInforUser)){
            $tourClass = new Tour([
                'title'=>$user->first_name." ".$user->last_name,
                'create_user'=>$user->id,
                'status'=> setting_item('vendor_auto_approved') ? 'publish' : 'draft',
            ]);
            $tourClass->save();
            $customInforUser = $tourClass;
        }

        $data = [
            'dataUser' => $user,
            'row' => $customInforUser,
            'translation' => $customInforUser->translateOrOrigin($request->query('lang')),
            'attributes'    => Attributes::where('service', 'tour')->get(),
            'selected_terms' => $customInforUser->tour_term->pluck('term_id'),
            'page_title'         => __("Profile"),
            'breadcrumbs'        => [
                [
                    'name' => __('Edit Profile'),
                    'class' => 'active'
                ]
            ]
        ];
        return view('User::frontend.profile-calendar', $data);
    }

    public function store(Request $request){
        $user = Auth::user();
        if($request->form_submit == 'submitForApproval'){
            $request->validate([
                'title'                        => 'required|string|max:250',
                'content'                        => 'required|string|maxWords:300',
                'introductions_ownership_policy' => 'required',
               /* 'faqs2.*.content'                => 'maxWords:200',*/

                'address' => 'required',
                'country' => 'required',
                'city' => 'required',
                'enable_open_hours' => 'required',

                'bank_details.*' => 'required',
            ],[
                /*'faqs2.*.content.max_words' => __('The Q&A content not be greater than 200 words.'),*/
                'enable_open_hours.required' => __('The set working hours field is required.'),
                'bank_details.first_name.required' => __('The bank\'s first name  field is required'),
                'bank_details.last_name.required' => __('The bank\'s last name  field is required'),
                'bank_details.account_number.required' => __('The bank\'s account number  field is required'),
                'bank_details.short_code.required' => __('The bank\'s sort code  field is required'),
            ]);

            //validate for faqs2
            /*$faqs2 = $request->input('faqs2');
            if(!empty($faqs2)){
                $check = true;
                foreach ($faqs2 as $item){
                    if($item['required']){
                        if(empty($item['content'])){
                            $check = false;
                        }
                    }
                }
                if($check == false){
                    if(empty($row)){
                        $data = $request->input();
                        unset($data['_token']);
                        unset($data['form_submit']);
                        return back()->with('error', __('Please answer all the required questions for Profile Q & A') )->withInput($data);
                    }
                }
            }*/
        }

        if($user->hasPermissionTo('dashboard_vendor_access')){
            $row = Tour::where("create_user",Auth::id())->first();
            if(empty($row)){
                return back()->with('success',  __('Cannot update profile!') );
            }
            $row->fillByAttr([
                'title',
                'content',
                'image_id',
                'address',
                'map_lat',
                'map_lng',
                'map_zoom',
                'price',
                'enable_sale_of_first_booking',
                'max_people',
                'min_people',
                'faqs',
                'highlights',
                'country',
                'city',
                'bank_details',
                'introductions_ownership_policy',
                'faqs2',
            ], $request->input());
	        $row->highlights = $request->input('highlights');
	        $row->country = $request->input('country');
	        $row->city = $request->input('city');
            $res = $row->saveOriginOrTranslation($request->input('lang'),true);

            //Update name for user
            $userClass = User::where("id",Auth::id())->first();
            $userClass->last_name = $request->input('title');
            $userClass->first_name = "";
            $userClass->save();
            if ($res) {
                if(!$request->input('lang') or is_default_lang($request->input('lang'))) {
                    $this->saveTerms($row, $request);
                }
                $row->saveMeta($request);
				if($request->form_submit == 'submitForApproval'){
					$message = __('Profile for approval successfully submitted');
					Mail::to($userClass->email)->send(new TourSubmitedEmail($row));
				}else{
					$message = __('Profile successfully submitted');

				}

                return back()->with('success', $message  );
            }
        }
        return parent::profile($request);
    }

    public function storeCalendar(Request $request){
        $user = Auth::user();

        if($user->hasPermissionTo('dashboard_vendor_access')){
            {
                $row = Tour::where("create_user",Auth::id())->first();
                if(empty($row)){
                    return back()->with('success',  __('Cannot update calendar') );
                }

                $meta = $row->meta;

                $meta->open_hours = $request->input('open_hours');
                $meta->enable_open_hours = 1;

                $meta->save();

                return back()->with('success',  __('Updated') );
            }
        }
    }

    public function saveTerms($row, $request)
    {
        if (empty($request->input('terms'))) {
            TourTerm::where('tour_id', $row->id)->delete();
        } else {
            $term_ids = $request->input('terms');
            foreach ($term_ids as $term_id) {
                TourTerm::firstOrCreate([
                    'term_id' => $term_id,
                    'tour_id' => $row->id
                ]);
            }
            TourTerm::where('tour_id', $row->id)->whereNotIn('term_id', $term_ids)->delete();
        }
    }

    public function bookingHistory(Request $request)
    {
        $user_id = Auth::id();
        $data = [
            'bookings' => $this->bookingClass::getBookingHistory($request->input('status'), $user_id),
            'statues'  => config('booking.statuses'),
            'breadcrumbs'        => [
                [
                    'name' => __('Booking History'),
                    'class' => 'active'
                ]
            ],
            'page_title'         => __("Booking History"),
        ];
        return view('User::frontend.bookingHistory', $data);
    }

    public function userLogin(Request $request)
    {
        $rules = [
            'email'    => 'required|email',
            'password' => 'required'
        ];
        $messages = [
            'email.required'    => __('Email is required field'),
            'email.email'       => __('Email invalidate'),
            'password.required' => __('Password is required field'),
        ];
        if (ReCaptchaEngine::isEnable() and setting_item("user_enable_login_recaptcha")) {
            $codeCapcha = $request->input('g-recaptcha-response');
            if (!$codeCapcha or !ReCaptchaEngine::verify($codeCapcha)) {
                $errors = new MessageBag(['message_error' => __('Please verify the captcha')]);
                return response()->json(['error'    => true,
                                         'messages' => $errors
                ], 200);
            }
        }
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['error'    => true,
                                     'messages' => $validator->errors()
            ], 200);
        } else {
            $email = $request->input('email');
            $password = $request->input('password');
            if (Auth::attempt(['email'    => $email,
                               'password' => $password
            ], $request->has('remember'))) {
                if(in_array(Auth::user()->status,['blocked'])){
                    Auth::logout();
                    $errors = new MessageBag(['message_error' => __('Your account has been blocked')]);
                    return response()->json([
                        'error'    => true,
                        'messages' => $errors,
                        'redirect' => false
                    ], 200);

                }

                $referer = false;

                if($this->hasPermission('dashboard_vendor_access')){
                    $referer = route("vendor.profile.calendar.custom");
                }

                return response()->json([
                    'error'    => false,
                    'messages' => false,
                    'redirect' => $referer ??  $request->input('referer') ?? $request->headers->get('referer') ?? url(app_get_locale(false,'/'))
                ], 200);
            } else {
                $errors = new MessageBag(['message_error' => __('Username or password incorrect')]);
                return response()->json([
                    'error'    => true,
                    'messages' => $errors,
                    'redirect' => false
                ], 200);
            }
        }
    }
    public function invoice($code){
        $booking = Booking::where('code',$code)->first();

        $user_id = Auth::id();

        if(empty($booking)){
            return redirect('user/booking-history');
        }

        if($booking->customer_id != $user_id and $booking->vendor_id != $user_id and !$this->hasPermission('report_view')){
            return redirect('user/booking-history');
        }

        $data = [
            'booking'=>$booking,
            'service'=>$booking->service,
            'page_title' => __("Invoice")
        ];

        return view('User::frontend.invoice',$data);
    }
}
