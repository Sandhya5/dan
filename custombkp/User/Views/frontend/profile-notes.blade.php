@extends('layouts.user')
@section('head')

    <link rel="stylesheet" href="{{asset('libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('libs/fullcalendar-4.2.0/core/main.css')}}">
    <link rel="stylesheet" href="{{asset('libs/fullcalendar-4.2.0/daygrid/main.min.css')}}">
    <link rel="stylesheet" href="{{asset('libs/fullcalendar-4.2.0/timegrid/main.min.css')}}">
    <link rel="stylesheet" href="{{asset('libs/daterange/daterangepicker.css')}}">
    <style>
        .event-name{
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }
        #dates-calendar .loading{

        }
    </style>
@endsection
@section('content')
    <h2 class="title-bar no-border-bottom">
        {{__("Therapist Notes")}}
    </h2>
    @include('admin.message')
    <div class="lang-content-box">
        @csrf
        <div class="form-add-service">
            <div class="form-add-service">
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane show fade active" id="nav-availability">

                        <div id="dates-calendar" class="dates-calendar normal"></div>
                        <div id="bravo_modal_calendar" class="modal fade">
                            <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">{{__('Add Notes')}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row" novalidate onsubmit="return false">
                                            <div class="col-12" >
                                                <div class="form-group">
                                                    <label>{{__('Client Name:')}}</label>
                                                    <input type="text" class="form-control" v-model="form.client_name">
                                                </div>
                                            </div>
                                            <div class="col-12" >
                                                <div class="form-group">
                                                    <label>{{__('Content:')}}</label>
                                                    <textarea class="form-control" v-model="form.content" rows="15"  style="min-height: 150px;"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div v-if="lastResponse.message">
                                        <br>
                                        <div class="alert" :class="!lastResponse.status ? 'alert-danger':'alert-success'">@{{ lastResponse.message }}</div>
                                    </div>
                                    <div class="modal-footer  justify-content-between">
                                        <div>
                                            <button type="button" @click="deleteNote" class="btn btn-danger">{{__('Delete')}}</button>
                                        </div>
                                        <div>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                                            <button type="button" class="btn btn-primary" @click="saveForm">{{__('Save changes')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<style>
    .dates-calendar .fc-event{
        display: block;
    }
</style>
@endsection
@section('footer')
    <script type="text/javascript" src="{{ asset('libs/tinymce/js/tinymce/tinymce.min.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('js/condition.js?_ver='.config('app.version')) }}"></script>

    <script src="{{asset('libs/daterange/moment.min.js')}}"></script>
    <script src="{{asset('libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('libs/daterange/daterangepicker.min.js?_ver='.config('app.version'))}}"></script>
    <script src="{{asset('libs/fullcalendar-4.2.0/core/main.js')}}"></script>
    <script src="{{asset('libs/fullcalendar-4.2.0/interaction/main.js')}}"></script>
    <script src="{{asset('libs/fullcalendar-4.2.0/daygrid/main.js')}}"></script>
    <script src="{{asset('libs/fullcalendar-4.2.0/timegrid/main.js')}}"></script>
    <script>
        var calendarMonthEl,calendarEl,calendarMonth,calendar,lastId,formModal;
        calendarMonthEl = document.getElementById('months-calendar');
        calendarEl = document.getElementById('dates-calendar');
        if(calendar){
            calendar.destroy();
        }
        if(calendarMonth){
            calendarMonth.destroy();
        }

        calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: [ 'dayGrid','interaction'],
            selectable: true,
            selectMirror: false,
            allDay:false,
            editable: false,
            eventLimit: true,
			events:{
                url:"{{route('vendor.profile.notes.loadDates.custom')}}",
            },
            loading:function (isLoading) {
                if(!isLoading){
                    $(calendarEl).removeClass('loading');
                }else{
                    $(calendarEl).addClass('loading');
                }
            },
            select: function(arg) {
                formModal.show({
                    date:moment(arg.start).format('YYYY-MM-DD'),
                    content:'',
                });
            },
            eventClick:function (info) {
				var form = Object.assign({},info.event.extendedProps);
                if(!form.booking_online){
					form.date = moment(info.event.start).format('YYYY-MM-DD');
					formModal.show(form);
                }
            },
            eventRender: function (info) {
                $(info.el).find('.fc-title').html(info.event.title);
            }
        });
        calendar.render();
        $('.event-name:first-child a').trigger('click');

        formModal = new Vue({
            el:'#bravo_modal_calendar',
            data:{
                lastResponse:{
                    status:null,
                    message:''
                },
                form:{
                    id:'',
                    content:'',
                    client_name:''
                },
                formDefault:{
                    id:'',
                    content:'',
                    client_name:''
                },
                person_types:[

                ],
                person_type_item:{
                    name:'',
                    desc:'',
                    min:'',
                    max:'',
                    price:'',
                },
                onSubmit:false
            },
            methods:{
                show:function (form) {
                    $(this.$el).modal('show');
                    this.lastResponse.message = '';
                    this.onSubmit = false;

                    if(typeof form !='undefined'){
                        this.form = Object.assign({},form);
                    }
                },
                hide:function () {
                    $(this.$el).modal('hide');
                    this.form = Object.assign({},this.formDefault);
                },
                saveForm:function () {
                    var me = this;
                    me.lastResponse.message = '';
                    if(this.onSubmit) return;

                    if(!this.validateForm()) return;

                    this.onSubmit = true;
                    $.ajax({
                        url:'{{route('vendor.profile.notes.store.custom')}}',
                        data:this.form,
                        dataType:'json',
                        method:'post',
                        success:function (json) {
                            if(json.status){
                                if(calendar)
                                    calendar.refetchEvents();
                                me.hide();
                            }
                            me.lastResponse = json;
                            me.onSubmit = false;

                            if(json.message){
                                takeASeatApp.showAjaxMessage(json);
                            }
                        },
                        error:function (e) {
                            me.onSubmit = false;
                            takeASeatApp.showAjaxError(e);
                        }
                    });
                },
                validateForm:function(){
//                    if(!this.form.start_date) return false;
//                    if(!this.form.end_date) return false;
//                    if(!this.form.start_time) return false;
//                    if(!this.form.end_time) return false;
//                    if(this.form.end_time < this.form.start_time) return  false;

                    return true;
                },
                /*addItem:function () {
                    console.log(this.person_types);
                    this.person_types.push(Object.assign([],this.person_type_item));
                },
                deleteItem:function (index) {
                    this.person_types.splice(index,1);
                }*/
                deleteNote:function(){
                    var me = this;
                    takeASeatApp.showConfirm({
                        title: "Delete the note?",
                        message:"Do you want to delete?",
                        callback:function (res) {
                            if(res){
                                $.ajax({
                                    url:'{{route('vendor.profile.notes.delete.custom')}}',
                                    data:me.form,
                                    dataType:'json',
                                    method:'post',
                                    success:function (json) {
                                        if(json.status){
                                            if(calendar)
                                                calendar.refetchEvents();
                                            me.hide();
                                        }

                                        if(json.message){
                                            takeASeatApp.showAjaxMessage(json);
                                        }
                                    },
                                    error:function (e) {
                                        takeASeatApp.showAjaxError(e);
                                    }
                                });
                            }
                        }
                    })
                },
            },
            created:function () {
                var me = this;
                this.$nextTick(function () {
                    $(me.$el).on('hide.bs.modal',function () {

                        this.form = Object.assign({},this.formDefault);
                        this.person_types = [];

                    });

                })
            },
            mounted:function () {
                // $(this.$el).modal();
            }
        });

    </script>
@endsection
