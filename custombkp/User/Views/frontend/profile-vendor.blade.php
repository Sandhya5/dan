@extends('layouts.user')
@section('head')

    <link rel="stylesheet" href="{{asset('libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('libs/fullcalendar-4.2.0/core/main.css')}}">
    <link rel="stylesheet" href="{{asset('libs/fullcalendar-4.2.0/daygrid/main.min.css')}}">
    <link rel="stylesheet" href="{{asset('libs/fullcalendar-4.2.0/timegrid/main.min.css')}}">
    <link rel="stylesheet" href="{{asset('libs/daterange/daterangepicker.css')}}">
    <style>
        .event-name{
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }
        #dates-calendar .loading{

        }
    </style>
@endsection
@section('content')
    <h2 class="title-bar no-border-bottom">
        {{__("Edit Profile")}}s
    </h2>
    @include('Layout::message-vendor')
    @if($row->id)
        @include('Language::admin.navigation')
    @endif
    <div class="lang-content-box">
        <form id="bravo-custom-form-vendor" action="{{route('vendor.profile.store.custom',['lang'=>request()->query('lang')])}}" method="post">
            @csrf
            <div class="form-add-service">
                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                    <a data-toggle="tab" href="#nav-tour-content" aria-selected="true" class="active">{{__("1. Content")}}</a>
                    <a data-toggle="tab" href="#nav-tour-location" aria-selected="false">{{__("2. Locations")}}</a>
                    @if(is_default_lang())
                        <a data-toggle="tab" href="#nav-tour-pricing" aria-selected="false">{{__("3. Pricing")}}</a>
{{--                        <a data-toggle="tab" href="#nav-availability" aria-selected="false">{{__("4. Availability")}}</a>--}}
                        <a data-toggle="tab" href="#nav-attribute" aria-selected="false">{{__("4. Areas of Counselling")}}</a>
                        <a data-toggle="tab" href="#nav-bank-detail" aria-selected="false">{{__("5. Bank details")}}</a>
                    @endif
                </div>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-tour-content">
                        @include('Tour::admin/tour/tour-content')
                    </div>
                    <div class="tab-pane fade" id="nav-tour-location">
                        @include('Tour::admin/tour/tour-location')
                    </div>
                    @if(is_default_lang())
                        <div class="tab-pane fade" id="nav-tour-pricing">
                            @include('Tour::admin/tour/pricing')
                            <input type="hidden" name="enable_open_hours" value="1">
                        </div>
{{--                        <div class="tab-pane fade" id="nav-availability">--}}
{{--                            @include('Tour::admin/tour/custom-availability')--}}
{{--                        </div>--}}
                        <div class="tab-pane fade" id="nav-attribute">
                            @include('Tour::admin/tour/attributes')
                        </div>
                        <div class="tab-pane fade" id="nav-bank-detail">
                            @include('Tour::admin/tour/bank-details')
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label>
                        <input type="checkbox" name="introductions_ownership_policy" @if(!empty($row->introductions_ownership_policy)) checked @endif value="1">
                        @php
                            $policy = setting_item('custom_page_introductions_ownership_policy');
                        @endphp
                        {{__('Please accept:')}} <a target="_blank" href="{{get_page_url($policy)}}">{{__('Introductions & Ownership policy')}}</a>
                    </label>
                </div>
            </div>
            <div class="d-flex justify-content-between">
                <button name="form_submit" class="btn btn-primary" type="submit" value="submit"  onClick="submit()"><i class="fa fa-save"></i> {{__('Save Changes')}}</button>
                <button name="form_submit" class="btn btn-primary" type="submit" value="submitForApproval"  onClick="submit()"><i class="fa fa-save"></i> {{__('Submit profile for approval')}}</button>
            </div>
        </form>
    </div>


@endsection
@section('footer')
    <script type="text/javascript" src="{{ asset('libs/tinymce/js/tinymce/tinymce.min.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('js/condition.js?_ver='.config('app.version')) }}"></script>
    <script type="text/javascript" src="{{url('module/core/js/map-engine.js?_ver='.config('app.version'))}}"></script>
    <script>
        jQuery(function ($) {
            new BravoMapEngine('map_content', {
                fitBounds: true,
                center: [{{$row->map_lat ?? "51.505"}}, {{$row->map_lng ?? "-0.09"}}],
                zoom:{{$row->map_zoom ?? "8"}},
                ready: function (engineMap) {
                    @if($row->map_lat && $row->map_lng)
                    engineMap.addMarker([{{$row->map_lat}}, {{$row->map_lng}}], {
                        icon_options: {}
                    });
                    @endif
                    engineMap.on('click', function (dataLatLng) {
                        engineMap.clearMarkers();
                        engineMap.addMarker(dataLatLng, {
                            icon_options: {}
                        });
                        $("input[name=map_lat]").attr("value", dataLatLng[0]);
                        $("input[name=map_lng]").attr("value", dataLatLng[1]);
                    });
                    engineMap.on('zoom_changed', function (zoom) {
                        $("input[name=map_zoom]").attr("value", zoom);
                    });
                    engineMap.searchBox($('.bravo_searchbox'),function (dataLatLng,place) {
                        console.log(place.address_components);
                        engineMap.clearMarkers();
                        engineMap.addMarker(dataLatLng, {
                            icon_options: {}
                        });
                        $("input[name=map_lat]").attr("value", dataLatLng[0]);
                        $("input[name=map_lng]").attr("value", dataLatLng[1]);

                        var country = '';
                        var city = '';
                        var list_item = place.address_components;
                        for (var index in list_item) {
                            var item = list_item[index];

                            for( var index2 in item.types){
                                var type = item.types[index2];
                                if(type === "country"){
                                    country = item.long_name
                                }
                                if(type === "administrative_area_level_1"){
                                    city = item.long_name
                                }
                            }
                        }

                        $("input[name=country]").attr("value", country );
                        $("input[name=city]").attr("value", city );
                    });
                }
            });

            $(".bravo_faqs2 textarea.form-control").each(function () {
                if( $(this).val() !== "")
                {
                    $(this).closest(".item").find(".number_char").html( WordCount( $(this).val())  );
                }
                if( WordCount($(this).val()) > 200){
                    $(this).css("border-color","#ff3535").css("color","#ff3535")
                }else{
                    $(this).css("border-color","#dae1e7").css("color","#495057")
                }
                $(this).keyup(function () {
                    $(this).closest(".item").find(".number_char").html( WordCount($(this).val()) );
                    if( WordCount($(this).val()) > 200){
                        $(this).css("border-color","#ff3535").css("color","#ff3535")
                    }else{
                        $(this).css("border-color","#dae1e7").css("color","#495057")
                    }
                });
            });
            function submit(){ jQuery('#bravo-custom-form-vendor').submit()};

            function WordCount(str) {
                return str.split(" ").length;
            }
        })
    </script>

    <script src="{{asset('libs/daterange/moment.min.js')}}"></script>
    <script src="{{asset('libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('libs/daterange/daterangepicker.min.js?_ver='.config('app.version'))}}"></script>
@endsection
