@extends('layouts.user')
@section('head')

    <link rel="stylesheet" href="{{asset('libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('libs/fullcalendar-4.2.0/core/main.css')}}">
    <link rel="stylesheet" href="{{asset('libs/fullcalendar-4.2.0/daygrid/main.min.css')}}">
    <link rel="stylesheet" href="{{asset('libs/fullcalendar-4.2.0/timegrid/main.min.css')}}">
    <link rel="stylesheet" href="{{asset('libs/daterange/daterangepicker.css')}}">
    <style>
        .event-name{
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }
        #dates-calendar .loading{

        }
    </style>
@endsection
@section('content')
    <h2 class="title-bar no-border-bottom">
        {{__("Calendar")}}
    </h2>
    @include('admin.message')
    <div class="lang-content-box">
        <form id="bravo-custom-form-vendor" action="{{route('vendor.profile.store.calendar.custom',['lang'=>request()->query('lang')])}}" method="post">
            @csrf
            <div class="form-add-service">
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane show fade active" id="nav-availability">
                        @include('Tour::admin/tour/custom-availability')
                        <hr>
                        @include('Tour::admin/tour/open-hours')
                        <div class="d-flex justify-content-between">
                            <button class="btn btn-primary" type="submit" ><i class="fa fa-save"></i> {{__('Save Changes')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>


@endsection
@section('footer')
    <script type="text/javascript" src="{{ asset('libs/tinymce/js/tinymce/tinymce.min.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('js/condition.js?_ver='.config('app.version')) }}"></script>

    <script src="{{asset('libs/daterange/moment.min.js')}}"></script>
    <script src="{{asset('libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('libs/daterange/daterangepicker.min.js?_ver='.config('app.version'))}}"></script>
    <script src="{{asset('libs/fullcalendar-4.2.0/core/main.js')}}"></script>
    <script src="{{asset('libs/fullcalendar-4.2.0/interaction/main.js')}}"></script>
    <script src="{{asset('libs/fullcalendar-4.2.0/daygrid/main.js')}}"></script>
    <script src="{{asset('libs/fullcalendar-4.2.0/timegrid/main.js')}}"></script>
    <script src="{{asset('libs/fullcalendar-4.2.0/moment/main.js')}}"></script>
    <script>
        var calendarMonthEl,calendarEl,calendarMonth,calendar,lastId,formModal;
        calendarMonthEl = document.getElementById('months-calendar');
        calendarEl = document.getElementById('dates-calendar');
        lastId = "{{$row->id}}";
        if(calendar){
            calendar.destroy();
        }
        if(calendarMonth){
            calendarMonth.destroy();
        }
//        calendarMonth  = new FullCalendar.Calendar(calendarMonthEl,{
//            plugins: [ 'dayGrid' ,'interaction'],
//            header: {},
//            selectable: true,
//            selectMirror: false,
//            allDay:false,
//            editable: false,
//            eventLimit: true,
//            defaultView: 'dayGridMonth',
//        });
//        calendarMonth.render();
        $('#months-calendar').datepicker({
            "setDate": new Date(),
        }).on('changeDate', function() {
			var val = $('#months-calendar').datepicker('getDate');
			var m = moment(val);
			console.log(m.format('YYYY-MM-DD'));
			console.log(val);
            calendar.gotoDate(m.format('YYYY-MM-DD'));
		});

        calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: [ 'timeGrid' ,'interaction',"moment"],
            header: false,
            selectable: true,
            selectMirror: false,
            allDay:false,
            editable: false,
            eventLimit: true,
            defaultView: 'timeGridWeek',
			allDaySlot: false,
            minTime:"06:00:00",
            maxTime:"23:00:00",
			events:{
                url:"{{route('tour.vendor.availability.loadDates')}}",
                extraParams:{
                    id:lastId,
                },
                timeZoneParam:'UTC'
            },
            views:{
                timeGrid:{
                    columnHeaderFormat:"dd MM/DD"
                }
            },
            loading:function (isLoading) {
                if(!isLoading){
                    $(calendarEl).removeClass('loading');
                }else{
                    $(calendarEl).addClass('loading');
                }
            },
            select: function(arg) {
                formModal.show({
                    start_date:moment(arg.start).format('YYYY-MM-DD'),
                    end_date:moment(arg.end).format('YYYY-MM-DD'),
                });
            },
            eventClick:function (info) {
				var form = Object.assign({},info.event.extendedProps);
                if(!form.booking_online){
					form.start_date = moment(info.event.start).format('YYYY-MM-DD');
					form.end_date = moment(info.event.start).format('YYYY-MM-DD');
					console.log(form);
					console.log(form);
					formModal.show(form);
                }else{
                    if( $("body").find("#modal-booking-"+form.booking_id).length === 0 ){
                        $("body").append("<div class='bravo_custom_detail_booking_for_calendar'>"+form.booking_detail+"</div>");
                    }
                    $("body").find("#modal-booking-"+form.booking_id).modal('show');
                }
            },
            eventRender: function (info) {
                $(info.el).find('.fc-title').html(info.event.title);
            }
        });
        calendar.render();
        $('.event-name:first-child a').trigger('click');

        $('#nav-availability').on('shown.bs.tab',function () {
            window.setTimeout(function () {
                calendar.updateSize();
            },100)
		});

        formModal = new Vue({
            el:'#bravo_modal_calendar',
            data:{
                lastResponse:{
                    status:null,
                    message:''
                },
                form:{
                    id:'',
                    price:'',
                    start_date:'',
                    end_date:'',
                    start_time:1,
                    end_time:23,
                    min_guests:0,
                    max_guests:0,
                    active:0,
                    first_name:'',
                    notes:''
                },
                formDefault:{
                    id:'',
                    price:'',
                    start_date:'',
                    end_date:'',
                    min_guests:0,
                    max_guests:0,
                    active:0
                },
                person_types:[

                ],
                person_type_item:{
                    name:'',
                    desc:'',
                    min:'',
                    max:'',
                    price:'',
                },
                onSubmit:false
            },
            methods:{
                show:function (form) {
                    $(this.$el).modal('show');
                    this.lastResponse.message = '';
                    this.onSubmit = false;

                    if(typeof form !='undefined'){
                        this.form = Object.assign({},form);
                        if(typeof this.form.person_types == 'object'){
                            this.person_types = this.form.person_types;
                        }else{
                            this.person_types = false;
                        }
                        if(form.start_date){
                            var drp = $('.has-daterangepicker').data('daterangepicker');
                            drp.setStartDate(moment(form.start_date).format('MM/DD/YYYY'));
                            drp.setEndDate(moment(form.end_date).format('MM/DD/YYYY'));
                        }
                    }
                },
                hide:function () {
                    $(this.$el).modal('hide');
                    this.form = Object.assign({},this.formDefault);
                    this.person_types = false;
                },
                saveForm:function () {
                    this.form.target_id = lastId;
                    var me = this;
                    me.lastResponse.message = '';
                    if(this.onSubmit) return;

                    if(!this.validateForm()) return;

                    this.onSubmit = true;
                    this.form.person_types = this.person_types;
                    $.ajax({
                        url:'{{route('tour.vendor.availability.store')}}',
                        data:this.form,
                        dataType:'json',
                        method:'post',
                        success:function (json) {
                            if(json.status){
                                if(calendar)
                                    calendar.refetchEvents();
                                me.hide();
                            }
                            me.lastResponse = json;
                            me.onSubmit = false;
                        },
                        error:function (e) {
                            me.onSubmit = false;
                        }
                    });
                },
                validateForm:function(){
//                    if(!this.form.start_date) return false;
//                    if(!this.form.end_date) return false;
//                    if(!this.form.start_time) return false;
//                    if(!this.form.end_time) return false;
//                    if(this.form.end_time < this.form.start_time) return  false;

                    return true;
                },
                /*addItem:function () {
                    console.log(this.person_types);
                    this.person_types.push(Object.assign([],this.person_type_item));
                },
                deleteItem:function (index) {
                    this.person_types.splice(index,1);
                }*/
            },
            created:function () {
                var me = this;
                this.$nextTick(function () {
                    $('.has-daterangepicker').daterangepicker()
                        .on('apply.daterangepicker',function (e,picker) {
                            console.log(picker);
                            me.form.start_date = picker.startDate.format('YYYY-MM-DD');
                            me.form.end_date = picker.endDate.format('YYYY-MM-DD');
                        });

                    $(me.$el).on('hide.bs.modal',function () {

                        this.form = Object.assign({},this.formDefault);
                        this.person_types = [];

                    });

                })
            },
            mounted:function () {
                // $(this.$el).modal();
            }
        });

    </script>
@endsection
