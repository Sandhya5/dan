<?php
namespace Custom\User\Models;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model;

class UserNote extends BaseModel
{
   protected $table = 'user_notes';

}
