<?php

namespace Custom\User;

use Custom\ModuleServiceProvider;
use Illuminate\Support\Facades\Validator;

class ModuleProvider extends ModuleServiceProvider
{

    public function boot(){

        $this->loadMigrationsFrom(__DIR__ . '/Database/Migrations');
        Validator::extend('maxWords', 'Custom\User\Validator\MaxWordsValidator@validate');

    }
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouterServiceProvider::class);
    }

}
