<?php
namespace Custom\Core\Models;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

use Custom\Tour\Models\Tour;
use Modules\Core\Models\Attributes;

class Terms extends \Modules\Core\Models\Terms
{
    protected $fillable = [
        'name',
        'content',
        'image_id',
    ];
    public function getDisplayNumberTourInTerm($term_id){

        $model_Tour = Tour::select("bravo_tours.id");
        $model_Tour->join('bravo_tour_term as tt', 'tt.tour_id', "bravo_tours.id")->whereIn('tt.term_id', [$term_id]);
        $count = $model_Tour->count("bravo_tours.id");
        if($count > 1){
            return  __(":number Therapists",['number' => $count]);
        }else{
            return  __(":number Therapist",['number' => $count]);
        }
    }

    public function getLinkForPageSearchTour($term_id)
    {
        $param = ['terms['.$this->attr_id.'][]' => $term_id];
        return url(app_get_locale(false, false, '/') . config('tour.tour_route_prefix') . "?" . http_build_query($param));
    }
    public function attr(){
    	return $this->belongsTo(Attributes::class,'attr_id')->where('service','tour');
    }
}
