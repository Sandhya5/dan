<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BravoBookingSlot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
	    Schema::create('bravo_booking_slot',function (Blueprint $table){
		    $table->bigIncrements('id');
		    $table->integer('booking_id')->nullable();
		    $table->integer('slot')->nullable();
		    $table->timestamp('date')->nullable();
		    $table->string('flag', 100)->nullable();
		    $table->integer('create_user')->nullable();
		    $table->integer('update_user')->nullable();

		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::dropIfExists('bravo_booking_slot');
        //
    }
}
