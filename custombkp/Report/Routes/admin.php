<?php
use \Illuminate\Support\Facades\Route;
Route::get('/booking','BookingController@index');
Route::get('/booking/email_preview/{id}','BookingController@email_preview');