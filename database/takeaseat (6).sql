-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 27, 2020 at 10:33 PM
-- Server version: 5.6.47-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `takeaseat`
--

-- --------------------------------------------------------

--
-- Table structure for table `bravo_attrs`
--

CREATE TABLE `bravo_attrs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bravo_attrs`
--

INSERT INTO `bravo_attrs` (`id`, `name`, `slug`, `service`, `create_user`, `update_user`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Eating Disorders', 'travel-styles', 'tour', 1, NULL, '2020-05-13 18:13:41', '2020-07-20 14:52:28', NULL),
(2, 'Psychotic Disorders', 'facilities', 'tour', 1, NULL, '2020-05-13 18:13:42', '2020-07-20 14:52:04', NULL),
(4, 'Personality Disorders', 'therapy-type', 'tour', 1, 1, '2020-06-06 11:28:21', '2020-07-20 14:51:43', NULL),
(5, 'Mood Disorders', 'accreditations', 'tour', 1, 1, '2020-06-08 11:49:27', '2020-07-20 14:51:02', NULL),
(6, 'Anxiety', 'therapy-issue', 'tour', 1, 1, '2020-06-08 12:16:55', '2020-07-20 14:50:32', NULL),
(7, 'Addiction', 'addiction', 'tour', 1, NULL, '2020-07-20 14:52:44', '2020-07-24 12:59:35', '2020-07-24 12:59:35'),
(8, 'Trauma Related Issues', 'trauma-related-issues', 'tour', 1, NULL, '2020-07-20 14:53:08', '2020-07-22 18:19:21', '2020-07-22 18:19:21'),
(10, 'Testing therepy', 'testing-therepy', 'tour', 1, NULL, '2020-07-23 09:46:12', '2020-07-23 09:49:42', '2020-07-23 09:49:42'),
(11, 'Addiction', 'addiction', 'tour', 1, NULL, '2020-07-24 13:00:10', '2020-07-24 13:00:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bravo_attrs_translations`
--

CREATE TABLE `bravo_attrs_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bravo_bookings`
--

CREATE TABLE `bravo_bookings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `payment_id` int(11) DEFAULT NULL,
  `gateway` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `object_id` int(11) DEFAULT NULL,
  `object_model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `total_guests` int(11) DEFAULT NULL,
  `currency` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deposit` decimal(10,2) DEFAULT NULL,
  `deposit_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commission` decimal(10,2) DEFAULT NULL,
  `commission_type` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_notes` text COLLATE utf8mb4_unicode_ci,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `buyer_fees` text COLLATE utf8mb4_unicode_ci,
  `total_before_fees` decimal(10,2) DEFAULT NULL,
  `paid_vendor` tinyint(4) DEFAULT NULL,
  `first_booking` tinyint(1) DEFAULT NULL,
  `sess_loc` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bravo_bookings`
--

INSERT INTO `bravo_bookings` (`id`, `code`, `vendor_id`, `customer_id`, `payment_id`, `gateway`, `object_id`, `object_model`, `start_date`, `end_date`, `total`, `total_guests`, `currency`, `status`, `deposit`, `deposit_type`, `commission`, `commission_type`, `email`, `first_name`, `last_name`, `phone`, `address`, `address2`, `city`, `state`, `zip_code`, `country`, `customer_notes`, `create_user`, `update_user`, `deleted_at`, `created_at`, `updated_at`, `buyer_fees`, `total_before_fees`, `paid_vendor`, `first_booking`, `sess_loc`) VALUES
(1, '74221f85a278b23bf13c68e153d23f27', 4, 1, NULL, 'offline_payment', 11, 'space', '2020-07-22 00:00:00', '2020-08-14 00:00:00', '3750.00', 1, NULL, 'processing', NULL, NULL, '345.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'admin@dev.com', 'System', 'Admin', '112 666 888', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2020-07-04 23:08:39', '2020-07-04 23:08:53', '[{\"name\":\"Cleaning fee\",\"desc\":\"One-time fee charged by host to cover the cost of cleaning their space.\",\"name_ja\":\"\\u30af\\u30ea\\u30fc\\u30cb\\u30f3\\u30b0\\u4ee3\",\"desc_ja\":\"\\u30b9\\u30da\\u30fc\\u30b9\\u3092\\u6383\\u9664\\u3059\\u308b\\u8cbb\\u7528\\u3092\\u30db\\u30b9\\u30c8\\u304c\\u8acb\\u6c42\\u3059\\u308b1\\u56de\\u9650\\u308a\\u306e\\u6599\\u91d1\\u3002\",\"price\":\"100\",\"type\":\"one_time\"},{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"200\",\"type\":\"one_time\"}]', '3450.00', NULL, NULL, NULL),
(3, 'bf37439ce33142d7697a277c472af6cc', 4, 1, NULL, 'offline_payment', 11, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2200.00', 0, NULL, 'processing', NULL, NULL, '210.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'admin@dev.com', 'System', 'Admin', '112 666 888', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2020-07-06 01:17:50', '2020-07-06 01:18:25', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '2100.00', NULL, 0, NULL),
(5, '17cfc5ea67b2578042921730aec22b3e', 1, 1, NULL, 'offline_payment', 14, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2200.00', 0, NULL, 'processing', NULL, NULL, '210.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'admin@dev.com', 'System', 'Admin', '112 666 888', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2020-07-06 13:06:47', '2020-07-06 13:08:21', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '2100.00', NULL, 0, NULL),
(31, 'f776e72423bd65fa8e38e842e28637bd', 1, 1, NULL, 'offline_payment', 22, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1076.00', 0, NULL, 'processing', NULL, NULL, '97.60', '{\"amount\":\"10\",\"type\":\"percent\"}', 'mohddanish85564@gmail.com', 'Danish', 'Ali', '8299226971', NULL, NULL, 'Kanpur', NULL, '208023', NULL, NULL, 1, 1, NULL, '2020-07-06 23:28:10', '2020-07-06 23:28:33', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '976.00', NULL, 0, NULL),
(33, 'd49cbc3cb10dc2bd1ab1c1a021476fe4', 1, 1, NULL, 'offline_payment', 12, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2200.00', 0, NULL, 'processing', NULL, NULL, '210.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'admin@dev.com', 'Danish', 'Ali', '8299226971', NULL, NULL, 'Kanpur', NULL, '208023', NULL, NULL, 1, 1, NULL, '2020-07-07 01:10:28', '2020-07-07 01:11:02', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '2100.00', NULL, 0, NULL),
(34, '14a302cf2421ef2814dcd4199cb680c5', 1, 1, NULL, 'offline_payment', 12, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2200.00', 0, NULL, 'processing', NULL, NULL, '210.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'admin@dev.com', 'Danish', 'Ali', '8299226971', NULL, NULL, 'Kanpur', NULL, '208023', NULL, NULL, 1, 1, NULL, '2020-07-07 01:25:03', '2020-07-07 01:25:08', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '2100.00', NULL, 0, NULL),
(37, '86b91b79cd79eae562350ae63baff516', 1, 1, NULL, 'offline_payment', 12, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2200.00', 0, NULL, 'processing', NULL, NULL, '210.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'admin@dev.com', 'Danish', 'Ali', '8299226971', NULL, NULL, 'Kanpur', NULL, '208023', NULL, NULL, 1, 1, NULL, '2020-07-07 01:29:18', '2020-07-07 01:29:24', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '2100.00', NULL, 0, NULL),
(38, '16528c9640da5b2bf446c0d32ef74b05', 1, 1, NULL, 'offline_payment', 2, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1000.00', 0, NULL, 'processing', NULL, NULL, '90.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'admin@dev.com', 'Danish', 'Ali', '8299226971', NULL, NULL, 'Kanpur', NULL, '208023', NULL, NULL, 1, 1, NULL, '2020-07-07 07:12:34', '2020-07-07 07:12:43', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '900.00', NULL, 0, NULL),
(39, 'e073e5a4562bdee7a6bfdc26ae1298e9', 1, 19, NULL, 'offline_payment', 22, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1076.00', 0, NULL, 'processing', NULL, NULL, '97.60', '{\"amount\":\"10\",\"type\":\"percent\"}', 'websitedemoonline@gmail.com', 'david', 'hussey', '4543456756', NULL, NULL, 'test', NULL, '208023', NULL, NULL, 19, 19, NULL, '2020-07-07 12:34:14', '2020-07-07 12:35:49', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '976.00', NULL, 0, NULL),
(45, '4cfbaad0a4699a2e4a3150d02c027bcd', 1, 19, NULL, 'offline_payment', 18, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '700.00', 0, NULL, 'processing', NULL, NULL, '60.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'websitedemoonline@gmail.com', 'david', 'hussey', '4543456756', NULL, NULL, 'test', NULL, '208023', NULL, NULL, 19, 19, NULL, '2020-07-07 14:30:55', '2020-07-07 14:32:18', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '600.00', NULL, 0, NULL),
(46, '2e777ca8f71e4e2be1556fc40e120bf3', 1, 19, NULL, 'offline_payment', 22, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1076.00', 0, NULL, 'processing', NULL, NULL, '97.60', '{\"amount\":\"10\",\"type\":\"percent\"}', 'websitedemoonline@gmail.com', 'david', 'hussey', '4543456756', NULL, NULL, 'test', NULL, '208023', NULL, NULL, 19, 19, NULL, '2020-07-07 14:50:49', '2020-07-07 14:51:04', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '976.00', NULL, 0, NULL),
(47, 'd6cba9a8d41146e013565a96e0079d66', 5, 19, NULL, 'offline_payment', 13, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2200.00', 0, NULL, 'processing', NULL, NULL, '210.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'websitedemoonline@gmail.com', 'david', 'hussey', '4543456756', NULL, NULL, 'test', NULL, '208023', NULL, NULL, 19, 19, NULL, '2020-07-07 20:54:29', '2020-07-07 20:54:40', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '2100.00', NULL, 0, NULL),
(48, '96781c837d84e72d1e1ed0c02755b24b', 1, 1, NULL, 'offline_payment', 14, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2200.00', 0, NULL, 'processing', NULL, NULL, '210.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'admin@dev.com', 'Danish', 'Ali', '8299226971', NULL, NULL, 'Kanpur', NULL, '208023', NULL, NULL, 1, 1, NULL, '2020-07-07 21:28:50', '2020-07-07 21:29:00', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '2100.00', NULL, 0, 'facetoface'),
(49, '881f10fea450be06b18734fdf856338c', 1, 1, NULL, 'offline_payment', 14, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2200.00', 0, NULL, 'processing', NULL, NULL, '210.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'admin@dev.com', 'Danish', 'Ali', '8299226971', NULL, NULL, 'Kanpur', NULL, '208023', NULL, NULL, 1, 1, NULL, '2020-07-07 21:40:04', '2020-07-07 21:40:12', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '2100.00', NULL, 0, 'online'),
(50, 'b78fddc9ab55d606f0cc698c144653a7', 1, 1, NULL, 'offline_payment', 22, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1076.00', 0, NULL, 'processing', NULL, NULL, '97.60', '{\"amount\":\"10\",\"type\":\"percent\"}', 'admin@dev.com', 'Danish', 'Sale', '8299226971', '12321', NULL, 'GURGAON', NULL, '122001', NULL, NULL, 1, 1, NULL, '2020-07-07 23:06:19', '2020-07-07 23:06:30', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '976.00', NULL, 0, NULL),
(51, '6b20e3ba3dcf0941b8d8b10cd6110b70', 1, 1, NULL, 'offline_payment', 22, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1076.00', 0, NULL, 'processing', NULL, NULL, '97.60', '{\"amount\":\"10\",\"type\":\"percent\"}', 'admin@dev.com', 'Danish', 'Sale', '8299226971', NULL, NULL, 'GURGAON', NULL, '122001', NULL, NULL, 1, 1, NULL, '2020-07-07 23:09:40', '2020-07-07 23:09:50', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '976.00', NULL, 0, 'facetoface'),
(54, '7ddfc2c6c86165170b71ea1a8920e8ed', 1, 19, NULL, 'offline_payment', 22, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1076.00', 0, NULL, 'processing', NULL, NULL, '97.60', '{\"amount\":\"10\",\"type\":\"percent\"}', 'websitedemoonline@gmail.com', 'david', 'hussey', '4543456756', NULL, NULL, 'test', NULL, '208023', NULL, NULL, 19, 19, NULL, '2020-07-08 12:32:10', '2020-07-08 12:32:15', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '976.00', NULL, 0, NULL),
(55, '70827afcc585f0d59b6ecba1ebb645bb', 1, 1, NULL, 'offline_payment', 22, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1076.00', 0, NULL, 'processing', NULL, NULL, '97.60', '{\"amount\":\"10\",\"type\":\"percent\"}', 'admin@dev.com', 'Danish', 'Sale', '8299226971', NULL, NULL, 'GURGAON', NULL, '122001', NULL, NULL, 1, 1, NULL, '2020-07-08 12:46:23', '2020-07-08 12:46:25', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '976.00', NULL, 0, NULL),
(56, '791ae32db179c78f925b0bc981b45213', 1, 19, NULL, 'offline_payment', 18, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '700.00', 0, NULL, 'processing', NULL, NULL, '60.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'websitedemoonline@gmail.com', 'david', 'hussey', '4543456756', NULL, NULL, 'test', NULL, '208023', NULL, NULL, 19, 19, NULL, '2020-07-10 15:54:23', '2020-07-10 15:54:37', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '600.00', NULL, 0, 'online'),
(57, 'cfa9dbe0028415b30ed2cd6b0fd61088', 1, 19, NULL, 'offline_payment', 18, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '700.00', 0, NULL, 'processing', NULL, NULL, '60.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'websitedemoonline@gmail.com', 'david', 'hussey', '4543456756', NULL, NULL, 'test', NULL, '208023', NULL, NULL, 19, 19, NULL, '2020-07-10 16:19:42', '2020-07-10 16:19:59', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '600.00', NULL, 0, 'online'),
(67, '831c8dddd073e49821d6304719ea0396', 1, 19, NULL, 'offline_payment', 4, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '950.00', 0, NULL, 'processing', NULL, NULL, '85.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'websitedemoonline@gmail.com', 'david', 'hussey', '4543456756', NULL, NULL, 'test', NULL, '208023', NULL, NULL, 19, 19, NULL, '2020-07-11 14:45:25', '2020-07-11 14:46:23', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '850.00', NULL, 0, 'facetoface'),
(68, '6a28f8c43ea632fc882d8cae3ac8d92a', 1, 19, NULL, 'offline_payment', 4, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '950.00', 0, NULL, 'processing', NULL, NULL, '85.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'websitedemoonline@gmail.com', 'david', 'hussey', '4543456756', NULL, NULL, 'test', NULL, '208023', NULL, NULL, 19, 19, NULL, '2020-07-11 14:57:19', '2020-07-11 14:57:25', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '850.00', NULL, 0, 'facetoface'),
(69, 'e1230674fd74ae8e2783a324a4113239', 1, 19, NULL, 'offline_payment', 4, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '950.00', 0, NULL, 'processing', NULL, NULL, '85.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'websitedemoonline@gmail.com', 'david', 'hussey', '4543456756', NULL, NULL, 'test', NULL, '208023', NULL, NULL, 19, 19, NULL, '2020-07-11 15:52:56', '2020-07-11 15:53:02', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '850.00', NULL, 0, 'facetoface'),
(70, 'f66338a4fba0d2d4deb3944754c0594d', 1, 19, NULL, 'offline_payment', 14, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2200.00', 0, NULL, 'processing', NULL, NULL, '210.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'websitedemoonline@gmail.com', 'david', 'hussey', '4543456756', NULL, NULL, 'test', NULL, '208023', NULL, NULL, 19, 19, NULL, '2020-07-11 16:03:15', '2020-07-11 16:03:23', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '2100.00', NULL, 0, 'facetoface'),
(71, '2c682aca769ab3b0567cb964469c9b3e', 1, 19, NULL, 'offline_payment', 14, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2200.00', 0, NULL, 'processing', NULL, NULL, '210.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'websitedemoonline@gmail.com', 'david', 'hussey', '4543456756', NULL, NULL, 'test', NULL, '208023', NULL, NULL, 19, 19, NULL, '2020-07-11 17:09:49', '2020-07-11 17:10:00', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '2100.00', NULL, 0, 'facetoface'),
(72, '01ce8f63b27ab1846b99a13b5d8e9d4e', 4, 19, NULL, 'offline_payment', 6, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2200.00', 0, NULL, 'processing', NULL, NULL, '210.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'misterwebhub@gmail.com', 'Misterwebhub', 'Ansari', '8299226971', '13', NULL, 'Kanpur', NULL, '208023', NULL, NULL, 19, 19, NULL, '2020-07-11 17:24:15', '2020-07-11 17:24:33', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '2100.00', NULL, 0, 'online'),
(73, 'c492af2a1ad1fbcae5351fce67d16734', 1, 19, NULL, 'offline_payment', 14, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2200.00', 0, NULL, 'processing', NULL, NULL, '210.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'websitedemoonline@gmail.com', 'Misterwebhub', 'Ansari', '8299226971', NULL, NULL, 'Kanpur', NULL, '208023', NULL, NULL, 19, 19, NULL, '2020-07-12 15:58:01', '2020-07-12 15:58:14', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '2100.00', NULL, 0, 'facetoface'),
(75, 'bccd2031a2df6105427f6a34c0f5c1de', 1, 19, NULL, 'offline_payment', 14, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2200.00', 0, NULL, 'processing', NULL, NULL, '210.00', '{\"amount\":\"10\",\"type\":\"percent\"}', 'websitedemoonline@gmail.com', 'Misterwebhub', 'Ansari', '8299226971', NULL, NULL, 'Kanpur', NULL, '208023', NULL, NULL, 19, 19, NULL, '2020-07-12 16:01:49', '2020-07-12 16:13:07', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '2100.00', NULL, 0, 'online'),
(79, 'd5c63b0f39221fd19b9ff1e8b185de62', 65, 46, NULL, 'offline_payment', 75, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '75.00', 0, NULL, 'processing', NULL, NULL, '0.00', '', 'sarvesh.dataman@gmail.com', 'sarvesh', 'kr', '778888888', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 46, 46, NULL, '2020-07-24 11:50:38', '2020-07-24 11:51:05', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '25.00', NULL, 1, 'facetoface'),
(80, '9927de8261d1ad518319ffe5a0dde4d4', 74, 46, NULL, 'offline_payment', 91, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '180.00', 0, NULL, 'processing', NULL, NULL, '0.00', '', 'sarvesh.dataman@gmail.com', 'sarvesh', 'kr', '778888888', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 46, 46, NULL, '2020-07-24 11:56:45', '2020-07-24 11:56:58', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '80.00', NULL, 0, 'online'),
(81, '734f2e4bbb87fed961d5946e35ed04bb', 79, 46, NULL, 'offline_payment', 101, 'tour', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '195.00', 0, NULL, 'processing', NULL, NULL, '0.00', '', 'sarvesh.dataman@gmail.com', 'sarvesh', 'kr', '778888888', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 46, 46, NULL, '2020-07-26 00:35:16', '2020-07-26 00:35:27', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', '95.00', NULL, 0, 'facetoface');

-- --------------------------------------------------------

--
-- Table structure for table `bravo_booking_meta`
--

CREATE TABLE `bravo_booking_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `booking_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `val` text COLLATE utf8mb4_unicode_ci,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bravo_booking_meta`
--

INSERT INTO `bravo_booking_meta` (`id`, `booking_id`, `name`, `val`, `create_user`, `update_user`, `created_at`, `updated_at`) VALUES
(1, 1, 'duration', NULL, NULL, NULL, NULL, NULL),
(2, 1, 'base_price', '300.00', NULL, NULL, NULL, NULL),
(3, 1, 'sale_price', '150.00', NULL, NULL, NULL, NULL),
(4, 1, 'guests', '1', NULL, NULL, NULL, NULL),
(5, 1, 'adults', '1', NULL, NULL, NULL, NULL),
(6, 1, 'children', '0', NULL, NULL, NULL, NULL),
(7, 1, 'locale', 'en', NULL, NULL, NULL, NULL),
(8, 2, 'duration', NULL, NULL, NULL, NULL, NULL),
(9, 2, 'base_price', '300.00', NULL, NULL, NULL, NULL),
(10, 2, 'sale_price', '150.00', NULL, NULL, NULL, NULL),
(11, 2, 'guests', '1', NULL, NULL, NULL, NULL),
(12, 2, 'adults', '1', NULL, NULL, NULL, NULL),
(13, 2, 'children', '0', NULL, NULL, NULL, NULL),
(14, 3, 'term_id', NULL, NULL, NULL, NULL, NULL),
(15, 3, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(16, 3, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(17, 3, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(18, 3, 'locale', 'en', NULL, NULL, NULL, NULL),
(19, 4, 'term_id', NULL, NULL, NULL, NULL, NULL),
(20, 4, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(21, 4, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(22, 4, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(23, 5, 'term_id', '23', NULL, NULL, NULL, NULL),
(24, 5, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(25, 5, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(26, 5, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(27, 5, 'locale', 'en', NULL, NULL, NULL, NULL),
(28, 6, 'term_id', '8', NULL, NULL, NULL, NULL),
(29, 6, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(30, 6, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(31, 6, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(32, 7, 'term_id', '22', NULL, NULL, NULL, NULL),
(33, 7, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(34, 7, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":600}}', NULL, NULL, NULL, NULL),
(35, 7, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(36, 8, 'term_id', NULL, NULL, NULL, NULL, NULL),
(37, 8, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(38, 8, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":600}}', NULL, NULL, NULL, NULL),
(39, 8, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(40, 9, 'term_id', '22', NULL, NULL, NULL, NULL),
(41, 9, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(42, 9, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":600}}', NULL, NULL, NULL, NULL),
(43, 9, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(44, 10, 'term_id', '22', NULL, NULL, NULL, NULL),
(45, 10, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(46, 10, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":600}}', NULL, NULL, NULL, NULL),
(47, 10, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(48, 11, 'term_id', NULL, NULL, NULL, NULL, NULL),
(49, 11, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(50, 11, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":600}}', NULL, NULL, NULL, NULL),
(51, 11, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(52, 12, 'term_id', '23', NULL, NULL, NULL, NULL),
(53, 12, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(54, 12, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(55, 12, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(56, 13, 'term_id', NULL, NULL, NULL, NULL, NULL),
(57, 13, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(58, 13, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(59, 13, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(60, 14, 'term_id', NULL, NULL, NULL, NULL, NULL),
(61, 14, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(62, 14, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(63, 14, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(64, 15, 'term_id', NULL, NULL, NULL, NULL, NULL),
(65, 15, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(66, 15, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(67, 15, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(68, 16, 'term_id', NULL, NULL, NULL, NULL, NULL),
(69, 16, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(70, 16, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(71, 16, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(72, 17, 'term_id', NULL, NULL, NULL, NULL, NULL),
(73, 17, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(74, 17, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(75, 17, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(76, 18, 'term_id', '23', NULL, NULL, NULL, NULL),
(77, 18, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(78, 18, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(79, 18, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(80, 19, 'term_id', NULL, NULL, NULL, NULL, NULL),
(81, 19, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(82, 19, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":900}}', NULL, NULL, NULL, NULL),
(83, 19, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(84, 20, 'term_id', '3', NULL, NULL, NULL, NULL),
(85, 20, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(86, 20, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(87, 20, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(88, 21, 'term_id', NULL, NULL, NULL, NULL, NULL),
(89, 21, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(90, 21, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(91, 21, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(92, 22, 'term_id', NULL, NULL, NULL, NULL, NULL),
(93, 22, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(94, 22, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(95, 22, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(96, 23, 'term_id', NULL, NULL, NULL, NULL, NULL),
(97, 23, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(98, 23, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(99, 23, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(100, 24, 'term_id', '23', NULL, NULL, NULL, NULL),
(101, 24, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(102, 24, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(103, 24, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(104, 25, 'term_id', '23', NULL, NULL, NULL, NULL),
(105, 25, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(106, 25, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(107, 25, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(108, 26, 'term_id', NULL, NULL, NULL, NULL, NULL),
(109, 26, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(110, 26, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(111, 26, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(112, 27, 'term_id', NULL, NULL, NULL, NULL, NULL),
(113, 27, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(114, 27, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(115, 27, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(116, 28, 'term_id', NULL, NULL, NULL, NULL, NULL),
(117, 28, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(118, 28, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(119, 28, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(120, 29, 'term_id', NULL, NULL, NULL, NULL, NULL),
(121, 29, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(122, 29, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(123, 29, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(124, 30, 'term_id', NULL, NULL, NULL, NULL, NULL),
(125, 30, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(126, 30, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(127, 30, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(128, 31, 'term_id', NULL, NULL, NULL, NULL, NULL),
(129, 31, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(130, 31, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(131, 31, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(132, 31, 'locale', 'en', NULL, NULL, NULL, NULL),
(133, 32, 'term_id', NULL, NULL, NULL, NULL, NULL),
(134, 32, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(135, 32, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(136, 32, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(137, 33, 'term_id', NULL, NULL, NULL, NULL, NULL),
(138, 33, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(139, 33, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(140, 33, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(141, 33, 'locale', 'en', NULL, NULL, NULL, NULL),
(142, 34, 'term_id', NULL, NULL, NULL, NULL, NULL),
(143, 34, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(144, 34, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(145, 34, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(146, 34, 'locale', 'en', NULL, NULL, NULL, NULL),
(147, 35, 'term_id', NULL, NULL, NULL, NULL, NULL),
(148, 35, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(149, 35, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(150, 35, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(151, 36, 'term_id', NULL, NULL, NULL, NULL, NULL),
(152, 36, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(153, 36, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(154, 36, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(155, 37, 'term_id', NULL, NULL, NULL, NULL, NULL),
(156, 37, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(157, 37, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(158, 37, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(159, 37, 'locale', 'en', NULL, NULL, NULL, NULL),
(160, 38, 'term_id', NULL, NULL, NULL, NULL, NULL),
(161, 38, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(162, 38, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":900}}', NULL, NULL, NULL, NULL),
(163, 38, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(164, 38, 'locale', 'en', NULL, NULL, NULL, NULL),
(165, 39, 'term_id', '23', NULL, NULL, NULL, NULL),
(166, 39, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(167, 39, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(168, 39, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(169, 39, 'locale', 'en', NULL, NULL, NULL, NULL),
(170, 40, 'term_id', NULL, NULL, NULL, NULL, NULL),
(171, 40, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(172, 40, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(173, 40, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(174, 41, 'term_id', '23', NULL, NULL, NULL, NULL),
(175, 41, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(176, 41, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(177, 41, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(178, 42, 'term_id', '23', NULL, NULL, NULL, NULL),
(179, 42, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(180, 42, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(181, 42, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(182, 43, 'term_id', NULL, NULL, NULL, NULL, NULL),
(183, 43, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(184, 43, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(185, 43, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(186, 44, 'term_id', NULL, NULL, NULL, NULL, NULL),
(187, 44, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(188, 44, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(189, 44, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(190, 45, 'term_id', '22', NULL, NULL, NULL, NULL),
(191, 45, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(192, 45, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":600}}', NULL, NULL, NULL, NULL),
(193, 45, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(194, 45, 'locale', 'en', NULL, NULL, NULL, NULL),
(195, 46, 'term_id', NULL, NULL, NULL, NULL, NULL),
(196, 46, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(197, 46, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(198, 46, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(199, 46, 'locale', 'en', NULL, NULL, NULL, NULL),
(200, 47, 'term_id', '13', NULL, NULL, NULL, NULL),
(201, 47, 'tmp_dates', '{\"2020-05-07\":{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(202, 47, 'tmp_slots', '{\"2020-05-07 1:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(203, 47, 'selected_dates', '[{\"date\":\"2020-05-07\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(204, 47, 'locale', 'en', NULL, NULL, NULL, NULL),
(205, 48, 'term_id', 'Satellite Office', NULL, NULL, NULL, NULL),
(206, 48, 'tmp_dates', '{\"2020-07-09\":{\"date\":\"2020-07-09\",\"slots\":[\"1\"]}}', NULL, NULL, NULL, NULL),
(207, 48, 'tmp_slots', '{\"2020-07-09 1:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(208, 48, 'selected_dates', '[{\"date\":\"2020-07-09\",\"slots\":[\"1\"]}]', NULL, NULL, NULL, NULL),
(209, 48, 'locale', 'en', NULL, NULL, NULL, NULL),
(210, 49, 'term_id', 'Nature & Adventure', NULL, NULL, NULL, NULL),
(211, 49, 'tmp_dates', '{\"2020-07-15\":{\"date\":\"2020-07-15\",\"slots\":[\"13:00\"]}}', NULL, NULL, NULL, NULL),
(212, 49, 'tmp_slots', '{\"2020-07-15 13:00:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(213, 49, 'selected_dates', '[{\"date\":\"2020-07-15\",\"slots\":[\"13:00\"]}]', NULL, NULL, NULL, NULL),
(214, 49, 'locale', 'en', NULL, NULL, NULL, NULL),
(215, 50, 'term_id', 'Federation of Holistic Therapists (FHT)', NULL, NULL, NULL, NULL),
(216, 50, 'tmp_dates', '{\"2020-07-08\":{\"date\":\"2020-07-08\",\"slots\":[\"14:00\"]}}', NULL, NULL, NULL, NULL),
(217, 50, 'tmp_slots', '{\"2020-07-08 14:00:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(218, 50, 'selected_dates', '[{\"date\":\"2020-07-08\",\"slots\":[\"14:00\"]}]', NULL, NULL, NULL, NULL),
(219, 50, 'locale', 'en', NULL, NULL, NULL, NULL),
(220, 51, 'term_id', 'Federation of Holistic Therapists (FHT)', NULL, NULL, NULL, NULL),
(221, 52, 'term_id', 'Federation of Holistic Therapists (FHT)', NULL, NULL, NULL, NULL),
(222, 51, 'tmp_dates', '{\"2020-07-10\":{\"date\":\"2020-07-10\",\"slots\":[\"14:00\"]}}', NULL, NULL, NULL, NULL),
(223, 52, 'tmp_dates', '{\"2020-07-10\":{\"date\":\"2020-07-10\",\"slots\":[\"14:00\"]}}', NULL, NULL, NULL, NULL),
(224, 51, 'tmp_slots', '{\"2020-07-10 14:00:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(225, 52, 'tmp_slots', '{\"2020-07-10 14:00:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(226, 51, 'selected_dates', '[{\"date\":\"2020-07-10\",\"slots\":[\"14:00\"]}]', NULL, NULL, NULL, NULL),
(227, 52, 'selected_dates', '[{\"date\":\"2020-07-10\",\"slots\":[\"14:00\"]}]', NULL, NULL, NULL, NULL),
(228, 51, 'locale', 'en', NULL, NULL, NULL, NULL),
(229, 53, 'term_id', 'Federation of Holistic Therapists (FHT)', NULL, NULL, NULL, NULL),
(230, 53, 'tmp_dates', '{\"2020-07-10\":{\"date\":\"2020-07-10\",\"slots\":[\"14:00\"]}}', NULL, NULL, NULL, NULL),
(231, 53, 'tmp_slots', '{\"2020-07-10 14:00:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(232, 53, 'selected_dates', '[{\"date\":\"2020-07-10\",\"slots\":[\"14:00\"]}]', NULL, NULL, NULL, NULL),
(233, 54, 'term_id', 'Federation of Holistic Therapists (FHT)', NULL, NULL, NULL, NULL),
(234, 54, 'tmp_dates', '{\"2020-07-08\":{\"date\":\"2020-07-08\",\"slots\":[\"12:00\"]}}', NULL, NULL, NULL, NULL),
(235, 54, 'tmp_slots', '{\"2020-07-08 12:00:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(236, 54, 'selected_dates', '[{\"date\":\"2020-07-08\",\"slots\":[\"12:00\"]}]', NULL, NULL, NULL, NULL),
(237, 54, 'locale', 'en', NULL, NULL, NULL, NULL),
(238, 55, 'term_id', 'Federation of Holistic Therapists (FHT)', NULL, NULL, NULL, NULL),
(239, 55, 'tmp_dates', '{\"2020-07-08\":{\"date\":\"2020-07-08\",\"slots\":[\"14:00\"]}}', NULL, NULL, NULL, NULL),
(240, 55, 'tmp_slots', '{\"2020-07-08 14:00:00:00\":{\"price\":976}}', NULL, NULL, NULL, NULL),
(241, 55, 'selected_dates', '[{\"date\":\"2020-07-08\",\"slots\":[\"14:00\"]}]', NULL, NULL, NULL, NULL),
(242, 55, 'locale', 'en', NULL, NULL, NULL, NULL),
(243, 56, 'term_id', 'Mindfulness Based', NULL, NULL, NULL, NULL),
(244, 56, 'tmp_dates', '{\"2020-07-10\":{\"date\":\"2020-07-10\",\"slots\":[\"12:00\"]}}', NULL, NULL, NULL, NULL),
(245, 56, 'tmp_slots', '{\"2020-07-10 12:00:00:00\":{\"price\":600}}', NULL, NULL, NULL, NULL),
(246, 56, 'selected_dates', '[{\"date\":\"2020-07-10\",\"slots\":[\"12:00\"]}]', NULL, NULL, NULL, NULL),
(247, 56, 'locale', 'en', NULL, NULL, NULL, NULL),
(248, 57, 'term_id', 'Mindfulness Based', NULL, NULL, NULL, NULL),
(249, 57, 'tmp_dates', '{\"2020-07-12\":{\"date\":\"2020-07-12\",\"slots\":[\"12:00\"]}}', NULL, NULL, NULL, NULL),
(250, 57, 'tmp_slots', '{\"2020-07-12 12:00:00:00\":{\"price\":600}}', NULL, NULL, NULL, NULL),
(251, 57, 'selected_dates', '[{\"date\":\"2020-07-12\",\"slots\":[\"12:00\"]}]', NULL, NULL, NULL, NULL),
(252, 57, 'locale', 'en', NULL, NULL, NULL, NULL),
(279, 64, 'term_id', 'Wifi', NULL, NULL, NULL, NULL),
(280, 64, 'tmp_dates', '{\"2020-07-12\":{\"date\":[\"2020-07-12\",\"2020-07-13\"],\"slots\":[[\"13\",\"14\"]]}}', NULL, NULL, NULL, NULL),
(281, 64, 'tmp_slots', '{\"2020-07-12 13:00:00\":{\"price\":850}}', NULL, NULL, NULL, NULL),
(282, 64, 'selected_dates', '[{\"date\":[\"2020-07-12\",\"2020-07-13\"],\"slots\":[[\"13\",\"14\"]]}]', NULL, NULL, NULL, NULL),
(283, 65, 'term_id', 'Gymnasium', NULL, NULL, NULL, NULL),
(284, 65, 'tmp_dates', '{\"2020-07-12\":{\"date\":[\"2020-07-12\",\"2020-07-13\"],\"slots\":[[\"12\",\"14\"]]}}', NULL, NULL, NULL, NULL),
(285, 65, 'tmp_slots', '{\"2020-07-12 12:00:00\":{\"price\":850}}', NULL, NULL, NULL, NULL),
(286, 65, 'selected_dates', '[{\"date\":[\"2020-07-12\",\"2020-07-13\"],\"slots\":[[\"12\",\"14\"]]}]', NULL, NULL, NULL, NULL),
(291, 67, 'term_id', 'Gymnasium', NULL, NULL, NULL, NULL),
(292, 67, 'tmp_dates', '{\"2020-07-12\":{\"date\":[\"2020-07-12\",\"2020-07-13\"],\"slots\":[[\"12\",\"14\"]]}}', NULL, NULL, NULL, NULL),
(293, 67, 'tmp_slots', '{\"2020-07-12 12:00:00\":{\"price\":850}}', NULL, NULL, NULL, NULL),
(294, 67, 'selected_dates', '[{\"date\":[\"2020-07-12\",\"2020-07-13\"],\"slots\":[[\"12\",\"14\"]]}]', NULL, NULL, NULL, NULL),
(295, 67, 'locale', 'en', NULL, NULL, NULL, NULL),
(296, 68, 'term_id', 'Gymnasium', NULL, NULL, NULL, NULL),
(297, 68, 'tmp_dates', '{\"2020-07-12\":{\"date\":[\"2020-07-12\",\"2020-07-14\"],\"slots\":[[\"12\",\"14\"]]}}', NULL, NULL, NULL, NULL),
(298, 68, 'tmp_slots', '{\"2020-07-12 12:00\":{\"price\":850}}', NULL, NULL, NULL, NULL),
(299, 68, 'selected_dates', '[{\"date\":[\"2020-07-12\",\"2020-07-14\"],\"slots\":[[\"12\",\"14\"]]}]', NULL, NULL, NULL, NULL),
(300, 68, 'locale', 'en', NULL, NULL, NULL, NULL),
(301, 69, 'term_id', 'Activities', NULL, NULL, NULL, NULL),
(302, 69, 'tmp_dates', '{\"2020-07-14\":{\"date\":[\"2020-07-14\"],\"slots\":[[\"14\"]]}}', NULL, NULL, NULL, NULL),
(303, 69, 'tmp_slots', '{\"2020-07-14 14:00:00\":{\"price\":850}}', NULL, NULL, NULL, NULL),
(304, 69, 'selected_dates', '[{\"date\":[\"2020-07-14\"],\"slots\":[[\"14\"]]}]', NULL, NULL, NULL, NULL),
(305, 69, 'locale', 'en', NULL, NULL, NULL, NULL),
(306, 70, 'term_id', 'Mountain Bike', NULL, NULL, NULL, NULL),
(307, 70, 'tmp_dates', '{\"2020-07-13\":{\"date\":[\"2020-07-13\",\"2020-07-20\"],\"slots\":[[\"12:00\",\"14:00\"]]}}', NULL, NULL, NULL, NULL),
(308, 70, 'tmp_slots', '{\"2020-07-13 12:12:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(309, 70, 'selected_dates', '[{\"date\":[\"2020-07-13\",\"2020-07-20\"],\"slots\":[[\"12:00\",\"14:00\"]]}]', NULL, NULL, NULL, NULL),
(310, 70, 'locale', 'en', NULL, NULL, NULL, NULL),
(311, 71, 'term_id', 'Gymnasium', NULL, NULL, NULL, NULL),
(312, 71, 'tmp_dates', '{\"2020-07-15\":{\"date\":[\"2020-07-15\",\"2020-07-16\"],\"slots\":[[\"12:00\",\"14:00\"]]}}', NULL, NULL, NULL, NULL),
(313, 71, 'tmp_slots', '{\"2020-07-15 12:00:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(314, 71, 'selected_dates', '[{\"date\":[\"2020-07-15\",\"2020-07-16\"],\"slots\":[[\"12:00\",\"14:00\"]]}]', NULL, NULL, NULL, NULL),
(315, 71, 'locale', 'en', NULL, NULL, NULL, NULL),
(316, 72, 'term_id', 'Wifi', NULL, NULL, NULL, NULL),
(317, 72, 'tmp_dates', '{\"2020-07-29\":{\"date\":[\"2020-07-29\",\"2020-07-14\",\"2020-07-17\"],\"slots\":[[\"12:00\",\"13:00\",\"14:00\"]]}}\r\n', NULL, NULL, NULL, NULL),
(318, 72, 'tmp_slots', '{\"2020-07-29 12:00:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(319, 72, 'selected_dates', '[{\"date\":[\"2020-07-29\",\"2020-07-14\",\"2020-07-17\"],\"slots\":[[\"12:00\",\"13:00\",\"14:00\"]]}]', NULL, NULL, NULL, NULL),
(320, 72, 'locale', 'en', NULL, NULL, NULL, NULL),
(321, 73, 'term_id', 'Mountain Bike', NULL, NULL, NULL, NULL),
(322, 73, 'tmp_dates', '{\"2020-07-13\":{\"date\":[\"2020-07-13\",\"2020-07-14\"],\"slots\":[[\"12:00\",\"13:00\"]]}}', NULL, NULL, NULL, NULL),
(323, 73, 'tmp_slots', '{\"2020-07-13 12:00:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(324, 73, 'selected_dates', '[{\"date\":[\"2020-07-13\",\"2020-07-14\"],\"slots\":[[\"12:00\",\"13:00\"]]}]', NULL, NULL, NULL, NULL),
(325, 73, 'locale', 'en', NULL, NULL, NULL, NULL),
(326, 74, 'term_id', 'Satellite Office', NULL, NULL, NULL, NULL),
(327, 74, 'tmp_dates', '{\"2020-07-13\":{\"date\":[\"2020-07-13\",\"2020-07-14\"],\"slots\":[[\"12:00\",\"13:00\"]]}}', NULL, NULL, NULL, NULL),
(328, 74, 'tmp_slots', '{\"2020-07-13 12:00:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(329, 74, 'selected_dates', '[{\"date\":[\"2020-07-13\",\"2020-07-14\"],\"slots\":[[\"12:00\",\"13:00\"]]}]', NULL, NULL, NULL, NULL),
(330, 75, 'term_id', 'Aerobics Room', NULL, NULL, NULL, NULL),
(331, 75, 'tmp_dates', '{\"2020-07-14\":{\"date\":[\"2020-07-14\"],\"slots\":[[\"14:00\"]]}}', NULL, NULL, NULL, NULL),
(332, 75, 'tmp_slots', '{\"2020-07-14 14:00:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(333, 75, 'selected_dates', '[{\"date\":[\"2020-07-14\"],\"slots\":[[\"14:00\"]]}]', NULL, NULL, NULL, NULL),
(334, 75, 'locale', 'en', NULL, NULL, NULL, NULL),
(335, 76, 'term_id', 'Satellite Office', NULL, NULL, NULL, NULL),
(336, 76, 'tmp_dates', '{\"2020-07-15\":{\"date\":[\"2020-07-15\"],\"slots\":[[\"14:00\"]]}}', NULL, NULL, NULL, NULL),
(337, 76, 'tmp_slots', '{\"2020-07-15 14:00:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(338, 76, 'selected_dates', '[{\"date\":[\"2020-07-15\"],\"slots\":[[\"14:00\"]]}]', NULL, NULL, NULL, NULL),
(339, 77, 'term_id', 'Satellite Office', NULL, NULL, NULL, NULL),
(340, 77, 'tmp_dates', '{\"2020-07-14\":{\"date\":[\"2020-07-14\"],\"slots\":[[\"12:00\"]]}}', NULL, NULL, NULL, NULL),
(341, 77, 'tmp_slots', '{\"2020-07-14 12:00:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(342, 77, 'selected_dates', '[{\"date\":[\"2020-07-14\"],\"slots\":[[\"12:00\"]]}]', NULL, NULL, NULL, NULL),
(343, 78, 'term_id', 'Mountain Bike', NULL, NULL, NULL, NULL),
(344, 78, 'tmp_dates', '{\"2020-07-14\":{\"date\":[\"2020-07-14\"],\"slots\":[[\"12:00\"]]}}', NULL, NULL, NULL, NULL),
(345, 78, 'tmp_slots', '{\"2020-07-14 12:00:00:00\":{\"price\":2100}}', NULL, NULL, NULL, NULL),
(346, 78, 'selected_dates', '[{\"date\":[\"2020-07-14\"],\"slots\":[[\"12:00\"]]}]', NULL, NULL, NULL, NULL),
(347, 79, 'term_id', 'SELF ESTEEM', NULL, NULL, NULL, NULL),
(348, 79, 'tmp_dates', '{\"2020-07-24\":{\"date\":[\"2020-07-24\",\"2020-07-25\"],\"slots\":[[\"12:00\",\"13:00\"]]}}', NULL, NULL, NULL, NULL),
(349, 79, 'tmp_slots', '{\"2020-07-24 12:00:00:00\":{\"price\":50}}', NULL, NULL, NULL, NULL),
(350, 79, 'selected_dates', '[{\"date\":[\"2020-07-24\",\"2020-07-25\"],\"slots\":[[\"12:00\",\"13:00\"]]}]', NULL, NULL, NULL, NULL),
(351, 79, 'locale', 'en', NULL, NULL, NULL, NULL),
(352, 80, 'term_id', 'ADDICTION', NULL, NULL, NULL, NULL),
(353, 80, 'tmp_dates', '{\"2020-07-24\":{\"date\":[\"2020-07-24\",\"2020-07-25\"],\"slots\":[[\"12:00\",\"13:00\"]]}}', NULL, NULL, NULL, NULL),
(354, 80, 'tmp_slots', '{\"2020-07-24 12:00:00:00\":{\"price\":80}}', NULL, NULL, NULL, NULL),
(355, 80, 'selected_dates', '[{\"date\":[\"2020-07-24\",\"2020-07-25\"],\"slots\":[[\"12:00\",\"13:00\"]]}]', NULL, NULL, NULL, NULL),
(356, 80, 'locale', 'en', NULL, NULL, NULL, NULL),
(357, 81, 'term_id', 'ADHD', NULL, NULL, NULL, NULL),
(358, 81, 'tmp_dates', '{\"2020-07-25\":{\"date\":[\"2020-07-25\",\"2020-07-26\"],\"slots\":[[\"12:00\",\"13:00\"]]}}', NULL, NULL, NULL, NULL),
(359, 81, 'tmp_slots', '{\"2020-07-25 12:00:00:00\":{\"price\":95}}', NULL, NULL, NULL, NULL),
(360, 81, 'selected_dates', '[{\"date\":[\"2020-07-25\",\"2020-07-26\"],\"slots\":[[\"12:00\",\"13:00\"]]}]', NULL, NULL, NULL, NULL),
(361, 81, 'locale', 'en', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bravo_booking_payments`
--

CREATE TABLE `bravo_booking_payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `booking_id` int(11) DEFAULT NULL,
  `payment_gateway` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `currency` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `converted_amount` decimal(10,2) DEFAULT NULL,
  `converted_currency` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_rate` decimal(10,2) DEFAULT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logs` text COLLATE utf8mb4_unicode_ci,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bravo_booking_slot`
--

CREATE TABLE `bravo_booking_slot` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `booking_id` int(11) DEFAULT NULL,
  `slots` int(11) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `flag` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bravo_booking_slot`
--

INSERT INTO `bravo_booking_slot` (`id`, `booking_id`, `slots`, `date`, `flag`, `create_user`, `update_user`, `created_at`, `updated_at`) VALUES
(1, 3, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 01:17:50', '2020-07-06 01:17:50'),
(2, 4, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 01:22:08', '2020-07-06 01:22:08'),
(3, 5, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 13:06:47', '2020-07-06 13:06:47'),
(4, 6, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 14:02:59', '2020-07-06 14:02:59'),
(5, 7, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 14:07:41', '2020-07-06 14:07:41'),
(6, 8, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 14:17:17', '2020-07-06 14:17:17'),
(7, 9, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 14:32:45', '2020-07-06 14:32:45'),
(8, 10, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 16:20:35', '2020-07-06 16:20:35'),
(9, 11, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 16:23:40', '2020-07-06 16:23:40'),
(10, 12, 0, '2020-05-07 07:00:00', NULL, 19, NULL, '2020-07-06 18:55:29', '2020-07-06 18:55:29'),
(11, 13, 0, '2020-05-07 07:00:00', NULL, 19, NULL, '2020-07-06 18:58:33', '2020-07-06 18:58:33'),
(12, 14, 0, '2020-05-07 07:00:00', NULL, 19, NULL, '2020-07-06 18:58:59', '2020-07-06 18:58:59'),
(13, 15, 0, '2020-05-07 07:00:00', NULL, 19, NULL, '2020-07-06 19:00:10', '2020-07-06 19:00:10'),
(14, 16, 0, '2020-05-07 07:00:00', NULL, 19, NULL, '2020-07-06 19:00:30', '2020-07-06 19:00:30'),
(15, 17, 0, '2020-05-07 07:00:00', NULL, 19, NULL, '2020-07-06 19:05:07', '2020-07-06 19:05:07'),
(16, 18, 0, '2020-05-07 07:00:00', NULL, 19, NULL, '2020-07-06 19:07:44', '2020-07-06 19:07:44'),
(17, 19, 0, '2020-05-07 07:00:00', NULL, 19, NULL, '2020-07-06 19:17:24', '2020-07-06 19:17:24'),
(18, 20, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 19:28:34', '2020-07-06 19:28:34'),
(19, 21, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 19:40:45', '2020-07-06 19:40:45'),
(20, 22, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 19:42:31', '2020-07-06 19:42:31'),
(21, 23, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 20:43:28', '2020-07-06 20:43:28'),
(22, 24, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 22:40:43', '2020-07-06 22:40:43'),
(23, 25, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 22:42:13', '2020-07-06 22:42:13'),
(24, 26, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 22:47:00', '2020-07-06 22:47:00'),
(25, 27, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 22:48:25', '2020-07-06 22:48:25'),
(26, 28, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 22:48:38', '2020-07-06 22:48:38'),
(27, 29, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 23:01:30', '2020-07-06 23:01:30'),
(28, 30, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 23:24:55', '2020-07-06 23:24:55'),
(29, 31, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-06 23:28:10', '2020-07-06 23:28:10'),
(30, 32, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-07 01:02:06', '2020-07-07 01:02:06'),
(31, 33, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-07 01:10:28', '2020-07-07 01:10:28'),
(32, 34, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-07 01:25:03', '2020-07-07 01:25:03'),
(33, 35, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-07 01:25:55', '2020-07-07 01:25:55'),
(34, 36, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-07 01:28:44', '2020-07-07 01:28:44'),
(35, 37, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-07 01:29:18', '2020-07-07 01:29:18'),
(36, 38, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-07 07:12:34', '2020-07-07 07:12:34'),
(37, 39, 0, '2020-05-07 07:00:00', NULL, 19, NULL, '2020-07-07 12:34:14', '2020-07-07 12:34:14'),
(38, 40, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-07 12:38:33', '2020-07-07 12:38:33'),
(39, 41, 0, '2020-05-07 07:00:00', NULL, 19, NULL, '2020-07-07 12:48:25', '2020-07-07 12:48:25'),
(40, 42, 0, '2020-05-07 07:00:00', NULL, 19, NULL, '2020-07-07 12:50:22', '2020-07-07 12:50:22'),
(41, 43, 0, '2020-05-07 07:00:00', NULL, 1, NULL, '2020-07-07 12:52:14', '2020-07-07 12:52:14'),
(42, 44, 0, '2020-05-07 07:00:00', NULL, 19, NULL, '2020-07-07 13:01:25', '2020-07-07 13:01:25'),
(43, 45, 0, '2020-05-07 07:00:00', NULL, 19, NULL, '2020-07-07 14:30:55', '2020-07-07 14:30:55'),
(44, 46, 0, '2020-05-07 07:00:00', NULL, 19, NULL, '2020-07-07 14:50:49', '2020-07-07 14:50:49'),
(45, 47, 0, '2020-05-07 07:00:00', NULL, 19, NULL, '2020-07-07 20:54:29', '2020-07-07 20:54:29'),
(46, 48, 0, '2020-07-09 07:00:00', NULL, 1, NULL, '2020-07-07 21:28:50', '2020-07-07 21:28:50'),
(47, 49, 0, '2020-07-15 07:00:00', NULL, 1, NULL, '2020-07-07 21:40:04', '2020-07-07 21:40:04'),
(48, 50, 0, '2020-07-08 07:00:00', NULL, 1, NULL, '2020-07-07 23:06:19', '2020-07-07 23:06:19'),
(49, 51, 0, '2020-07-10 07:00:00', NULL, 1, NULL, '2020-07-07 23:09:40', '2020-07-07 23:09:40'),
(50, 52, 0, '2020-07-10 07:00:00', NULL, 1, NULL, '2020-07-07 23:09:40', '2020-07-07 23:09:40'),
(51, 53, 0, '2020-07-10 07:00:00', NULL, 1, NULL, '2020-07-07 23:10:28', '2020-07-07 23:10:28'),
(52, 54, 0, '2020-07-08 07:00:00', NULL, 19, NULL, '2020-07-08 12:32:10', '2020-07-08 12:32:10'),
(53, 55, 0, '2020-07-08 07:00:00', NULL, 1, NULL, '2020-07-08 12:46:23', '2020-07-08 12:46:23'),
(54, 56, 0, '2020-07-10 07:00:00', NULL, 19, NULL, '2020-07-10 15:54:23', '2020-07-10 15:54:23'),
(61, 68, 0, '2020-07-12 07:00:00', NULL, 19, NULL, '2020-07-11 14:57:19', '2020-07-11 14:57:19'),
(62, 69, 0, '2020-07-14 07:00:00', NULL, 19, NULL, '2020-07-11 15:52:56', '2020-07-11 15:52:56'),
(63, 70, 0, '2020-07-13 07:00:00', NULL, 19, NULL, '2020-07-11 16:03:15', '2020-07-11 16:03:15'),
(64, 71, 0, '2020-07-15 07:00:00', NULL, 19, NULL, '2020-07-11 17:09:49', '2020-07-11 17:09:49'),
(65, 72, 0, '2020-07-29 07:00:00', NULL, 19, NULL, '2020-07-11 17:24:15', '2020-07-11 17:24:15'),
(66, 73, 0, '2020-07-13 07:00:00', NULL, 19, NULL, '2020-07-12 15:58:01', '2020-07-12 15:58:01'),
(67, 74, 0, '2020-07-13 07:00:00', NULL, 19, NULL, '2020-07-12 16:00:08', '2020-07-12 16:00:08'),
(68, 75, 0, '2020-07-14 07:00:00', NULL, 19, NULL, '2020-07-12 16:01:49', '2020-07-12 16:01:49'),
(69, 76, 0, '2020-07-15 07:00:00', NULL, 19, NULL, '2020-07-12 16:20:50', '2020-07-12 16:20:50'),
(70, 77, 0, '2020-07-14 07:00:00', NULL, 19, NULL, '2020-07-12 16:27:54', '2020-07-12 16:27:54'),
(71, 78, 0, '2020-07-14 07:00:00', NULL, 19, NULL, '2020-07-12 16:28:42', '2020-07-12 16:28:42'),
(72, 79, 0, '2020-07-24 07:00:00', NULL, 46, NULL, '2020-07-24 11:50:38', '2020-07-24 11:50:38'),
(73, 80, 0, '2020-07-24 07:00:00', NULL, 46, NULL, '2020-07-24 11:56:45', '2020-07-24 11:56:45'),
(74, 81, 0, '2020-07-25 07:00:00', NULL, 46, NULL, '2020-07-26 00:35:16', '2020-07-26 00:35:16');

-- --------------------------------------------------------

--
-- Table structure for table `bravo_contact`
--

CREATE TABLE `bravo_contact` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bravo_contact`
--

INSERT INTO `bravo_contact` (`id`, `name`, `email`, `message`, `status`, `create_user`, `update_user`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'fdf dfdf', 'prem@gmail.com', 'dsadsadsadsadsa', 'sent', NULL, NULL, NULL, '2020-06-17 20:12:59', '2020-06-17 20:12:59'),
(2, 'Eric Jones', 'eric@talkwithwebvisitor.com', 'Cool website!\r\n\r\nMy name’s Eric, and I just found your site - websitedemoonline.com - while surfing the net. You showed up at the top of the search results, so I checked you out. Looks like what you’re doing is pretty cool.\r\n \r\nBut if you don’t mind me asking – after someone like me stumbles across websitedemoonline.com, what usually happens?\r\n\r\nIs your site generating leads for your business? \r\n \r\nI’m guessing some, but I also bet you’d like more… studies show that 7 out 10 who land on a site wind up leaving without a trace.\r\n\r\nNot good.\r\n\r\nHere’s a thought – what if there was an easy way for every visitor to “raise their hand” to get a phone call from you INSTANTLY… the second they hit your site and said, “call me now.”\r\n\r\nYou can –\r\n  \r\nTalk With Web Visitor is a software widget that’s works on your site, ready to capture any visitor’s Name, Email address and Phone Number.  It lets you know IMMEDIATELY – so that you can talk to that lead while they’re literally looking over your site.\r\n\r\nCLICK HERE http://www.talkwithwebvisitor.com to try out a Live Demo with Talk With Web Visitor now to see exactly how it works.\r\n\r\nTime is money when it comes to connecting with leads – the difference between contacting someone within 5 minutes versus 30 minutes later can be huge – like 100 times better!\r\n\r\nThat’s why we built out our new SMS Text With Lead feature… because once you’ve captured the visitor’s phone number, you can automatically start a text message (SMS) conversation.\r\n  \r\nThink about the possibilities – even if you don’t close a deal then and there, you can follow up with text messages for new offers, content links, even just “how you doing?” notes to build a relationship.\r\n\r\nWouldn’t that be cool?\r\n\r\nCLICK HERE http://www.talkwithwebvisitor.com to discover what Talk With Web Visitor can do for your business.\r\n\r\nYou could be converting up to 100X more leads today!\r\nEric\r\n\r\nPS: Talk With Web Visitor offers a FREE 14 days trial – and it even includes International Long Distance Calling. \r\nYou have customers waiting to talk with you right now… don’t keep them waiting. \r\nCLICK HERE http://www.talkwithwebvisitor.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithwebvisitor.com/unsubscribe.aspx?d=websitedemoonline.com', 'sent', NULL, NULL, NULL, '2020-06-24 22:58:23', '2020-06-24 22:58:23'),
(3, 'JoshuaThatt', 'no-replyBig@gmail.com', 'Gооd dаy!  websitedemoonline.com \r\n \r\nDid yоu knоw thаt it is pоssiblе tо sеnd businеss оffеr uttеrly lаwfully? \r\nWе prоvidе а nеw wаy оf sеnding mеssаgе thrоugh соntасt fоrms. Suсh fоrms аrе lосаtеd оn mаny sitеs. \r\nWhеn suсh businеss prоpоsаls аrе sеnt, nо pеrsоnаl dаtа is usеd, аnd mеssаgеs аrе sеnt tо fоrms spесifiсаlly dеsignеd tо rесеivе mеssаgеs аnd аppеаls. \r\nаlsо, mеssаgеs sеnt thrоugh fееdbасk Fоrms dо nоt gеt intо spаm bесаusе suсh mеssаgеs аrе соnsidеrеd impоrtаnt. \r\nWе оffеr yоu tо tеst оur sеrviсе fоr frее. Wе will sеnd up tо 50,000 mеssаgеs fоr yоu. \r\nThе соst оf sеnding оnе milliоn mеssаgеs is 49 USD. \r\n \r\nThis оffеr is сrеаtеd аutоmаtiсаlly. Plеаsе usе thе соntасt dеtаils bеlоw tо соntасt us. \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nWhatsApp - +375259112693', 'sent', NULL, NULL, NULL, '2020-06-27 06:12:41', '2020-06-27 06:12:41'),
(4, 'Eric Jones', 'eric@talkwithwebvisitor.com', 'My name’s Eric and I just found your site websitedemoonline.com.\r\n\r\nIt’s got a lot going for it, but here’s an idea to make it even MORE effective.\r\n\r\nTalk With Web Visitor – CLICK HERE http://www.talkwithwebvisitor.com for a live demo now.\r\n\r\nTalk With Web Visitor is a software widget that’s works on your site, ready to capture any visitor’s Name, Email address and Phone Number.  It signals you the moment they let you know they’re interested – so that you can talk to that lead while they’re literally looking over your site.\r\n\r\nAnd once you’ve captured their phone number, with our new SMS Text With Lead feature, you can automatically start a text (SMS) conversation… and if they don’t take you up on your offer then, you can follow up with text messages for new offers, content links, even just “how you doing?” notes to build a relationship.\r\n\r\nCLICK HERE http://www.talkwithwebvisitor.com to discover what Talk With Web Visitor can do for your business.\r\n\r\nThe difference between contacting someone within 5 minutes versus a half-hour means you could be converting up to 100X more leads today!\r\n\r\nEric\r\nPS: Studies show that 70% of a site’s visitors disappear and are gone forever after just a moment. Don’t keep losing them. \r\nTalk With Web Visitor offers a FREE 14 days trial – and it even includes International Long Distance Calling. \r\nYou have customers waiting to talk with you right now… don’t keep them waiting. \r\nCLICK HERE http://www.talkwithwebvisitor.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithwebvisitor.com/unsubscribe.aspx?d=websitedemoonline.com', 'sent', NULL, NULL, NULL, '2020-06-27 23:40:01', '2020-06-27 23:40:01'),
(5, 'Keith Bachus', 'bachus.keith52@gmail.com', 'Hi,\r\n\r\nDo you have a Website? Of course you do because I am looking at your website websitedemoonline.com now.\r\n\r\nAre you struggling for Leads and Sales?\r\n\r\nYou’re not the only one.\r\n\r\nSo many Website owners struggle to convert their Visitors into Leads & Sales.\r\n\r\nThere’s a simple way to fix this problem.\r\n\r\nYou could use a Live Chat app on your Website websitedemoonline.com and hire Chat Agents.\r\n\r\nBut only if you’ve got deep pockets and you’re happy to fork out THOUSANDS of dollars for the quality you need.\r\n\r\n=====\r\n\r\nBut what if you could automate Live Chat so it’s HUMAN-FREE?\r\n\r\nWhat if you could exploit NEW “AI” Technology to engage with your Visitors INSTANTLY.\r\n\r\nAnd AUTOMATICALLY convert them into Leads & Sales.\r\n\r\nWITHOUT spending THOUSANDS of dollars on Live Chat Agents.\r\n\r\nAnd WITHOUT hiring expensive coders.\r\n\r\nIn fact, all you need to do to activate this LATEST “AI” Website Tech..\r\n\r\n..is to COPY & PASTE a single line of “Website Code”.\r\n\r\n==> http://www.zoomsoft.net/ConversioBot\r\n\r\n======\r\n\r\nJoin HUGE Fortune 500 companies like:\r\n\r\nFacebook Spotify Starbucks Staples The Wall Street Journal Pizza Hut Amtrak Disney H&M & Mastercard\r\n\r\nThey all use similar “AI” Chat Technology to ConversioBot - the Internet’s #1 Chatbot for Website Owners.\r\n\r\nThe founders of ConversioBot have used their highly sophisticated ChatBot to:\r\n\r\n- AUTOMATICALLY build a massive Email List of 11,643 Subscribers in just 7 Days\r\n\r\n- AUTOMATICALLY add 6,386 Sales in only 6 Months\r\n\r\n- AUTOMATICALLY explode their Conversion Rate by 198% in only 6 Hours.\r\n\r\n=====\r\n\r\nNow it’s your turn to get in on this exciting NEW Cloud-Based App.\r\n\r\nYou can start using ConversioBot today by copying and pasting ONE line of “Automated Bot Code\" to your Website.\r\n\r\nWatch this short video to find out how >> http://www.zoomsoft.net/ConversioBot\r\n\r\nRegards,\r\n\r\nConversioBot Team\r\n\r\nP.S. This “AI” Technology works with: - Affiliate Review Sites - List-Building Pages - WordPress Blogs (it comes with a Plugin) - Sales Letters - eCommerce Websites - Local Business Sites - Webinar Registration Pages - Consultancy Websites - Freelance Websites\r\n\r\nAlmost ANY Website you can think of..\r\n\r\n==> This could be happening on your Website TODAY.. http://www.zoomsoft.net/ConversioBot\r\n\r\nUNSUBSCRIBE http://www.zoomsoft.net/unsubscribe', 'sent', NULL, NULL, NULL, '2020-07-03 19:33:00', '2020-07-03 19:33:00'),
(6, 'Eric Jones', 'eric@talkwithwebvisitor.com', 'Hi, Eric here with a quick thought about your website websitedemoonline.com...\r\n\r\nI’m on the internet a lot and I look at a lot of business websites.\r\n\r\nLike yours, many of them have great content. \r\n\r\nBut all too often, they come up short when it comes to engaging and connecting with anyone who visits.\r\n\r\nI get it – it’s hard.  Studies show 7 out of 10 people who land on a site, abandon it in moments without leaving even a trace.  You got the eyeball, but nothing else.\r\n\r\nHere’s a solution for you…\r\n\r\nTalk With Web Visitor is a software widget that’s works on your site, ready to capture any visitor’s Name, Email address and Phone Number.  You’ll know immediately they’re interested and you can call them directly to talk with them literally while they’re still on the web looking at your site.\r\n\r\nCLICK HERE http://www.talkwithwebvisitor.com to try out a Live Demo with Talk With Web Visitor now to see exactly how it works.\r\n\r\nIt could be huge for your business – and because you’ve got that phone number, with our new SMS Text With Lead feature, you can automatically start a text (SMS) conversation – immediately… and contacting someone in that 5 minute window is 100 times more powerful than reaching out 30 minutes or more later.\r\n\r\nPlus, with text messaging you can follow up later with new offers, content links, even just follow up notes to keep the conversation going.\r\n\r\nEverything I’ve just described is extremely simple to implement, cost-effective, and profitable. \r\n \r\nCLICK HERE http://www.talkwithwebvisitor.com to discover what Talk With Web Visitor can do for your business.\r\n\r\nYou could be converting up to 100X more eyeballs into leads today!\r\n\r\nEric\r\nPS: Talk With Web Visitor offers a FREE 14 days trial – and it even includes International Long Distance Calling. \r\nYou have customers waiting to talk with you right now… don’t keep them waiting. \r\nCLICK HERE http://www.talkwithwebvisitor.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithwebvisitor.com/unsubscribe.aspx?d=websitedemoonline.com', 'sent', NULL, NULL, NULL, '2020-07-06 07:17:47', '2020-07-06 07:17:47'),
(7, 'Joesph Ann', 'ann.joesph@gmail.com', 'Hi,\r\n\r\nDo you have a Website? Of course you do because I am looking at your website websitedemoonline.com now.\r\n\r\nAre you struggling for Leads and Sales?\r\n\r\nYou’re not the only one.\r\n\r\nSo many Website owners struggle to convert their Visitors into Leads & Sales.\r\n\r\nThere’s a simple way to fix this problem.\r\n\r\nYou could use a Live Chat app on your Website websitedemoonline.com and hire Chat Agents.\r\n\r\nBut only if you’ve got deep pockets and you’re happy to fork out THOUSANDS of dollars for the quality you need.\r\n\r\n=====\r\n\r\nBut what if you could automate Live Chat so it’s HUMAN-FREE?\r\n\r\nWhat if you could exploit NEW “AI” Technology to engage with your Visitors INSTANTLY.\r\n\r\nAnd AUTOMATICALLY convert them into Leads & Sales.\r\n\r\nWITHOUT spending THOUSANDS of dollars on Live Chat Agents.\r\n\r\nAnd WITHOUT hiring expensive coders.\r\n\r\nIn fact, all you need to do to activate this LATEST “AI” Website Tech..\r\n\r\n..is to COPY & PASTE a single line of “Website Code”.\r\n\r\n==> http://www.zoomsoft.net/12Minutes\r\n\r\n======\r\n\r\nJoin HUGE Fortune 500 companies like:\r\n\r\nFacebook Spotify Starbucks Staples The Wall Street Journal Pizza Hut Amtrak Disney H&M & Mastercard\r\n\r\nThey all use similar “AI” Chat Technology to ConversioBot - the Internet’s #1 Chatbot for Website Owners.\r\n\r\nThe founders of ConversioBot have used their highly sophisticated ChatBot to:\r\n\r\n- AUTOMATICALLY build a massive Email List of 11,643 Subscribers in just 7 Days\r\n\r\n- AUTOMATICALLY add 6,386 Sales in only 6 Months\r\n\r\n- AUTOMATICALLY explode their Conversion Rate by 198% in only 6 Hours.\r\n\r\n=====\r\n\r\nNow it’s your turn to get in on this exciting NEW Cloud-Based App.\r\n\r\nYou can start using ConversioBot today by copying and pasting ONE line of “Automated Bot Code\" to your Website.\r\n\r\nWatch this short video to find out how >> http://www.zoomsoft.net/12Minutes\r\n\r\nRegards,\r\n\r\nConversioBot Team\r\n\r\nP.S. This “AI” Technology works with: - Affiliate Review Sites - List-Building Pages - WordPress Blogs (it comes with a Plugin) - Sales Letters - eCommerce Websites - Local Business Sites - Webinar Registration Pages - Consultancy Websites - Freelance Websites\r\n\r\nAlmost ANY Website you can think of..\r\n\r\n==> This could be happening on your Website TODAY.. http://www.zoomsoft.net/ConversioBot\r\n\r\nUNSUBSCRIBE http://www.zoomsoft.net/12MinuteUnsubscribe', 'sent', NULL, NULL, NULL, '2020-07-24 10:37:32', '2020-07-24 10:37:32'),
(8, 'Eric Jones', 'eric@talkwithwebvisitor.com', 'Hello, my name’s Eric and I just ran across your website at websitedemoonline.com...\r\n\r\nI found it after a quick search, so your SEO’s working out…\r\n\r\nContent looks pretty good…\r\n\r\nOne thing’s missing though…\r\n\r\nA QUICK, EASY way to connect with you NOW.\r\n\r\nBecause studies show that a web lead like me will only hang out a few seconds – 7 out of 10 disappear almost instantly, Surf Surf Surf… then gone forever.\r\n\r\nI have the solution:\r\n\r\nTalk With Web Visitor is a software widget that’s works on your site, ready to capture any visitor’s Name, Email address and Phone Number.  You’ll know immediately they’re interested and you can call them directly to TALK with them - literally while they’re still on the web looking at your site.\r\n\r\nCLICK HERE http://www.talkwithwebvisitor.com to try out a Live Demo with Talk With Web Visitor now to see exactly how it works and even give it a try… it could be huge for your business.\r\n\r\nPlus, now that you’ve got that phone number, with our new SMS Text With Lead feature, you can automatically start a text (SMS) conversation pronto… which is so powerful, because connecting with someone within the first 5 minutes is 100 times more effective than waiting 30 minutes or more later.\r\n\r\nThe new text messaging feature lets you follow up regularly with new offers, content links, even just follow up notes to build a relationship.\r\n\r\nEverything I’ve just described is extremely simple to implement, cost-effective, and profitable.\r\n \r\nCLICK HERE http://www.talkwithwebvisitor.com to discover what Talk With Web Visitor can do for your business, potentially converting up to 100X more eyeballs into leads today!\r\n\r\nEric\r\nPS: Talk With Web Visitor offers a FREE 14 days trial – and it even includes International Long Distance Calling. \r\nYou have customers waiting to talk with you right now… don’t keep them waiting. \r\nCLICK HERE http://www.talkwithwebvisitor.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithwebvisitor.com/unsubscribe.aspx?d=websitedemoonline.com', 'sent', NULL, NULL, NULL, '2020-07-27 00:11:22', '2020-07-27 00:11:22');

-- --------------------------------------------------------

--
-- Table structure for table `bravo_locations`
--

CREATE TABLE `bravo_locations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `map_lat` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map_lng` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map_zoom` int(11) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `lang` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `banner_image_id` int(11) DEFAULT NULL,
  `trip_ideas` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bravo_locations`
--

INSERT INTO `bravo_locations` (`id`, `name`, `content`, `slug`, `image_id`, `map_lat`, `map_lng`, `map_zoom`, `status`, `_lft`, `_rgt`, `parent_id`, `create_user`, `update_user`, `deleted_at`, `origin_id`, `lang`, `created_at`, `updated_at`, `banner_image_id`, `trip_ideas`) VALUES
(1, 'Paris', 'New York, a city that doesnt sleep, as Frank Sinatra sang. The Big Apple is one of the largest cities in the United States and one of the most popular in the whole country and the world. Millions of tourists visit it every year attracted by its various iconic symbols and its wide range of leisure and cultural offer. New York is the birth place of new trends and developments in many fields such as art, gastronomy, technology,...', 'paris', 88, '48.856613', '2.352222', 12, 'publish', 1, 2, NULL, 1, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39', 93, '\"[{\\\"title\\\":\\\"Experiencing the best jazz in Harlem, birthplace of bebop\\\",\\\"link\\\":\\\"#\\\",\\\"content\\\":\\\"New Orleans might be the home of jazz, but New York City is where many of the genre\\u2019s greats became stars \\u2013 and Harlem was at the heart of it.The neighborhood experienced a rebirth during the...\\\",\\\"image_id\\\":\\\"94\\\"},{\\\"title\\\":\\\"Reflections from the road: transformative \\u2018Big Trip\\u2019 experiences\\\",\\\"link\\\":\\\"#\\\",\\\"content\\\":\\\"Whether it\\u2019s a gap year after finishing school, a well-earned sabbatical from work or an overseas adventure in celebration of your retirement, a big trip is a rite of passage for every traveller, with myriad life lessons to be ...\\\",\\\"image_id\\\":\\\"95\\\"}]\"'),
(2, 'New York, United States', NULL, 'new-york-united-states', 89, '40.712776', '-74.005974', 12, 'publish', 3, 4, NULL, 1, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39', NULL, NULL),
(3, 'California', NULL, 'california', 90, '36.778259', '-119.417931', 12, 'publish', 5, 6, NULL, 1, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39', NULL, NULL),
(4, 'United States', NULL, 'united-states', 91, '37.090240', '-95.712891', 12, 'publish', 7, 8, NULL, 1, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39', NULL, NULL),
(5, 'Los Angeles', NULL, 'los-angeles', 92, '34.052235', '-118.243683', 12, 'publish', 9, 10, NULL, 1, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39', NULL, NULL),
(6, 'New Jersey', NULL, 'new-jersey', 88, '40.058323', '-74.405663', 12, 'publish', 11, 12, NULL, 1, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39', NULL, NULL),
(7, 'San Francisco', NULL, 'san-francisco', 89, '37.774929', '-122.419418', 12, 'publish', 13, 14, NULL, 1, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39', NULL, NULL),
(8, 'Virginia', NULL, 'virginia', 90, '37.431572', '-78.656891', 12, 'publish', 15, 16, NULL, 1, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bravo_location_translations`
--

CREATE TABLE `bravo_location_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `trip_ideas` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bravo_review`
--

CREATE TABLE `bravo_review` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `object_model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `rate_number` int(11) DEFAULT NULL,
  `author_ip` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `lang` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `vendor_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bravo_review`
--

INSERT INTO `bravo_review` (`id`, `object_id`, `object_model`, `title`, `content`, `rate_number`, `author_ip`, `status`, `publish_date`, `create_user`, `update_user`, `deleted_at`, `lang`, `created_at`, `updated_at`, `vendor_id`) VALUES
(1, 1, 'tour', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:39', 12, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39', 1),
(2, 1, 'tour', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:39', 13, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39', 1),
(3, 1, 'tour', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:39', 14, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39', 1),
(4, 2, 'tour', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:39', 15, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39', 1),
(5, 3, 'tour', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 12, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 6),
(6, 3, 'tour', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 15, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 6),
(7, 3, 'tour', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 14, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 6),
(8, 3, 'tour', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 14, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 6),
(9, 4, 'tour', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 11, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 1),
(10, 4, 'tour', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 9, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 1),
(11, 4, 'tour', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 12, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 1),
(12, 4, 'tour', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 10, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 1),
(13, 4, 'tour', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 9, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 1),
(14, 5, 'tour', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 12, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 6),
(15, 5, 'tour', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 13, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 6),
(16, 5, 'tour', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 11, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 6),
(17, 5, 'tour', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 12, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 6),
(18, 6, 'tour', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 10, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 4),
(19, 6, 'tour', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 9, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 4),
(20, 7, 'tour', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 7, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 5),
(21, 7, 'tour', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 12, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 5),
(22, 7, 'tour', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 12, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 5),
(23, 8, 'tour', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 8, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 5),
(24, 8, 'tour', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 15, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 5),
(25, 8, 'tour', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 11, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 5),
(26, 9, 'tour', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 16, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 6),
(27, 9, 'tour', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 9, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 6),
(28, 9, 'tour', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 10, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 6),
(29, 9, 'tour', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 13, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 6),
(30, 9, 'tour', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:40', 14, NULL, NULL, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40', 6),
(31, 10, 'tour', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 10, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 5),
(32, 10, 'tour', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 16, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 5),
(33, 10, 'tour', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 15, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 5),
(34, 11, 'tour', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 7, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 4),
(35, 11, 'tour', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 9, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 4),
(36, 11, 'tour', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 8, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 4),
(37, 12, 'tour', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 12, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 1),
(38, 12, 'tour', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 10, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 1),
(39, 12, 'tour', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 12, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 1),
(40, 12, 'tour', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 12, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 1),
(41, 13, 'tour', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 10, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 5),
(42, 14, 'tour', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 16, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 1),
(43, 14, 'tour', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 7, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 1),
(44, 14, 'tour', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 9, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 1),
(45, 15, 'tour', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 13, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 6),
(46, 15, 'tour', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 7, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 6),
(47, 15, 'tour', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 7, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 6),
(48, 15, 'tour', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 10, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 6),
(49, 16, 'tour', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 11, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 1),
(50, 16, 'tour', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 15, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 1),
(51, 16, 'tour', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:41', 8, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41', 1),
(52, 1, 'space', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 15, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 1),
(53, 1, 'space', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 8, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 1),
(54, 2, 'space', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 10, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 1),
(55, 2, 'space', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 16, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 1),
(56, 2, 'space', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 14, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 1),
(57, 2, 'space', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 10, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 1),
(58, 3, 'space', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 13, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 6),
(59, 3, 'space', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 8, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 6),
(60, 3, 'space', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 7, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 6),
(61, 3, 'space', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 8, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 6),
(62, 4, 'space', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 9, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 6),
(63, 4, 'space', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 8, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 6),
(64, 5, 'space', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 8, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 5),
(65, 5, 'space', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 15, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 5),
(66, 5, 'space', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 16, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 5),
(67, 6, 'space', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 10, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 5),
(68, 6, 'space', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 11, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 5),
(69, 6, 'space', 'Great Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 8, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 5),
(70, 7, 'space', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 11, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 6),
(71, 7, 'space', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 16, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 6),
(72, 7, 'space', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:43', 12, NULL, NULL, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43', 6),
(73, 7, 'space', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:44', 12, NULL, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44', 6),
(74, 8, 'space', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:44', 8, NULL, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44', 5),
(75, 8, 'space', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:44', 15, NULL, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44', 5),
(76, 9, 'space', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:44', 11, NULL, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44', 4),
(77, 9, 'space', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:44', 7, NULL, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44', 4),
(78, 10, 'space', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:44', 16, NULL, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44', 1),
(79, 10, 'space', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 4, '127.0.0.1', 'approved', '2020-05-13 23:43:44', 13, NULL, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44', 1),
(80, 10, 'space', 'Trip was great', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:44', 15, NULL, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44', 1),
(81, 11, 'space', 'Good Trip', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:44', 10, NULL, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44', 4),
(82, 11, 'space', 'Easy way to discover the city', 'Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te', 5, '127.0.0.1', 'approved', '2020-05-13 23:43:44', 14, NULL, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44', 4);

-- --------------------------------------------------------

--
-- Table structure for table `bravo_review_meta`
--

CREATE TABLE `bravo_review_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `review_id` int(11) DEFAULT NULL,
  `object_id` int(11) DEFAULT NULL,
  `object_model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `val` text COLLATE utf8mb4_unicode_ci,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bravo_review_meta`
--

INSERT INTO `bravo_review_meta` (`id`, `review_id`, `object_id`, `object_model`, `name`, `val`, `create_user`, `update_user`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(2, 1, 1, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(3, 1, 1, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(4, 1, 1, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(5, 1, 1, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(6, 2, 1, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(7, 2, 1, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(8, 2, 1, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(9, 2, 1, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(10, 2, 1, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(11, 3, 1, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(12, 3, 1, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(13, 3, 1, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(14, 3, 1, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(15, 3, 1, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(16, 4, 2, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(17, 4, 2, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(18, 4, 2, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(19, 4, 2, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(20, 4, 2, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(21, 5, 3, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(22, 5, 3, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(23, 5, 3, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(24, 5, 3, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(25, 5, 3, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(26, 6, 3, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(27, 6, 3, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(28, 6, 3, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(29, 6, 3, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(30, 6, 3, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(31, 7, 3, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(32, 7, 3, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(33, 7, 3, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(34, 7, 3, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(35, 7, 3, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(36, 8, 3, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(37, 8, 3, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(38, 8, 3, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(39, 8, 3, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(40, 8, 3, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(41, 9, 4, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(42, 9, 4, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(43, 9, 4, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(44, 9, 4, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(45, 9, 4, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(46, 10, 4, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(47, 10, 4, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(48, 10, 4, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(49, 10, 4, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(50, 10, 4, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(51, 11, 4, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(52, 11, 4, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(53, 11, 4, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(54, 11, 4, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(55, 11, 4, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(56, 12, 4, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(57, 12, 4, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(58, 12, 4, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(59, 12, 4, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(60, 12, 4, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(61, 13, 4, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(62, 13, 4, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(63, 13, 4, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(64, 13, 4, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(65, 13, 4, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(66, 14, 5, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(67, 14, 5, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(68, 14, 5, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(69, 14, 5, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(70, 14, 5, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(71, 15, 5, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(72, 15, 5, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(73, 15, 5, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(74, 15, 5, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(75, 15, 5, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(76, 16, 5, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(77, 16, 5, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(78, 16, 5, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(79, 16, 5, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(80, 16, 5, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(81, 17, 5, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(82, 17, 5, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(83, 17, 5, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(84, 17, 5, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(85, 17, 5, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(86, 18, 6, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(87, 18, 6, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(88, 18, 6, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(89, 18, 6, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(90, 18, 6, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(91, 19, 6, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(92, 19, 6, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(93, 19, 6, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(94, 19, 6, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(95, 19, 6, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(96, 20, 7, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(97, 20, 7, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(98, 20, 7, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(99, 20, 7, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(100, 20, 7, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(101, 21, 7, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(102, 21, 7, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(103, 21, 7, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(104, 21, 7, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(105, 21, 7, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(106, 22, 7, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(107, 22, 7, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(108, 22, 7, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(109, 22, 7, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(110, 22, 7, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(111, 23, 8, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(112, 23, 8, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(113, 23, 8, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(114, 23, 8, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(115, 23, 8, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(116, 24, 8, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(117, 24, 8, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(118, 24, 8, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(119, 24, 8, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(120, 24, 8, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(121, 25, 8, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(122, 25, 8, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(123, 25, 8, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(124, 25, 8, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(125, 25, 8, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(126, 26, 9, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(127, 26, 9, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(128, 26, 9, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(129, 26, 9, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(130, 26, 9, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(131, 27, 9, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(132, 27, 9, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(133, 27, 9, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(134, 27, 9, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(135, 27, 9, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(136, 28, 9, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(137, 28, 9, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(138, 28, 9, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(139, 28, 9, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(140, 28, 9, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(141, 29, 9, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(142, 29, 9, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(143, 29, 9, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(144, 29, 9, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(145, 29, 9, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(146, 30, 9, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(147, 30, 9, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(148, 30, 9, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(149, 30, 9, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(150, 30, 9, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:40', '2020-05-13 18:13:40'),
(151, 31, 10, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(152, 31, 10, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(153, 31, 10, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(154, 31, 10, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(155, 31, 10, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(156, 32, 10, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(157, 32, 10, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(158, 32, 10, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(159, 32, 10, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(160, 32, 10, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(161, 33, 10, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(162, 33, 10, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(163, 33, 10, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(164, 33, 10, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(165, 33, 10, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(166, 34, 11, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(167, 34, 11, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(168, 34, 11, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(169, 34, 11, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(170, 34, 11, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(171, 35, 11, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(172, 35, 11, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(173, 35, 11, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(174, 35, 11, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(175, 35, 11, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(176, 36, 11, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(177, 36, 11, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(178, 36, 11, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(179, 36, 11, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(180, 36, 11, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(181, 37, 12, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(182, 37, 12, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(183, 37, 12, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(184, 37, 12, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(185, 37, 12, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(186, 38, 12, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(187, 38, 12, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(188, 38, 12, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(189, 38, 12, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(190, 38, 12, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(191, 39, 12, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(192, 39, 12, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(193, 39, 12, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(194, 39, 12, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(195, 39, 12, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(196, 40, 12, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(197, 40, 12, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(198, 40, 12, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(199, 40, 12, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(200, 40, 12, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(201, 41, 13, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(202, 41, 13, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(203, 41, 13, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(204, 41, 13, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(205, 41, 13, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(206, 42, 14, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(207, 42, 14, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(208, 42, 14, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(209, 42, 14, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(210, 42, 14, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(211, 43, 14, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(212, 43, 14, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(213, 43, 14, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(214, 43, 14, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(215, 43, 14, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(216, 44, 14, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(217, 44, 14, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(218, 44, 14, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(219, 44, 14, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(220, 44, 14, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(221, 45, 15, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(222, 45, 15, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(223, 45, 15, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(224, 45, 15, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(225, 45, 15, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(226, 46, 15, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(227, 46, 15, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(228, 46, 15, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(229, 46, 15, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(230, 46, 15, 'tour', 'Safety', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(231, 47, 15, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(232, 47, 15, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(233, 47, 15, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(234, 47, 15, 'tour', 'Area Expert', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(235, 47, 15, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(236, 48, 15, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(237, 48, 15, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(238, 48, 15, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(239, 48, 15, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(240, 48, 15, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(241, 49, 16, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(242, 49, 16, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(243, 49, 16, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(244, 49, 16, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(245, 49, 16, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(246, 50, 16, 'tour', 'Service', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(247, 50, 16, 'tour', 'Organization', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(248, 50, 16, 'tour', 'Friendliness', '4', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(249, 50, 16, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(250, 50, 16, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(251, 51, 16, 'tour', 'Service', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(252, 51, 16, 'tour', 'Organization', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(253, 51, 16, 'tour', 'Friendliness', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(254, 51, 16, 'tour', 'Area Expert', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(255, 51, 16, 'tour', 'Safety', '5', 1, NULL, '2020-05-13 18:13:41', '2020-05-13 18:13:41'),
(256, 52, 1, 'space', 'Sleep', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(257, 52, 1, 'space', 'Location', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(258, 52, 1, 'space', 'Service', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(259, 52, 1, 'space', 'Clearness', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(260, 52, 1, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(261, 53, 1, 'space', 'Sleep', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(262, 53, 1, 'space', 'Location', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(263, 53, 1, 'space', 'Service', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(264, 53, 1, 'space', 'Clearness', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(265, 53, 1, 'space', 'Rooms', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(266, 54, 2, 'space', 'Sleep', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(267, 54, 2, 'space', 'Location', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(268, 54, 2, 'space', 'Service', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(269, 54, 2, 'space', 'Clearness', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(270, 54, 2, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(271, 55, 2, 'space', 'Sleep', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(272, 55, 2, 'space', 'Location', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(273, 55, 2, 'space', 'Service', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(274, 55, 2, 'space', 'Clearness', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(275, 55, 2, 'space', 'Rooms', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(276, 56, 2, 'space', 'Sleep', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(277, 56, 2, 'space', 'Location', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(278, 56, 2, 'space', 'Service', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(279, 56, 2, 'space', 'Clearness', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(280, 56, 2, 'space', 'Rooms', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(281, 57, 2, 'space', 'Sleep', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(282, 57, 2, 'space', 'Location', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(283, 57, 2, 'space', 'Service', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(284, 57, 2, 'space', 'Clearness', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(285, 57, 2, 'space', 'Rooms', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(286, 58, 3, 'space', 'Sleep', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(287, 58, 3, 'space', 'Location', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(288, 58, 3, 'space', 'Service', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(289, 58, 3, 'space', 'Clearness', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(290, 58, 3, 'space', 'Rooms', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(291, 59, 3, 'space', 'Sleep', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(292, 59, 3, 'space', 'Location', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(293, 59, 3, 'space', 'Service', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(294, 59, 3, 'space', 'Clearness', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(295, 59, 3, 'space', 'Rooms', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(296, 60, 3, 'space', 'Sleep', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(297, 60, 3, 'space', 'Location', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(298, 60, 3, 'space', 'Service', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(299, 60, 3, 'space', 'Clearness', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(300, 60, 3, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(301, 61, 3, 'space', 'Sleep', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(302, 61, 3, 'space', 'Location', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(303, 61, 3, 'space', 'Service', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(304, 61, 3, 'space', 'Clearness', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(305, 61, 3, 'space', 'Rooms', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(306, 62, 4, 'space', 'Sleep', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(307, 62, 4, 'space', 'Location', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(308, 62, 4, 'space', 'Service', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(309, 62, 4, 'space', 'Clearness', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(310, 62, 4, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(311, 63, 4, 'space', 'Sleep', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(312, 63, 4, 'space', 'Location', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(313, 63, 4, 'space', 'Service', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(314, 63, 4, 'space', 'Clearness', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(315, 63, 4, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(316, 64, 5, 'space', 'Sleep', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(317, 64, 5, 'space', 'Location', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(318, 64, 5, 'space', 'Service', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(319, 64, 5, 'space', 'Clearness', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(320, 64, 5, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(321, 65, 5, 'space', 'Sleep', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(322, 65, 5, 'space', 'Location', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(323, 65, 5, 'space', 'Service', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(324, 65, 5, 'space', 'Clearness', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(325, 65, 5, 'space', 'Rooms', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(326, 66, 5, 'space', 'Sleep', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(327, 66, 5, 'space', 'Location', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(328, 66, 5, 'space', 'Service', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(329, 66, 5, 'space', 'Clearness', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(330, 66, 5, 'space', 'Rooms', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(331, 67, 6, 'space', 'Sleep', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(332, 67, 6, 'space', 'Location', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(333, 67, 6, 'space', 'Service', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(334, 67, 6, 'space', 'Clearness', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(335, 67, 6, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(336, 68, 6, 'space', 'Sleep', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(337, 68, 6, 'space', 'Location', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(338, 68, 6, 'space', 'Service', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(339, 68, 6, 'space', 'Clearness', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(340, 68, 6, 'space', 'Rooms', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(341, 69, 6, 'space', 'Sleep', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(342, 69, 6, 'space', 'Location', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(343, 69, 6, 'space', 'Service', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(344, 69, 6, 'space', 'Clearness', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(345, 69, 6, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(346, 70, 7, 'space', 'Sleep', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(347, 70, 7, 'space', 'Location', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(348, 70, 7, 'space', 'Service', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(349, 70, 7, 'space', 'Clearness', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(350, 70, 7, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(351, 71, 7, 'space', 'Sleep', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(352, 71, 7, 'space', 'Location', '4', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(353, 71, 7, 'space', 'Service', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(354, 71, 7, 'space', 'Clearness', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(355, 71, 7, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(356, 72, 7, 'space', 'Sleep', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(357, 72, 7, 'space', 'Location', '5', 1, NULL, '2020-05-13 18:13:43', '2020-05-13 18:13:43'),
(358, 72, 7, 'space', 'Service', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(359, 72, 7, 'space', 'Clearness', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(360, 72, 7, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(361, 73, 7, 'space', 'Sleep', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(362, 73, 7, 'space', 'Location', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(363, 73, 7, 'space', 'Service', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(364, 73, 7, 'space', 'Clearness', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(365, 73, 7, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(366, 74, 8, 'space', 'Sleep', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(367, 74, 8, 'space', 'Location', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(368, 74, 8, 'space', 'Service', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(369, 74, 8, 'space', 'Clearness', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(370, 74, 8, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(371, 75, 8, 'space', 'Sleep', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(372, 75, 8, 'space', 'Location', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(373, 75, 8, 'space', 'Service', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(374, 75, 8, 'space', 'Clearness', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(375, 75, 8, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(376, 76, 9, 'space', 'Sleep', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(377, 76, 9, 'space', 'Location', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(378, 76, 9, 'space', 'Service', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(379, 76, 9, 'space', 'Clearness', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(380, 76, 9, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(381, 77, 9, 'space', 'Sleep', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(382, 77, 9, 'space', 'Location', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(383, 77, 9, 'space', 'Service', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(384, 77, 9, 'space', 'Clearness', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(385, 77, 9, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(386, 78, 10, 'space', 'Sleep', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(387, 78, 10, 'space', 'Location', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(388, 78, 10, 'space', 'Service', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(389, 78, 10, 'space', 'Clearness', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(390, 78, 10, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(391, 79, 10, 'space', 'Sleep', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(392, 79, 10, 'space', 'Location', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(393, 79, 10, 'space', 'Service', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(394, 79, 10, 'space', 'Clearness', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(395, 79, 10, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(396, 80, 10, 'space', 'Sleep', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(397, 80, 10, 'space', 'Location', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(398, 80, 10, 'space', 'Service', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(399, 80, 10, 'space', 'Clearness', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(400, 80, 10, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(401, 81, 11, 'space', 'Sleep', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(402, 81, 11, 'space', 'Location', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(403, 81, 11, 'space', 'Service', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(404, 81, 11, 'space', 'Clearness', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(405, 81, 11, 'space', 'Rooms', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(406, 82, 11, 'space', 'Sleep', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(407, 82, 11, 'space', 'Location', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(408, 82, 11, 'space', 'Service', '5', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(409, 82, 11, 'space', 'Clearness', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(410, 82, 11, 'space', 'Rooms', '4', 1, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44');

-- --------------------------------------------------------

--
-- Table structure for table `bravo_seo`
--

CREATE TABLE `bravo_seo` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `object_model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_index` tinyint(4) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_desc` text COLLATE utf8mb4_unicode_ci,
  `seo_image` int(11) DEFAULT NULL,
  `seo_share` text COLLATE utf8mb4_unicode_ci,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `lang` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bravo_seo`
--

INSERT INTO `bravo_seo` (`id`, `object_id`, `object_model`, `seo_index`, `seo_title`, `seo_desc`, `seo_image`, `seo_share`, `create_user`, `update_user`, `origin_id`, `lang`, `created_at`, `updated_at`) VALUES
(1, 1, 'page', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, NULL, NULL, NULL, '2020-05-28 03:09:19', '2020-05-28 03:09:19'),
(2, 17, 'tour', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, 1, NULL, NULL, '2020-06-06 11:42:29', '2020-06-06 11:47:39'),
(3, 18, 'tour', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, NULL, NULL, NULL, '2020-06-07 03:03:24', '2020-06-07 03:03:24'),
(4, 19, 'tour', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, NULL, NULL, NULL, '2020-06-08 11:53:43', '2020-06-08 11:53:43'),
(5, 20, 'tour', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, 1, NULL, NULL, '2020-06-08 12:27:30', '2020-07-20 16:58:44'),
(6, 21, 'tour', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, 1, NULL, NULL, '2020-06-08 12:31:15', '2020-07-20 16:48:29'),
(7, 22, 'tour', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, 1, NULL, NULL, '2020-06-08 12:34:40', '2020-07-20 16:45:45'),
(8, 23, 'tour', 1, NULL, NULL, 106, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":\"105\"},\"twitter\":{\"title\":null,\"desc\":null,\"image\":\"105\"}}', 1, 1, NULL, NULL, '2020-06-08 12:35:57', '2020-07-20 17:06:40'),
(9, 2, 'tour', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, NULL, NULL, NULL, '2020-06-22 13:59:07', '2020-06-22 13:59:07'),
(10, 3, 'tour', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, NULL, NULL, NULL, '2020-06-22 14:13:01', '2020-06-22 14:13:01'),
(11, 4, 'page', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, 1, NULL, NULL, '2020-07-02 13:32:17', '2020-07-02 13:33:48'),
(12, 3, 'page', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, NULL, NULL, NULL, '2020-07-19 22:54:08', '2020-07-19 22:54:08'),
(13, 1, 'news_category', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, 1, NULL, NULL, '2020-07-20 14:41:17', '2020-07-20 14:41:41'),
(14, 43, 'tour', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 45, 1, NULL, NULL, '2020-07-23 09:28:19', '2020-07-23 09:48:15'),
(15, 255, 'tour', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, NULL, NULL, NULL, '2020-07-28 04:10:45', '2020-07-28 04:10:45'),
(16, 256, 'tour', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 44, 1, NULL, NULL, '2020-07-28 11:43:13', '2020-07-28 11:45:22');

-- --------------------------------------------------------

--
-- Table structure for table `bravo_spaces`
--

CREATE TABLE `bravo_spaces` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `image_id` int(11) DEFAULT NULL,
  `banner_image_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map_lat` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map_lng` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map_zoom` int(11) DEFAULT NULL,
  `is_featured` tinyint(4) DEFAULT NULL,
  `gallery` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faqs` text COLLATE utf8mb4_unicode_ci,
  `price` decimal(12,2) DEFAULT NULL,
  `sale_price` decimal(12,2) DEFAULT NULL,
  `is_instant` tinyint(4) DEFAULT '0',
  `allow_children` tinyint(4) DEFAULT '0',
  `allow_infant` tinyint(4) DEFAULT '0',
  `max_guests` tinyint(4) DEFAULT '0',
  `bed` tinyint(4) DEFAULT '0',
  `bathroom` tinyint(4) DEFAULT '0',
  `square` tinyint(4) DEFAULT '0',
  `enable_extra_price` tinyint(4) DEFAULT NULL,
  `extra_price` text COLLATE utf8mb4_unicode_ci,
  `discount_by_days` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_state` tinyint(4) DEFAULT '1',
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bravo_spaces`
--

INSERT INTO `bravo_spaces` (`id`, `title`, `slug`, `content`, `image_id`, `banner_image_id`, `location_id`, `address`, `map_lat`, `map_lng`, `map_zoom`, `is_featured`, `gallery`, `video`, `faqs`, `price`, `sale_price`, `is_instant`, `allow_children`, `allow_infant`, `max_guests`, `bed`, `bathroom`, `square`, `enable_extra_price`, `extra_price`, `discount_by_days`, `status`, `default_state`, `create_user`, `update_user`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'LUXURY STUDIO', 'luxury-studio', '<p>Libero sem vitae sed donec conubia integer nisi integer rhoncus imperdiet orci odio libero est integer a integer tincidunt sollicitudin blandit fusce nibh leo vulputate lobortis egestas dapibus faucibus metus conubia maecenas cras potenti cum hac arcu rhoncus nullam eros dictum torquent integer cursus bibendum sem sociis molestie tellus purus</p><p>Quam fusce convallis ipsum malesuada amet velit aliquam urna nullam vehicula fermentum id morbi dis magnis porta sagittis euismod etiam</p><h4>HIGHLIGHTS</h4><ul><li>Visit the Museum of Modern Art in Manhattan</li><li>See amazing works of contemporary art, including Vincent van Gogh\'s The Starry Night</li><li>Check out Campbell\'s Soup Cans by Warhol and The Dance (I) by Matisse</li><li>Behold masterpieces by Gauguin, Dali, Picasso, and Pollock</li><li>Enjoy free audio guides available in English, French, German, Italian, Spanish, Portuguese</li></ul>', 59, 80, 2, 'Arrondissement de Paris', '48.852754', '2.339155', 12, 1, '72,73,74,75,76,77,78', 'https://www.youtube.com/watch?v=UfEiKK-iX70', '[{\"title\":\"Check-in time?\",\"content\":\"As a rough guide, the check-in time is after 12 a.m. Let us know your arrival time in case you schedule and early check in we\\u2018ll do our best to have your room available.\"},{\"title\":\"Check-out time?\",\"content\":\"As a rough guide, the check-out time is before 12pm. If you plan a late check out kindly let us know your departure time, we\\u2019ll our best to satisfy your needs.\"},{\"title\":\"Is Reception open 24 hours?\",\"content\":\"Yes, Reception service is available 24 hours.\"},{\"title\":\"Which languages are spoken at Reception?\",\"content\":\"Italian, English, French, German and Spanish.\"},{\"title\":\"Can I leave my luggage?\",\"content\":\"Yes, we can look after your luggage. If at check in your room is not ready yet or in case of early check out after .We will store your luggage free of charge on your check-in and check-out days.\"},{\"title\":\"Internet connection?\",\"content\":\"A wireless internet connection is available throughout the hotel.\\r\\n\\r\\nThe guest rooms feature hi-speed web connectivity (both wireless and cabled).\"}]', '300.00', '583.00', 0, 0, 0, 6, 10, 7, 127, NULL, NULL, NULL, 'publish', 1, 1, NULL, NULL, '2020-05-13 18:13:43', NULL),
(2, 'LUXURY APARTMENT', 'luxury-apartment', '<p>Libero sem vitae sed donec conubia integer nisi integer rhoncus imperdiet orci odio libero est integer a integer tincidunt sollicitudin blandit fusce nibh leo vulputate lobortis egestas dapibus faucibus metus conubia maecenas cras potenti cum hac arcu rhoncus nullam eros dictum torquent integer cursus bibendum sem sociis molestie tellus purus</p><p>Quam fusce convallis ipsum malesuada amet velit aliquam urna nullam vehicula fermentum id morbi dis magnis porta sagittis euismod etiam</p><h4>HIGHLIGHTS</h4><ul><li>Visit the Museum of Modern Art in Manhattan</li><li>See amazing works of contemporary art, including Vincent van Gogh\'s The Starry Night</li><li>Check out Campbell\'s Soup Cans by Warhol and The Dance (I) by Matisse</li><li>Behold masterpieces by Gauguin, Dali, Picasso, and Pollock</li><li>Enjoy free audio guides available in English, French, German, Italian, Spanish, Portuguese</li></ul>', 60, 81, 3, 'Porte de Vanves', '48.853917', '2.307199', 12, 0, '72,73,74,75,76,77,78', 'https://www.youtube.com/watch?v=UfEiKK-iX70', '[{\"title\":\"Check-in time?\",\"content\":\"As a rough guide, the check-in time is after 12 a.m. Let us know your arrival time in case you schedule and early check in we\\u2018ll do our best to have your room available.\"},{\"title\":\"Check-out time?\",\"content\":\"As a rough guide, the check-out time is before 12pm. If you plan a late check out kindly let us know your departure time, we\\u2019ll our best to satisfy your needs.\"},{\"title\":\"Is Reception open 24 hours?\",\"content\":\"Yes, Reception service is available 24 hours.\"},{\"title\":\"Which languages are spoken at Reception?\",\"content\":\"Italian, English, French, German and Spanish.\"},{\"title\":\"Can I leave my luggage?\",\"content\":\"Yes, we can look after your luggage. If at check in your room is not ready yet or in case of early check out after .We will store your luggage free of charge on your check-in and check-out days.\"},{\"title\":\"Internet connection?\",\"content\":\"A wireless internet connection is available throughout the hotel.\\r\\n\\r\\nThe guest rooms feature hi-speed web connectivity (both wireless and cabled).\"}]', '900.00', '0.00', 1, 0, 0, 7, 3, 6, 127, NULL, NULL, NULL, 'publish', 1, 1, NULL, NULL, '2020-05-13 18:13:43', NULL),
(3, 'BEAUTIFUL LOFT', 'beautiful-loft', '<p>Libero sem vitae sed donec conubia integer nisi integer rhoncus imperdiet orci odio libero est integer a integer tincidunt sollicitudin blandit fusce nibh leo vulputate lobortis egestas dapibus faucibus metus conubia maecenas cras potenti cum hac arcu rhoncus nullam eros dictum torquent integer cursus bibendum sem sociis molestie tellus purus</p><p>Quam fusce convallis ipsum malesuada amet velit aliquam urna nullam vehicula fermentum id morbi dis magnis porta sagittis euismod etiam</p><h4>HIGHLIGHTS</h4><ul><li>Visit the Museum of Modern Art in Manhattan</li><li>See amazing works of contemporary art, including Vincent van Gogh\'s The Starry Night</li><li>Check out Campbell\'s Soup Cans by Warhol and The Dance (I) by Matisse</li><li>Behold masterpieces by Gauguin, Dali, Picasso, and Pollock</li><li>Enjoy free audio guides available in English, French, German, Italian, Spanish, Portuguese</li></ul>', 61, 79, 3, 'Porte de Vanves', '48.853917', '2.307199', 12, 0, '72,73,74,75,76,77,78', 'https://www.youtube.com/watch?v=UfEiKK-iX70', '[{\"title\":\"Check-in time?\",\"content\":\"As a rough guide, the check-in time is after 12 a.m. Let us know your arrival time in case you schedule and early check in we\\u2018ll do our best to have your room available.\"},{\"title\":\"Check-out time?\",\"content\":\"As a rough guide, the check-out time is before 12pm. If you plan a late check out kindly let us know your departure time, we\\u2019ll our best to satisfy your needs.\"},{\"title\":\"Is Reception open 24 hours?\",\"content\":\"Yes, Reception service is available 24 hours.\"},{\"title\":\"Which languages are spoken at Reception?\",\"content\":\"Italian, English, French, German and Spanish.\"},{\"title\":\"Can I leave my luggage?\",\"content\":\"Yes, we can look after your luggage. If at check in your room is not ready yet or in case of early check out after .We will store your luggage free of charge on your check-in and check-out days.\"},{\"title\":\"Internet connection?\",\"content\":\"A wireless internet connection is available throughout the hotel.\\r\\n\\r\\nThe guest rooms feature hi-speed web connectivity (both wireless and cabled).\"}]', '650.00', '0.00', 1, 0, 0, 9, 8, 7, 111, NULL, NULL, NULL, 'publish', 1, 6, NULL, NULL, '2020-05-13 18:13:43', NULL),
(4, 'BEST OF WEST VILLAGE', 'best-of-west-village', '<p>Libero sem vitae sed donec conubia integer nisi integer rhoncus imperdiet orci odio libero est integer a integer tincidunt sollicitudin blandit fusce nibh leo vulputate lobortis egestas dapibus faucibus metus conubia maecenas cras potenti cum hac arcu rhoncus nullam eros dictum torquent integer cursus bibendum sem sociis molestie tellus purus</p><p>Quam fusce convallis ipsum malesuada amet velit aliquam urna nullam vehicula fermentum id morbi dis magnis porta sagittis euismod etiam</p><h4>HIGHLIGHTS</h4><ul><li>Visit the Museum of Modern Art in Manhattan</li><li>See amazing works of contemporary art, including Vincent van Gogh\'s The Starry Night</li><li>Check out Campbell\'s Soup Cans by Warhol and The Dance (I) by Matisse</li><li>Behold masterpieces by Gauguin, Dali, Picasso, and Pollock</li><li>Enjoy free audio guides available in English, French, German, Italian, Spanish, Portuguese</li></ul>', 62, 79, 4, 'Porte de Vanves', '48.853917', '2.307199', 12, 1, '72,73,74,75,76,77,78', 'https://www.youtube.com/watch?v=UfEiKK-iX70', '[{\"title\":\"Check-in time?\",\"content\":\"As a rough guide, the check-in time is after 12 a.m. Let us know your arrival time in case you schedule and early check in we\\u2018ll do our best to have your room available.\"},{\"title\":\"Check-out time?\",\"content\":\"As a rough guide, the check-out time is before 12pm. If you plan a late check out kindly let us know your departure time, we\\u2019ll our best to satisfy your needs.\"},{\"title\":\"Is Reception open 24 hours?\",\"content\":\"Yes, Reception service is available 24 hours.\"},{\"title\":\"Which languages are spoken at Reception?\",\"content\":\"Italian, English, French, German and Spanish.\"},{\"title\":\"Can I leave my luggage?\",\"content\":\"Yes, we can look after your luggage. If at check in your room is not ready yet or in case of early check out after .We will store your luggage free of charge on your check-in and check-out days.\"},{\"title\":\"Internet connection?\",\"content\":\"A wireless internet connection is available throughout the hotel.\\r\\n\\r\\nThe guest rooms feature hi-speed web connectivity (both wireless and cabled).\"}]', '800.00', '0.00', 1, 0, 0, 8, 10, 4, 127, NULL, NULL, NULL, 'publish', 1, 6, NULL, NULL, '2020-05-13 18:13:43', NULL),
(5, 'DUPLEX GREENWICH', 'duplex-greenwich', '<p>Libero sem vitae sed donec conubia integer nisi integer rhoncus imperdiet orci odio libero est integer a integer tincidunt sollicitudin blandit fusce nibh leo vulputate lobortis egestas dapibus faucibus metus conubia maecenas cras potenti cum hac arcu rhoncus nullam eros dictum torquent integer cursus bibendum sem sociis molestie tellus purus</p><p>Quam fusce convallis ipsum malesuada amet velit aliquam urna nullam vehicula fermentum id morbi dis magnis porta sagittis euismod etiam</p><h4>HIGHLIGHTS</h4><ul><li>Visit the Museum of Modern Art in Manhattan</li><li>See amazing works of contemporary art, including Vincent van Gogh\'s The Starry Night</li><li>Check out Campbell\'s Soup Cans by Warhol and The Dance (I) by Matisse</li><li>Behold masterpieces by Gauguin, Dali, Picasso, and Pollock</li><li>Enjoy free audio guides available in English, French, German, Italian, Spanish, Portuguese</li></ul>', 63, 79, 1, 'Porte de Vanves', '48.853917', '2.307199', 12, 1, '72,73,74,75,76,77,78', 'https://www.youtube.com/watch?v=UfEiKK-iX70', '[{\"title\":\"Check-in time?\",\"content\":\"As a rough guide, the check-in time is after 12 a.m. Let us know your arrival time in case you schedule and early check in we\\u2018ll do our best to have your room available.\"},{\"title\":\"Check-out time?\",\"content\":\"As a rough guide, the check-out time is before 12pm. If you plan a late check out kindly let us know your departure time, we\\u2019ll our best to satisfy your needs.\"},{\"title\":\"Is Reception open 24 hours?\",\"content\":\"Yes, Reception service is available 24 hours.\"},{\"title\":\"Which languages are spoken at Reception?\",\"content\":\"Italian, English, French, German and Spanish.\"},{\"title\":\"Can I leave my luggage?\",\"content\":\"Yes, we can look after your luggage. If at check in your room is not ready yet or in case of early check out after .We will store your luggage free of charge on your check-in and check-out days.\"},{\"title\":\"Internet connection?\",\"content\":\"A wireless internet connection is available throughout the hotel.\\r\\n\\r\\nThe guest rooms feature hi-speed web connectivity (both wireless and cabled).\"}]', '220.00', '0.00', 0, 0, 0, 7, 4, 4, 127, NULL, NULL, NULL, 'publish', 1, 5, NULL, NULL, '2020-05-13 18:13:43', NULL),
(6, 'THE MEATPACKING SUITES', 'the-meatpacking-suites', '<p>Libero sem vitae sed donec conubia integer nisi integer rhoncus imperdiet orci odio libero est integer a integer tincidunt sollicitudin blandit fusce nibh leo vulputate lobortis egestas dapibus faucibus metus conubia maecenas cras potenti cum hac arcu rhoncus nullam eros dictum torquent integer cursus bibendum sem sociis molestie tellus purus</p><p>Quam fusce convallis ipsum malesuada amet velit aliquam urna nullam vehicula fermentum id morbi dis magnis porta sagittis euismod etiam</p><h4>HIGHLIGHTS</h4><ul><li>Visit the Museum of Modern Art in Manhattan</li><li>See amazing works of contemporary art, including Vincent van Gogh\'s The Starry Night</li><li>Check out Campbell\'s Soup Cans by Warhol and The Dance (I) by Matisse</li><li>Behold masterpieces by Gauguin, Dali, Picasso, and Pollock</li><li>Enjoy free audio guides available in English, French, German, Italian, Spanish, Portuguese</li></ul>', 64, 79, 1, 'Porte de Vanves', '48.853917', '2.307199', 12, 0, '72,73,74,75,76,77,78', 'https://www.youtube.com/watch?v=UfEiKK-iX70', '[{\"title\":\"Check-in time?\",\"content\":\"As a rough guide, the check-in time is after 12 a.m. Let us know your arrival time in case you schedule and early check in we\\u2018ll do our best to have your room available.\"},{\"title\":\"Check-out time?\",\"content\":\"As a rough guide, the check-out time is before 12pm. If you plan a late check out kindly let us know your departure time, we\\u2019ll our best to satisfy your needs.\"},{\"title\":\"Is Reception open 24 hours?\",\"content\":\"Yes, Reception service is available 24 hours.\"},{\"title\":\"Which languages are spoken at Reception?\",\"content\":\"Italian, English, French, German and Spanish.\"},{\"title\":\"Can I leave my luggage?\",\"content\":\"Yes, we can look after your luggage. If at check in your room is not ready yet or in case of early check out after .We will store your luggage free of charge on your check-in and check-out days.\"},{\"title\":\"Internet connection?\",\"content\":\"A wireless internet connection is available throughout the hotel.\\r\\n\\r\\nThe guest rooms feature hi-speed web connectivity (both wireless and cabled).\"}]', '320.00', '0.00', 1, 0, 0, 8, 3, 8, 127, NULL, NULL, NULL, 'publish', 1, 5, NULL, NULL, '2020-05-13 18:13:43', NULL),
(7, 'EAST VILLAGE', 'east-village', '<p>Libero sem vitae sed donec conubia integer nisi integer rhoncus imperdiet orci odio libero est integer a integer tincidunt sollicitudin blandit fusce nibh leo vulputate lobortis egestas dapibus faucibus metus conubia maecenas cras potenti cum hac arcu rhoncus nullam eros dictum torquent integer cursus bibendum sem sociis molestie tellus purus</p><p>Quam fusce convallis ipsum malesuada amet velit aliquam urna nullam vehicula fermentum id morbi dis magnis porta sagittis euismod etiam</p><h4>HIGHLIGHTS</h4><ul><li>Visit the Museum of Modern Art in Manhattan</li><li>See amazing works of contemporary art, including Vincent van Gogh\'s The Starry Night</li><li>Check out Campbell\'s Soup Cans by Warhol and The Dance (I) by Matisse</li><li>Behold masterpieces by Gauguin, Dali, Picasso, and Pollock</li><li>Enjoy free audio guides available in English, French, German, Italian, Spanish, Portuguese</li></ul>', 65, 81, 1, 'Porte de Vanves', '48.853917', '2.307199', 12, 0, '72,73,74,75,76,77,78', 'https://www.youtube.com/watch?v=UfEiKK-iX70', '[{\"title\":\"Check-in time?\",\"content\":\"As a rough guide, the check-in time is after 12 a.m. Let us know your arrival time in case you schedule and early check in we\\u2018ll do our best to have your room available.\"},{\"title\":\"Check-out time?\",\"content\":\"As a rough guide, the check-out time is before 12pm. If you plan a late check out kindly let us know your departure time, we\\u2019ll our best to satisfy your needs.\"},{\"title\":\"Is Reception open 24 hours?\",\"content\":\"Yes, Reception service is available 24 hours.\"},{\"title\":\"Which languages are spoken at Reception?\",\"content\":\"Italian, English, French, German and Spanish.\"},{\"title\":\"Can I leave my luggage?\",\"content\":\"Yes, we can look after your luggage. If at check in your room is not ready yet or in case of early check out after .We will store your luggage free of charge on your check-in and check-out days.\"},{\"title\":\"Internet connection?\",\"content\":\"A wireless internet connection is available throughout the hotel.\\r\\n\\r\\nThe guest rooms feature hi-speed web connectivity (both wireless and cabled).\"}]', '300.00', '260.00', 1, 0, 0, 7, 9, 7, 127, NULL, NULL, NULL, 'publish', 1, 6, NULL, NULL, '2020-05-13 18:13:43', NULL),
(8, 'PARIS GREENWICH VILLA', 'paris-greenwich-villa', '<p>Libero sem vitae sed donec conubia integer nisi integer rhoncus imperdiet orci odio libero est integer a integer tincidunt sollicitudin blandit fusce nibh leo vulputate lobortis egestas dapibus faucibus metus conubia maecenas cras potenti cum hac arcu rhoncus nullam eros dictum torquent integer cursus bibendum sem sociis molestie tellus purus</p><p>Quam fusce convallis ipsum malesuada amet velit aliquam urna nullam vehicula fermentum id morbi dis magnis porta sagittis euismod etiam</p><h4>HIGHLIGHTS</h4><ul><li>Visit the Museum of Modern Art in Manhattan</li><li>See amazing works of contemporary art, including Vincent van Gogh\'s The Starry Night</li><li>Check out Campbell\'s Soup Cans by Warhol and The Dance (I) by Matisse</li><li>Behold masterpieces by Gauguin, Dali, Picasso, and Pollock</li><li>Enjoy free audio guides available in English, French, German, Italian, Spanish, Portuguese</li></ul>', 66, 80, 1, 'Porte de Vanves', '48.853917', '2.307199', 12, 1, '72,73,74,75,76,77,78', 'https://www.youtube.com/watch?v=UfEiKK-iX70', '[{\"title\":\"Check-in time?\",\"content\":\"As a rough guide, the check-in time is after 12 a.m. Let us know your arrival time in case you schedule and early check in we\\u2018ll do our best to have your room available.\"},{\"title\":\"Check-out time?\",\"content\":\"As a rough guide, the check-out time is before 12pm. If you plan a late check out kindly let us know your departure time, we\\u2019ll our best to satisfy your needs.\"},{\"title\":\"Is Reception open 24 hours?\",\"content\":\"Yes, Reception service is available 24 hours.\"},{\"title\":\"Which languages are spoken at Reception?\",\"content\":\"Italian, English, French, German and Spanish.\"},{\"title\":\"Can I leave my luggage?\",\"content\":\"Yes, we can look after your luggage. If at check in your room is not ready yet or in case of early check out after .We will store your luggage free of charge on your check-in and check-out days.\"},{\"title\":\"Internet connection?\",\"content\":\"A wireless internet connection is available throughout the hotel.\\r\\n\\r\\nThe guest rooms feature hi-speed web connectivity (both wireless and cabled).\"}]', '500.00', '0.00', 1, 0, 0, 9, 8, 6, 127, NULL, NULL, NULL, 'publish', 1, 5, NULL, NULL, '2020-05-13 18:13:43', NULL),
(9, 'LUXURY SINGLE', 'luxury-single', '<p>Libero sem vitae sed donec conubia integer nisi integer rhoncus imperdiet orci odio libero est integer a integer tincidunt sollicitudin blandit fusce nibh leo vulputate lobortis egestas dapibus faucibus metus conubia maecenas cras potenti cum hac arcu rhoncus nullam eros dictum torquent integer cursus bibendum sem sociis molestie tellus purus</p><p>Quam fusce convallis ipsum malesuada amet velit aliquam urna nullam vehicula fermentum id morbi dis magnis porta sagittis euismod etiam</p><h4>HIGHLIGHTS</h4><ul><li>Visit the Museum of Modern Art in Manhattan</li><li>See amazing works of contemporary art, including Vincent van Gogh\'s The Starry Night</li><li>Check out Campbell\'s Soup Cans by Warhol and The Dance (I) by Matisse</li><li>Behold masterpieces by Gauguin, Dali, Picasso, and Pollock</li><li>Enjoy free audio guides available in English, French, German, Italian, Spanish, Portuguese</li></ul>', 67, 79, 1, 'Porte de Vanves', '48.853917', '2.307199', 12, 0, '72,73,74,75,76,77,78', 'https://www.youtube.com/watch?v=UfEiKK-iX70', '[{\"title\":\"Check-in time?\",\"content\":\"As a rough guide, the check-in time is after 12 a.m. Let us know your arrival time in case you schedule and early check in we\\u2018ll do our best to have your room available.\"},{\"title\":\"Check-out time?\",\"content\":\"As a rough guide, the check-out time is before 12pm. If you plan a late check out kindly let us know your departure time, we\\u2019ll our best to satisfy your needs.\"},{\"title\":\"Is Reception open 24 hours?\",\"content\":\"Yes, Reception service is available 24 hours.\"},{\"title\":\"Which languages are spoken at Reception?\",\"content\":\"Italian, English, French, German and Spanish.\"},{\"title\":\"Can I leave my luggage?\",\"content\":\"Yes, we can look after your luggage. If at check in your room is not ready yet or in case of early check out after .We will store your luggage free of charge on your check-in and check-out days.\"},{\"title\":\"Internet connection?\",\"content\":\"A wireless internet connection is available throughout the hotel.\\r\\n\\r\\nThe guest rooms feature hi-speed web connectivity (both wireless and cabled).\"}]', '400.00', '350.00', 1, 0, 0, 5, 10, 6, 126, NULL, NULL, NULL, 'publish', 1, 4, NULL, NULL, '2020-05-13 18:13:43', NULL),
(10, 'LILY DALE VILLAGE', 'lily-dale-village', '<p>Libero sem vitae sed donec conubia integer nisi integer rhoncus imperdiet orci odio libero est integer a integer tincidunt sollicitudin blandit fusce nibh leo vulputate lobortis egestas dapibus faucibus metus conubia maecenas cras potenti cum hac arcu rhoncus nullam eros dictum torquent integer cursus bibendum sem sociis molestie tellus purus</p><p>Quam fusce convallis ipsum malesuada amet velit aliquam urna nullam vehicula fermentum id morbi dis magnis porta sagittis euismod etiam</p><h4>HIGHLIGHTS</h4><ul><li>Visit the Museum of Modern Art in Manhattan</li><li>See amazing works of contemporary art, including Vincent van Gogh\'s The Starry Night</li><li>Check out Campbell\'s Soup Cans by Warhol and The Dance (I) by Matisse</li><li>Behold masterpieces by Gauguin, Dali, Picasso, and Pollock</li><li>Enjoy free audio guides available in English, French, German, Italian, Spanish, Portuguese</li></ul>', 68, 79, 1, 'Porte de Vanves', '48.853917', '2.307199', 12, 1, '72,73,74,75,76,77,78', 'https://www.youtube.com/watch?v=UfEiKK-iX70', '[{\"title\":\"Check-in time?\",\"content\":\"As a rough guide, the check-in time is after 12 a.m. Let us know your arrival time in case you schedule and early check in we\\u2018ll do our best to have your room available.\"},{\"title\":\"Check-out time?\",\"content\":\"As a rough guide, the check-out time is before 12pm. If you plan a late check out kindly let us know your departure time, we\\u2019ll our best to satisfy your needs.\"},{\"title\":\"Is Reception open 24 hours?\",\"content\":\"Yes, Reception service is available 24 hours.\"},{\"title\":\"Which languages are spoken at Reception?\",\"content\":\"Italian, English, French, German and Spanish.\"},{\"title\":\"Can I leave my luggage?\",\"content\":\"Yes, we can look after your luggage. If at check in your room is not ready yet or in case of early check out after .We will store your luggage free of charge on your check-in and check-out days.\"},{\"title\":\"Internet connection?\",\"content\":\"A wireless internet connection is available throughout the hotel.\\r\\n\\r\\nThe guest rooms feature hi-speed web connectivity (both wireless and cabled).\"}]', '250.00', '0.00', 1, 0, 0, 9, 5, 7, 127, NULL, NULL, NULL, 'publish', 1, 1, NULL, NULL, '2020-05-13 18:13:43', NULL),
(11, 'STAY GREENWICH VILLAGE', 'stay-greenwich-village', '<p>Libero sem vitae sed donec conubia integer nisi integer rhoncus imperdiet orci odio libero est integer a integer tincidunt sollicitudin blandit fusce nibh leo vulputate lobortis egestas dapibus faucibus metus conubia maecenas cras potenti cum hac arcu rhoncus nullam eros dictum torquent integer cursus bibendum sem sociis molestie tellus purus</p><p>Quam fusce convallis ipsum malesuada amet velit aliquam urna nullam vehicula fermentum id morbi dis magnis porta sagittis euismod etiam</p><h4>HIGHLIGHTS</h4><ul><li>Visit the Museum of Modern Art in Manhattan</li><li>See amazing works of contemporary art, including Vincent van Gogh\'s The Starry Night</li><li>Check out Campbell\'s Soup Cans by Warhol and The Dance (I) by Matisse</li><li>Behold masterpieces by Gauguin, Dali, Picasso, and Pollock</li><li>Enjoy free audio guides available in English, French, German, Italian, Spanish, Portuguese</li></ul>', 69, 80, 1, 'Porte de Vanves', '48.853917', '2.307199', 12, 1, '72,73,74,75,76,77,78', 'https://www.youtube.com/watch?v=UfEiKK-iX70', '[{\"title\":\"Check-in time?\",\"content\":\"As a rough guide, the check-in time is after 12 a.m. Let us know your arrival time in case you schedule and early check in we\\u2018ll do our best to have your room available.\"},{\"title\":\"Check-out time?\",\"content\":\"As a rough guide, the check-out time is before 12pm. If you plan a late check out kindly let us know your departure time, we\\u2019ll our best to satisfy your needs.\"},{\"title\":\"Is Reception open 24 hours?\",\"content\":\"Yes, Reception service is available 24 hours.\"},{\"title\":\"Which languages are spoken at Reception?\",\"content\":\"Italian, English, French, German and Spanish.\"},{\"title\":\"Can I leave my luggage?\",\"content\":\"Yes, we can look after your luggage. If at check in your room is not ready yet or in case of early check out after .We will store your luggage free of charge on your check-in and check-out days.\"},{\"title\":\"Internet connection?\",\"content\":\"A wireless internet connection is available throughout the hotel.\\r\\n\\r\\nThe guest rooms feature hi-speed web connectivity (both wireless and cabled).\"}]', '300.00', '150.00', 0, 0, 0, 8, 4, 4, 127, NULL, NULL, NULL, 'publish', 1, 4, NULL, NULL, '2020-05-13 18:13:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bravo_space_dates`
--

CREATE TABLE `bravo_space_dates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `target_id` bigint(20) DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `price` decimal(12,2) DEFAULT NULL,
  `max_guests` tinyint(4) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '0',
  `note_to_customer` text COLLATE utf8mb4_unicode_ci,
  `note_to_admin` text COLLATE utf8mb4_unicode_ci,
  `is_instant` tinyint(4) DEFAULT '0',
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bravo_space_term`
--

CREATE TABLE `bravo_space_term` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `term_id` int(11) DEFAULT NULL,
  `target_id` int(11) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bravo_space_term`
--

INSERT INTO `bravo_space_term` (`id`, `term_id`, `target_id`, `create_user`, `update_user`, `created_at`, `updated_at`) VALUES
(1, 15, 1, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(2, 17, 1, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(3, 18, 1, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(4, 19, 1, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(5, 20, 1, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(6, 17, 2, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(7, 18, 2, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(8, 15, 3, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(9, 16, 3, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(10, 18, 3, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(11, 19, 3, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(12, 20, 3, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(13, 15, 4, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(14, 16, 4, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(15, 17, 4, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(16, 19, 4, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(17, 20, 4, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(18, 15, 5, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(19, 16, 5, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(20, 19, 5, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(21, 20, 5, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(22, 15, 6, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(23, 16, 6, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(24, 18, 6, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(25, 19, 6, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(26, 15, 7, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(27, 16, 7, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(28, 18, 7, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(29, 15, 8, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(30, 16, 8, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(31, 18, 8, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(32, 15, 9, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(33, 17, 9, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(34, 18, 9, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(35, 19, 9, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(36, 16, 10, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(37, 17, 10, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(38, 18, 10, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(39, 15, 11, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(40, 16, 11, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44'),
(41, 17, 11, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44');

-- --------------------------------------------------------

--
-- Table structure for table `bravo_space_translations`
--

CREATE TABLE `bravo_space_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `origin_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `faqs` text COLLATE utf8mb4_unicode_ci,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bravo_terms`
--

CREATE TABLE `bravo_terms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `attr_id` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `lang` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bravo_terms`
--

INSERT INTO `bravo_terms` (`id`, `name`, `content`, `attr_id`, `slug`, `create_user`, `update_user`, `origin_id`, `lang`, `created_at`, `updated_at`, `deleted_at`, `image_id`) VALUES
(1, 'INSOMNIA', NULL, 1, 'cultural', 1, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-07-20 15:04:07', NULL, NULL),
(2, 'SPORTS ISSUE', NULL, 1, 'nature-adventure', 1, 1, NULL, NULL, '2020-05-13 18:13:41', '2020-07-20 15:05:01', NULL, NULL),
(3, 'OBESITY', NULL, 1, 'marine', 1, 1, NULL, NULL, '2020-05-13 18:13:41', '2020-07-20 15:05:29', NULL, NULL),
(4, 'OCD', NULL, 1, 'independent', 1, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-07-20 15:05:47', NULL, NULL),
(5, 'NARCISSISTIC PERSONALITY', NULL, 1, 'activities', 1, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-07-20 15:06:22', NULL, NULL),
(6, 'EATING DISORDER', NULL, 1, 'festival-events', 1, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-07-20 15:07:26', NULL, NULL),
(7, 'WEIGHT LOSS', NULL, 1, 'special-interest', 1, NULL, NULL, NULL, '2020-05-13 18:13:41', '2020-07-20 15:07:52', NULL, NULL),
(8, 'BEHAVIORAL ISSUE', NULL, 2, 'wifi', 1, NULL, NULL, NULL, '2020-05-13 18:13:42', '2020-07-20 15:10:57', NULL, NULL),
(9, 'BORDERLINE PERSONALITY', NULL, 2, 'gymnasium', 1, NULL, NULL, NULL, '2020-05-13 18:13:42', '2020-07-20 15:11:39', NULL, NULL),
(10, 'DOMESTIC ABUSE', NULL, 2, 'mountain-bike', 1, NULL, NULL, NULL, '2020-05-13 18:13:42', '2020-07-20 15:12:05', NULL, NULL),
(11, 'RADICAL IDENTITY', NULL, 2, 'satellite-office', 1, NULL, NULL, NULL, '2020-05-13 18:13:42', '2020-07-20 15:12:34', NULL, NULL),
(12, 'SELF-HARMING', NULL, 2, 'staff-lounge', 1, NULL, NULL, NULL, '2020-05-13 18:13:42', '2020-07-20 15:15:58', NULL, NULL),
(13, 'SEX THERAPY', NULL, 2, 'golf-cages', 1, NULL, NULL, NULL, '2020-05-13 18:13:42', '2020-07-20 15:16:16', NULL, NULL),
(14, 'SUICIDAL IDEATION', NULL, 2, 'aerobics-room', 1, NULL, NULL, NULL, '2020-05-13 18:13:42', '2020-07-20 15:16:38', NULL, NULL),
(15, 'Air Conditioning', NULL, 3, 'air-conditioning', NULL, NULL, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44', NULL, 82),
(16, 'Breakfast', NULL, 3, 'breakfast', NULL, NULL, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44', NULL, 83),
(17, 'Kitchen', NULL, 3, 'kitchen', NULL, NULL, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44', NULL, 84),
(18, 'Parking', NULL, 3, 'parking', NULL, NULL, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44', NULL, 85),
(19, 'Pool', NULL, 3, 'pool', NULL, NULL, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44', NULL, 86),
(20, 'Wi-Fi Internet', NULL, 3, 'wi-fi-internet', NULL, NULL, NULL, NULL, '2020-05-13 18:13:44', '2020-05-13 18:13:44', NULL, 87),
(21, 'WOMEN ISSUES', NULL, 4, 'therapy-type-t1', 1, 1, NULL, NULL, '2020-06-06 11:29:19', '2020-07-20 15:19:18', NULL, 80),
(22, 'PREGNANCY', NULL, 4, 'therapy-type-t2', 1, 1, NULL, NULL, '2020-06-06 11:29:36', '2020-07-20 15:18:41', NULL, 93),
(23, 'STRESS', NULL, 5, 'federation-of-holistic-therapists-fht', 1, 1, NULL, NULL, '2020-06-08 11:50:09', '2020-07-20 14:57:25', NULL, NULL),
(24, 'DEPRESSION', NULL, 5, 'the-association-of-cognitive-analytic-therapy-acat', 1, 1, NULL, NULL, '2020-06-08 11:50:41', '2020-07-20 14:57:09', NULL, NULL),
(25, 'ANXIETY', NULL, 5, 'association-of-christian-counsellors-acc', 1, 1, NULL, NULL, '2020-06-08 11:51:26', '2020-07-20 14:54:36', NULL, NULL),
(26, 'PARENTING', NULL, 4, 'aedp', 1, 1, NULL, NULL, '2020-06-08 12:14:06', '2020-07-20 15:18:23', NULL, NULL),
(27, 'ANGER MANAGEMENT', NULL, 6, 'anxiety', 1, 1, NULL, NULL, '2020-06-08 12:19:39', '2020-07-20 15:20:50', NULL, NULL),
(28, 'ADDICTION', NULL, 6, 'depression', 1, 1, NULL, NULL, '2020-06-08 12:20:04', '2020-07-20 15:20:33', NULL, NULL),
(29, 'ADHD', NULL, 6, 'stress', 1, 1, NULL, NULL, '2020-06-08 12:20:32', '2020-07-20 15:20:15', NULL, NULL),
(30, 'RELATIONSHIPS', NULL, 6, 'relationships', 1, 1, NULL, NULL, '2020-06-08 12:21:05', '2020-07-20 15:19:56', NULL, NULL),
(31, 'DIVORCE', NULL, 8, 'divorce', 1, NULL, NULL, NULL, '2020-07-20 15:02:27', '2020-07-20 15:02:27', NULL, NULL),
(32, 'DOMESTIC ISSUE', NULL, 8, 'domestic-issue', 1, NULL, NULL, NULL, '2020-07-20 15:02:40', '2020-07-20 15:02:40', NULL, NULL),
(33, 'SELF ESTEEM', NULL, 2, 'self-esteem', 1, NULL, NULL, NULL, '2020-07-20 15:17:27', '2020-07-20 15:17:27', NULL, NULL),
(34, 'MEN\'S  ISSUES', NULL, 4, 'mens-issues', 1, NULL, NULL, NULL, '2020-07-20 15:19:00', '2020-07-20 15:19:00', NULL, NULL),
(35, 'ASPERGER\'S SYNDROME', NULL, 6, 'aspergers-syndrome', 1, NULL, NULL, NULL, '2020-07-20 15:21:24', '2020-07-20 15:21:24', NULL, NULL),
(36, 'DEPRESSION', NULL, 6, 'depression-1', 1, NULL, NULL, NULL, '2020-07-20 15:22:09', '2020-07-20 15:22:09', NULL, NULL),
(37, 'STRESS', NULL, 6, 'stress-1', 1, NULL, NULL, NULL, '2020-07-20 15:22:23', '2020-07-20 15:22:23', NULL, NULL),
(38, 'ANXIETY', NULL, 6, 'anxiety-1', 1, NULL, NULL, NULL, '2020-07-20 15:22:40', '2020-07-20 15:22:40', NULL, NULL),
(39, 'DRUG ABUSE', NULL, 7, 'drug-abuse', 1, NULL, NULL, NULL, '2020-07-24 12:58:31', '2020-07-24 12:58:31', NULL, NULL),
(40, 'DRUG ABUSE', NULL, 11, 'drug-abuse-1', 1, NULL, NULL, NULL, '2020-07-24 13:00:29', '2020-07-24 13:00:29', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bravo_terms_translations`
--

CREATE TABLE `bravo_terms_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bravo_tours`
--

CREATE TABLE `bravo_tours` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `image_id` int(11) DEFAULT NULL,
  `banner_image_id` int(11) DEFAULT NULL,
  `short_desc` text COLLATE utf8mb4_unicode_ci,
  `category_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map_lat` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map_lng` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map_zoom` int(11) DEFAULT NULL,
  `is_featured` tinyint(4) DEFAULT NULL,
  `gallery` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(12,2) DEFAULT NULL,
  `sale_price` decimal(12,2) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `min_people` int(11) DEFAULT NULL,
  `max_people` int(11) DEFAULT NULL,
  `faqs` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `lang` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `default_state` tinyint(4) DEFAULT '1',
  `enable_sale_of_first_booking` tinyint(4) DEFAULT '0',
  `highlights` text COLLATE utf8mb4_unicode_ci,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_for_4_slot` smallint(6) DEFAULT NULL,
  `discount_for_10_slot` smallint(6) DEFAULT NULL,
  `bank_details` text COLLATE utf8mb4_unicode_ci,
  `introductions_ownership_policy` smallint(6) DEFAULT NULL,
  `age` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faqs2` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bravo_tours`
--

INSERT INTO `bravo_tours` (`id`, `title`, `slug`, `content`, `image_id`, `banner_image_id`, `short_desc`, `category_id`, `location_id`, `address`, `map_lat`, `map_lng`, `map_zoom`, `is_featured`, `gallery`, `video`, `price`, `sale_price`, `duration`, `min_people`, `max_people`, `faqs`, `status`, `publish_date`, `create_user`, `update_user`, `deleted_at`, `origin_id`, `lang`, `created_at`, `updated_at`, `default_state`, `enable_sale_of_first_booking`, `highlights`, `country`, `city`, `discount_for_4_slot`, `discount_for_10_slot`, `bank_details`, `introductions_ownership_policy`, `age`, `faqs2`) VALUES
(17, 'System Admin', 'system-admin', NULL, 348, NULL, NULL, NULL, NULL, 'Coventry, UK', '52.406822', '-1.519693', 10, NULL, NULL, NULL, '144.00', NULL, NULL, 0, 0, NULL, 'draft', NULL, 1, 1, NULL, NULL, NULL, '2019-10-02 21:20:59', '2020-01-30 11:14:46', 1, 1, '{\"3\":{\"content\":\"Sed at commodo est. In condimentum suscipit\"},\"4\":{\"content\":\"Sed orci tortor, laoreet in iaculis tempor\"}}', 'United Kingdom', 'England', NULL, NULL, '{\"first_name\":\"1\",\"last_name\":\"1\",\"account_number\":\"1\",\"short_code\":\"1\"}', 1, NULL, NULL),
(18, 'Sarah Adams', 'sarah-adams', NULL, 125, NULL, NULL, NULL, NULL, '29 Albert Road, Evesham, UK', NULL, '-1.9503674999999703', 15, NULL, NULL, NULL, '130.00', NULL, NULL, 0, 0, NULL, 'draft', NULL, 2, 18, '2019-11-20 03:26:31', NULL, NULL, '2019-10-02 21:20:59', '2019-11-20 03:26:31', 1, 0, NULL, 'United Kingdom', 'Evesham', NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', NULL, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":null},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":null},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":null},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":null},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(19, 'Elise Aarohi', 'elise-aarohi', NULL, 124, NULL, NULL, NULL, NULL, '22 Cunard Close, Manchester, UK', NULL, '-2.2244075999999495', 15, NULL, NULL, NULL, '100.00', NULL, NULL, 0, 0, NULL, 'draft', NULL, 4, 18, '2019-11-20 03:26:31', NULL, NULL, '2019-10-02 21:20:59', '2019-11-20 03:26:31', 1, 0, NULL, 'United Kingdom', 'Manchester', NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', NULL, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":null},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":null},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":null},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":null},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(20, 'Kaytlyn Alvapriya', 'kaytlyn-alvapriya', NULL, 122, NULL, NULL, NULL, NULL, '27 Beach Road, Birmingham, UK', NULL, '-1.8708179999999857', 15, NULL, NULL, NULL, '151.00', NULL, NULL, 0, 0, NULL, 'draft', NULL, 5, 18, '2019-11-20 03:26:31', NULL, NULL, '2019-10-02 21:21:00', '2019-11-20 03:26:31', 1, 0, NULL, 'United Kingdom', 'Birmingham', NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', NULL, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":null},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":null},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":null},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":null},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(21, 'Lynne Victoria', 'lynne-victoria', NULL, 125, NULL, NULL, NULL, NULL, 'Wilmslow Town Council, Cliff Road, Wilmslow, UK', NULL, '-2.231435000000033', 15, 1, NULL, NULL, '101.00', NULL, NULL, 0, 0, NULL, 'draft', NULL, 6, 18, '2019-11-20 03:26:47', NULL, NULL, '2019-10-02 21:21:00', '2019-11-20 03:26:47', 1, 0, NULL, 'United Kingdom', 'Wilmslow', NULL, NULL, '{\"first_name\":\"Lorem\",\"last_name\":\"ipsum\",\"account_number\":\"12345678\",\"short_code\":\"123456\"}', NULL, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":null},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":null},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":null},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":null},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(22, 'Andy Davis', 'andy-davis', '<p>xabixbqxa</p>', NULL, NULL, NULL, NULL, NULL, '139 Southbank Road, Coventry, UK', '52.4178363', '-1.5411182999999937', 15, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'draft', NULL, 18, 18, '2019-11-20 03:26:47', NULL, NULL, '2019-10-09 06:15:29', '2019-11-20 03:26:47', 1, NULL, NULL, 'United Kingdom', 'Coventry', NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', 1, NULL, ''),
(23, 'Andy Test', 'andy-test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 20, NULL, '2019-11-16 02:48:15', NULL, NULL, '2019-10-18 10:10:15', '2019-11-16 02:48:15', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(24, 'james doyle', 'james-doyle', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 21, NULL, '2019-11-08 05:10:49', NULL, NULL, '2019-10-19 12:17:11', '2019-11-08 05:10:49', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(25, 'joe Hassett', 'joe-hassett', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'draft', NULL, 25, 1, '2019-11-07 14:06:42', NULL, NULL, '2019-11-07 11:28:53', '2019-11-07 14:06:42', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(26, 'joe hassett', 'joe-hassett-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'draft', NULL, 25, 1, '2019-11-07 14:03:15', NULL, NULL, '2019-11-07 11:31:51', '2019-11-07 14:03:15', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(27, 'joe Hassett', 'joe-hassett-2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 28, NULL, '2019-11-07 12:14:23', NULL, NULL, '2019-11-07 11:38:06', '2019-11-07 12:14:23', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(28, 'ryan Kenny', 'ryan-kenny', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'draft', NULL, 29, 25, '2019-11-16 02:48:15', NULL, NULL, '2019-11-07 11:46:07', '2019-11-16 02:48:15', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(29, 'Joe Hassett', 'joe-hassett-3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 25, NULL, '2019-11-07 14:06:48', NULL, NULL, '2019-11-07 11:58:06', '2019-11-07 14:06:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(30, 'Vendor Test Test', 'vendor-test-test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 30, NULL, '2019-11-20 03:26:47', NULL, NULL, '2019-11-08 04:28:20', '2019-11-20 03:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(31, 'Joe Hassett', 'joe-hassett', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 25, NULL, '2019-11-16 02:48:15', NULL, NULL, '2019-11-08 06:45:17', '2019-11-16 02:48:15', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(32, 'James Doyle', 'james-doyle', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 32, NULL, '2019-11-20 03:26:47', NULL, NULL, '2019-11-11 12:02:03', '2019-11-20 03:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(33, 'Andrew Davis', 'andrew-davis', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 33, NULL, '2019-11-16 09:30:00', NULL, NULL, '2019-11-11 13:47:10', '2019-11-16 09:30:00', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(34, 'Jess Bolton', 'jess-bolton', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 34, NULL, '2019-11-15 10:02:25', NULL, NULL, '2019-11-14 14:24:27', '2019-11-15 10:02:25', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(35, 'Jess Bolton', 'jess-bolton-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 35, NULL, '2019-11-15 05:06:55', NULL, NULL, '2019-11-14 14:26:02', '2019-11-15 05:06:55', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(36, 'Dung Trung', 'dung-trung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, NULL, NULL, '2019-11-15 05:06:55', NULL, NULL, '2019-11-15 02:30:30', '2019-11-15 05:06:55', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(37, 'Dung Trung', 'dung-trung-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 36, NULL, '2019-11-15 05:07:00', NULL, NULL, '2019-11-15 02:37:40', '2019-11-15 05:07:00', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(38, 'Dung Trung', 'dung-trung-2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 37, NULL, '2019-11-15 05:06:55', NULL, NULL, '2019-11-15 02:51:13', '2019-11-15 05:06:55', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(39, 'Dung Trung', 'dung-trung-3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 38, NULL, '2019-11-15 05:06:55', NULL, NULL, '2019-11-15 02:53:33', '2019-11-15 05:06:55', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(40, 'Dung Trung', 'dung-trung-4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 39, NULL, '2019-11-15 05:06:55', NULL, NULL, '2019-11-15 02:53:33', '2019-11-15 05:06:55', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(41, 'Test Kenny', 'test-kenny', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, NULL, NULL, '2019-11-15 10:02:25', NULL, NULL, '2019-11-15 05:13:55', '2019-11-15 10:02:25', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(42, 'Test Kenny', 'test-kenny-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 40, NULL, '2019-11-15 10:02:25', NULL, NULL, '2019-11-15 05:13:55', '2019-11-15 10:02:25', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(43, 'ryan kenny', 'ryan-kenny', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'draft', NULL, 1, 1, NULL, NULL, NULL, '2019-11-15 10:26:39', '2019-11-16 08:56:20', 1, 0, NULL, NULL, NULL, NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', NULL, NULL, ''),
(44, 'ryan kenny', 'ryan-kenny-1', NULL, 124, NULL, NULL, NULL, NULL, '15 Overslade Crescent, Coventry, UK', NULL, '-1.5392808000000286', 15, NULL, NULL, NULL, '1.00', NULL, NULL, 0, 0, NULL, 'draft', NULL, 41, 1, NULL, NULL, NULL, '2019-11-15 10:26:41', '2019-12-03 12:15:58', 1, 0, NULL, 'United Kingdom', 'England', NULL, NULL, '{\"first_name\":\"rya\",\"last_name\":\"kenny\",\"account_number\":\"12345567\",\"short_code\":\"123456\"}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"test\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"test\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"test\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"test\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(45, 'Andy Davis', 'andy-davis-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, '60.00', NULL, NULL, 0, 0, NULL, 'draft', NULL, 18, 18, NULL, NULL, NULL, '2019-11-15 13:04:55', '2019-11-22 07:16:52', 1, NULL, NULL, NULL, NULL, NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', NULL, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":null},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":null},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":null},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":null},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(46, 'Andy Davis', 'andy-davis-2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'draft', NULL, 42, 18, NULL, NULL, NULL, '2019-11-15 13:04:55', '2019-11-20 03:42:21', 1, 0, NULL, NULL, NULL, NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', NULL, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":null},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":null},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":null},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":null},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(47, 'Charlie test', 'charlie-test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'draft', NULL, 18, NULL, NULL, NULL, NULL, '2019-11-15 14:16:54', '2019-11-20 03:42:06', 1, 0, NULL, NULL, NULL, NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', NULL, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":null},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":null},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":null},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":null},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(48, 'Charlie test', 'charlie-test-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'draft', NULL, 43, 18, NULL, NULL, NULL, '2019-11-15 14:16:55', '2019-11-20 03:41:41', 1, 0, NULL, NULL, NULL, NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":null},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":null},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":null},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":null},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(49, ' ', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 1, NULL, '2019-12-16 08:15:13', NULL, NULL, '2019-11-16 03:18:41', '2019-12-16 08:15:13', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(50, ' ', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 44, NULL, '2019-12-16 08:15:12', NULL, NULL, '2019-11-16 03:18:41', '2019-12-16 08:15:12', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(51, ' ', '-2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-16 08:57:13', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(52, ' ', '-3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 45, NULL, '2019-12-16 08:15:12', NULL, NULL, '2019-11-16 08:57:14', '2019-12-16 08:15:12', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(53, 'Andy Test', 'andy-test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'draft', NULL, 20, 18, '2019-12-16 08:15:12', NULL, NULL, '2019-11-17 14:25:05', '2019-12-16 08:15:12', 1, 0, NULL, NULL, NULL, NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', NULL, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":null},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":null},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":null},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":null},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(54, ' ', '-4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-18 05:02:08', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(55, ' ', '-5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-18 05:03:24', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(56, ' ', '-6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-18 05:05:36', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(57, ' ', '-7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-18 05:07:22', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(58, ' ', '-8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-18 05:53:57', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(59, ' ', '-9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 1, NULL, '2019-12-16 08:15:12', NULL, NULL, '2019-11-18 06:07:56', '2019-12-16 08:15:12', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(60, ' ', '-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-18 07:45:20', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(61, ' ', '-11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 1, NULL, '2019-12-16 08:15:12', NULL, NULL, '2019-11-18 07:51:11', '2019-12-16 08:15:12', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(62, ' ', '-12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-18 07:55:43', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(63, ' ', '-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-18 07:57:46', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(64, ' ', '-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-18 08:00:09', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(65, ' ', '-15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-18 08:07:45', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(66, ' ', '-16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-18 08:13:54', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(67, ' ', '-17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-18 08:15:37', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(68, ' ', '-18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-18 08:27:49', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(69, ' ', '-19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 61, NULL, '2019-12-16 08:15:12', NULL, NULL, '2019-11-18 08:37:43', '2019-12-16 08:15:12', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(70, ' ', '-20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 62, NULL, '2020-01-26 05:36:48', NULL, NULL, '2019-11-18 08:38:48', '2020-01-26 05:36:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(71, 'John Smith', 'john-smith', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 57, 1, NULL, NULL, NULL, '2019-11-18 08:50:26', '2020-01-26 05:41:06', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(72, ' ', '21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 1, 1, NULL, NULL, NULL, '2019-11-18 09:01:56', '2020-01-26 05:39:15', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(73, ' ', '-22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 64, NULL, '2020-01-26 05:36:30', NULL, NULL, '2019-11-18 09:01:57', '2020-01-26 05:36:30', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(74, ' ', '-23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-18 09:03:32', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(75, 'Chris Hampson', '24', '<p style=\"padding: 0px; margin: 0px 0px 10px; caret-color: #878d94; color: #878d94; font-family: \'Open Sans\', Helvetica, Arial, Verdana, sans-serif; font-size: 14px;\">I am a warm friendly counsellor who is passionate about their profession.</p>\r\n<p style=\"padding: 0px; margin: 0px 0px 10px; caret-color: #878d94; color: #878d94; font-family: \'Open Sans\', Helvetica, Arial, Verdana, sans-serif; font-size: 14px;\">I studied counselling at Leeds University St. John specialising in relationship counselling. I have completed an externship in EFT and have counselled couples and individuals, as part of the second largest relationship counselling organisation in the UK, since 2010. During 2014 I started coaching professionally and I have recently been appointed as a shared lives carer, approved to work with vulnerable adults.</p>\r\n<p style=\"padding: 0px; margin: 0px; caret-color: #878d94; color: #878d94; font-family: \'Open Sans\', Helvetica, Arial, Verdana, sans-serif; font-size: 14px;\">I am a BACP member.</p>', 257, NULL, NULL, NULL, NULL, 'Leeds Train Station, New Station Street, Leeds, UK', NULL, '-1.5475810000000365', 15, NULL, NULL, NULL, '50.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 65, 65, NULL, NULL, NULL, '2019-11-18 09:03:33', '2020-01-15 08:25:33', 1, 1, NULL, NULL, 'England', NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', 1, NULL, NULL),
(76, ' ', '-25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-18 09:09:22', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(77, 'Joanne Jones', 'joanne-jones', '<p style=\"box-sizing: border-box; margin: 0px 0px 10px; font-family: Lato, arial, sans-serif; font-size: 17px;\">I am an experienced UKCP registered Integrative Psychotherapist and Counsellor, working in London. If you are reading this, I&rsquo;m guessing you are feeling something that is getting in the way of how you would like to be. Maybe you are sad, angry, anxious, low or struggling to like yourself. Maybe you are feeling ok, but you would like to feel better about yourself, or improve your relationship with other people in your life &ndash; such as a partner, family members, friends or colleagues at work.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; font-family: Lato, arial, sans-serif; font-size: 17px;\">My experience has convinced me that talking about your concerns with someone trained to hear them, and respond to them, in a particular way, can be very helpful. It won&rsquo;t &lsquo;cure&rsquo; you, or cancel out a painful past, but I am confident that it can help you to move on with greater ease, and even pave the way for further positive change.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; font-family: Lato, arial, sans-serif; font-size: 17px;\">As well as my private practice, I have worked in Higher Education counselling services, NHS psychotherapy services, and for a charitable counselling service, which means I have had the privilege of working with a number of presenting issues with people from many walks of life.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: Lato, arial, sans-serif; font-size: 17px;\">&nbsp;</p>', 351, NULL, NULL, NULL, NULL, 'UKCBC Holborn, Red Lion Street, London, UK', '51.5182564', '-0.1167808', 15, NULL, NULL, NULL, '60.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 66, 66, NULL, NULL, NULL, '2019-11-18 09:09:23', '2020-01-31 12:32:03', 1, 1, NULL, 'United Kingdom', 'England', NULL, NULL, '{\"first_name\":\"chris\",\"last_name\":\"hampson\",\"account_number\":\"201084\",\"short_code\":\"50840084\"}', 1, NULL, NULL),
(78, ' ', '-27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-18 09:11:00', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(79, ' ', '-28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 67, NULL, '2020-01-26 05:36:30', NULL, NULL, '2019-11-18 09:11:01', '2020-01-26 05:36:30', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(80, ' ', '-29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-18 09:29:49', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(81, ' ', '-30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 68, NULL, '2020-01-26 05:36:30', NULL, NULL, '2019-11-18 09:29:50', '2020-01-26 05:36:30', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(82, ' ', '31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 1, 1, NULL, NULL, NULL, '2019-11-18 09:32:33', '2020-01-26 05:39:15', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(83, ' ', '-32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 69, NULL, '2020-01-26 05:36:30', NULL, NULL, '2019-11-18 09:32:35', '2020-01-26 05:36:30', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(84, ' ', '33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 1, 1, NULL, NULL, NULL, '2019-11-18 09:40:20', '2020-01-26 05:39:15', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(85, ' ', '-34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 70, NULL, '2020-01-26 05:36:30', NULL, NULL, '2019-11-18 09:40:22', '2020-01-26 05:36:30', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(86, ' ', '35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 71, 1, NULL, NULL, NULL, '2019-11-18 12:09:13', '2020-01-26 05:39:15', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(87, ' ', '-36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 72, NULL, '2020-01-26 05:36:30', NULL, NULL, '2019-11-18 12:09:15', '2020-01-26 05:36:30', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(88, ' ', '-37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-18 12:16:14', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(89, ' ', '-38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 73, NULL, '2020-01-26 05:36:30', NULL, NULL, '2019-11-18 12:16:16', '2020-01-26 05:36:30', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(90, ' ', '39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 71, 1, NULL, NULL, NULL, '2019-11-18 12:48:30', '2020-01-26 05:39:15', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(91, 'Brenda Dawson', '40', NULL, 202, NULL, NULL, NULL, NULL, 'Leicester, UK', NULL, '-1.1397591999999576', 10, NULL, NULL, NULL, '80.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 74, 74, NULL, NULL, NULL, '2019-11-18 12:48:31', '2019-11-19 04:55:18', 1, NULL, NULL, 'United Kingdom', 'England', NULL, NULL, '{\"first_name\":\"Dawson\",\"last_name\":\"Breda\",\"account_number\":\"43099856\",\"short_code\":\"881645\"}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"To support people when they are feeling vulnerable.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"I am a Counselling Psychologist offering tailored psychological interventions based on your needs using different therapeutic approaches\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"As a psychologist, I work with individuals on a range of difficulties including depression, anxiety, stress, self harm, relationship difficulties and more. I utilise a variety of the therapeutic approaches to deliver psychological interventions that are tailored to your individual needs.\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"I strive to work collaboratively with my clients to identify difficulties and goals for change. The therapeutic relationship is core to my practice and my first goal is to provide clients with a safe space in which they can develop trust with me to explore their difficulties.\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"Cooking and running.\"},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":\"It all depends on the individual client and the severity of the concern. Sessions can range from one to whatever it takes\"},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":\"I am HCPC registered and a Chartered Member with the British Psychological Society. \\r\\nI adhere to standard\'s\\/personal development as set out by the HCPC and BPS.\"},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":\"Tony Robbins.\"},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"Active listening.\"},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":\"Relaxed and reassured.\"},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(92, ' ', '-41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 03:11:07', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(93, 'Diane lillis', 'diane-lillis', NULL, 201, NULL, NULL, NULL, NULL, '27 Clayton Drive, Birmingham B36 0AN, UK', NULL, '-1.7792610000000195', 15, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'publish', NULL, 75, 75, NULL, NULL, NULL, '2019-11-19 03:11:09', '2019-11-19 09:01:14', 1, NULL, NULL, 'United Kingdom', 'Birmingham', NULL, NULL, '{\"first_name\":\"Diane\",\"last_name\":\"Lillis\",\"account_number\":\"91454889\",\"short_code\":\"401625\"}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"I feel that it is a privilege to listen to people and try to understand what they think is the problem.  I worked with young people and found helping them find their way was satisfying.  Listening and connecting to people seemed to be natural to me. This skill is the most important part of therapy helping people to feel comfortable enough.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"I offer a combination of therapy using different models. \\r\\nMy strongest skill is developing a trusting working relationship.\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"My approach is integrative. \\r\\nThis includes CBT, Psychodynamic, Person Centred , TA, Sleep Assessment CBT,\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"People have reported that I am friendly and professional. I work at your pace to suit you or your budget. I will respect you and not judge you.\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"I am very creative and do many crafts. I love waking my dogs and go to the spa\\/gym.\"},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":\"Writing down what is bothering you can help before you go to therapy.\"},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":\"People can benefit from the first session.\"}]'),
(94, ' ', '-26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 06:27:48', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, ' ', '-40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 76, NULL, '2020-01-26 05:36:30', NULL, NULL, '2019-11-19 06:27:50', '2020-01-26 05:36:30', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(96, ' ', '-42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 09:51:53', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `bravo_tours` (`id`, `title`, `slug`, `content`, `image_id`, `banner_image_id`, `short_desc`, `category_id`, `location_id`, `address`, `map_lat`, `map_lng`, `map_zoom`, `is_featured`, `gallery`, `video`, `price`, `sale_price`, `duration`, `min_people`, `max_people`, `faqs`, `status`, `publish_date`, `create_user`, `update_user`, `deleted_at`, `origin_id`, `lang`, `created_at`, `updated_at`, `default_state`, `enable_sale_of_first_booking`, `highlights`, `country`, `city`, `discount_for_4_slot`, `discount_for_10_slot`, `bank_details`, `introductions_ownership_policy`, `age`, `faqs2`) VALUES
(97, 'Marie Asaolu', '43', '<p>Sometimes you may feel like you have lost your voice and who you are; you may feel deeply affected by life events such as separation, a traumatic event, the loss of someone close to you or the loss of something important to you.</p>\r\n<p>You wish to find freedom from those feelings and get yourself unstuck but you find that dealing with those emotions and day to day life is too difficult, maybe because events in your past are affecting your present. It could be that life is happening around you but you find yourself lost in the routine and want to find your dreams again...</p>\r\n<p>I offer a space where you can pause, reflect and be truly heard. I am sensitive to the needs of each individual and offer compassion, safety and no judgement; so that you can be free to express yourself without fear. My approach integrates different styles of therapy and together we can explore what will be most useful for your journey towards healing.</p>\r\n<p>I offer session in person and online (by email, chat or videocall). I am happy to offer you a 30 minutes phone conversation, so that we can discuss how we can work together. alternatively, we can do so by email. If you want to know more about me and how I work, you can find more information on my website: www.marieasa.co.uk</p>', 271, NULL, NULL, NULL, NULL, '75 Station Passage, London, UK', '51.592017', '0.028300299999955314', 15, NULL, NULL, NULL, '45.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 77, 77, NULL, NULL, NULL, '2019-11-19 09:51:55', '2019-12-30 11:57:03', 1, NULL, '[{\"content\":null}]', 'United Kingdom', 'London', NULL, NULL, '{\"first_name\":\"Marie\",\"last_name\":\"Asaolu\",\"account_number\":\"61721631\",\"short_code\":\"40-09-07\"}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"I have been working for over 10 years supporting people as a support worker and an advocate and wanted to be able to help people on a deeper level, not just with the practical things. \\r\\nI am passionate about offering a space where people can feel confortable to express their emotions and feelings and take the time to make sense of them.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"I have experiences working with different types of issues and I have a partcular interest working with people around current and past trauma as well as faith issue.\\r\\nIn my previous jobs, I have worked with people survivors of trafficking and domestic violence as well as women in prison, and this has help me to grow a better understanding on how trauma can impact life. I have also completed training sepcifically around working with people who have experienced traumatic event.\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"I work in an integrative way, which means that I integrate in my work diffrent types of therapy. I can use a more person centred approach or CBT as well as creative tools. I am also able to integrate Christian counselling if it is needed by the client.\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"I hope that client will feel safe and confortable when working with me. I also understand that life is busy and offer some flexibility with time and bookings of appoinment. I encourage feedback from client at the end of session so that they can let me know if they would have like the session to go differently: Too much talking? not enough? more practical excercise? more listening?\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"I have recently started to learn calligraphy, it helps me to relax and tecah me patience!\"},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":\"It is difficult to answer as it depends for each person. Most clients have told me that they start feeling better after a few sessions, although some sessions might be more difficult than others.\"},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":\"I am regsitered with the BACP and ACC and as such I have to do at least 25h of ongoing personal development a year. I tried to do training that will enhance my tools to help my clients.\"},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":\"I can\'t say that I have a particular role model, but I am inspired by resilience. I am inspired by some of the people I have worked with in the past and the strength that they have showed in the middle of adversity.\"},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"I think honesty, fairness and being non-judgemental make a good counsellor. I feel that showing understanding, listening and offering flexibility are also good skills to have.\"},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":\"During the session, client should feel safe and feel able to talk freely without fear of consequences. Some sessions, can feel challenging and upsetting, but client should always feel safe. \\r\\nAfter a session,  a client might feel emotional , a bit less anxious and a bit lighter but should not be feeling overwhelmed . I always ensure that the last 5 minutes of the session are about getting the client ready to go out. If needed, I will give them a bit more time.\"},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":\"I think my only advice would be to think about what they could do after the session. Maybe plan extra time after for a walk, a coffee or do something that make them feel good.\"},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(98, ' ', '-44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 10:14:36', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(99, ' ', '-45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 78, NULL, '2020-01-26 05:36:30', NULL, NULL, '2019-11-19 10:14:37', '2020-01-26 05:36:30', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(100, ' ', '-46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 10:15:23', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(101, 'Duncan Long', '47', '<p>Hello! A very warm welcome to you.</p>\r\n<p>Life is all about relationship in one form or another, how we relate to everything and everybody around us and of course, our relationship with ourselves. You may be struggling in relationships at home, with friends, lovers, work colleagues or maybe your relationship with drugs, alcohol or sex is becoming too conflicted and feels out of control. These are just a few examples of some areas that us humans can come up against in our lives.</p>\r\n<p>The therapeutic relationship is paramount to effective therapy, so it is important that you take some time and consideration to find a therapist that feels right for you. It&rsquo;s hard to know where to start sometimes, I know.</p>\r\n<p>&nbsp;</p>\r\n<p>My passion to become a therapist developed during my time in therapy, in which I wanted to understand self-destructive behaviours that were impacting on my life. I have been in practice for 7 years after and I am professionally supervised and insured counsellor based in Central London. I hold a space that is safe, non-judgemental, empathetic and compassionate. Our work together is collaborative with the goal of helping you move forward towards positive change in your life, to gain a clearer sense of self and to have a handle on your mental health.</p>\r\n<p>I have both a wide lived and professional experience, working with addiction, anxiety, childhood abuse, stress, burnout, loneliness, relationship issues and depression. I also work with other gay men with issues around sex and sexuality.</p>\r\n<p>Together we will look at your expectations. In our work together I will provide you with helpful insights and tools towards personal growth. Therapy is not a \"quick fix\" or a sticking plaster over a wound, but a space in which we can explore your patterns around self-destructive behaviours. Therapy helps us to develop self-compassion, to quieten our critical mind and to experience a calmer and more peaceful life. It is about learning to accept our human imperfections. Therapy can be challenging at times, but you will also have many moments of feeling a deep sense of relief and empowerment. Ultimately, the results show in the positive impacts on your home life, work life and relationships as you begin to take back control moving towards greater contentment.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', 204, NULL, NULL, NULL, NULL, 'Grosvenor Street, London W1K 3JN, UK', '51.5117911', '-0.1467631', 15, NULL, NULL, NULL, '95.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 79, 79, NULL, NULL, NULL, '2019-11-19 10:15:25', '2020-01-14 08:29:23', 1, NULL, NULL, 'United Kingdom', 'London', NULL, NULL, '{\"first_name\":\"Duncan\",\"last_name\":\"Long\",\"account_number\":\"71203169\",\"short_code\":\"40-17-53\"}', 1, NULL, NULL),
(102, ' ', '-48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 10:16:43', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(103, ' ', '49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 80, 1, NULL, NULL, NULL, '2019-11-19 10:16:45', '2020-01-26 05:38:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(104, ' ', '-50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 10:25:52', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(105, ' ', '51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 81, 1, NULL, NULL, NULL, '2019-11-19 10:25:53', '2020-01-26 05:38:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(106, ' ', '-52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 10:39:46', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(107, NULL, '53', NULL, 206, NULL, NULL, NULL, NULL, '109 Tamworth Road, Hertford SG13 7DN, UK', NULL, '-0.06150470000000041', 15, NULL, NULL, NULL, '50.00', NULL, NULL, 0, 0, NULL, 'draft', NULL, 82, 1, NULL, NULL, NULL, '2019-11-19 10:39:47', '2020-01-26 05:38:48', 1, NULL, NULL, 'United Kingdom', 'England', NULL, NULL, '{\"first_name\":\"Clare\",\"last_name\":\"Connolly\",\"account_number\":\"53261409\",\"short_code\":\"292037\"}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"I became a counsellor so I could help other people with MS but discovered that it translates into other theories as well.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"Chronic illness recovery counselling, bereavement counselling, addiction, anger management,\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"Person centred to begin with then can choose which approach is needed from my multi-module training.\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"An exploration to discover the right course to get you to where you need to be.\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"Gardening, painting, watching rugby, reading\"},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":\"Depends on the clients needs have done as little as 4 and up to 10\"},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":\"I continually retrain and look at new theories\"},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":\"Milton Ericson\"},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":\"My school art teacher taught be you can be creative in everything\"},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"Own experiences\"},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":\"Comfortable and relaxed\"},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":\"Be ready to explore your mind with me\"},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":\"I have had clients that saw change immediately but most would be within two or three\"}]'),
(108, ' ', '-54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 10:56:19', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(109, ' ', '55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 83, 1, NULL, NULL, NULL, '2019-11-19 10:56:20', '2020-01-26 05:38:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(110, ' ', '-56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 10:59:00', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(111, ' ', '57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 84, 1, NULL, NULL, NULL, '2019-11-19 10:59:02', '2020-01-26 05:38:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, ' ', '-58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 11:11:50', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(113, ' ', '59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 85, 1, NULL, NULL, NULL, '2019-11-19 11:11:51', '2020-01-26 05:38:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(114, ' ', '-60', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 11:12:01', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(115, 'Chris Lewis', 'chris-lewis', NULL, 207, NULL, NULL, NULL, NULL, '30 Moira Road, Ashby-de-la-Zouch, UK', NULL, '-1.4869072000000187', 15, NULL, NULL, NULL, '50.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 86, 86, NULL, NULL, NULL, '2019-11-19 11:12:02', '2019-11-20 12:30:06', 1, NULL, '[{\"content\":null}]', 'Great Britain', 'Ashby de la Zouch', NULL, NULL, '{\"first_name\":\"Christopher\",\"last_name\":\"Lewis\",\"account_number\":\"00183949\",\"short_code\":\"30-92-99\"}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"I have always enjoyed listening to and engaging with others.  I wanted to provide help for people struggling with many of the issues we face today such as anxiety, anger management, bereavement, relationship problems as well as issues of self-esteem and self worth.  I had done some voluntary work in this area so at the age of 55 motivated to have a change of career and wanting to work for myself I took the necessary steps over 4 years to achieve my registration as a psychotherapist with the UKCP and set up my own practice shortly afterwards.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"All mental health issues including anxiety, depression, bipolar disorder, personality disorders, panic attacks.  Relationship issues working with both individuals and couples.  Offering unconditional positive regard to my Client whatever state they present themselves in so as to help them share what is really going on for them and so get the maximum benefit out of our time together.\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"Non-directive style in accordance with the traditional personal-centred approach; I trained in the person-centred modality and have an MSC in person-centred and experiential psychotherapy.  Listening to what Clients say (and don\'t say), how they say it, watching their body language are all key.  I seek to build a strong therapeutic relationship from the outset as well as create a safe and confidential space with which to work in.\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"My undivided attention for 60 minutes once a week and the benefit of my professional expertise and experience.  I would expect my Clients to experience a warm and supportive environment and a flexibility from me to deal with whatever issue(s) they are currently facing and\\/or want to deal with.\\r\\nA Client can expect to work with me a registered and supervised professional.  I have been registered with the UKCP (Reg No 201116704) since qualification back in 2016.\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"I am a dog owner (2 Dalmatians) and enjoy long distance touring cycling.  I am also a member of a Musical Theatre Group and the Coordinator of our local Street Pastors.\"},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":\"From experience I find that usually by Session 4 we begin to see changes in the Client\'s way of dealing with the presenting problems \\/ issues.\"},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":\"I am in a peer support group and I have regular 1:1 supervision as well as group supervision.  I attend CPD training events aiming to get around 40 - 50 hours of CPD a year.  I read extensively, both articles from therapy journals and books.\"},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":\"Carl Rodgers (guess most person centred therapists will say that!)\"},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":\"Mentors at work in my previous career (Development Manager for Housing Associations) - they taught me to persevere, to think out of the box and to take the lead when called upon to do so.\"},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"Somebody who is warm and empathic and can build a good rapport with people.  Fundamentally somebody who likes people and is a good listener.  Someone older with some considerable experience of life.\"},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":\"Accepted during the session - knowing that they are being listened to and heard.  Afterwards, unburdened - hopefully feeling that things have begun to shift for them.\"},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":\"Whatever preparation is needed to arrive in a relative relaxed and calm state.  Sometimes it can be helpful to think through (and may be jot down) they key issues they want to discuss in the session.\"},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":\"This obviously depends to some extent on that the issue or difficulty they are facing is but I would expect to see some results after an initial 6 sessions.\"}]'),
(116, ' ', '-62', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 11:35:16', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(117, ' ', '63', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 87, 1, NULL, NULL, NULL, '2019-11-19 11:35:17', '2020-01-26 05:38:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, ' ', '-61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 12:31:06', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(119, ' ', '64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 88, 1, NULL, NULL, NULL, '2019-11-19 12:31:08', '2020-01-26 05:38:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(120, ' ', '-65', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 13:05:25', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(121, 'Sophia Malaspina', 'sophia-malaspina', NULL, 208, NULL, NULL, NULL, NULL, 'The Regency Practice, Clarence Street, Cheltenham, UK', NULL, '-2.0779188000000204', 15, NULL, NULL, NULL, '45.00', NULL, NULL, 0, 0, NULL, 'draft', NULL, 89, 18, NULL, NULL, NULL, '2019-11-19 13:05:27', '2019-11-19 15:02:29', 1, 0, '[{\"content\":null}]', 'United Kingdom', 'England', NULL, NULL, '{\"first_name\":\"Sophia\",\"last_name\":\"Malaspina\",\"account_number\":\"00062307\",\"short_code\":\"23-11-85\"}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"I had an interest and passion in the psychology  of how counselling works. Equally I wanted to assist and support people with personal growth.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"I offer understanding and empathy to clients in order for them to feel safe and secure in talking about their feelings or previous experiences.\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"I am intergrative, but work in a person-centred style using psychodynamic and CBT modalities when I deem it appropriate while taking from different approaches.\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"You can expect a safe space to explore difficult feelings and thoughts. I practice in quiet, private rooms and hope that you will feel comfortable there.\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(122, ' ', '-67', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 13:11:46', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(123, ' ', '-68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 90, NULL, '2020-01-26 05:37:50', NULL, NULL, '2019-11-19 13:11:47', '2020-01-26 05:37:50', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(124, ' ', '-69', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 13:26:43', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(125, 'Lauretta Wilson', '70', NULL, 209, NULL, NULL, NULL, NULL, '3 Lynwood Drive, Worcester Park, UK', NULL, '-0.24409000000002834', 15, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'publish', NULL, 91, 91, NULL, NULL, NULL, '2019-11-19 13:26:46', '2019-11-20 12:32:21', 1, NULL, NULL, 'United Kingdom', 'England', NULL, NULL, '{\"first_name\":\"Lauret\",\"last_name\":\"Wilson\",\"account_number\":\"90067902\",\"short_code\":\"400234\"}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"I want to offer people effective successful therapy that changes their life. Tools to manage better and understand themselves.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"Mental health such as anxiety depression mood disorders \\r\\n\\r\\nCouple counselling \\r\\n\\r\\nChildren \\r\\nAdults\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"Solution focused \\r\\n\\r\\nCbt\\r\\n\\r\\nNlp\\r\\n\\r\\nMindfulness \\r\\n\\r\\nClinical hypnotherapy\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"A sense of control \\r\\n\\r\\nTools that help from the first session and a sense of hope.\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"Movie \\r\\nDance \\r\\nMusic \\r\\nTheatre \\r\\nArt\"},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":\"1-6 sessions\"},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":\"Training \\r\\nReading \\r\\nInterviews \\r\\nPodcasts\"},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":\"My patients\"},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":\"My parents \\r\\nTolerance \\r\\nAcceptance \\r\\nLove\"},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"Good tools \\r\\nExperience \\r\\nEmpathy \\r\\nUnderstanding\"},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":\"Hope \\r\\nControl \\r\\nEmpowerment\"},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":\"No\"},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":\"1-6 sessions\"}]'),
(126, ' ', '-66', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 13:59:28', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(127, ' ', '-71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 92, NULL, '2020-01-26 05:37:50', NULL, NULL, '2019-11-19 13:59:59', '2020-01-26 05:37:50', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(128, ' ', '-72', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 93, NULL, '2020-01-26 05:37:50', NULL, NULL, '2019-11-19 14:00:01', '2020-01-26 05:37:50', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(129, ' ', '-73', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:47', NULL, NULL, '2019-11-19 15:00:15', '2019-12-07 02:26:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(130, 'Susan Iacobucci', '74', NULL, 212, NULL, NULL, NULL, NULL, '14 Basingstoke Road, Kingsclere, Newbury, UK', NULL, '-1.2369661000000178', 15, NULL, NULL, NULL, '45.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 94, 94, NULL, NULL, NULL, '2019-11-19 15:00:17', '2019-11-21 13:54:49', 1, NULL, NULL, 'United Kingdom', 'England', NULL, NULL, '{\"first_name\":\"Susan\",\"last_name\":\"Iacobucci\",\"account_number\":\"06536786\",\"short_code\":\"601335\"}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"I feel facing personal challenges has given me an understanding; empathy and insight into the struggles people face in life. I believe that counselling can; as a client said to me; \\u201c shine a light into the dark places and make them less scary\\u201d. Counselling gives the space for a person to be heard and through the therapeutic relationship bring about the changes they want to make in their lives - to be alongside someone during that process can be challenging, difficult but also extremely rewarding.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"I have several years working with loss; end of life ; grief and bereavement. I have also experience of working with teenagers and young people. Family and personal relationships; self esteem; sexual abuse.\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"I draw on person-centred: psychodynamic and transpersonal theories. I have a working knowledge of CBT.\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"I am led by a clients agenda and allow the work to unfold as often there are deeper issues that emerge from the presenting problem.  I am down to earth in my approach, I think there is a place for humour in the counselling room when appropriate!\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"I\\u2019ve joined a yoga class. Walking my dog. Music. Reading. Painting.\"},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":\"I think that very much depends on the issue a client brings. I have worked short term I.e 3- 12 weeks and clients have been able to gain insight and make changes in this timeframe. I have also worked longer term over 2 years.\"},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":\"I attend a meditation and discussion group when I can. I use an app for meditation.  I belong to a peer supervision group to share our thoughts and experiences from our work.\"},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":\"Anyone who has the courage to face their problems and overcome obstacles. The psychotherapist Irvin Yalom has written some great books, I admire his honesty.\"},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":\"My work colleague has shared her vast knowledge of working in palliative care and bereavement. She has also taught me how to communicate with sensitivity with volunteers.  The chaplains at work who have given me the opportunity to explore meaning, purpose and spirituality. My college tutors provided a safe and supportive space to learn and gain confidence when training to be a counsellor. My clinical supervisor for helping me trust and believe in myself when I\\u2019ve doubted my ability to be a counsellor.\"},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"A warm approachable human being, not a distant cool expert. Warmth, humour and empathy, humility. I don\\u2019t say non judgmental because I think it\\u2019s human to judge - but an openness to different people and their experiences. Clear boundaries.\"},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":\"I don\\u2019t think there\\u2019s a right or wrong way for a client to feel in a session, feelings will emerge.  However clients should feel safe and contained within a session - but this can take time. Sometimes clients working through painful experiences can feel worse after a session but ultimately I would hope feel lighter.\"},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":\"Perhaps think about what they\\u2019d like to bring to the session. Review how things have gone over the previous week.\"},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":\"As I mentioned earlier I think this very much depends on the issue - someone experiencing work related stress for example may see results in 4 -6 sessions. A client who has been abused may take a long time to trust the therapist.\"}]'),
(131, ' ', '-75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-19 18:53:09', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(132, ' ', '-76', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 95, NULL, '2020-01-26 05:37:50', NULL, NULL, '2019-11-19 18:53:11', '2020-01-26 05:37:50', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(133, ' ', '-77', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-19 19:02:28', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(134, ' ', '-78', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 96, NULL, '2020-01-26 05:37:50', NULL, NULL, '2019-11-19 19:02:30', '2020-01-26 05:37:50', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(135, ' ', '-79', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 96, NULL, '2020-01-26 05:37:50', NULL, NULL, '2019-11-19 20:37:42', '2020-01-26 05:37:50', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(136, ' ', '-80', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 97, NULL, '2020-01-26 05:37:50', NULL, NULL, '2019-11-19 20:37:45', '2020-01-26 05:37:50', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(137, ' ', '-81', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 01:54:42', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(138, ' ', '-82', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 98, NULL, '2020-01-26 05:37:50', NULL, NULL, '2019-11-20 01:54:45', '2020-01-26 05:37:50', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(139, 'Andy Davis', 'andy-davis', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'draft', NULL, 18, 18, NULL, NULL, NULL, '2019-11-20 03:36:21', '2019-11-20 03:38:47', 1, 0, NULL, NULL, NULL, NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', NULL, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":null},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":null},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":null},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":null},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(140, ' ', '-83', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 05:50:43', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(141, ' ', '84', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 100, 1, NULL, NULL, NULL, '2019-11-20 05:50:44', '2020-01-26 05:38:56', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(142, ' ', '-85', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 06:35:36', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(143, ' ', '86', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 102, 1, NULL, NULL, NULL, '2019-11-20 06:35:39', '2020-01-26 05:38:56', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(144, ' ', '-87', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 07:07:08', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(145, ' ', '88', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 103, 1, NULL, NULL, NULL, '2019-11-20 07:07:09', '2020-01-26 05:38:56', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(146, ' ', '-89', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 07:07:39', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(147, ' ', '90', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 104, 1, NULL, NULL, NULL, '2019-11-20 07:07:40', '2020-01-26 05:38:56', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(148, ' ', '-91', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 07:29:24', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(149, 'Rebecca Hepson', '92', NULL, 226, NULL, NULL, NULL, NULL, 'Studio 157, Embroidery Mill Paisley PA11TJ', NULL, NULL, 8, NULL, NULL, NULL, '40.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 106, 106, NULL, NULL, NULL, '2019-11-20 07:29:25', '2019-11-20 08:24:56', 1, NULL, NULL, 'United Kingdom', 'Paisley', NULL, NULL, '{\"first_name\":\"Rebecca\",\"last_name\":\"Hepson\",\"account_number\":\"00674336\",\"short_code\":\"831817\"}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"I became a counsellor because I am a naturally inteoverted person who enjoys listening to others and being an uplifter. I have the necessary skills and temperament to help other enhance their emotional, mental, physical and spiritual life and want to use those skills to facilitate others to enhance their lives in an ever positive way.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"Releasing trauma, raising self esteem, relationship counselling\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"Person Centred Approach\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"A safe, comfortable space where we can build upon and explore new and healthy ways of embracing life experiences and relating with others.\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"Outside of being a counsellor I am an avid painter. I also enjoy baking and making confectionery.\"},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":\"This is something that, in my experience is different for everyone. The time it takes to see results really depends on the complexity of your experience and the receptivity you have towards change. For some this may be a few sessions, for others it may take a few months. As long as you are moving forward in some way you are making a profound difference to your life. What really helps is to be as clear as possible in each session about what you need and we can explore how to get you there.\"},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":\"I meditate as a daily practice. This quietens my mind and allows me to be fully present with every individual that comes through my door.\\r\\nI have actively practiced unpacking stereotypes and narratives about different social groups  in order to really sit with the person right in front of me.\\r\\nI undertake regular supervision, which enables me to process my experience as a counsellor and explore possible techniques that can help with clients in future sessions.\\r\\nI regularly read books and watch seminars related to humanistic counselling theory and attend training sessions.\"},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":\"Maya Angelou, Oprah Winfrey, Eckhart Tolle, Abraham(Esther) Hicks, Tara Brach, Brene Brown, Louise Hay, Dalai Llama, Ross Rosenberg, Lisa A. Romano, Craig Kenneth, Margaret Foley\"},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":\"My Mum taught me how to love unconditionally.\\r\\nMy Dad taught me to always embrace creativity.\\r\\nMy Counselling lecturers collectively taught me how to embrace who I am, facilitate groups, utilise counselling skills with clients, organise my time wisely, how to speak up for what I think and believe, honour my experience, various humanistic counselling theories and therapies.\\r\\n\\r\\nMy placement manager taught me that my soft temperament can be extremely helpful when working with vulnerable groups. She taught me about boundaries and organising my caseload, self care and putting myself out there.\\r\\nMy personal counsellors taught me how to embrace my past and integrate in into the present, to accept myself and other for who we are, even when it\'s difficult.\"},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"I am a soft spoken and natured individual who has a great amount of time and patience for myself and others. I really enjoy talking with others at a deeper level and naturally embrace this.  I feel that these qualities allow the clients that I work with to feel listened to and appreciated fpr who they are and how they want to expand.\"},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":\"During and after each session the client should feel however they choose to, there is no right or wrong way to feel.\"},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":\"It is not necessary to prepare fpr a session. However, acknowledge as much as they can how they have been feeling and how they feel when they get there is helpful.\"},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":\"It depends on the complexity of their situation and their committment to change. There is no set amount of time.\"}]'),
(150, ' ', '-93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 07:55:05', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `bravo_tours` (`id`, `title`, `slug`, `content`, `image_id`, `banner_image_id`, `short_desc`, `category_id`, `location_id`, `address`, `map_lat`, `map_lng`, `map_zoom`, `is_featured`, `gallery`, `video`, `price`, `sale_price`, `duration`, `min_people`, `max_people`, `faqs`, `status`, `publish_date`, `create_user`, `update_user`, `deleted_at`, `origin_id`, `lang`, `created_at`, `updated_at`, `default_state`, `enable_sale_of_first_booking`, `highlights`, `country`, `city`, `discount_for_4_slot`, `discount_for_10_slot`, `bank_details`, `introductions_ownership_policy`, `age`, `faqs2`) VALUES
(151, 'Michael Leunig', 'michael-leunig', NULL, 250, NULL, NULL, NULL, NULL, 'Office 104, The Chambers Business Centre, Oldham, OL8 4QQ', NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'publish', NULL, 107, 1, NULL, NULL, NULL, '2019-11-20 07:55:07', '2019-12-03 05:14:15', 1, 0, NULL, 'Great Britain', 'Oldham', NULL, NULL, '{\"first_name\":\"Michael\",\"last_name\":\"Leunig\",\"account_number\":\"37822468\",\"short_code\":\"77-19-38\"}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"Due to my life experiences, my journey, I would like to help as many people as I am able to. I would like to help them move on in their lives, so that they can find some peace. There are many people who are stuck, or have the feeling of being stuck. I was that person too and i would like to share my knowledge and experience with you. Life is for living.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"I began my journey as a Psychotherapist, however my strength lay in Clinical Hypnotherapy, Emotional Freedom Technique (EFT), and Mindfulness. I am also a Tutor and teach Hypnotherapy and Mindfulness. I also run Inner Child workshops\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"I believe that you are the expert in your life. I am here to help you achieve your potential. we can achieve this in several different ways, together.\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"I am transparent, there is nothing hidden. I offer a warm welcoming space where you can feel safe, whilst exploring that which is keeping you stuck, that which is holding you back from reaching your full potential.\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"I love my allotment and I spend several hours a week there. I also love to read and listen to music. Spending time with my wife and grandchildren is fantastic and I love that quality time with them. Mostly, I love to teach and that takes up most weekends.\"},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":\"Well, that really depends on the service user. Clients have had good results within 4 to 5 sessions, and yet some have needed a little bit more. Smoking Cessation is usual one, two hour session.\"},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":\"Well I have just completed a course on Emotional Freedom Technique and I have a Reike course planned for January. I am always looking at ways to improve what I know and discovering new ways to help clients, this is my passion.\"},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":\"Service users, always.\"},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":\"Brian Glenn, he has taught me that anything is possible and that you can achieve anything you want if you want to. The quality of your life is only as good as the quality of your thoughts.\"},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"Empathy, good listening skills for what the clients is saying and what the client isn\'t saying. Having Unconditional positive regard for all people. Transparency and congruence.\"},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":\"I do not like the word \'should\'. I would like the client to feel safe, listened to, and empowered.\"},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":\"This depends on the therapy. If it is smoking Cessation, then I would like the client to not smoke for up to 12 hours before they see me. This will show me that they are committed and also it will greatly help the therapy.\"},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":\"This depends on the therapy. If it is smoking cessation then the results is immediate. The results can be very quick and profound for Emotional Freedom Technique too. Counselling can take a little longer, however Hypnotherapy can take less time.\"}]'),
(152, ' ', '-95', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 08:04:43', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(153, ' ', '-96', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 108, NULL, '2020-01-26 05:37:35', NULL, NULL, '2019-11-20 08:04:45', '2020-01-26 05:37:35', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(154, ' ', '-97', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 08:06:49', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(155, ' ', '-98', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 109, NULL, '2020-01-26 05:37:35', NULL, NULL, '2019-11-20 08:06:50', '2020-01-26 05:37:35', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(156, ' ', '-92', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 08:50:34', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(157, ' ', '-99', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 110, NULL, '2020-01-26 05:37:35', NULL, NULL, '2019-11-20 08:50:36', '2020-01-26 05:37:35', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(158, ' ', '-100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 09:08:49', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(159, ' ', '-101', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 111, NULL, '2020-01-26 05:37:35', NULL, NULL, '2019-11-20 09:08:51', '2020-01-26 05:37:35', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(160, ' ', '-102', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 10:16:54', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(161, ' ', '-103', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 112, NULL, '2020-01-26 05:37:35', NULL, NULL, '2019-11-20 10:16:56', '2020-01-26 05:37:35', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(162, ' ', '-104', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 10:33:09', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `bravo_tours` (`id`, `title`, `slug`, `content`, `image_id`, `banner_image_id`, `short_desc`, `category_id`, `location_id`, `address`, `map_lat`, `map_lng`, `map_zoom`, `is_featured`, `gallery`, `video`, `price`, `sale_price`, `duration`, `min_people`, `max_people`, `faqs`, `status`, `publish_date`, `create_user`, `update_user`, `deleted_at`, `origin_id`, `lang`, `created_at`, `updated_at`, `default_state`, `enable_sale_of_first_booking`, `highlights`, `country`, `city`, `discount_for_4_slot`, `discount_for_10_slot`, `bank_details`, `introductions_ownership_policy`, `age`, `faqs2`) VALUES
(163, 'Tim McLoughlin', '105', '<p><!-- [if gte mso 9]><xml>\r\n <o:OfficeDocumentSettings>\r\n  <o:AllowPNG/>\r\n </o:OfficeDocumentSettings>\r\n</xml><![endif]--><!-- [if gte mso 9]><xml>\r\n <w:WordDocument>\r\n  <w:View>Normal</w:View>\r\n  <w:Zoom>0</w:Zoom>\r\n  <w:TrackMoves/>\r\n  <w:TrackFormatting/>\r\n  <w:PunctuationKerning/>\r\n  <w:ValidateAgainstSchemas/>\r\n  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>\r\n  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>\r\n  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>\r\n  <w:DoNotPromoteQF/>\r\n  <w:LidThemeOther>EN-GB</w:LidThemeOther>\r\n  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>\r\n  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>\r\n  <w:Compatibility>\r\n   <w:BreakWrappedTables/>\r\n   <w:SnapToGridInCell/>\r\n   <w:WrapTextWithPunct/>\r\n   <w:UseAsianBreakRules/>\r\n   <w:DontGrowAutofit/>\r\n   <w:SplitPgBreakAndParaMark/>\r\n   <w:EnableOpenTypeKerning/>\r\n   <w:DontFlipMirrorIndents/>\r\n   <w:OverrideTableStyleHps/>\r\n  </w:Compatibility>\r\n  <m:mathPr>\r\n   <m:mathFont m:val=\"Cambria Math\"/>\r\n   <m:brkBin m:val=\"before\"/>\r\n   <m:brkBinSub m:val=\"&#45;-\"/>\r\n   <m:smallFrac m:val=\"off\"/>\r\n   <m:dispDef/>\r\n   <m:lMargin m:val=\"0\"/>\r\n   <m:rMargin m:val=\"0\"/>\r\n   <m:defJc m:val=\"centerGroup\"/>\r\n   <m:wrapIndent m:val=\"1440\"/>\r\n   <m:intLim m:val=\"subSup\"/>\r\n   <m:naryLim m:val=\"undOvr\"/>\r\n  </m:mathPr></w:WordDocument>\r\n</xml><![endif]--><!-- [if gte mso 9]><xml>\r\n <w:LatentStyles DefLockedState=\"false\" DefUnhideWhenUsed=\"false\"\r\n  DefSemiHidden=\"false\" DefQFormat=\"false\" DefPriority=\"99\"\r\n  LatentStyleCount=\"376\">\r\n  <w:LsdException Locked=\"false\" Priority=\"0\" QFormat=\"true\" Name=\"Normal\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 7\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 8\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 9\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 4\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 5\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 6\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 7\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 8\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 9\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 7\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 8\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 9\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Normal Indent\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"footnote text\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"annotation text\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"header\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"footer\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index heading\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"35\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"caption\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"table of figures\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"envelope address\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"envelope return\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"footnote reference\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"annotation reference\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"line number\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"page number\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"endnote reference\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"endnote text\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"table of authorities\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"macro\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"toa heading\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Bullet\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Number\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List 4\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List 5\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Bullet 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Bullet 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Bullet 4\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Bullet 5\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Number 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Number 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Number 4\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Number 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"10\" QFormat=\"true\" Name=\"Title\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Closing\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Signature\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"1\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"Default Paragraph Font\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text Indent\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Continue\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Continue 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Continue 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Continue 4\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Continue 5\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Message Header\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"11\" QFormat=\"true\" Name=\"Subtitle\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Salutation\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Date\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text First Indent\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text First Indent 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Note Heading\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text Indent 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text Indent 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Block Text\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Hyperlink\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"FollowedHyperlink\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"22\" QFormat=\"true\" Name=\"Strong\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"20\" QFormat=\"true\" Name=\"Emphasis\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Document Map\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Plain Text\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"E-mail Signature\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Top of Form\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Bottom of Form\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Normal (Web)\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Acronym\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Address\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Cite\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Code\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Definition\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Keyboard\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Preformatted\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Sample\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Typewriter\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Variable\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Normal Table\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"annotation subject\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"No List\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Outline List 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Outline List 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Outline List 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Simple 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Simple 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Simple 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Classic 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Classic 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Classic 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Classic 4\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Colorful 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Colorful 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Colorful 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Columns 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Columns 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Columns 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Columns 4\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Columns 5\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 4\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 5\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 6\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 7\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 8\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 4\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 5\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 6\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 7\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 8\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table 3D effects 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table 3D effects 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table 3D effects 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Contemporary\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Elegant\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Professional\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Subtle 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Subtle 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Web 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Web 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Web 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Balloon Text\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"Table Grid\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Theme\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" Name=\"Placeholder Text\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"1\" QFormat=\"true\" Name=\"No Spacing\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" Name=\"Revision\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"34\" QFormat=\"true\"\r\n   Name=\"List Paragraph\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"29\" QFormat=\"true\" Name=\"Quote\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"30\" QFormat=\"true\"\r\n   Name=\"Intense Quote\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"19\" QFormat=\"true\"\r\n   Name=\"Subtle Emphasis\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"21\" QFormat=\"true\"\r\n   Name=\"Intense Emphasis\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"31\" QFormat=\"true\"\r\n   Name=\"Subtle Reference\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"32\" QFormat=\"true\"\r\n   Name=\"Intense Reference\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"33\" QFormat=\"true\" Name=\"Book Title\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"37\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"Bibliography\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"TOC Heading\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"41\" Name=\"Plain Table 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"42\" Name=\"Plain Table 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"43\" Name=\"Plain Table 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"44\" Name=\"Plain Table 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"45\" Name=\"Plain Table 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"40\" Name=\"Grid Table Light\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\" Name=\"Grid Table 1 Light\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\" Name=\"Grid Table 6 Colorful\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\" Name=\"Grid Table 7 Colorful\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"Grid Table 1 Light Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"Grid Table 6 Colorful Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"Grid Table 7 Colorful Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"Grid Table 1 Light Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"Grid Table 6 Colorful Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"Grid Table 7 Colorful Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"Grid Table 1 Light Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"Grid Table 6 Colorful Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"Grid Table 7 Colorful Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"Grid Table 1 Light Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"Grid Table 6 Colorful Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"Grid Table 7 Colorful Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"Grid Table 1 Light Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"Grid Table 6 Colorful Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"Grid Table 7 Colorful Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"Grid Table 1 Light Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"Grid Table 6 Colorful Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"Grid Table 7 Colorful Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\" Name=\"List Table 1 Light\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\" Name=\"List Table 6 Colorful\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\" Name=\"List Table 7 Colorful\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"List Table 1 Light Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"List Table 6 Colorful Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"List Table 7 Colorful Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"List Table 1 Light Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"List Table 6 Colorful Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"List Table 7 Colorful Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"List Table 1 Light Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"List Table 6 Colorful Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"List Table 7 Colorful Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"List Table 1 Light Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"List Table 6 Colorful Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"List Table 7 Colorful Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"List Table 1 Light Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"List Table 6 Colorful Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"List Table 7 Colorful Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"List Table 1 Light Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"List Table 6 Colorful Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"List Table 7 Colorful Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Mention\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Smart Hyperlink\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Hashtag\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Unresolved Mention\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Smart Link\"/>\r\n </w:LatentStyles>\r\n</xml><![endif]--><!-- [if gte mso 10]>\r\n<style>\r\n /* Style Definitions */\r\n table.MsoNormalTable\r\n	{mso-style-name:\"Table Normal\";\r\n	mso-tstyle-rowband-size:0;\r\n	mso-tstyle-colband-size:0;\r\n	mso-style-noshow:yes;\r\n	mso-style-priority:99;\r\n	mso-style-parent:\"\";\r\n	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;\r\n	mso-para-margin-top:0cm;\r\n	mso-para-margin-right:0cm;\r\n	mso-para-margin-bottom:8.0pt;\r\n	mso-para-margin-left:0cm;\r\n	line-height:107%;\r\n	mso-pagination:widow-orphan;\r\n	font-size:11.0pt;\r\n	font-family:\"Calibri\",sans-serif;\r\n	mso-ascii-font-family:Calibri;\r\n	mso-ascii-theme-font:minor-latin;\r\n	mso-hansi-font-family:Calibri;\r\n	mso-hansi-theme-font:minor-latin;\r\n	mso-bidi-font-family:\"Times New Roman\";\r\n	mso-bidi-theme-font:minor-bidi;\r\n	mso-fareast-language:EN-US;}\r\n</style>\r\n<![endif]--></p>\r\n<p class=\"MsoNormal\">My name is Tim McLoughlin<span style=\"mso-spacerun: yes;\">&nbsp; </span>and I am a practicing counsellor and therapist<span style=\"mso-spacerun: yes;\">&nbsp; </span>in<span style=\"mso-spacerun: yes;\">&nbsp; </span>High Wycombe, Amersham and Beaconsfield.<span style=\"mso-spacerun: yes;\">&nbsp; </span>I provide counselling and psychotherapy in a safe, confidential and non-judgemental environment. I am a Registered Member of the British Association for Counselling and Psychotherapy. I have practice rooms in High Wycombe and Amersham where I am also able to provide outdoor therapy alongside more traditional approaches.</p>\r\n<p class=\"MsoNormal\">&nbsp;</p>\r\n<p class=\"MsoNormal\">Over the years, I have worked with individuals<span style=\"mso-spacerun: yes;\">&nbsp; </span>who have experienced difficulties with:</p>\r\n<p class=\"MsoNormal\">&nbsp;</p>\r\n<p class=\"MsoNormal\">Stress</p>\r\n<p class=\"MsoNormal\">Anger Management</p>\r\n<p class=\"MsoNormal\">Redundancy</p>\r\n<p class=\"MsoNormal\">Depression</p>\r\n<p class=\"MsoNormal\">Addictions</p>\r\n<p class=\"MsoNormal\">Relationships</p>\r\n<p class=\"MsoNormal\">Fertility</p>\r\n<p class=\"MsoNormal\">Anxiety</p>\r\n<p class=\"MsoNormal\">Panic Attacks</p>\r\n<p class=\"MsoNormal\">Bereavement</p>\r\n<p class=\"MsoNormal\">&nbsp;</p>\r\n<p class=\"MsoNormal\">Being a therapist or counsellor is about understanding your particular needs, dilemmas and situation.</p>\r\n<p class=\"MsoNormal\">&nbsp;For many, using a counsellor or therapist would only be considered in times of real crisis but I think it is more useful to see therapy as a real chance to explore your situation, the problems you are facing and to make a positive lifelong change.<span style=\"mso-spacerun: yes;\">&nbsp; </span>I believe that judicious use of talking therapies can<span style=\"mso-spacerun: yes;\">&nbsp; </span>offer ways of developing life-changing insights and behaviours, which in-turn lead to a more balanced approach to life.</p>', 230, NULL, NULL, NULL, NULL, 'Mill Lane, Little Missenden. Amersham.', NULL, NULL, 8, NULL, NULL, NULL, '45.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 113, 113, NULL, NULL, NULL, '2019-11-20 10:33:11', '2019-12-07 13:30:27', 1, NULL, NULL, 'United Kingdom', 'Amersham', NULL, NULL, '{\"first_name\":\"Tim\",\"last_name\":\"McLoughlin\",\"account_number\":\"82742950\",\"short_code\":\"400919\"}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"I wanted to make a positive difference to people\'s lives. I think counsellig provides a great opportunity tp take \'time out\' from our busy lives to focus on improving the quality of life itself.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"I am a good listener with a wide life experience. I have worked across a wide range of professional situations including a number of years in a hospice setting.\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"Essentially, I am person-centred though my training was from a psychodynamic orientation. As such, I refer to myself  as integrative.\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"A regular focussed session in a natural space. Time to reflect, recover and grow.\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"I breed pigs and have a love of nature and the natural world.\"},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":\"For some, only one session and for others it may take upward of six months. It all depends on what the client is hoping to gain.\"},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":\"I attend regualr supervision and training each month.\"},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":\"I think I am inspired by my children. They\'re friendly, hopeful and honest. Three qualities that i really value in everyone.\"},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":\"I am fortunate in having some excellent teachrs at The Metanoia Instutute in London where I studied for four years on the Counselling Psychology doctoral programme.\"},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"The ability to remain a calm, benign presence whatver is happening in the session.\"},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":\"Perhaps reflective but definately heard. They may also feel lighter towards life and even at times challenged.\"},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":\"They can check out my website to see if they like the way I like to work. Maybe bring their wellies as my therapy room is located in a field.\"},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":\"Perhaps as little as one or two sessions.\"}]'),
(164, ' ', '-106', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 10:51:06', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `bravo_tours` (`id`, `title`, `slug`, `content`, `image_id`, `banner_image_id`, `short_desc`, `category_id`, `location_id`, `address`, `map_lat`, `map_lng`, `map_zoom`, `is_featured`, `gallery`, `video`, `price`, `sale_price`, `duration`, `min_people`, `max_people`, `faqs`, `status`, `publish_date`, `create_user`, `update_user`, `deleted_at`, `origin_id`, `lang`, `created_at`, `updated_at`, `default_state`, `enable_sale_of_first_booking`, `highlights`, `country`, `city`, `discount_for_4_slot`, `discount_for_10_slot`, `bank_details`, `introductions_ownership_policy`, `age`, `faqs2`) VALUES
(165, 'Empathy counselling services- ECS', '107', NULL, 232, NULL, NULL, NULL, NULL, '60A Tolver Street, Saint Helens, UK', NULL, '-2.733821799999987', 15, NULL, NULL, NULL, '35.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 114, 114, NULL, NULL, NULL, '2019-11-20 10:51:07', '2019-11-22 07:20:58', 1, NULL, NULL, 'United Kingdom', 'England', NULL, NULL, '{\"first_name\":\"Rachel\",\"last_name\":\"Burns\",\"account_number\":\"18943302\",\"short_code\":\"236972\"}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"After losing my friend in 2013 and seeing how counselling positively affected his wife and daughter I knew I wanted to help in the same way. I began my training and fell in love with helping others achieve their goals.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"I specialise in infertility and trauma but have experience in work related stress, anxiety, depression, relationships, medical diagnoses, grief and loss and much more.\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"I am a person centred counsellor so my approach is very much client led. I also do a lot of work with EAP\\u2019s so I also effectively work within a short term solution focused way.\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"I regard myself as a friendly approachable and empathetic counsellor who genuinely cares.\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"I\\u2019m a big fan of self care so anything that does that. Mainly days out with my little girl. I had my daughter through IVF so along with my qualifications I also know first hand how extremely difficult infertility is.\"},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":\"I usually see clients between 6-12 sessions. Should clients have attended longer while some have finished a lot sooner. It all depends on the client as an individual.\"},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":\"I constantly attend various training courses, most recently safe guarding and transgender.\"},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":\"My friends and family. Without them my practice wouldn\\u2019t be possible.\"},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":\"There\\u2019s no amount of training or qualifications that can compare to real life experiences. Many people have taught me so much about myself and them over the years.\"},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"I regard myself as being empathic and non judgemental. I hold all my clients with unconditional positive regard.\"},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":\"During a session a client should feel safe and secure. Allowing them to fully open up and to work through how they are feeling. Some sessions may be draining and that\\u2019s completely normal. It\\u2019s a process and discussing our emotions can we hard.\"},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":\"Try not to overthink it, or think of what to say or how.\"},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":\"Within a few sessions a client can start to understand why they feel the way they do and coping techniques.\"}]'),
(166, ' ', '-108', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 10:52:00', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(167, 'Heather Finlay', '109', NULL, 243, NULL, NULL, NULL, NULL, '17 Gosfield Street, London, W1W 6HE', NULL, '-0.14180229999999483', 15, NULL, NULL, NULL, '90.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 115, 115, NULL, NULL, NULL, '2019-11-20 10:52:01', '2019-12-05 10:16:21', 1, NULL, '[{\"content\":\"I offer therapy in a safe environment, providing confidential, non-judgemental support to help you explore the difficulties that have led you to seek help. I strive to build a supportive and trusting relationship through listening and talking, to work through issues that may be upsetting you, such as relationship problems, loss, anxiety, depression, shame, trauma, addictions or low self-esteem. We will explore these problems together and find ways to manage feelings that may overwhelm you, challenge self defeating behaviour, identify goals and increase motivation to change, through sessions tailor made to meet your needs. \\r\\n\\r\\nI specialise in working with emotional difficulties such as relationship issues, loss (of others and of self), trauma,  emotional avoidance or dependency, and addictions. I aim to build good self esteem, a strong sense of self, healthy relationships with others, but most importantly a healthy relationship with yourself.\\r\\n\\r\\nSessions are tailor made to meet your needs, integrating the right psychological approach for you. It takes courage to seek help and to find the right therapist for you, so I offer a free 15 minute phone consultation prior to an initial session. I also offer online therapy sessions. I am a registered member of the BACP and abide by their code of ethics.\"},{\"content\":null},{\"content\":null}]', 'United Kingdom', 'LONDON', NULL, NULL, '{\"first_name\":\"heather\",\"last_name\":\"finlay\",\"account_number\":null,\"short_code\":null}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"I have always been fascinated by the human psyche and my first degree was in psychology. I\'ve has therapy on and off for 20 years and it has helped me immeasurably so after 25 years of working as a PR in the music industry I decided to retrain as a therapist.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"Building a strong therapeutic alliance with my clients and supporting them to work through whatever the issue is that has brought them to therapy.\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"I work in a relational way with my clients and I tailor the psychological approach to their specific needs.\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"A safe, confidential space in which to explore their issues and a close supportive relationship.\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"Yoga, meditations, keep fit, cinema.\"},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":\"That very much depends on the individual and what their needs are but I recommend a minimum of 12 sessions.\"},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":\"I am doing further training in Transactional analysis and attachment  theory and regularly attend CPD workshops.\"},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":\"Great thinks and great writers.\"},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":\"My yoga teacher who is also a therapist. She taught me the importance of self care and taking time for me.\"},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"Empathy, unconditional positive regard and congruence.\"},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":\"Safe, heard, seen and understood.\"},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":\"Think about what it is they want to bring to the session.\"},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":\"Again, this is a difficult question to answer because everyone is different but I would say around six sessions.\"}]'),
(168, ' ', '110', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 115, 1, NULL, NULL, NULL, '2019-11-20 10:59:51', '2020-01-26 05:40:33', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(169, ' ', '-111', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 116, NULL, '2020-01-26 05:37:19', NULL, NULL, '2019-11-20 10:59:53', '2020-01-26 05:37:19', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(170, ' ', '-94', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 11:22:23', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(171, ' ', '-107', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 117, NULL, '2020-01-26 05:37:19', NULL, NULL, '2019-11-20 11:22:25', '2020-01-26 05:37:19', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(172, ' ', '-70', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 12:42:26', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(173, ' ', '-112', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 118, NULL, '2020-01-26 05:37:19', NULL, NULL, '2019-11-20 12:42:27', '2020-01-26 05:37:19', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(174, ' ', '-113', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 13:05:19', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(175, 'Romany Smith', '114', '<p>&nbsp;</p>\r\n<p class=\"western\" style=\"margin-bottom: 0cm;\"><br />Romany Smith MBACP (accredited) <br />I am an experienced therapist and an empathy specialist. I wrote my dissertation on empathy and have continued to study and make empathy the core of my practice. I am trained in person centred therapy cognitive behavioural therapy, existential and psychodynamic therapies as well as a number of specialised areas of trauma and life coaching. I work integritively with an emphasis on the core conditions of Carl Rogers person centred theory of psychotherapy; those being, empathy, congruence and unconditional positive regard toward my clients. <br />After a, long career in the arts and having been a peace campaigner and also having been an outreach worker with the street homeless I found I wanted to care for individuals in a deeper way and so decided to retrain as a psychotherapist.&nbsp; I believe and research attests to this, that it is the client, therapist relationship which holds the power to heal and at that with empathy at its core.&nbsp;</p>\r\n<p class=\"western\" style=\"margin-bottom: 0cm;\"><span style=\"font-family: sans-serif, serif, EmojiFont;\">\"<em>Research evidence keeps piling up and it points strongly to the conclusion that a high degree of empathy in the relationship is one of the most potent factors in bring about positive change</em>.\"</span><br /><span style=\"font-family: sans-serif, serif, EmojiFont;\">Carl Rogers</span></p>', 272, NULL, NULL, NULL, NULL, '110 Gloucester Avenue, London, UK', '51.5417291', '-0.15407260000006318', 15, NULL, NULL, NULL, '60.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 119, 119, NULL, NULL, NULL, '2019-11-20 13:05:20', '2020-01-16 08:43:05', 1, 1, NULL, 'United Kingdom', 'England', NULL, NULL, '{\"first_name\":\"In\",\"last_name\":\"Empathy\",\"account_number\":\"69795932\",\"short_code\":\"089299\"}', 1, NULL, NULL),
(176, ' ', '-74', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-20 16:46:52', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(177, 'Ilse leenders MNCS (Acc) Ad. Dip CP', '115', NULL, 235, NULL, NULL, NULL, NULL, 'Milton Keynes MK15 0DL, UK', NULL, '-0.7326679999999897', 15, NULL, NULL, NULL, '50.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 120, 120, NULL, NULL, NULL, '2019-11-20 16:46:54', '2019-11-22 10:32:26', 1, NULL, '[{\"content\":null}]', 'United Kingdom', 'England', NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"I originally trained and qualified as a nurse, moving the into the business world.   I have been a qualified psychotherapist for 10 years   I vast experience and understand the pressure and stress of life .  I believe in enabling people to make sense of the world and recognise their patterns of thoughts and behaviours.   Enable them to make changes and move forward in their lives\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"I have experience and training in working with Trauma, and abuse both physical and psychological (sexual).  I also have vast experience in working with anxiety and stress.  I understand the struggle we face in juggling our lives and the impact this can have on your health and life but also your family, friends and relationships\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"I use a combination of approaches.  Focusing on a safe space.  Using Compassion Focused therapy (CFT) together with Cognitive Behaviour Therapy  (CBT) and Integrated Therapy.  Focusing always on the Clients needs\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"When you reach a cross roads in your life I offer a safe space to help and support you to make changes.     I offer counselling on various issues such as depression, stress anxiety and panic attacks .  I also help and support people who have suffered abuse, helped them to understand their life and empower them to move forward in their lives\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"I also volunteer and run workshops and support groups for people who struggle with self harm and depression  \\r\\nI also play badminton twice a week and enjoy walking and swimming\"},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":\"It is difficult to say and it is dependent on the clients needs.  I do offer Trauma Focused CBT,  This sometimes requires between 6-8 sessions\"},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":\"I am constantly Learning and regularly attend CPD workshops.  The most recent CPD  in 2019 were - \\r\\n1. Working with shame \\r\\n2. Diploma in Inner Child\\r\\n3.  Suicide awareness \\r\\n4. Domestic awareness\"},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":\"I am inspired when I see a client move forward in their lives.    I have had clients who have gone on to complete their degrees and  change their lives.\"},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":\"Two key people have influenced my life.  The first was my mother who gave me the strength and belief in myself.  And a supervisor who helped me through some difficult times.  They always believed in me and taught me to trust and most importantly take care of myself\"},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"I believe the relationship is key.  Someone who you feel safe with, trust and is genuine.    Listening to the client without any judgement, accepting and being compassionate\"},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":\"A client should feel safe, able to open up and talk about their fears and worries.  I always focus on grounding and safety at the start and end of the session, grounding them into a safe space\"},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":\"The only thing I say to clients is to be kind to themselves\"},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":\"This is varied and depends on the client and the issues they bring\"}]'),
(178, ' ', '-116', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-21 04:41:03', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(179, ' ', '-117', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 121, NULL, '2020-01-26 05:37:05', NULL, NULL, '2019-11-21 04:41:05', '2020-01-26 05:37:05', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(180, ' ', '-118', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-21 05:48:16', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(181, 'Madeleine Narciso', '119', NULL, 237, NULL, NULL, NULL, NULL, 'Brighton Road, Surbiton KT6 5LR, UK', NULL, '-0.31001530000003186', 15, NULL, NULL, NULL, '60.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 122, 122, NULL, NULL, NULL, '2019-11-21 05:48:18', '2019-11-29 19:39:23', 1, NULL, NULL, 'United Kingdom', 'England', NULL, NULL, '{\"first_name\":\"Madeleine\",\"last_name\":\"Narciso\",\"account_number\":\"56383575\",\"short_code\":\"165710\"}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"Being born as 1st generation Filipino in Vienna\\/ Austria, I understand what it means to perhaps feel confused about your sense of identity and belonging. Especially in today\\u2019s fast-paced and competitive world, I was struggling to find a sense of purpose or meaning, when working for large companies in the City for years. Through listening to my own inner struggle and yearning, I have found the courage and resource within myself to make a gradual change to retrain as a counsellor and today I am able to practise both professions in a flexible way.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"I work with various issues from anxiety and depression, bereavements and trauma but particularly enjoy working with clients wanting to explore cultural issues, finding a deeper meaning and purpose in their life, relationship issues and self-esteem issues.\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"As a Psychosynthesis counsellor I am able to hold both your present challenges with compassion as well as have an open mind as to what may be trying to emerge for you at times where you may struggle to see clearly.\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"My aim as a counsellor is to provide a reflective and non-judgemental space to explore any issues in a containing and safe manner. Raising awareness about your own process may help to look at yourself, and how you relate in the world, in a different light to enable you to make different choices in life and strengthen undiscovered parts of your personality.\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":\"Depending on your needs from therapy, we can agree to meet on a short-term basis (8-12 sessions) to address a specific issue or long-term for deeper explorations of themes and patterns in your life that may have shaped who you are today.\"},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":\"I am  a registered member of the BACP and adhere to their ethical framework. I attend regular supervision to continuously explore creative ways or through meditation and mindfulness to work on myself to hold and support my clients in the best way they deserve.\"},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"Presence.\"},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":\"Someone where you feel comfortable to bring the whole of yourself and feel listened to, understood and seen by.\"},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(182, ' ', '-119', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-21 09:57:06', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(183, 'James Farrelly', '120', NULL, 238, NULL, NULL, NULL, NULL, 'Unit 2 Harforde Court', NULL, '-0.05346063572619641', 13, NULL, NULL, NULL, '97.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 123, 123, NULL, NULL, NULL, '2019-11-21 09:57:08', '2019-11-21 10:12:48', 1, NULL, NULL, 'United Kingdom', 'Hertford', NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":null},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":null},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":null},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":null},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(184, 'Joe Hassett', 'joe-hassett', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'draft', NULL, 25, 25, NULL, NULL, NULL, '2019-11-21 10:51:11', '2019-12-10 09:51:59', 1, 0, NULL, NULL, NULL, NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', NULL, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"I am groot\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"I am groot\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"I am groot\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"I am groot\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(185, ' ', '-53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-22 07:47:39', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(186, 'Louise Morris', '120-1', NULL, 239, NULL, NULL, NULL, NULL, 'Cathedral Road, Cardiff CF11 9LJ, UK', NULL, '-3.18869559999996', 15, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'publish', NULL, 124, 124, NULL, NULL, NULL, '2019-11-22 07:47:41', '2019-11-22 07:57:22', 1, NULL, NULL, NULL, 'Wales', NULL, NULL, '{\"first_name\":\"Louise\",\"last_name\":\"Morris\",\"account_number\":\"18097474\",\"short_code\":\"08 93 00\"}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":null},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"Counsellor\\/Therapist\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"Humanistic\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"A warm, safe, non judgemental space to explore and gain clarity of their emotions and feelings to help them work towards a happy healthy future that works for them.\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"Amateur dramatics, cross stitch\"},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":\"6-8 sessions\"},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":\"30 hours CPD a year - particular interest in working with trauma\"},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"Empathy, understanding, warmth, welcoming and an excellent active listener...\"},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":\"Depends which session! Those first sessions can be particularly hard and you can feel worse before you feel better... towards the end start to feel a sense of achievement and enlightenment.\"},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(187, ' ', '-121', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-22 07:47:51', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(188, ' ', '-122', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 125, NULL, '2020-01-26 05:37:05', NULL, NULL, '2019-11-22 07:47:53', '2020-01-26 05:37:05', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(189, ' ', '-120', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-22 08:06:25', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(190, ' ', '-123', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 126, NULL, '2020-01-26 05:37:05', NULL, NULL, '2019-11-22 08:06:26', '2020-01-26 05:37:05', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(191, ' ', '-124', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-22 08:30:41', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(192, 'Penny Skyrme', '125', NULL, NULL, NULL, NULL, NULL, NULL, 'Lon Hir, Carmarthen SA31 1SL, UK', NULL, '-4.316626600000063', 15, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'publish', NULL, 127, 127, NULL, NULL, NULL, '2019-11-22 08:30:43', '2019-11-22 09:13:40', 1, NULL, NULL, 'Great Britain', 'Carmarthen', NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"I became a counsellor so that I could support people though tough times.  I received counselling through my 20s and I found it very helpful in lots of different ways.  I wanted to give back.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"Sexual Abuse, Bereavement, CBT, Anxiety and Depression, Smoking Cessation, working with athletes and health rehabilitation\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"Integrative\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"Ability to prioritise, to make choices and decisions, to feel calmer and valued as a person.  Not to feel judged or criticised.\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"Health & Fitness, Cooking, Family\"},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":\"Usually after the first session the client may feel calmer, but may be emotional. After six sessions there is a review to look at what we have covered and if the client wants to continue.\"},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":\"Various courses and online Webinars in line with British Association of Counsellors and Psychotherapists\"},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":\"Family and friends, also clients.\"},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":\"Lecturers and coaches - you can achieve more than you think, you sometimes know more than you think\"},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"Unconditional positive regard, non-judgemental, congruence, approachable and adaptable person\"},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":\"Sometimes emotional, sometimes relieved - depends on the session and the client.  Self-care after the session is very important, which will be discussed.\"},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":\"Nothing needed, just courage.\"},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":\"Between 1 and 6 sessions.\"}]'),
(193, ' ', '-115', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-22 10:24:58', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(194, 'Sarah Lee', '125-1', NULL, 240, NULL, NULL, NULL, NULL, 'Samedaydoctor Manchester, Fabric Building, Queen Street, Manchester M2 5HX, UK', NULL, '-2.247386199999937', 15, NULL, NULL, NULL, '75.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 128, 128, NULL, NULL, NULL, '2019-11-22 10:24:59', '2019-11-23 04:02:26', 1, NULL, NULL, 'United Kingdom', 'England', NULL, NULL, '{\"first_name\":\"Sarah\",\"last_name\":\"Lee\",\"account_number\":\"66487737\",\"short_code\":\"608371\"}', NULL, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"I\'d had a lot of therapy myself; some of which was helpful and some of which wasn\'t. I was interested in what made this difference. I trained in the type of therapy that helped me the most. My original aim was to understand myself better and I decided that once I did that I would help other people who were experiencing similar issues.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"I specialise in clients who come from dysfunctional family or difficult family backgrounds. Some clients identify as having CPTSD or complex trauma.  This manifests in anxiety and depression, low self esteem or feelings of worthlessness, difficulties in relationships such as choosing the wrong people or sabotaging relationships and difficulties in managing emotions.\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"I work relationally. This means I consider how you respond to me in therapy and what your relationships and friendships  are like outside of therapy.  We also consider how your previous relationships and experiences  have affected you and the impact they have had.  I aim to be supportive and encouraging. I\'m not going to tell you what to do  but I will help you to figure out what you want to do and how you want to be.\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"We start by looking into your background; your family, how you grew up,  your school experience and consider your current situation such as your job, your relationship and the support you have. I help people to communicate better, identify their emotions, learn to tolerate them and express them to others. We also look at behaviours and how to change them or perhaps look at options for alternatives if you aren\'t sure what you want to change.\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":\"Anywhere between a couple of months upwards. It really depends on how long the problem has been going on for and what kind of support you have.\"},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":\"I read a lot. I have my own therapy and supervision (this is where a therapist can talk to another therapist about their work, ask questions and talk about anything that may have affected them. This is confidential). I believe that it\'s critical for therapists to  have had their own therapy so that they know the difference between your issues and their issues.\"},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":\"I\'m inspired by my clients. They work hard to understand themselves and learn how to manage their emotions and relationships. I\'m inspired by people who accept themselves and refuse to conform  to restrictions placed on them by others. I\'m inspired by people who stand up for themselves and others and tell the truth about what happened to them.\"},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":\"I had excellent trainers. They taught me not to give up and to keep going even when I thought I couldn\'t do it. Also my therapist. He taught me to laugh in therapy, it doesn\'t always need to be serious.\"},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"Somebody who can tailor their approach to the client, not somebody who does the same type of therapy no matter who the client is. They need to be flexible, patient, understanding and have a good sense of humour. And they need to understand themselves and  have worked through their own issues.\"},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":\"During a session they should feel understood and listened to.  I hope clients leave a session feeling less alone and more supported. They may also have a better understanding of their own behaviour and how they can do things differently.\"},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":\"It\'s helpful for clients to consider what they want to talk about before a session. This can be how their week has been or might be something they have been thinking about and want to tell me.\"},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":\"Understanding normally increases from the first session. We build on this in subsequent sessions, maybe looking at different areas they are struggling with or talking about important people in their life. I encourage clients to pay attention to any progress they are making as this encourages them to keep going and motivates them to continue.\"}]'),
(195, ' ', '-125', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-22 18:19:05', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(196, ' ', '-126', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 129, NULL, '2020-01-26 05:37:05', NULL, NULL, '2019-11-22 18:19:07', '2020-01-26 05:37:05', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(197, ' ', '-127', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-22 22:27:03', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(198, 'Gina Howard', '128', '<p>Your story matters to me and as a qualified counsellor and a caring human being I want to help you cope with whatever you are facing at the moment. I offer a free 30 minute consultation to see how we can work together.&nbsp; If you are experiencing depression, anxiety, stress, relationship issues, phobia\'s, addictions, bereavement or lonliness I will do my best to help you. My counselling room is near Horsham and the surrounding area\'s of Crawley, Haywards Heath, Rudgwick and Billingshurst.</p>\r\n<p>&nbsp;</p>', 241, NULL, NULL, NULL, NULL, '5 Masons Field,, Mannings Heath,', NULL, NULL, 8, NULL, NULL, NULL, '40.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 130, 130, NULL, NULL, NULL, '2019-11-22 22:27:06', '2020-01-31 18:05:04', 1, 1, NULL, 'United Kingdom', 'Horsham', NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', 1, NULL, NULL),
(199, ' ', '-128', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-23 05:21:48', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(200, 'Claire Parrish', '129', NULL, 242, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'publish', NULL, 131, 131, NULL, NULL, NULL, '2019-11-23 05:21:51', '2019-11-23 05:58:51', 1, NULL, NULL, NULL, NULL, NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', NULL, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"Being there for another person, seeing their potential, listening, offering support, validating them, helping them connect and belong - this is why I enjoy being a therapist and work in this field . I believe therapy allows a person to experience their basic emotional needs being met and assists personal stability and growth.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"Trauma, Anxiety, Depression, Panic attacks and feelings of low self esteem.\\r\\nI also work with self harm, issues relating to food and  negative body image.\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":null},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":null},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(201, ' ', '-129', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-24 09:23:26', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(202, ' ', '130', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 132, 1, NULL, NULL, NULL, '2019-11-24 09:23:30', '2020-01-26 05:38:27', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(203, ' ', '-131', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-24 11:48:47', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(204, ' ', '132', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 133, 1, NULL, NULL, NULL, '2019-11-24 11:48:51', '2020-01-26 05:38:27', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(205, ' ', '-109', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-24 14:06:14', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(206, ' ', '133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 134, 1, NULL, NULL, NULL, '2019-11-24 14:06:16', '2020-01-26 05:38:27', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(207, ' ', '-134', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-25 05:27:20', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(208, 'Sean Sullivan', '135', '<p>Hi I\'m Sean, I\'m glad you found my profile.</p>\r\n<p>Sometimes life can be paiful, confusing and things might be disturbing you. Perhaps you\'re feeling alone anxious and strugglin to find meaning in your life but find it difficult to reach out for help.</p>\r\n<p>I work in a non-judgmental way, in a safe, confidential and caring environment, supporting you to work through painful, challenging feelings and experiences. I work in a variety of ways to meet your needs. Get in touch to find out how we can work together.</p>\r\n<p>I\'m also a qualified drug and alcohol counsellor and I understand there are many reasons why people develop addictions. We can explore behaviours and triggers, identify issues and reasons to help us understand your relationship with substances.</p>\r\n<p>Together we can explore how you can make positive changes in your life, develop your strengths and recources, building up your reslience to live in the here and now.</p>\r\n<p>I also counsel carers of people with addictions and complex needs. Taking responsibility for someone you love can be overwhelming and feel like it\'s taken over your life. You may feel trapped and your life is about meeting the needs of others and you may have conflicting needs such as love and anger. Together we\'ll look at enabling behaviours and how you can take less responsibility through detaching with love, and explore your own sel-care, how you can make positive changes in your life, so you can have your needs met also.</p>\r\n<p>I encourage you to be open and honest about your feelings, the more you share the more you will benefit from any insights into your experience, the more I can support you understand how you relate to yourself and others, and learn useful ways to resolve your problems.</p>', 356, NULL, NULL, NULL, NULL, 'Brighton Consulting Rooms, 18A Clermont Road, Brighton, UK', '50.8458037', '-0.1547691', 15, NULL, NULL, NULL, '45.00', NULL, NULL, 0, 0, NULL, 'draft', NULL, 135, 135, NULL, NULL, NULL, '2019-11-25 05:27:22', '2020-03-11 06:32:50', 1, 1, NULL, 'United Kingdom', 'Brighton BN1 6SG', NULL, NULL, '{\"first_name\":\"Sean\",\"last_name\":\"Sullivan\",\"account_number\":\"4658598308071018\",\"short_code\":\"201280\"}', 1, NULL, NULL),
(209, ' ', '-136', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-25 08:06:15', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `bravo_tours` (`id`, `title`, `slug`, `content`, `image_id`, `banner_image_id`, `short_desc`, `category_id`, `location_id`, `address`, `map_lat`, `map_lng`, `map_zoom`, `is_featured`, `gallery`, `video`, `price`, `sale_price`, `duration`, `min_people`, `max_people`, `faqs`, `status`, `publish_date`, `create_user`, `update_user`, `deleted_at`, `origin_id`, `lang`, `created_at`, `updated_at`, `default_state`, `enable_sale_of_first_booking`, `highlights`, `country`, `city`, `discount_for_4_slot`, `discount_for_10_slot`, `bank_details`, `introductions_ownership_policy`, `age`, `faqs2`) VALUES
(210, 'Bobby Bradstock', '137', NULL, 245, NULL, NULL, NULL, NULL, 'Woodfield Farm, Earlswood, Solihull B94 5LS, UK', NULL, '-1.83773020000001', 15, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'publish', NULL, 136, 136, NULL, NULL, NULL, '2019-11-25 08:06:16', '2019-11-25 08:21:44', 1, NULL, NULL, 'United Kingdom', 'England', NULL, NULL, '{\"first_name\":\"Bobby\",\"last_name\":\"Bradstock\",\"account_number\":\"4462918920234727\",\"short_code\":\"110699\"}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"I became a psychotherapist as I am experienced in working with stress, anxiety, depression, trauma and low self-esteem in many areas of life. My professional background has been in working with Autistic young adults with non-verbal communication.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"I am particularly interested in working with clients with Chronic Illness, such as Fibromyalgia and Myalgic Encephalomyelitis, with the approach to supporting clients with the debilitating and isolating effects that illness\\u2019s such as these can have on a person\\u2019s wellbeing and identity.\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"My approach looks at the individual and the factors that have involved in developing that person strategies for coping with life. Through this understanding and our working relationship we can begin to change those strategies for healthier choices. My approach combines different techniques associated with Person Centred Therapy and Transactional Analysis.\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"I offer a private, confidential service committed to facilitating your journey to better mental health.\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(211, ' ', '-137', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-25 10:37:05', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(212, 'Suzanne Walsh', '138', NULL, 246, NULL, NULL, NULL, NULL, 'Crewe Road, Crewe CW2 6PU, UK', NULL, '-2.4602001000000655', 15, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'publish', NULL, 137, 137, NULL, NULL, NULL, '2019-11-25 10:37:08', '2019-11-25 11:57:43', 1, 1, '[{\"content\":\"Counselling can provide support through difficult periods in your life and help you to make some sense of what is happening. I can offer support in areas such as\\u2026\\r\\n\\r\\n\\u2022 Abortion \\u2022 Abuse \\u2022 Addictions \\u2022  AIDS and HIV \\u2022 Anxiety \\u2022 Bereavement and Loss \\u2022 \\r\\n\\u2022 Coping with Change \\u2022 Cancer and Terminal Illness \\u2022 Death and Dying \\u2022 Depression \\u2022 \\r\\n\\u2022 Emotional Abuse \\u2022 Low Self Esteem and Lack of Confidence \\u2022 Mental Health \\u2022 \\r\\n\\u2022  Panic Attacks \\u2022 Relationships \\u2022 Self Harm \\u2022  Stress \\u2022\\r\\n\\r\\n\\u2022 Couples Counselling \\u2022 \\r\\n\\r\\n\\u2022 Vulnerability to the effects of Media Pressure \\u2022 Body Dissatisfaction \\u2022 Social Phobia \\u2022Counselling can provide support through difficult periods in your life and help you to make some sense of what is happening. I can offer support in areas such as\\u2026\\r\\n\\r\\n\\u2022 Abortion \\u2022 Abuse \\u2022 Addictions \\u2022  AIDS and HIV \\u2022 Anxiety \\u2022 Bereavement and Loss \\u2022 \\r\\n\\u2022 Coping with Change \\u2022 Cancer and Terminal Illness \\u2022 Death and Dying \\u2022 Depression \\u2022 \\r\\n\\u2022 Emotional Abuse \\u2022 Low Self Esteem and Lack of Confidence \\u2022 Mental Health \\u2022 \\r\\n\\u2022  Panic Attacks \\u2022 Relationships \\u2022 Self Harm \\u2022  Stress \\u2022\\r\\n\\r\\n\\u2022 Couples Counselling \\u2022 \\r\\n\\r\\n\\u2022 Vulnerability to the effects of Media Pressure \\u2022 Body Dissatisfaction \\u2022 Social Phobia \\u2022\"}]', 'United Kingdom', 'Crewe', NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"I started my working career nursing in a major London hospital where I gained insight into how stressful situations, be this acute or chronic periods of illness, life threatening and terminal illness such as cancer, addiction and trauma can affect a person and their family and friends.\\r\\n\\r\\nHaving also worked in the corporate and entertainment industries, I have an understanding of the stresses inherent in both environments. And whilst bringing up a family, as a mother, I continued to nurse the disabled and the terminally ill.\\r\\n\\r\\nI re-trained after my children left school and am now an experienced Person-Centred counsellor, an accredited member of the British Association for Counselling and Psychotherapy and  on the United Kingdom Register of  Counsellors and Psychotherapists.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"I run a Private Practice from my home in Cheshire and I have had many years experience working as Counsellor and Clinical Lead in a Substance Misuse Agency, providing drug and alcohol counselling, for misusers and anyone affected by someone else\\u2019s use at all levels of society. .\\r\\n\\r\\nI have worked  as a counsellor in a G.P\\u2019s surgery in West London, a Day Care Hospice and in the Lynda Jackson Macmillan Centre, at Mount Vernon Hospital in Middlesex, offering support to patients, families and friends living with cancer or a life-threatening illness.\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"My philosophy is based upon that of Carl Rogers, pioneer of the Person-Centred Approach. I share his belief in the actualising tendency, the innate capacity of every individual to grow and develop.\\r\\n\\r\\nI feel my life\\u2019s experiences have equipped me with an empathic understanding, which I aim to bring  to my counselling work...\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"At times life can be difficult and I would like to offer you the opportunity to engage in a dialogue, that is different from talking with a loved one or friend, as a way of enabling you to work towards living in a more satisfying and resourceful way.\\r\\n\\r\\nBy offering a confidential place you can talk about yourself and your life, which may, at times, be confusing and you may feel uncomfortable, angry or unhappy. A place, where you are free to work through any issues or difficulties. By thinking  and talking openly about yourself and your concerns you can take the first steps to making changes in your life.\\r\\n\\r\\nI do this by listening to the way you feel and how this affects you, accepting the way you are without judging and working with you to enable you to make the changes that you want to happen. I understand that it is not always easy to talk about problems and to express your feelings so I aim to bring warmth and empathy to enable you to talk about those things that bother you.\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":\"There are no set time limits with counselling, it is a question of listening and talking through everything that is brought to the session so we both have a greater understanding.   The client may then go away and reflect and decide to make changes or not. \\r\\nI see the quality of the engagement, between the client and counsellor, as the main therapeutic agent so I offer the first half hour of the first session free, for us to see how we can best work together.\"},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":\"The curious paradox is that when I accept myself just as I am, then I can change.                                                                                                Carl Rogers 1961\\r\\n\\r\\nMy philosophy is based upon that of Carl Rogers, pioneer of the Person-Centred Approach. I share his belief in the actualising tendency, the innate capacity of every individual to grow and develop.\"},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":\"There are no set time limits with counselling, it is a question of listening and talking through everything that is brought to the session so we both have a greater understanding.   The client may then go away and reflect and decide to make changes or not. \\r\\nI see the quality of the engagement, between the client and counsellor, as the main therapeutic agent so I offer the first half hour of the first session free, for us to see how we can best work together.\"}]'),
(213, ' ', '-47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-25 15:18:26', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(214, 'Danielle La Roche', '138-1', NULL, 247, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'publish', NULL, 138, 138, NULL, NULL, NULL, '2019-11-25 15:18:29', '2019-11-25 15:24:46', 1, NULL, NULL, NULL, NULL, NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":null},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":null},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":null},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":null},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(215, ' ', '-138', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-26 03:35:35', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(216, 'Robert Halliday FdSc MNCS Accred (RS Counselling)', '139', NULL, 248, NULL, NULL, NULL, NULL, '4 roddens crescent,  belfast,  bt5 7jn', NULL, NULL, 8, NULL, NULL, NULL, '35.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 139, 139, NULL, NULL, NULL, '2019-11-26 03:35:37', '2019-11-26 04:04:42', 1, 1, NULL, 'United Kingdom', 'belfast', NULL, NULL, '{\"first_name\":\"robert\",\"last_name\":\"halliday\",\"account_number\":\"00596820\",\"short_code\":\"110974\"}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"I became a counsellor to try and fill the gap for affordable professional mental health in my community.\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"my main strength is building relationship and trust between myself and the client allowing them to feel as safe as possible.\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"Integrative\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"Any client who works with me will have the time, space and encouragement to explore any mental health issues they may currently, or have historically trouble overcoming.\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"I am an avid reader, always learning and developing, and I love the outdoors and exercise.\"},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":\"Impossible to predict, depends on client, presenting issue, underlying issue and goals to be achieved. I review every six sessions.\"},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":\"I am required as part of my accreditation, to attend supervision monthly, attend courses for my own development and occasionally attend counselling myself.\"},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":\"Any of my clients who have the courage to tackle head on the difficult issues they face.\"},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":\"I have had many, but and old boss, and my grandfather, both taught me the value of persistence against all opposition when following what I believe to be true.\"},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"Client progression and learning from each new session.\"},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":\"During, very safe and heard, after, sometimes tired and drained but with hope and challenges for the road ahead.\"},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":\"No, just bring yourself as you are.\"},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":\"As above, it is Impossible to predict, depends on client, presenting issue, underlying issue and goals to be achieved. I review every six sessions.\"}]'),
(217, ' ', '-139', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-26 06:07:42', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(218, ' ', '140', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 140, 1, NULL, NULL, NULL, '2019-11-26 06:07:44', '2020-01-26 05:38:17', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(219, ' ', '-141', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-27 18:56:37', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(220, ' ', '142', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 141, 1, NULL, NULL, NULL, '2019-11-27 18:56:39', '2020-01-26 05:38:17', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(221, ' ', '-143', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-11-30 11:05:55', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(222, ' ', '144', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 142, 1, NULL, NULL, NULL, '2019-11-30 11:05:58', '2020-01-26 05:38:17', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(223, ' ', '-145', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-12-03 07:22:45', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(224, ' ', '146', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 143, 1, NULL, NULL, NULL, '2019-12-03 07:22:48', '2020-01-26 05:38:17', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(225, ' ', '-105', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-12-03 11:05:55', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(226, 'testing testing', 'testing-testing', NULL, 251, NULL, NULL, NULL, NULL, '180 Strand, London, UK', NULL, '-0.1149583999999777', 15, NULL, NULL, NULL, '1.00', NULL, NULL, 0, 0, NULL, 'draft', NULL, 145, 1, NULL, NULL, NULL, '2019-12-03 11:05:58', '2020-01-26 05:38:17', 1, 0, NULL, 'United Kingdom', 'London', NULL, NULL, '{\"first_name\":\"testing\",\"last_name\":\"testing\",\"account_number\":\"12334534\",\"short_code\":\"123456\"}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":\"testing testing testing testing testing testing testing testing testing testing testing testing testing testing\"},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":\"testing testing testing testing testing testing testing testing testing testing testing testing testing testing testing testing testing testing testing\"},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":\"testing testing testing testing testing testing testing testing testing testing testing testing\"},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":\"testing testing testing testing testing testing testing testing testing testing testing\"},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":\"testing testing testing testing testing testing testing testing testing testing\"},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(227, ' ', '-147', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-12-04 01:36:34', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(228, ' ', '148', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 146, 1, NULL, NULL, NULL, '2019-12-04 01:36:36', '2020-01-26 05:38:17', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(229, ' ', '-149', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-12-04 03:37:57', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(230, 'Sandie Meacher', '150', '<h1 class=\"entry-title\" style=\"box-sizing: border-box; border: 0px; font-family: \'Open Sans\', sans-serif; font-size: 25.76816px; font-weight: 300; margin: 0px 0px 1.5em; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 1.3; color: #014c8d;\">Are You Looking For A Counsellor in Central London to Help You?</h1>\r\n<h2 style=\"box-sizing: border-box; border: 0px; font-family: \'Open Sans\', sans-serif; font-size: 23.4256px; font-weight: 300; margin: 0px 0px 1.3em; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 1.3; color: #014c8d;\">&nbsp;Can I Help?</h2>\r\n<ul style=\"box-sizing: border-box; border: 0px; font-family: \'Open Sans\', sans-serif; font-size: 14px; margin: 0px 0px 1.5em 3em; outline: 0px; padding: 0px; vertical-align: baseline; list-style-position: initial; list-style-image: initial; caret-color: #555555; color: #555555;\">\r\n<li style=\"box-sizing: border-box; border: 0px; font-family: inherit; font-style: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">Helping you work out what&rsquo;s most important in life</li>\r\n<li style=\"box-sizing: border-box; border: 0px; font-family: inherit; font-style: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">Identifying&nbsp;an issue that is affecting your day-to-day life</li>\r\n<li style=\"box-sizing: border-box; border: 0px; font-family: inherit; font-style: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">Helping&nbsp;you to make important decisions when you are &nbsp;not sure what to do next</li>\r\n<li style=\"box-sizing: border-box; border: 0px; font-family: inherit; font-style: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">Developing good communication strategies</li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; border: 0px; font-family: \'Open Sans\', sans-serif; font-size: 14px; margin: 0px 0px 1.5em; outline: 0px; padding: 0px; vertical-align: baseline; caret-color: #555555; color: #555555;\">I work with clients who are seeking help to affect long term change.</p>\r\n<p style=\"box-sizing: border-box; border: 0px; font-family: \'Open Sans\', sans-serif; font-size: 14px; margin: 0px 0px 1.5em; outline: 0px; padding: 0px; vertical-align: baseline; caret-color: #555555; color: #555555; text-align: justify;\">I am an experienced senior accredited counsellor. I offer therapy in a private and confidential setting in Central London. We explore and acknowledge difficult feelings in a supportive and gentle environment. I work with my clients to find perspective and containment with difficult and stressful situations whilst supporting your change.</p>\r\n<p style=\"box-sizing: border-box; border: 0px; font-family: \'Open Sans\', sans-serif; font-size: 14px; margin: 0px 0px 1.5em; outline: 0px; padding: 0px; vertical-align: baseline; caret-color: #555555; color: #555555; text-align: justify;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; border: 0px; font-family: \'Open Sans\', sans-serif; font-size: 14px; margin: 0px 0px 1.5em; outline: 0px; padding: 0px; vertical-align: baseline; caret-color: #555555; color: #555555; text-align: justify;\"><span style=\"background-color: #3598db;\"><em><strong>www.smcentrallondoncounselling.co.uk</strong></em></span></p>', 252, NULL, NULL, NULL, NULL, 'Holborn Gate, 330 High Holborn, London WC1V 7QT', NULL, NULL, 8, NULL, NULL, NULL, '90.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 147, 147, NULL, NULL, NULL, '2019-12-04 03:37:59', '2020-02-28 11:23:34', 1, NULL, '[{\"content\":\"My website - smcentrallondoncounselling.co.uk\"}]', 'United Kingdom', 'London', NULL, NULL, '{\"first_name\":\"Sandie\",\"last_name\":\"Meacher\",\"account_number\":\"63778495\",\"short_code\":\"207164\"}', 1, NULL, NULL),
(231, ' ', '-150', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, '2019-12-07 02:26:48', NULL, NULL, '2019-12-04 04:12:45', '2019-12-07 02:26:48', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(232, 'Đỗ Quân', '151', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, '200.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 148, 148, '2019-12-20 05:05:02', NULL, NULL, '2019-12-04 04:12:47', '2019-12-20 05:05:02', 1, 1, NULL, NULL, NULL, NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":null},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":null},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":null},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":null},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(233, 'DoQuan', 'doquan', NULL, NULL, NULL, NULL, NULL, NULL, 'Vietnam Veterans Memorial Park, Albuquerque, NM, USA', NULL, '-106.63428820000001', 15, NULL, NULL, NULL, '500.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 149, 149, '2019-12-20 05:05:02', NULL, NULL, '2019-12-04 04:41:48', '2019-12-20 05:05:02', 1, 1, NULL, NULL, 'New Mexico', NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":null},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":null},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":null},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":null},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(234, 'DoQuangQuan', 'doquangquan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, '100.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 150, 150, '2019-12-20 05:05:02', NULL, NULL, '2019-12-04 22:22:44', '2019-12-20 05:05:02', 1, 1, NULL, NULL, NULL, NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":null},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":null},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":null},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":null},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(235, ' Christel', 'christel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 151, NULL, NULL, NULL, NULL, '2019-12-05 14:59:29', '2019-12-05 14:59:29', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(236, ' Andrew Davis', 'andrew-davis', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 152, 20, NULL, NULL, NULL, '2019-12-12 04:42:02', '2019-12-12 04:42:02', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(237, ' Do Trung Dung', 'do-trung-dung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 153, NULL, NULL, NULL, NULL, '2019-12-12 20:24:52', '2019-12-12 20:24:52', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(238, ' mydevtest', 'mydevtest', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 154, NULL, NULL, NULL, NULL, '2019-12-12 20:51:51', '2019-12-12 20:51:51', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(239, 'Sal Paradise', '2', '<div class=\"standard-arrow bullet-top\" style=\"padding: 0px 0px 0px 20px; margin: 0px 0px 15px; caret-color: #878d94; color: #878d94; font-family: \'Open Sans\', Helvetica, Arial, Verdana, sans-serif; font-size: 14px;\">\r\n<ul style=\"padding: 0px; margin: 0px; list-style-position: outside; list-style-image: none;\">\r\n<li style=\"padding: 0px 0px 0px 24px; margin: 0px 0px 10px; position: relative; list-style: none;\">Do you feel down, anxious or lost?</li>\r\n<li style=\"padding: 0px 0px 0px 24px; margin: 0px 0px 10px; position: relative; list-style: none;\">Are you having difficulty getting over a past relationship?</li>\r\n<li style=\"padding: 0px 0px 0px 24px; margin: 0px 0px 10px; position: relative; list-style: none;\">Or do you have a series of failed relationships and wonder what has gone wrong?</li>\r\n<li style=\"padding: 0px 0px 0px 24px; margin: 0px 0px 10px; position: relative; list-style: none;\">Are you at a cross roads in your life and not sure which way to turn?</li>\r\n<li style=\"padding: 0px 0px 0px 24px; margin: 0px 0px 10px; position: relative; list-style: none;\">Do you feel stressed out at work or at home?</li>\r\n<li style=\"padding: 0px 0px 0px 24px; margin: 0px 0px 10px; position: relative; list-style: none;\">Are you determined to make a change and not sure where to start?</li>\r\n<li style=\"padding: 0px 0px 0px 24px; margin: 0px 0px 10px; position: relative; list-style: none;\">Or do you need to work through a major decision?</li>\r\n</ul>\r\n</div>\r\n<div class=\"wpb_text_column wpb_content_element  vc_custom_1473066761866\" style=\"caret-color: #878d94; color: #878d94; font-family: \'Open Sans\', Helvetica, Arial, Verdana, sans-serif; font-size: 14px; padding: 0px 20px !important 0px 20px !important; margin: 0px 0px 0px !important 0px;\">\r\n<div class=\"wpb_wrapper\" style=\"padding: 0px; margin: 0px;\">\r\n<p style=\"padding: 0px; margin: 0px 0px 10px;\">Counselling can help you explore your thoughts and feelings, find the answers within, so you can move forward in a positive way.</p>\r\n<p style=\"padding: 0px; margin: 0px 0px 10px;\">Sometimes you just need to talk&nbsp;</p>\r\n<p style=\"padding: 0px; margin: 0px;\">Start looking forward to a happier future.</p>\r\n</div>\r\n</div>', 259, NULL, NULL, NULL, NULL, 'Liverpool L23, UK', '53.4982488', '-3.027816600000051', 12, NULL, NULL, NULL, '90.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 46, 46, NULL, NULL, NULL, '2019-12-13 09:32:12', '2020-01-14 08:49:15', 1, 1, NULL, NULL, 'England', NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', 1, NULL, NULL),
(240, 'Jenny Harrison', 'jenny-harrison', '<p><span style=\"font-family: avenir-lt-w01_35-light1475496, sans-serif; font-size: 15px; text-align: justify;\">I\'m a qualified psychotherapist working in private practice with people of all age groups and backgrounds. I provide psychological support for people who are suffering from anxiety, depression, emotional problems, stress and the related issues that can ensue. You can phone me anytime to talk about your situation and how therapy can help you. I offer an initial consultation in which you can meet me to discuss what you would like assistance with, without being obliged to continue. Then you can decide if you want to attend a further session.&nbsp;</span></p>', 263, NULL, NULL, NULL, NULL, 'Newcastle upon Tyne, UK', NULL, '-1.5343844652160215', 11, NULL, NULL, NULL, '65.00', NULL, NULL, 0, 0, NULL, 'draft', NULL, 155, 155, NULL, NULL, NULL, '2019-12-13 10:35:54', '2019-12-13 10:49:59', 1, 1, NULL, 'United Kingdom', 'Newcastle', NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', 1, NULL, '[{\"title\":\"Why did you become a therapist\\/coach\\/counsellor?\",\"required\":\"1\",\"content\":null},{\"title\":\"What area of therapist\\/coach\\/counsellor do you feel you are strongest in?\",\"required\":\"1\",\"content\":null},{\"title\":\"What type of approach do you take?\",\"required\":\"1\",\"content\":null},{\"title\":\"What can a client\\/service user expect from the experience of working with you?\",\"required\":\"1\",\"content\":null},{\"title\":\"What interests do you have outside of therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long do you feel it takes for the service user to get results?\",\"required\":\"0\",\"content\":null},{\"title\":\"What ongoing personal development do you do?\",\"required\":\"0\",\"content\":null},{\"title\":\"Who inspires you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What mentors have you had in life and what did they teach you?\",\"required\":\"0\",\"content\":null},{\"title\":\"What do you feel makes a good therapist\\/coach\\/counsellor?\",\"required\":\"0\",\"content\":null},{\"title\":\"How should a client feel during and after a session?\",\"required\":\"0\",\"content\":null},{\"title\":\"Is there anything a client can do before your session to prepare?\",\"required\":\"0\",\"content\":null},{\"title\":\"How long does it take for the client to see results?\",\"required\":\"0\",\"content\":null}]'),
(241, 'Chris Harrison', 'chris-harrison', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 156, 1, NULL, NULL, NULL, '2019-12-13 10:52:06', '2020-01-26 05:43:29', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(242, ' Michael Dandy', 'michael-dandy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 158, 157, NULL, NULL, NULL, '2019-12-17 07:40:41', '2019-12-17 07:40:41', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(243, ' test', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 159, NULL, '2020-01-26 05:35:30', NULL, NULL, '2019-12-18 08:56:31', '2020-01-26 05:35:30', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(244, ' Kirsty', 'kirsty', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 160, NULL, NULL, NULL, NULL, '2020-01-07 04:42:23', '2020-01-07 04:42:23', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(245, 'Kirsty Potter', 'kirsty-potter', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'draft', NULL, 161, 1, NULL, NULL, NULL, '2020-01-10 05:24:33', '2020-01-26 05:43:42', 1, 0, NULL, NULL, NULL, NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', NULL, NULL, NULL),
(246, 'Michele Lamonaca', 'michele-lamonaca', '<p><strong>Welcome</strong></p>\r\n<p>My name is Michele. I\'m a registered psychotherapist (BACP) and accredited psychodynamic counsellor (UKCP) - I help individuals and couples with their emotional health. I have private sector experience as counsellor and psychotherapist. My focus involves abuse, depression, narcissistic personality disorder, and the specific problems encountered by men, women and LGBT+ people. I offer therapy to individuals, partners and couple affected by sex &amp; porn addiction, in addition to substance and other behavioural addictions and eating disorders. <strong>I also work with those people who feel their sexual drive may be unnatural and driven towards compulsive and illegal behaviours such as sexual and internet sexual abuse.</strong></p>\r\n<p><strong>What to expect </strong></p>\r\n<p>An initial assessment meeting allows you to see if you feel comfortable with me, discuss what concerns you, ask questions, clarify confidentiality, and determine if therapy could assist you.</p>\r\n<p><strong>Counselling</strong></p>\r\n<p>Counselling centres on listening to you and assisting you. ou determine which issue regarding your early years or adulthood to explore. The aim of counselling is to help you to talk about difficulties and discover a solution which is appropriate for you.</p>\r\n<p>The National Institute for Health and Care Excellence (NICE) England and Wales recommend counselling for depression.</p>\r\n<p><strong>Psychodynamic Psychotherapy</strong></p>\r\n<p>Psychodynamic Psychotherapy centres on the unconscious meaning of the issues you experience. It facilitates you in being in conscious of aspects of your inner dynamics. Psychodynamic psychotherapy aims to increase your self-comprehension to give you choices.</p>\r\n<p>The National Institute for Health and Care Excellence (NICE) England and Wales suggest psychodynamic psychotherapy for depression, self-harm and social anxiety disorder.</p>\r\n<p>&nbsp;<strong>Psychoterapy</strong></p>\r\n<p>Psychotherapy focuses on you and obtaining insight into the difficulties you face. It considers your ideas, feelings and significant moments in your history. Psychotherapy aims to help you change or discover suitable methods of managing.</p>\r\n<p>&nbsp;</p>', 353, NULL, NULL, NULL, NULL, '140, Bldg 45 Hopton Road - Woolwich Royal Arsenal - SE18 6TL', NULL, NULL, 8, NULL, NULL, NULL, '50.00', NULL, NULL, 0, 0, NULL, 'draft', NULL, 162, 162, NULL, NULL, NULL, '2020-01-13 12:05:08', '2020-02-15 08:59:12', 1, NULL, NULL, 'United Kingdom', 'London', NULL, NULL, '{\"first_name\":\"Michele\",\"last_name\":\"Lamonaca\",\"account_number\":\"06054228\",\"short_code\":\"60-83-71\"}', 1, NULL, NULL),
(247, 'Test Admin', 'test-admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, 163, NULL, '2020-01-26 05:35:24', NULL, NULL, '2020-01-15 13:50:06', '2020-01-26 05:35:24', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(248, 'Victoria Day', 'victoria-day', '<p class=\"MsoNormal\">TELEPHONE COUNSELLING SESSIONS FOR WOMEN WITH RELATIONSHIP ISSUES</p>\r\n<p class=\"MsoNormal\">I specialise in individual relationship Counselling for women over the telephone.&nbsp; Are you going through some kind of life transition? Perhaps you\'ve been experiencing relationship issues of some kind? Maybe you are lacking the emotional support you need within your family dynamic or perhaps you are having a distressing time co-parenting with an Ex?&nbsp; Then chances are you&rsquo;ll be needing someone independent from those you know, where you can talk about your feelings without fear of criticism or judgement.&nbsp; Talking to someone who can relate to what you are experiencing and can empathise with you on a deep level, will help you to connect to what you really want and need during these times of transition.&nbsp;&nbsp;</p>\r\n<p class=\"MsoNormal\">I provide therapy for those desperate to make sense of their relationships and or for those seeking support for leaving relationships that are unhealthy, toxic or emotionally abusive and are hoping to find the strength to move on and heal.</p>\r\n<p class=\"MsoNormal\">Together we will look at your relationship needs and what is missing or being neglected and then look at more effective ways for you to speak your truth &amp; own your feelings and really be honest about what you need moving forward so that you can move towards happier healthier ways of being in (or indeed out of) relationship with others so that you can move towards a more (spiritually) fulfilling life.&nbsp;&nbsp;</p>\r\n<p class=\"MsoNormal\">&nbsp;</p>', 287, NULL, NULL, NULL, NULL, '329b Torquay Rd, Paignton, UK', '50.44683810000001', '-3.5594311', 15, NULL, NULL, NULL, '40.00', NULL, NULL, 0, 0, NULL, 'draft', NULL, 164, 164, NULL, NULL, NULL, '2020-01-17 11:58:49', '2020-01-17 13:52:21', 1, NULL, '[{\"content\":\"I provide therapy for those desperate to make sense of their relationships and or for those seeking support for leaving relationships that are toxic and are hoping to find the strength to move on and heal.\"},{\"content\":\"I specialise in Telephone Counselling for women in the Uk and Europe.\"},{\"content\":\"PLEASE BE AWARE WHEN YOU ARE BOOKING A SESSION WITH ME IT IS FOR TELEPHONE COUNSELLING\"}]', 'United Kingdom', 'England', NULL, NULL, '{\"first_name\":\"Victoria\",\"last_name\":\"Day\",\"account_number\":\"16863468\",\"short_code\":\"308804\"}', 1, NULL, NULL),
(249, 'Gillian Swann', 'gillian-swann', '<p>I am a Person Centred Counsellor, with a Diploma in CBT also. I blend both modalities to suit your needs. I am a registered and accredited member of the BACP. I adhere to the BACP Ethical Framework for Good Practice. I offer face to face sessions on a short or long term basis. Telephone and online sessions can be provided depending on need. I offer a 50 minute session based in my home office environment. I have experience working with survivor\'s of sexual violence and clients\' struggling with bereavement. I also work with issues such as Depression, Anxiety, PTSD, Stress, Work related stress, pet bereavement, loss of job and relationships, anger, domestic violence and fear.</p>\r\n<p>I am passionate about the healing effects that counselling can bring and understand that client\'s sometimes feel nervous about starting the process. Therefore, i can offer a telephone assessment to discuss how we would work together. You can choose how many sessions you feel you may need, most people choose 6, but some need more and other\'s less. It\'s important that you feel in control of the number of sessions as we work collaboratively together to find a way through the issues you are stuggling with.</p>\r\n<p>I work for a charity two days per week and now see private clients at my home office for a further 2 days per week. I work with men, women and children aged 16 and over.&nbsp;</p>', 324, NULL, NULL, NULL, NULL, '118 Shelley Street, Leigh, UK', '53.508689', '-2.5366845', 15, NULL, NULL, NULL, '40.00', NULL, NULL, 0, 0, NULL, 'draft', NULL, 165, 165, NULL, NULL, NULL, '2020-01-28 11:56:52', '2020-01-28 12:27:11', 1, NULL, '[{\"content\":null}]', 'Great Britain', 'Leigh', NULL, NULL, '{\"first_name\":\"Gillian\",\"last_name\":\"Swann\",\"account_number\":\"00364750\",\"short_code\":\"110450\"}', 1, NULL, NULL),
(250, ' Jonathan Ward', 'jonathan-ward', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 166, NULL, NULL, NULL, NULL, '2020-02-13 11:43:51', '2020-02-13 11:43:51', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(251, 'jack ', 'jack', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 48, NULL, NULL, NULL, NULL, '2020-07-24 12:36:10', '2020-07-24 12:36:10', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(252, 'Misterwebhub Ansari', 'misterwebhub-ansari', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 19, NULL, NULL, NULL, NULL, '2020-07-24 12:37:59', '2020-07-24 12:37:59', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(253, 'Sandhya ', 'sandhya', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 49, NULL, NULL, NULL, NULL, '2020-07-24 18:46:05', '2020-07-24 18:46:05', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(254, 'Sandhya ', 'sandhya-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 50, NULL, NULL, NULL, NULL, '2020-07-27 12:17:34', '2020-07-27 12:17:34', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(255, 'Prem singh', 'prem-singh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'publish', NULL, 27, 1, NULL, NULL, NULL, '2020-07-27 12:27:28', '2020-07-28 04:10:45', 1, 0, NULL, NULL, NULL, NULL, NULL, '{\"first_name\":null,\"last_name\":null,\"account_number\":null,\"short_code\":null}', NULL, NULL, NULL);
INSERT INTO `bravo_tours` (`id`, `title`, `slug`, `content`, `image_id`, `banner_image_id`, `short_desc`, `category_id`, `location_id`, `address`, `map_lat`, `map_lng`, `map_zoom`, `is_featured`, `gallery`, `video`, `price`, `sale_price`, `duration`, `min_people`, `max_people`, `faqs`, `status`, `publish_date`, `create_user`, `update_user`, `deleted_at`, `origin_id`, `lang`, `created_at`, `updated_at`, `default_state`, `enable_sale_of_first_booking`, `highlights`, `country`, `city`, `discount_for_4_slot`, `discount_for_10_slot`, `bank_details`, `introductions_ownership_policy`, `age`, `faqs2`) VALUES
(256, 'Misterwebhub', 'misterwebhub', '<p>test Description Test Description</p>', 110, NULL, NULL, NULL, NULL, 'Mister Web Hub, Bakar ganj, Kidwai Nagar, Kanpur, Uttar Pradesh, India', '26.4394755', '80.3428421', 15, NULL, NULL, NULL, '12.00', NULL, NULL, 0, 0, NULL, 'publish', NULL, 44, 1, NULL, NULL, NULL, '2020-07-28 04:13:14', '2020-07-28 11:45:22', 1, 0, NULL, 'India', 'Uttar Pradesh', NULL, NULL, '{\"first_name\":\"Danish\",\"last_name\":\"Ali\",\"account_number\":\"855413132132131321\",\"short_code\":\"121\"}', 1, NULL, NULL),
(257, 'San ', 'san', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'draft', NULL, 51, NULL, NULL, NULL, NULL, '2020-07-28 12:22:47', '2020-07-28 12:22:47', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bravo_tour_category`
--

CREATE TABLE `bravo_tour_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `lang` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bravo_tour_category`
--

INSERT INTO `bravo_tour_category` (`id`, `name`, `content`, `slug`, `status`, `_lft`, `_rgt`, `parent_id`, `create_user`, `update_user`, `deleted_at`, `origin_id`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'City trips', '', 'city-trips', 'publish', 1, 2, NULL, NULL, NULL, '2019-10-07 07:35:45', NULL, NULL, '2019-10-02 21:20:42', '2019-10-07 07:35:45'),
(2, 'Ecotourism', '', 'ecotourism', 'publish', 3, 4, NULL, NULL, NULL, '2019-10-07 07:35:45', NULL, NULL, '2019-10-02 21:20:42', '2019-10-07 07:35:45'),
(3, 'Escorted tour', '', 'escorted-tour', 'publish', 5, 6, NULL, NULL, NULL, '2019-10-07 07:35:45', NULL, NULL, '2019-10-02 21:20:42', '2019-10-07 07:35:45'),
(4, 'Ligula', '', 'ligula', 'publish', 7, 8, NULL, NULL, NULL, '2019-10-07 07:35:45', NULL, NULL, '2019-10-02 21:20:42', '2019-10-07 07:35:45'),
(5, 'Newtest', NULL, 'newtest', 'publish', 9, 10, NULL, 1, NULL, '2019-10-24 10:08:25', NULL, NULL, '2019-10-24 10:07:52', '2019-10-24 10:08:25'),
(6, 'Test', NULL, 'test', 'publish', 11, 12, NULL, 1, NULL, NULL, NULL, NULL, '2020-03-05 00:37:37', '2020-03-05 00:37:37');

-- --------------------------------------------------------

--
-- Table structure for table `bravo_tour_category_translations`
--

CREATE TABLE `bravo_tour_category_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bravo_tour_dates`
--

CREATE TABLE `bravo_tour_dates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `target_id` bigint(20) DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `price` decimal(12,2) DEFAULT NULL,
  `person_types` text COLLATE utf8mb4_unicode_ci,
  `max_guests` tinyint(4) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '0',
  `note_to_customer` text COLLATE utf8mb4_unicode_ci,
  `note_to_admin` text COLLATE utf8mb4_unicode_ci,
  `is_instant` tinyint(4) DEFAULT '0',
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slots` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bravo_tour_dates`
--

INSERT INTO `bravo_tour_dates` (`id`, `target_id`, `start_date`, `end_date`, `price`, `person_types`, `max_guests`, `active`, `note_to_customer`, `note_to_admin`, `is_instant`, `create_user`, `update_user`, `created_at`, `updated_at`, `slots`) VALUES
(1, 17, '2019-09-30 20:30:00', '2019-10-01 01:30:00', '144.00', '\"false\"', 0, 1, NULL, NULL, 0, 1, NULL, '2019-10-05 03:29:50', '2019-10-05 03:29:50', NULL),
(2, 17, '2019-10-05 04:30:00', '2019-10-05 09:30:00', '144.00', '\"false\"', 0, 1, NULL, NULL, 0, 1, NULL, '2019-10-05 03:30:15', '2019-10-05 03:30:15', NULL),
(3, 17, '2019-10-06 02:30:00', '2019-10-06 07:30:00', '144.00', '\"false\"', 0, 1, NULL, NULL, 0, 1, NULL, '2019-10-05 03:30:56', '2019-10-05 03:30:56', NULL),
(4, 17, '2019-10-07 00:30:00', '2019-10-07 04:30:00', '144.00', '\"false\"', 0, 0, NULL, NULL, 0, 1, NULL, '2019-10-07 04:54:12', '2019-10-07 04:54:12', NULL),
(5, 17, '2019-10-07 22:30:00', '2019-10-08 11:30:00', '144.00', '\"false\"', 0, 1, NULL, NULL, 0, 1, NULL, '2019-10-07 04:56:10', '2019-10-07 04:56:10', NULL),
(6, 17, '2019-10-08 20:30:00', '2019-10-09 00:30:00', '144.00', '\"false\"', 0, 1, NULL, NULL, 0, 1, NULL, '2019-10-07 07:28:08', '2019-10-07 07:28:08', NULL),
(7, 17, '2019-10-08 20:30:00', '2019-10-09 00:30:00', '144.00', NULL, 0, 1, NULL, NULL, 0, 1, NULL, '2019-10-07 07:28:12', '2019-10-07 07:28:12', NULL),
(8, 17, '2019-10-17 22:30:00', '2019-10-18 10:30:00', '144.00', '\"false\"', 0, 1, NULL, NULL, 0, 1, NULL, '2019-10-07 07:28:26', '2019-10-07 07:28:26', NULL),
(9, 21, '2019-10-08 04:30:00', '2019-10-08 08:30:00', '101.00', '\"false\"', 0, 0, NULL, NULL, 0, 6, NULL, '2019-10-07 08:04:22', '2019-10-07 08:04:22', NULL),
(10, 21, '2019-10-10 01:30:00', '2019-10-10 10:30:00', '101.00', '\"false\"', 0, 0, NULL, NULL, 0, 6, NULL, '2019-10-07 08:04:28', '2019-10-07 08:04:28', NULL),
(11, 21, '2019-10-16 01:30:00', '2019-10-16 14:30:00', '101.00', '\"false\"', 0, 0, NULL, NULL, 0, 6, NULL, '2019-10-07 08:04:34', '2019-10-07 08:04:34', NULL),
(12, 21, '2019-10-16 01:30:00', '2019-10-16 14:30:00', '101.00', '\"false\"', 0, 1, NULL, NULL, 0, 6, NULL, '2019-10-07 08:04:38', '2019-10-07 08:04:38', NULL),
(13, 17, '2019-10-10 00:30:00', '2019-10-10 06:30:00', '144.00', '\"false\"', 0, 1, NULL, NULL, 0, 1, NULL, '2019-10-07 08:30:49', '2019-10-07 08:30:49', NULL),
(14, 17, '2019-10-10 00:30:00', '2019-10-10 06:30:00', '200.00', NULL, 0, 1, NULL, NULL, 0, 1, NULL, '2019-10-07 08:31:05', '2019-10-07 08:31:05', NULL),
(15, 17, '2019-10-10 23:30:00', '2019-10-11 06:30:00', '144.00', '\"false\"', 0, 1, NULL, NULL, 0, 1, NULL, '2019-10-08 08:04:27', '2019-10-08 08:04:27', NULL),
(16, 17, '2019-10-11 21:30:00', '2019-10-12 11:30:00', '144.00', '\"false\"', 0, 0, NULL, NULL, 0, 1, NULL, '2019-10-08 08:04:44', '2019-10-08 08:04:44', NULL),
(17, 17, '2019-10-11 21:30:00', '2019-10-12 11:30:00', '144.00', '\"false\"', 0, 1, NULL, NULL, 0, 1, NULL, '2019-10-08 08:04:47', '2019-10-08 08:04:47', NULL),
(18, 17, '2019-10-14 22:30:00', '2019-10-15 04:30:00', '144.00', '\"false\"', 0, 1, NULL, NULL, 0, 1, NULL, '2019-10-08 08:05:07', '2019-10-08 08:05:07', NULL),
(19, 22, '2019-10-10 01:30:00', '2019-10-10 08:30:00', '60.00', '\"false\"', NULL, 1, NULL, NULL, 0, 18, NULL, '2019-10-09 06:38:44', '2019-10-09 06:38:44', NULL),
(20, 22, '2019-10-11 05:30:00', '2019-10-11 10:30:00', '80.00', '\"false\"', NULL, 1, NULL, NULL, 0, 18, NULL, '2019-10-09 06:39:07', '2019-10-09 06:39:07', NULL),
(21, 22, '2019-10-11 23:30:00', '2019-10-12 10:30:00', NULL, '\"false\"', NULL, 0, NULL, NULL, 0, 18, NULL, '2019-10-09 06:41:46', '2019-10-09 06:41:46', NULL),
(22, 22, '2019-10-12 01:30:00', '2019-10-12 10:30:00', NULL, '\"false\"', NULL, 1, NULL, NULL, 0, 18, NULL, '2019-10-09 06:41:52', '2019-10-09 06:41:52', NULL),
(23, 22, '2019-10-16 20:30:00', '2019-10-16 21:30:00', NULL, '\"false\"', NULL, 0, NULL, NULL, 0, 18, NULL, '2019-10-18 10:38:43', '2019-10-18 10:38:43', NULL),
(24, 17, '2019-12-10 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, '2019-11-12 06:44:25', '2019-11-12 06:44:25', '[{\"slot\":\"3\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"144.00\"}]'),
(25, 23, '2019-11-11 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 20, 20, '2019-11-13 09:07:57', '2019-11-13 10:05:07', '[{\"slot\":\"9\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"12\",\"active\":\"2\",\"first_name\":\"Andy Davis\",\"notes\":\"test\",\"price\":null}]'),
(26, 17, '2019-11-10 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, 1, '2019-11-13 10:09:52', '2019-11-13 10:12:07', '[{\"slot\":\"9\",\"active\":\"1\",\"first_name\":\"djjr\",\"notes\":\";jwrilk\",\"price\":\"0\"}]'),
(27, 17, '2019-11-11 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, 1, '2019-11-13 10:10:04', '2019-11-13 10:13:00', '[{\"slot\":\"9\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"144.00\"}]'),
(28, 22, '2019-11-11 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 18, 18, '2019-11-14 14:20:18', '2019-11-14 14:20:22', '[{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(29, 22, '2019-12-02 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 18, NULL, '2019-11-14 14:21:24', '2019-11-14 14:21:24', '[{\"slot\":\"12\",\"active\":\"2\",\"first_name\":\"Andy Davis\",\"notes\":\"test\",\"price\":null}]'),
(30, 22, '2019-11-18 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 18, NULL, '2019-11-17 08:31:42', '2019-11-17 08:31:42', '[{\"slot\":\"12\",\"active\":\"2\",\"first_name\":\"John Doe\",\"notes\":\"Booking via referral.\\n\\nClient is struggling with depression.\",\"price\":null}]'),
(31, 77, '2019-11-16 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 66, 66, '2019-11-19 04:40:18', '2019-11-19 04:40:32', '[{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"50.00\"},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"50.00\"}]'),
(32, 105, '2019-11-20 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 81, 81, '2019-11-19 10:27:29', '2019-11-19 10:27:43', '[{\"slot\":\"21\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(33, 105, '2019-11-17 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 81, 81, '2019-11-19 10:28:35', '2019-11-19 10:29:09', '[{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(34, 105, '2019-11-19 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 81, 81, '2019-11-19 10:29:17', '2019-11-19 10:29:52', '[{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(35, 109, '2019-11-18 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 83, 83, '2019-11-19 10:57:23', '2019-11-19 11:03:40', '[{\"slot\":\"10\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"60\"},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"60\"},{\"slot\":\"7\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"8\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"60\"},{\"slot\":\"17\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"0\"},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"0\"},{\"slot\":\"21\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"22\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"6\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(36, 109, '2019-11-16 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 83, NULL, '2019-11-19 10:57:33', '2019-11-19 10:57:33', '[{\"slot\":\"6\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(37, 109, '2019-11-19 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 83, 83, '2019-11-19 11:00:49', '2019-11-19 11:03:32', '[{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"60\"},{\"slot\":\"11\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"0\"},{\"slot\":\"14\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"8\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"7\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"6\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"21\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"22\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(38, 109, '2019-11-20 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 83, 83, '2019-11-19 11:02:41', '2019-11-19 11:04:18', '[{\"slot\":\"7\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"8\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"6\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(39, 121, '2019-11-16 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 89, NULL, '2019-11-19 13:35:27', '2019-11-19 13:35:27', '[{\"slot\":\"6\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"}]'),
(40, 121, '2019-11-17 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 89, 89, '2019-11-19 13:35:46', '2019-11-19 13:35:50', '[{\"slot\":\"7\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"},{\"slot\":\"6\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"}]'),
(41, 121, '2019-11-18 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 89, NULL, '2019-11-19 13:36:11', '2019-11-19 13:36:11', '[{\"slot\":\"6\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"}]'),
(42, 121, '2019-11-19 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 89, NULL, '2019-11-19 13:36:13', '2019-11-19 13:36:13', '[{\"slot\":\"6\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"}]'),
(43, 121, '2019-11-21 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 89, NULL, '2019-11-19 13:36:23', '2019-11-19 13:36:23', '[{\"slot\":\"6\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"}]'),
(44, 134, '2019-11-16 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 96, NULL, '2019-11-19 19:06:56', '2019-11-19 19:06:56', '[{\"slot\":\"8\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(45, 134, '2019-11-19 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 96, NULL, '2019-11-19 19:07:11', '2019-11-19 19:07:11', '[{\"slot\":\"8\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(46, 132, '2019-11-26 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 95, NULL, '2019-11-19 19:09:59', '2019-11-19 19:09:59', '[{\"slot\":\"7\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(47, 132, '2019-11-27 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 95, 95, '2019-11-19 19:10:17', '2019-11-19 19:10:31', '[{\"slot\":\"7\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"75\"}]'),
(48, 134, '2019-11-17 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 96, 96, '2019-11-19 19:21:31', '2019-11-19 19:21:38', '[{\"slot\":\"8\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"0\"}]'),
(49, 107, '2019-11-16 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 82, 82, '2019-11-20 01:35:18', '2019-11-20 01:35:33', '[{\"slot\":\"6\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"7\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(50, 115, '2019-11-19 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 86, 86, '2019-11-20 01:37:37', '2019-11-20 01:43:14', '[{\"slot\":\"9\",\"active\":\"2\",\"first_name\":\"TT030\",\"notes\":null,\"price\":\"0\"},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"2\",\"first_name\":\"TT031\",\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"2\",\"first_name\":\"TT033\",\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"2\",\"first_name\":\"BCT004\",\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"21\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(51, 115, '2019-11-20 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 86, 86, '2019-11-20 01:44:25', '2019-11-20 01:46:32', '[{\"slot\":\"9\",\"active\":\"2\",\"first_name\":\"PC127\",\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"2\",\"first_name\":\"PC138\",\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"2\",\"first_name\":\"BCT021\",\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"2\",\"first_name\":\"PC132\",\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"2\",\"first_name\":\"PC095\",\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"2\",\"first_name\":\"PC096\",\"notes\":null,\"price\":\"PC096\"},{\"slot\":\"21\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(52, 151, '2019-11-16 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, 107, '2019-11-20 07:59:39', '2019-11-20 07:59:46', '[{\"slot\":\"6\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"7\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(53, 151, '2019-11-20 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, 107, '2019-11-20 08:00:08', '2019-11-20 08:06:07', '[{\"slot\":\"6\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"7\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"2\",\"first_name\":\"Ata\",\"notes\":\"consultation\",\"price\":null},{\"slot\":\"18\",\"active\":\"2\",\"first_name\":\"HA77\",\"notes\":null,\"price\":null},{\"slot\":\"22\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(54, 151, '2019-11-21 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, 107, '2019-11-20 08:00:32', '2019-11-20 08:06:13', '[{\"slot\":\"6\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"7\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"22\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(55, 151, '2019-11-22 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, 107, '2019-11-20 08:00:48', '2019-11-20 08:07:17', '[{\"slot\":\"6\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"7\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"22\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"21\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(56, 151, '2019-11-19 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, 107, '2019-11-20 08:05:32', '2019-11-20 08:05:59', '[{\"slot\":\"11\",\"active\":\"2\",\"first_name\":\"HA76\",\"notes\":null,\"price\":null},{\"slot\":\"22\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(57, 151, '2019-11-23 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, 107, '2019-11-20 08:07:50', '2019-11-20 08:09:10', '[{\"slot\":\"6\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"7\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"8\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(58, 151, '2019-11-26 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, NULL, '2019-11-20 08:11:47', '2019-11-20 08:11:47', '[{\"slot\":\"11\",\"active\":\"2\",\"first_name\":\"HA77\",\"notes\":null,\"price\":null}]'),
(59, 151, '2019-11-27 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, 107, '2019-11-20 08:22:10', '2019-11-20 08:23:16', '[{\"slot\":\"13\",\"active\":\"2\",\"first_name\":\"HA75\",\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"2\",\"first_name\":\"new client\",\"notes\":\"consultation\",\"price\":null},{\"slot\":\"18\",\"active\":\"2\",\"first_name\":\"HA76\",\"notes\":null,\"price\":null}]'),
(60, 151, '2019-11-28 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, 107, '2019-11-20 08:23:47', '2019-11-20 08:24:45', '[{\"slot\":\"8\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(61, 151, '2019-11-29 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, 107, '2019-11-20 08:24:53', '2019-11-20 08:25:48', '[{\"slot\":\"8\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(62, 151, '2019-12-01 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, 107, '2019-11-20 08:26:43', '2019-11-20 08:27:15', '[{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(63, 151, '2019-12-03 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, 107, '2019-11-20 08:35:04', '2019-11-20 08:35:19', '[{\"slot\":\"11\",\"active\":\"2\",\"first_name\":\"HA77\",\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(64, 151, '2019-12-04 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, 107, '2019-11-20 08:35:28', '2019-11-20 08:35:34', '[{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(65, 151, '2019-12-06 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, NULL, '2019-11-20 08:36:13', '2019-11-20 08:36:13', '[{\"slot\":\"10\",\"active\":\"2\",\"first_name\":\"new client\",\"notes\":null,\"price\":null}]'),
(66, 151, '2019-12-10 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, 107, '2019-11-20 08:37:02', '2019-11-20 08:38:11', '[{\"slot\":\"9\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"0\"},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"2\",\"first_name\":\"HA77\",\"notes\":null,\"price\":\"0\"},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(67, 151, '2019-12-13 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, 107, '2019-11-20 08:38:41', '2019-11-20 08:40:03', '[{\"slot\":\"8\",\"active\":\"2\",\"first_name\":\"Teaching\",\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"2\",\"first_name\":\"teaching\",\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"2\",\"first_name\":\"Teaching\",\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"2\",\"first_name\":\"Teaching\",\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"2\",\"first_name\":\"Teaching\",\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"2\",\"first_name\":\"Teaching\",\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"2\",\"first_name\":\"Teaching\",\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"2\",\"first_name\":\"Teaching\",\"notes\":null,\"price\":null}]'),
(68, 151, '2019-12-15 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, 107, '2019-11-20 08:40:49', '2019-11-20 08:41:31', '[{\"slot\":\"11\",\"active\":\"2\",\"first_name\":\"tutor meeting\",\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(69, 151, '2019-12-17 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, NULL, '2019-11-20 10:02:43', '2019-11-20 10:02:43', '[{\"slot\":\"11\",\"active\":\"2\",\"first_name\":\"HA76\",\"notes\":null,\"price\":null}]'),
(70, 171, '2019-11-19 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 117, 117, '2019-11-20 11:26:14', '2019-11-20 11:30:12', '[{\"slot\":\"10\",\"active\":\"2\",\"first_name\":\"BW\",\"notes\":null,\"price\":\"0\"},{\"slot\":\"15\",\"active\":\"2\",\"first_name\":\"LC\",\"notes\":null,\"price\":\"0\"},{\"slot\":\"18\",\"active\":\"2\",\"first_name\":\"NA\",\"notes\":null,\"price\":\"0\"},{\"slot\":\"19\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"85.00\"}]'),
(71, 171, '2019-11-17 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 117, 117, '2019-11-20 11:27:40', '2019-11-20 11:27:54', '[{\"slot\":\"12\",\"active\":\"2\",\"first_name\":\"KK\",\"notes\":null,\"price\":\"0\"},{\"slot\":\"13\",\"active\":\"2\",\"first_name\":\"BOC\",\"notes\":null,\"price\":null}]'),
(72, 171, '2019-11-20 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 117, 117, '2019-11-20 11:28:15', '2019-11-20 11:30:18', '[{\"slot\":\"16\",\"active\":\"2\",\"first_name\":\"RWR\",\"notes\":null,\"price\":\"0\"},{\"slot\":\"17\",\"active\":\"2\",\"first_name\":\"RWR\",\"notes\":null,\"price\":\"0\"},{\"slot\":\"18\",\"active\":\"2\",\"first_name\":\"RWR\",\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"85.00\"}]'),
(73, 171, '2019-11-21 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 117, NULL, '2019-11-20 11:30:26', '2019-11-20 11:30:26', '[{\"slot\":\"19\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"85.00\"}]'),
(74, 171, '2019-11-18 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 117, 117, '2019-11-20 11:31:03', '2019-11-20 11:32:15', '[{\"slot\":\"19\",\"active\":\"2\",\"first_name\":\"HR\",\"notes\":null,\"price\":null},{\"slot\":\"8\",\"active\":\"2\",\"first_name\":\"HS\",\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"2\",\"first_name\":\"HS\",\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"2\",\"first_name\":\"HS\",\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"2\",\"first_name\":\"HS\",\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"2\",\"first_name\":\"CS\",\"notes\":null,\"price\":null}]'),
(75, 175, '2019-11-22 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 119, 119, '2019-11-20 13:34:41', '2019-11-20 13:36:29', '[{\"slot\":\"11\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"\\u00a380\"},{\"slot\":\"10\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(76, 175, '2019-11-21 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 119, 119, '2019-11-20 13:35:17', '2019-11-20 13:35:36', '[{\"slot\":\"11\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(77, 151, '2019-12-02 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, NULL, '2019-11-21 05:35:53', '2019-11-21 05:35:53', '[{\"slot\":\"10\",\"active\":\"2\",\"first_name\":\"Ata\",\"notes\":null,\"price\":null}]'),
(78, 151, '2019-12-09 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, NULL, '2019-11-21 05:36:12', '2019-11-21 05:36:12', '[{\"slot\":\"10\",\"active\":\"2\",\"first_name\":\"Ata\",\"notes\":null,\"price\":null}]'),
(79, 151, '2019-12-16 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, NULL, '2019-11-21 05:36:30', '2019-11-21 05:36:30', '[{\"slot\":\"10\",\"active\":\"2\",\"first_name\":\"ata\",\"notes\":null,\"price\":null}]'),
(80, 151, '2019-12-19 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, 107, '2019-11-21 05:37:01', '2019-11-21 05:37:52', '[{\"slot\":\"8\",\"active\":\"2\",\"first_name\":\"Mind\",\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"2\",\"first_name\":\"Mind\",\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"2\",\"first_name\":\"Mind\",\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"2\",\"first_name\":\"Mind\",\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"2\",\"first_name\":\"Mind\",\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"2\",\"first_name\":\"Mind\",\"notes\":null,\"price\":null}]'),
(81, 151, '2019-12-12 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 107, 107, '2019-11-21 05:38:24', '2019-11-21 05:39:25', '[{\"slot\":\"8\",\"active\":\"2\",\"first_name\":\"Mind\",\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"2\",\"first_name\":\"Mind\",\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"2\",\"first_name\":\"Mind\",\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"2\",\"first_name\":\"Mind\",\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"2\",\"first_name\":\"Mind\",\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"2\",\"first_name\":\"Mind\",\"notes\":null,\"price\":null}]'),
(82, 181, '2019-11-17 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 122, 122, '2019-11-21 05:54:57', '2019-11-21 06:03:13', '[{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"8\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(83, 181, '2019-11-20 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 122, 122, '2019-11-21 05:56:52', '2019-11-21 05:57:27', '[{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(84, 181, '2019-12-04 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 122, 122, '2019-11-21 05:58:46', '2019-11-29 19:42:00', '[{\"slot\":\"16\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"60\"},{\"slot\":\"18\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"60\"},{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(85, 181, '2019-12-01 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 122, 122, '2019-11-21 05:59:18', '2019-11-21 06:08:08', '[{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"0\"},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"7\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(86, 181, '2019-11-25 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 122, 122, '2019-11-21 06:05:40', '2019-11-21 06:10:09', '[{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"8\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(87, 181, '2019-11-24 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 122, 122, '2019-11-21 06:06:22', '2019-11-21 06:11:05', '[{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"8\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(88, 181, '2019-11-27 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 122, 122, '2019-11-21 06:06:55', '2019-11-21 06:07:20', '[{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(89, 181, '2019-12-02 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 122, 122, '2019-11-21 06:13:20', '2019-11-29 19:41:11', '[{\"slot\":\"8\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"60.00\"},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"60.00\"}]'),
(90, 181, '2019-12-08 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 122, 122, '2019-11-21 06:14:38', '2019-11-21 06:15:27', '[{\"slot\":\"8\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(91, 181, '2019-12-09 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 122, 122, '2019-11-21 06:15:40', '2019-11-21 06:16:02', '[{\"slot\":\"8\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(92, 181, '2019-12-11 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 122, 122, '2019-11-21 06:16:30', '2019-11-21 06:16:45', '[{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(93, 181, '2019-12-16 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 122, 122, '2019-11-21 06:17:06', '2019-11-21 06:18:18', '[{\"slot\":\"8\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(94, 181, '2019-12-18 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 122, 122, '2019-11-21 06:18:24', '2019-11-21 06:18:55', '[{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"0\"},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(95, 181, '2019-12-15 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 122, 122, '2019-11-21 06:19:16', '2019-11-21 06:19:30', '[{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(96, 107, '2019-11-19 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 82, 82, '2019-11-21 15:05:02', '2019-11-21 15:05:51', '[{\"slot\":\"14\",\"active\":\"2\",\"first_name\":\"ScBu\",\"notes\":null,\"price\":\"50.00\"},{\"slot\":\"16\",\"active\":\"2\",\"first_name\":\"MaGe\",\"notes\":null,\"price\":\"50.00\"},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"50.00\"}]'),
(97, 107, '2019-11-20 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 82, NULL, '2019-11-21 15:06:18', '2019-11-21 15:06:18', '[{\"slot\":\"14\",\"active\":\"2\",\"first_name\":\"Di\",\"notes\":null,\"price\":\"50.00\"}]'),
(98, 107, '2019-11-22 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 82, NULL, '2019-11-21 15:07:19', '2019-11-21 15:07:19', '[{\"slot\":\"11\",\"active\":\"2\",\"first_name\":\"Glasses\",\"notes\":null,\"price\":\"50.00\"}]'),
(99, 107, '2019-11-24 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 82, 82, '2019-11-21 15:08:18', '2019-11-21 15:08:47', '[{\"slot\":\"14\",\"active\":\"2\",\"first_name\":\"Alre\",\"notes\":null,\"price\":\"50.00\"},{\"slot\":\"17\",\"active\":\"2\",\"first_name\":\"Lebe\",\"notes\":null,\"price\":\"50.00\"}]'),
(100, 107, '2019-11-25 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 82, NULL, '2019-11-21 15:09:26', '2019-11-21 15:09:26', '[{\"slot\":\"14\",\"active\":\"2\",\"first_name\":\"Yoga\",\"notes\":null,\"price\":\"50.00\"}]'),
(101, 107, '2019-11-26 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 82, 82, '2019-11-21 15:10:05', '2019-11-21 15:11:29', '[{\"slot\":\"13\",\"active\":\"2\",\"first_name\":\"Eisi\",\"notes\":null,\"price\":\"50.00\"},{\"slot\":\"14\",\"active\":\"2\",\"first_name\":\"Eisi\",\"notes\":null,\"price\":\"50.00\"},{\"slot\":\"17\",\"active\":\"2\",\"first_name\":\"Mage\",\"notes\":null,\"price\":\"50.00\"}]'),
(102, 107, '2019-11-27 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 82, NULL, '2019-11-21 15:12:03', '2019-11-21 15:12:03', '[{\"slot\":\"14\",\"active\":\"2\",\"first_name\":\"Scbu\",\"notes\":null,\"price\":\"50.00\"}]'),
(103, 107, '2019-11-28 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 82, NULL, '2019-11-21 15:12:43', '2019-11-21 15:12:43', '[{\"slot\":\"13\",\"active\":\"2\",\"first_name\":\"Nails\",\"notes\":null,\"price\":\"50.00\"}]'),
(104, 107, '2019-12-03 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 82, NULL, '2019-11-21 15:14:20', '2019-11-21 15:14:20', '[{\"slot\":\"17\",\"active\":\"2\",\"first_name\":\"Mage\",\"notes\":null,\"price\":\"50.00\"}]'),
(105, 107, '2019-12-05 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 82, 82, '2019-11-21 15:14:48', '2019-11-21 15:15:13', '[{\"slot\":\"16\",\"active\":\"2\",\"first_name\":\"Ware\",\"notes\":null,\"price\":\"50.00\"},{\"slot\":\"17\",\"active\":\"2\",\"first_name\":\"Ware\",\"notes\":null,\"price\":\"50.00\"}]'),
(106, 107, '2019-12-08 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 82, NULL, '2019-11-21 15:16:19', '2019-11-21 15:16:19', '[{\"slot\":\"14\",\"active\":\"2\",\"first_name\":\"Weli\",\"notes\":null,\"price\":\"50.00\"}]'),
(107, 107, '2019-12-10 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 82, NULL, '2019-11-21 15:16:49', '2019-11-21 15:16:49', '[{\"slot\":\"17\",\"active\":\"2\",\"first_name\":\"Mage\",\"notes\":null,\"price\":\"50.00\"}]'),
(108, 186, '2019-11-19 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 124, 124, '2019-11-22 08:14:44', '2019-11-22 08:14:58', '[{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(109, 186, '2019-11-26 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 124, 124, '2019-11-22 08:15:14', '2019-11-22 08:15:56', '[{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"14\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"15\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"}]'),
(110, 186, '2019-11-27 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 124, 124, '2019-11-22 08:16:48', '2019-11-22 09:06:15', '[{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"0\"},{\"slot\":\"14\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"0\"},{\"slot\":\"15\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(111, 186, '2019-12-03 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 124, NULL, '2019-11-22 08:27:15', '2019-11-22 08:27:15', '[{\"slot\":\"15\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(112, 186, '2019-12-04 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 124, 124, '2019-11-22 08:27:27', '2019-11-22 08:27:33', '[{\"slot\":\"13\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(113, 186, '2019-12-10 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 124, 124, '2019-11-22 08:27:57', '2019-11-22 08:30:05', '[{\"slot\":\"12\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"10\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"11\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"15\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"}]'),
(114, 186, '2019-12-11 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 124, 124, '2019-11-22 08:30:11', '2019-11-22 08:32:35', '[{\"slot\":\"9\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"10\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"11\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"12\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"13\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"14\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"15\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]');
INSERT INTO `bravo_tour_dates` (`id`, `target_id`, `start_date`, `end_date`, `price`, `person_types`, `max_guests`, `active`, `note_to_customer`, `note_to_admin`, `is_instant`, `create_user`, `update_user`, `created_at`, `updated_at`, `slots`) VALUES
(115, 186, '2019-12-18 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 124, 124, '2019-11-22 08:45:16', '2019-11-22 08:47:16', '[{\"slot\":\"9\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"10\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"11\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"12\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"13\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"14\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"15\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"16\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(116, 186, '2019-12-17 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 124, 124, '2019-11-22 08:45:28', '2019-11-22 08:46:00', '[{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(117, 186, '2019-12-24 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 124, 124, '2019-11-22 08:48:31', '2019-11-22 08:49:25', '[{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(118, 186, '2019-12-25 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 124, 124, '2019-11-22 08:49:31', '2019-11-22 08:51:18', '[{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(119, 194, '2019-11-28 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 128, 128, '2019-11-22 11:36:43', '2019-11-22 11:39:16', '[{\"slot\":\"6\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"75.00\"},{\"slot\":\"7\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"75.00\"},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"75.00\"},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"75.00\"},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"75.00\"}]'),
(120, 194, '2019-11-24 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 128, 128, '2019-11-22 11:38:12', '2019-11-22 11:38:33', '[{\"slot\":\"8\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"75.00\"},{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"75.00\"},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"75.00\"},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"75.00\"},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"75.00\"},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"75.00\"}]'),
(121, 194, '2019-11-26 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 128, 128, '2019-11-22 11:38:37', '2019-11-22 11:38:54', '[{\"slot\":\"8\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"75.00\"},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"75.00\"},{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"75.00\"},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"75.00\"},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"75.00\"},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"75.00\"}]'),
(122, 196, '2019-11-27 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 129, 129, '2019-11-22 18:23:35', '2019-11-22 18:24:56', '[{\"slot\":\"6\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"7\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(123, 198, '2019-11-21 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 130, 130, '2019-11-22 22:29:47', '2019-11-22 22:30:00', '[{\"slot\":\"19\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(124, 198, '2019-11-17 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 130, NULL, '2019-11-22 22:31:18', '2019-11-22 22:31:18', '[{\"slot\":\"10\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"\\u00a340\"}]'),
(125, 204, '2019-11-23 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 133, NULL, '2019-11-24 11:50:36', '2019-11-24 11:50:36', '[{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(126, 210, '2019-11-23 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 136, 136, '2019-11-25 08:07:46', '2019-11-25 08:08:00', '[{\"slot\":\"6\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"7\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"8\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(127, 210, '2019-11-25 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 136, 136, '2019-11-25 08:08:52', '2019-11-25 08:09:18', '[{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"0\"},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(128, 212, '2019-11-25 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 137, 137, '2019-11-25 10:43:46', '2019-11-25 10:43:52', '[{\"slot\":\"9\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(129, 212, '2019-11-28 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 137, 137, '2019-11-25 10:44:10', '2019-11-25 10:44:21', '[{\"slot\":\"11\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(130, 212, '2019-11-24 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 137, 137, '2019-11-25 10:44:28', '2019-11-25 10:44:36', '[{\"slot\":\"8\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"9\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(131, 214, '2019-11-24 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 138, 138, '2019-11-25 15:22:28', '2019-11-25 15:22:36', '[{\"slot\":\"11\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(132, 214, '2019-11-26 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 138, 138, '2019-11-25 15:22:50', '2019-11-25 15:23:04', '[{\"slot\":\"15\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(133, 214, '2019-11-27 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 138, 138, '2019-11-25 15:23:09', '2019-11-25 15:23:21', '[{\"slot\":\"15\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(134, 214, '2019-11-28 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 138, 138, '2019-11-25 15:23:32', '2019-11-25 15:23:47', '[{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(135, 216, '2019-11-25 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 139, 139, '2019-11-26 03:55:53', '2019-11-26 03:56:04', '[{\"slot\":\"18\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(136, 216, '2019-11-27 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 139, 139, '2019-11-26 03:56:16', '2019-11-26 03:56:28', '[{\"slot\":\"18\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(137, 216, '2019-12-01 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 139, NULL, '2019-11-26 03:57:20', '2019-11-26 03:57:20', '[{\"slot\":\"19\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(138, 216, '2019-12-03 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 139, NULL, '2019-11-26 03:57:34', '2019-11-26 03:57:34', '[{\"slot\":\"19\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(139, 149, '2019-11-26 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 106, 106, '2019-11-28 09:39:24', '2019-11-28 09:39:33', '[{\"slot\":\"9\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"}]'),
(140, 210, '2019-12-02 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 136, 136, '2019-12-02 11:54:00', '2019-12-02 11:55:38', '[{\"slot\":\"19\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"\\u00a345\"},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(141, 44, '2019-12-02 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 41, 41, '2019-12-03 10:21:26', '2019-12-03 10:22:23', '[{\"slot\":\"16\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"1.00\"},{\"slot\":\"17\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"1.00\"}]'),
(142, 230, '2019-11-30 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 147, NULL, '2019-12-04 04:04:34', '2019-12-04 04:04:34', '[{\"slot\":\"12\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"80\"}]'),
(143, 230, '2020-01-21 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 147, NULL, '2019-12-04 04:05:14', '2019-12-04 04:05:14', '[{\"slot\":\"9\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"80\"}]'),
(144, 167, '2019-12-01 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 115, NULL, '2019-12-05 10:17:04', '2019-12-05 10:17:04', '[{\"slot\":\"6\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"}]'),
(145, 234, '2019-12-01 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 150, 150, '2019-12-07 01:58:55', '2019-12-07 01:58:59', '[{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"100.00\"},{\"slot\":\"15\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"100.00\"}]'),
(146, 234, '2019-12-02 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 150, NULL, '2019-12-07 01:59:04', '2019-12-07 01:59:04', '[{\"slot\":\"10\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"100.00\"}]'),
(147, 234, '2019-11-30 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 150, NULL, '2019-12-07 01:59:09', '2019-12-07 01:59:09', '[{\"slot\":\"10\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"100.00\"}]'),
(148, 190, '2019-12-09 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 126, NULL, '2019-12-08 22:58:24', '2019-12-08 22:58:24', '[{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(149, 190, '2019-12-12 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 126, 126, '2019-12-08 22:58:35', '2019-12-08 22:58:46', '[{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(150, 77, '2019-12-07 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 66, 66, '2019-12-13 07:36:25', '2019-12-13 07:36:40', '[{\"slot\":\"16\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"60.00\"}]'),
(151, 77, '2019-12-09 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 66, 66, '2019-12-13 07:37:50', '2019-12-13 08:58:11', '[{\"slot\":\"8\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"60.00\"},{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"60.00\"},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"60.00\"},{\"slot\":\"11\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"60.00\"}]'),
(152, 77, '2019-12-23 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 66, NULL, '2019-12-13 11:00:22', '2019-12-13 11:00:22', '[{\"slot\":\"9\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"60.00\"}]'),
(153, 115, '2019-12-22 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 86, 86, '2019-12-17 07:30:04', '2019-12-17 07:30:39', '[{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"50.00\"},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"50.00\"},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"50.00\"},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"50.00\"},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"50.00\"},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"50.00\"},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"50.00\"},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"50.00\"}]'),
(154, 240, '2019-12-16 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 155, NULL, '2019-12-19 11:05:24', '2019-12-19 11:05:24', '[{\"slot\":\"9\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"65.00\"}]'),
(155, 239, '2019-12-16 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 46, 46, '2019-12-19 11:07:49', '2019-12-19 11:07:57', '[{\"slot\":\"10\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"}]'),
(156, 239, '2019-12-17 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 46, 46, '2019-12-19 11:08:10', '2019-12-19 11:11:07', '[{\"slot\":\"13\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"}]'),
(157, 239, '2019-12-23 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 46, 46, '2019-12-19 11:09:57', '2019-12-19 11:10:01', '[{\"slot\":\"10\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"}]'),
(158, 239, '2019-12-24 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 46, NULL, '2019-12-19 11:10:13', '2019-12-19 11:10:13', '[{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"}]'),
(159, 239, '2019-12-25 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 46, 46, '2019-12-19 11:10:18', '2019-12-19 11:10:28', '[{\"slot\":\"13\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"}]'),
(160, 239, '2019-12-18 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 46, NULL, '2019-12-19 11:11:16', '2019-12-19 11:11:16', '[{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"}]'),
(161, 75, '2019-12-23 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 65, 65, '2019-12-19 11:50:50', '2019-12-19 11:51:08', '[{\"slot\":\"11\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50.00\"},{\"slot\":\"16\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50.00\"}]'),
(162, 97, '2020-01-05 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 77, 77, '2019-12-30 11:10:31', '2019-12-30 12:00:02', '[{\"slot\":\"10\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"45\"},{\"slot\":\"13\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"}]'),
(163, 97, '2019-12-29 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 77, 77, '2019-12-30 11:12:15', '2019-12-30 11:14:24', '[{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40\"},{\"slot\":\"10\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"45\"},{\"slot\":\"11\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"45\"},{\"slot\":\"12\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"45\"},{\"slot\":\"13\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(164, 97, '2019-12-30 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 77, 77, '2019-12-30 11:14:12', '2019-12-30 11:14:19', '[{\"slot\":\"19\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"45\"},{\"slot\":\"20\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(165, 97, '2019-12-31 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 77, 77, '2019-12-30 11:14:43', '2019-12-30 11:14:50', '[{\"slot\":\"19\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"45\"}]'),
(166, 97, '2020-01-01 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 77, 77, '2019-12-30 11:15:18', '2019-12-30 11:15:58', '[{\"slot\":\"9\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"0\"},{\"slot\":\"10\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"45\"},{\"slot\":\"11\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"45\"},{\"slot\":\"12\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"45\"}]'),
(167, 97, '2020-01-06 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 77, NULL, '2019-12-30 12:00:12', '2019-12-30 12:00:12', '[{\"slot\":\"20\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"}]'),
(168, 97, '2020-01-07 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 77, NULL, '2019-12-30 12:00:19', '2019-12-30 12:00:19', '[{\"slot\":\"19\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"}]'),
(169, 97, '2020-01-08 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 77, NULL, '2019-12-30 12:00:28', '2019-12-30 12:00:28', '[{\"slot\":\"10\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"}]'),
(170, 97, '2020-01-12 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 77, NULL, '2019-12-30 12:00:44', '2019-12-30 12:00:44', '[{\"slot\":\"13\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"}]'),
(171, 97, '2020-01-13 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 77, 77, '2019-12-30 12:00:59', '2019-12-30 12:01:06', '[{\"slot\":\"19\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"},{\"slot\":\"20\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"}]'),
(172, 97, '2020-01-14 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 77, 77, '2019-12-30 12:01:19', '2019-12-30 12:01:32', '[{\"slot\":\"20\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"},{\"slot\":\"19\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"}]'),
(173, 244, '2020-01-04 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 160, 160, '2020-01-07 04:45:37', '2020-01-07 04:45:45', '[{\"slot\":\"6\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"200\"}]'),
(174, 17, '2020-01-07 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, '2020-01-07 11:55:31', '2020-01-07 11:55:31', '[{\"slot\":\"7\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"144.00\"}]'),
(175, 230, '2020-01-04 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 147, NULL, '2020-01-08 04:06:47', '2020-01-08 04:06:47', '[{\"slot\":\"8\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"}]'),
(176, 230, '2020-01-09 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 147, 147, '2020-01-08 04:07:09', '2020-01-08 04:08:22', '[{\"slot\":\"12\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"},{\"slot\":\"11\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"},{\"slot\":\"16\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"},{\"slot\":\"9\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"}]'),
(177, 244, '2020-01-05 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 160, 160, '2020-01-09 08:24:17', '2020-01-09 08:24:26', '[{\"slot\":\"10\",\"active\":\"2\",\"first_name\":\"Mike\",\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"2\",\"first_name\":\"David\",\"notes\":null,\"price\":null}]'),
(178, 245, '2020-01-04 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 161, NULL, '2020-01-10 06:26:49', '2020-01-10 06:26:49', '[{\"slot\":\"13\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"70\"}]'),
(179, 245, '2020-01-05 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 161, NULL, '2020-01-10 06:27:01', '2020-01-10 06:27:01', '[{\"slot\":\"11\",\"active\":\"2\",\"first_name\":\"Brian\",\"notes\":null,\"price\":null}]'),
(180, 246, '2020-01-11 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 162, 162, '2020-01-13 12:05:52', '2020-01-13 12:06:22', '[{\"slot\":\"14\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"\\u00a3 50\"}]'),
(181, 175, '2020-01-11 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 119, 119, '2020-01-16 08:39:21', '2020-01-16 08:41:52', '[{\"slot\":\"10\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"80.00\"}]'),
(182, 248, '2020-01-11 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 164, NULL, '2020-01-17 13:09:04', '2020-01-17 13:09:04', '[{\"slot\":\"10\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"}]'),
(183, 248, '2020-01-19 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 164, NULL, '2020-01-17 13:09:36', '2020-01-17 13:09:36', '[{\"slot\":\"10\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"}]'),
(184, 248, '2020-01-21 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 164, NULL, '2020-01-17 13:45:36', '2020-01-17 13:45:36', '[{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"}]'),
(185, 248, '2020-01-28 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 164, NULL, '2020-01-17 13:46:17', '2020-01-17 13:46:17', '[{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"}]'),
(186, 248, '2020-02-13 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 164, 164, '2020-01-17 13:47:25', '2020-01-17 13:47:34', '[{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"}]'),
(187, 248, '2020-02-16 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 164, 164, '2020-01-17 13:47:50', '2020-01-17 13:48:07', '[{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"}]'),
(188, 248, '2020-02-18 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 164, 164, '2020-01-17 13:48:12', '2020-01-17 13:48:32', '[{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"}]'),
(189, 248, '2020-02-19 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 164, 164, '2020-01-17 13:48:37', '2020-01-17 13:48:55', '[{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"}]'),
(190, 248, '2020-02-20 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 164, 164, '2020-01-17 13:49:00', '2020-01-17 13:49:09', '[{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"}]'),
(191, 248, '2020-02-24 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 164, 164, '2020-01-17 13:49:29', '2020-01-17 13:49:35', '[{\"slot\":\"10\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"}]'),
(192, 17, '2020-01-20 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, '2020-01-25 01:17:10', '2020-01-25 01:17:10', '[{\"slot\":\"15\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"144.00\"}]'),
(193, 249, '2020-01-26 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 165, 165, '2020-01-28 11:58:19', '2020-01-28 12:28:08', '[{\"slot\":\"11\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"}]'),
(194, 249, '2020-01-05 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 165, NULL, '2020-01-28 12:28:41', '2020-01-28 12:28:41', '[{\"slot\":\"11\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"40.00\"}]'),
(195, 246, '2020-02-08 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 162, 162, '2020-02-14 12:57:53', '2020-02-14 13:16:39', '[{\"slot\":\"7\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"9\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"10\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"11\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"12\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"13\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"14\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"15\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"16\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"17\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"18\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"19\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"20\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"21\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"}]'),
(196, 246, '2020-02-09 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 162, 162, '2020-02-14 12:58:29', '2020-02-14 13:13:08', '[{\"slot\":\"15\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"9\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"21\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(197, 246, '2020-02-16 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 162, NULL, '2020-02-14 12:59:37', '2020-02-14 12:59:37', '[{\"slot\":\"15\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"}]'),
(198, 246, '2020-02-10 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 162, 162, '2020-02-14 13:05:01', '2020-02-14 13:13:12', '[{\"slot\":\"9\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"21\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(199, 246, '2020-02-11 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 162, 162, '2020-02-14 13:07:07', '2020-02-14 13:08:49', '[{\"slot\":\"9\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"14\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"15\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"16\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"17\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"21\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"}]'),
(200, 246, '2020-02-12 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 162, 162, '2020-02-14 13:08:59', '2020-02-14 13:13:31', '[{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"17\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"20\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"21\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null}]'),
(201, 246, '2020-02-13 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 162, 162, '2020-02-14 13:10:13', '2020-02-14 13:14:24', '[{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"12\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"21\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"18\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"20\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"}]'),
(202, 246, '2020-02-14 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 162, 162, '2020-02-14 13:11:32', '2020-02-14 13:14:39', '[{\"slot\":\"9\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"10\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"11\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"12\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"13\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"14\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"15\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"16\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"17\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"18\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":null},{\"slot\":\"19\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"20\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"},{\"slot\":\"21\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"50\"}]'),
(203, 97, '2020-02-10 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 77, 77, '2020-02-15 16:41:36', '2020-02-15 16:41:43', '[{\"slot\":\"19\",\"active\":\"0\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"},{\"slot\":\"20\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"}]'),
(204, 97, '2020-02-11 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 77, 77, '2020-02-15 16:41:49', '2020-02-15 16:41:53', '[{\"slot\":\"19\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"},{\"slot\":\"20\",\"active\":\"2\",\"first_name\":null,\"notes\":null,\"price\":\"45.00\"}]'),
(205, 230, '2020-02-24 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 147, 147, '2020-02-28 11:08:24', '2020-02-28 11:08:37', '[{\"slot\":\"10\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"}]'),
(206, 230, '2020-04-05 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 147, NULL, '2020-02-28 11:08:58', '2020-02-28 11:08:58', '[{\"slot\":\"11\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"}]'),
(207, 230, '2020-04-06 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 147, 147, '2020-02-28 11:09:08', '2020-02-28 11:09:24', '[{\"slot\":\"11\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"}]'),
(208, 230, '2020-04-04 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 147, NULL, '2020-02-28 11:09:43', '2020-02-28 11:09:43', '[{\"slot\":\"13\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"}]'),
(209, 230, '2020-03-16 18:30:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 147, 147, '2020-03-17 11:10:14', '2020-03-17 11:10:25', '[{\"slot\":\"9\",\"active\":\"1\",\"first_name\":null,\"notes\":null,\"price\":\"90.00\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `bravo_tour_meta`
--

CREATE TABLE `bravo_tour_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tour_id` int(11) DEFAULT NULL,
  `enable_person_types` tinyint(4) DEFAULT NULL,
  `person_types` text COLLATE utf8mb4_unicode_ci,
  `enable_extra_price` tinyint(4) DEFAULT NULL,
  `extra_price` text COLLATE utf8mb4_unicode_ci,
  `discount_by_people` text COLLATE utf8mb4_unicode_ci,
  `enable_open_hours` tinyint(4) DEFAULT NULL,
  `open_hours` text COLLATE utf8mb4_unicode_ci,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bravo_tour_meta`
--

INSERT INTO `bravo_tour_meta` (`id`, `tour_id`, `enable_person_types`, `person_types`, `enable_extra_price`, `extra_price`, `discount_by_people`, `enable_open_hours`, `open_hours`, `create_user`, `update_user`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '[{\"name\":\"Adult\",\"desc\":\"Age 18+\",\"min\":\"1\",\"max\":\"10\",\"price\":\"1000\"},{\"name\":\"Child\",\"desc\":\"Age 6-17\",\"min\":\"0\",\"max\":\"10\",\"price\":\"300\"}]', 1, '[{\"name\":\"Clean\",\"price\":\"100\",\"type\":\"one_time\"}]', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 1, '[{\"name\":\"Adult\",\"desc\":\"Age 18+\",\"min\":\"1\",\"max\":\"10\",\"price\":\"1000\"},{\"name\":\"Child\",\"desc\":\"Age 6-17\",\"min\":\"0\",\"max\":\"10\",\"price\":\"300\"}]', 1, '[{\"name\":\"Clean\",\"price\":\"100\",\"type\":\"one_time\"}]', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 3, 1, '[{\"name\":\"Adult\",\"desc\":\"Age 18+\",\"min\":\"1\",\"max\":\"10\",\"price\":\"1000\"},{\"name\":\"Child\",\"desc\":\"Age 6-17\",\"min\":\"0\",\"max\":\"10\",\"price\":\"300\"}]', 1, '[{\"name\":\"Clean\",\"price\":\"100\",\"type\":\"one_time\"}]', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 4, 1, '[{\"name\":\"Adult\",\"desc\":\"Age 18+\",\"min\":\"1\",\"max\":\"10\",\"price\":\"1000\"},{\"name\":\"Child\",\"desc\":\"Age 6-17\",\"min\":\"0\",\"max\":\"10\",\"price\":\"300\"}]', 1, '[{\"name\":\"Clean\",\"price\":\"100\",\"type\":\"one_time\"}]', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 5, 1, '[{\"name\":\"Adult\",\"desc\":\"Age 18+\",\"min\":\"1\",\"max\":\"10\",\"price\":\"1000\"},{\"name\":\"Child\",\"desc\":\"Age 6-17\",\"min\":\"0\",\"max\":\"10\",\"price\":\"300\"}]', 1, '[{\"name\":\"Clean\",\"price\":\"100\",\"type\":\"one_time\"}]', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 6, 1, '[{\"name\":\"Adult\",\"desc\":\"Age 18+\",\"min\":\"1\",\"max\":\"10\",\"price\":\"1000\"},{\"name\":\"Child\",\"desc\":\"Age 6-17\",\"min\":\"0\",\"max\":\"10\",\"price\":\"300\"}]', 1, '[{\"name\":\"Clean\",\"price\":\"100\",\"type\":\"one_time\"}]', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 7, 1, '[{\"name\":\"Adult\",\"desc\":\"Age 18+\",\"min\":\"1\",\"max\":\"10\",\"price\":\"1000\"},{\"name\":\"Child\",\"desc\":\"Age 6-17\",\"min\":\"0\",\"max\":\"10\",\"price\":\"300\"}]', 1, '[{\"name\":\"Clean\",\"price\":\"100\",\"type\":\"one_time\"}]', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 8, 1, '[{\"name\":\"Adult\",\"desc\":\"Age 18+\",\"min\":\"1\",\"max\":\"10\",\"price\":\"1000\"},{\"name\":\"Child\",\"desc\":\"Age 6-17\",\"min\":\"0\",\"max\":\"10\",\"price\":\"300\"}]', 1, '[{\"name\":\"Clean\",\"price\":\"100\",\"type\":\"one_time\"}]', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 9, 1, '[{\"name\":\"Adult\",\"desc\":\"Age 18+\",\"min\":\"1\",\"max\":\"10\",\"price\":\"1000\"},{\"name\":\"Child\",\"desc\":\"Age 6-17\",\"min\":\"0\",\"max\":\"10\",\"price\":\"300\"}]', 1, '[{\"name\":\"Clean\",\"price\":\"100\",\"type\":\"one_time\"}]', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 10, 1, '[{\"name\":\"Adult\",\"desc\":\"Age 18+\",\"min\":\"1\",\"max\":\"10\",\"price\":\"1000\"},{\"name\":\"Child\",\"desc\":\"Age 6-17\",\"min\":\"0\",\"max\":\"10\",\"price\":\"300\"}]', 1, '[{\"name\":\"Clean\",\"price\":\"100\",\"type\":\"one_time\"}]', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 11, 1, '[{\"name\":\"Adult\",\"desc\":\"Age 18+\",\"min\":\"1\",\"max\":\"10\",\"price\":\"1000\"},{\"name\":\"Child\",\"desc\":\"Age 6-17\",\"min\":\"0\",\"max\":\"10\",\"price\":\"300\"}]', 1, '[{\"name\":\"Clean\",\"price\":\"100\",\"type\":\"one_time\"}]', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 12, 1, '[{\"name\":\"Adult\",\"desc\":\"Age 18+\",\"min\":\"1\",\"max\":\"10\",\"price\":\"1000\"},{\"name\":\"Child\",\"desc\":\"Age 6-17\",\"min\":\"0\",\"max\":\"10\",\"price\":\"300\"}]', 1, '[{\"name\":\"Clean\",\"price\":\"100\",\"type\":\"one_time\"}]', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 13, 1, '[{\"name\":\"Adult\",\"desc\":\"Age 18+\",\"min\":\"1\",\"max\":\"10\",\"price\":\"1000\"},{\"name\":\"Child\",\"desc\":\"Age 6-17\",\"min\":\"0\",\"max\":\"10\",\"price\":\"300\"}]', 1, '[{\"name\":\"Clean\",\"price\":\"100\",\"type\":\"one_time\"}]', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 14, 1, '[{\"name\":\"Adult\",\"desc\":\"Age 18+\",\"min\":\"1\",\"max\":\"10\",\"price\":\"1000\"},{\"name\":\"Child\",\"desc\":\"Age 6-17\",\"min\":\"0\",\"max\":\"10\",\"price\":\"300\"}]', 1, '[{\"name\":\"Clean\",\"price\":\"100\",\"type\":\"one_time\"}]', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 15, 1, '[{\"name\":\"Adult\",\"desc\":\"Age 18+\",\"min\":\"1\",\"max\":\"10\",\"price\":\"1000\"},{\"name\":\"Child\",\"desc\":\"Age 6-17\",\"min\":\"0\",\"max\":\"10\",\"price\":\"300\"}]', 1, '[{\"name\":\"Clean\",\"price\":\"100\",\"type\":\"one_time\"}]', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 16, 1, '[{\"name\":\"Adult\",\"desc\":\"Age 18+\",\"min\":\"1\",\"max\":\"10\",\"price\":\"1000\"},{\"name\":\"Child\",\"desc\":\"Age 6-17\",\"min\":\"0\",\"max\":\"10\",\"price\":\"300\"}]', 1, '[{\"name\":\"Clean\",\"price\":\"100\",\"type\":\"one_time\"}]', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 21, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"03:00\",\"to\":\"12:00\"},\"2\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"enable\":\"1\",\"from\":\"05:00\",\"to\":\"14:00\"},\"7\":{\"enable\":\"1\",\"from\":\"15:00\",\"to\":\"21:00\"}}', 1, 18, '2019-10-04 22:44:00', '2019-11-19 09:23:02'),
(18, 17, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"13:00\",\"to\":\"14:00\"},\"2\":{\"enable\":\"1\",\"from\":\"12:00\",\"to\":\"13:00\"},\"3\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 1, 1, '2019-10-04 22:48:58', '2020-02-28 03:57:31'),
(19, 20, NULL, NULL, NULL, NULL, NULL, NULL, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 1, 18, '2019-10-07 09:09:53', '2019-11-19 09:23:37'),
(20, 19, NULL, NULL, NULL, NULL, NULL, NULL, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 1, 18, '2019-10-07 09:11:05', '2019-11-20 03:22:30'),
(21, 18, NULL, NULL, NULL, NULL, NULL, NULL, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 1, 18, '2019-10-07 09:11:25', '2019-11-19 09:23:15'),
(22, 26, NULL, NULL, NULL, NULL, NULL, NULL, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 1, 25, '2019-11-07 11:33:55', '2019-11-07 14:03:04'),
(23, 25, NULL, NULL, NULL, NULL, NULL, NULL, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 1, 25, '2019-11-07 11:37:11', '2019-11-07 14:06:09'),
(24, 28, NULL, NULL, NULL, NULL, NULL, NULL, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 1, 25, '2019-11-07 11:46:43', '2019-11-07 14:07:14'),
(25, 44, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"enable\":\"1\",\"from\":\"15:00\",\"to\":\"23:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 41, 1, '2019-11-15 11:04:09', '2019-12-03 12:15:58'),
(26, 22, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"17:00\"},\"2\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"17:00\"},\"3\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"17:00\"},\"4\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"17:00\"},\"5\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"17:00\"},\"6\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"}}', 18, 18, '2019-11-15 13:15:20', '2019-11-17 08:17:10'),
(27, 48, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"09:00\"},\"2\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 43, 18, '2019-11-15 14:24:12', '2019-11-20 03:41:41'),
(28, 43, NULL, NULL, NULL, NULL, NULL, NULL, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 1, NULL, '2019-11-16 08:56:20', '2019-11-16 08:56:20'),
(29, 53, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 20, 18, '2019-11-17 14:25:28', '2019-11-20 03:41:11'),
(30, 93, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"20:00\"},\"2\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"20:00\"},\"3\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"20:00\"},\"4\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"20:00\"},\"5\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"13:00\"},\"6\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"13:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 75, 75, '2019-11-19 03:15:57', '2019-11-19 09:01:14'),
(31, 77, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"11:00\"},\"3\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"}}', 66, 66, '2019-11-19 04:15:32', '2019-12-13 11:21:07'),
(32, 91, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"}}', 74, 74, '2019-11-19 04:37:14', '2019-11-19 10:35:27'),
(33, 71, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"17:00\"},\"2\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"17:00\"},\"3\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"17:00\"},\"4\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"19:00\"},\"5\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"19:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 57, 57, '2019-11-19 05:40:54', '2019-11-19 06:21:20'),
(34, 95, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"20:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"19:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 76, 76, '2019-11-19 06:38:16', '2019-11-19 06:39:06'),
(35, 101, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"20:00\"},\"2\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"20:00\"},\"3\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"20:00\"},\"4\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"20:00\"},\"5\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"20:00\"},\"6\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"17:00\"},\"7\":{\"from\":\"10:00\",\"to\":\"17:00\"}}', 79, 79, '2019-11-19 10:17:32', '2019-11-25 13:28:10'),
(36, 105, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"17:00\"},\"2\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"18:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 81, 81, '2019-11-19 10:28:27', '2019-11-19 10:34:26'),
(37, 115, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 86, 86, '2019-11-19 11:15:36', '2019-12-17 07:35:33'),
(38, 123, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"from\":\"18:00\",\"to\":\"00:00\"},\"2\":{\"from\":\"10:00\",\"to\":\"00:00\"},\"3\":{\"from\":\"18:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"18:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"13:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 90, 90, '2019-11-19 13:15:23', '2019-11-19 13:16:12'),
(39, 125, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"15:00\"},\"2\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"15:00\"},\"3\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"15:00\"},\"4\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"15:00\"},\"5\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"15:00\"},\"6\":{\"enable\":\"1\",\"from\":\"13:00\",\"to\":\"18:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 91, 91, '2019-11-19 13:29:13', '2019-11-19 13:29:37'),
(40, 121, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"18:00\"},\"3\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"18:00\"},\"4\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"16:00\"},\"5\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"12:00\"},\"6\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"12:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 89, 18, '2019-11-19 13:34:58', '2019-11-19 15:02:29'),
(41, 132, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"19:00\",\"to\":\"00:00\"},\"2\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"enable\":\"1\",\"from\":\"11:00\",\"to\":\"00:00\"},\"4\":{\"enable\":\"1\",\"from\":\"07:00\",\"to\":\"00:00\"},\"5\":{\"enable\":\"1\",\"from\":\"07:00\",\"to\":\"00:00\"},\"6\":{\"enable\":\"1\",\"from\":\"07:00\",\"to\":\"00:00\"},\"7\":{\"enable\":\"1\",\"from\":\"19:00\",\"to\":\"00:00\"}}', 95, 95, '2019-11-19 19:09:10', '2019-11-19 19:10:46'),
(42, 134, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"20:00\"},\"2\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"20:00\"},\"3\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"20:00\"},\"4\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"20:00\"},\"5\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"20:00\"},\"6\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"13:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 96, 96, '2019-11-19 19:16:22', '2019-11-19 19:18:41'),
(43, 107, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"14:00\",\"to\":\"19:00\"},\"2\":{\"enable\":\"1\",\"from\":\"14:00\",\"to\":\"17:00\"},\"3\":{\"enable\":\"1\",\"from\":\"14:00\",\"to\":\"17:00\"},\"4\":{\"enable\":\"1\",\"from\":\"14:00\",\"to\":\"19:00\"},\"5\":{\"enable\":\"1\",\"from\":\"14:00\",\"to\":\"17:00\"},\"6\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"12:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 82, 82, '2019-11-20 01:39:23', '2019-11-21 14:37:36'),
(44, 139, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, NULL, '2019-11-20 03:38:47', '2019-11-20 03:38:47'),
(45, 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, NULL, '2019-11-20 03:42:06', '2019-11-20 03:42:06'),
(46, 46, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, NULL, '2019-11-20 03:42:21', '2019-11-20 03:42:21'),
(47, 45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 18, '2019-11-20 03:42:31', '2019-11-22 07:16:52'),
(48, 149, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"21:00\"},\"2\":{\"enable\":\"1\",\"from\":\"17:00\",\"to\":\"21:00\"},\"3\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"21:00\"},\"4\":{\"enable\":\"1\",\"from\":\"17:00\",\"to\":\"21:00\"},\"5\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"21:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 106, 106, '2019-11-20 07:32:09', '2019-11-28 09:40:10'),
(49, 151, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"16:00\"},\"2\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"16:00\"},\"3\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"18:00\"},\"4\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"20:00\"},\"5\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"18:00\"},\"6\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"15:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 107, 1, '2019-11-20 08:10:43', '2019-12-03 05:14:15'),
(50, 165, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"21:00\"},\"2\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"21:00\"},\"3\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"21:00\"},\"4\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"21:00\"},\"5\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"21:00\"},\"6\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"17:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 114, 114, '2019-11-20 10:57:58', '2019-11-20 11:16:58'),
(51, 171, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"19:00\"},\"2\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"19:00\"},\"3\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"19:00\"},\"4\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"19:00\"},\"5\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"19:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 117, 117, '2019-11-20 11:24:44', '2019-11-20 11:32:27'),
(52, 175, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"21:00\"},\"2\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"21:00\"},\"3\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"21:00\"},\"4\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"21:00\"},\"5\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"21:00\"},\"6\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"21:00\"},\"7\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"20:00\"}}', 119, 119, '2019-11-20 13:23:08', '2020-01-02 22:41:59'),
(53, 130, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 94, 94, '2019-11-20 16:19:47', '2019-11-20 16:20:00'),
(54, 181, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"20:00\"},\"2\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"20:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"enable\":\"1\",\"from\":\"14:00\",\"to\":\"20:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 122, 122, '2019-11-21 05:54:07', '2019-11-21 06:09:18'),
(55, 183, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"12:00\",\"to\":\"16:00\"},\"2\":{\"enable\":\"1\",\"from\":\"12:00\",\"to\":\"16:00\"},\"3\":{\"enable\":\"1\",\"from\":\"12:00\",\"to\":\"16:00\"},\"4\":{\"enable\":\"1\",\"from\":\"12:00\",\"to\":\"16:00\"},\"5\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"16:00\"},\"6\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"}}', 123, 123, '2019-11-21 10:00:42', '2019-11-21 10:04:06'),
(56, 188, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"21:00\"},\"2\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"21:00\"},\"3\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"21:00\"},\"4\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"21:00\"},\"5\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"21:00\"},\"6\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"16:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 125, NULL, '2019-11-22 07:52:33', '2019-11-22 07:52:33'),
(57, 186, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 124, 124, '2019-11-22 07:54:23', '2019-12-10 05:37:09'),
(58, 190, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"enable\":\"1\",\"from\":\"11:00\",\"to\":\"13:00\"},\"3\":{\"enable\":\"1\",\"from\":\"12:00\",\"to\":\"18:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"enable\":\"1\",\"from\":\"12:00\",\"to\":\"16:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 126, 126, '2019-11-22 08:08:25', '2019-11-22 08:08:34'),
(59, 192, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"17:00\"},\"2\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"17:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 127, 127, '2019-11-22 08:57:04', '2019-11-22 09:08:28'),
(60, 177, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"18:00\"},\"2\":{\"enable\":\"1\",\"from\":\"14:00\",\"to\":\"18:00\"},\"3\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"17:00\"},\"4\":{\"enable\":\"1\",\"from\":\"14:00\",\"to\":\"18:00\"},\"5\":{\"enable\":\"1\",\"from\":\"11:00\",\"to\":\"16:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 120, 120, '2019-11-22 09:37:27', '2019-11-22 10:36:11'),
(61, 194, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"13:00\"},\"2\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"13:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"13:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 128, 128, '2019-11-22 10:34:34', '2019-11-22 11:37:44'),
(62, 198, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"18:00\"},\"2\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"18:00\"},\"5\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"18:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 130, 130, '2019-11-22 22:29:25', '2019-11-22 22:30:28'),
(63, 200, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"17:00\"},\"2\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"17:00\"},\"3\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"17:00\"},\"4\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"17:00\"},\"5\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"17:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 131, 131, '2019-11-23 05:25:58', '2019-11-23 05:58:51'),
(64, 204, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"00:00\"},\"2\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"00:00\"},\"3\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 133, 133, '2019-11-24 11:51:06', '2019-11-24 11:51:24'),
(65, 167, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 115, 115, '2019-11-24 12:11:07', '2019-11-24 12:12:14'),
(66, 208, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"00:00\"},\"2\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"18:00\"},\"3\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"18:00\"},\"4\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"18:00\"},\"5\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"18:00\"},\"6\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"13:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 135, 135, '2019-11-25 05:37:13', '2020-03-11 06:34:54'),
(67, 210, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"enable\":\"1\",\"from\":\"14:00\",\"to\":\"19:00\"},\"3\":{\"from\":\"09:00\",\"to\":\"13:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 136, 136, '2019-11-25 08:08:40', '2019-12-02 11:55:28'),
(68, 212, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"18:00\"},\"2\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"17:00\"},\"3\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"14:00\"},\"5\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"12:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 137, 137, '2019-11-25 10:42:59', '2019-11-25 10:44:56'),
(69, 214, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"14:00\"},\"2\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"14:00\"},\"3\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"20:00\"},\"4\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"20:00\"},\"5\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"12:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 138, 138, '2019-11-25 15:21:19', '2019-11-25 15:23:55'),
(70, 216, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"18:00\",\"to\":\"21:00\"},\"2\":{\"enable\":\"1\",\"from\":\"18:00\",\"to\":\"21:00\"},\"3\":{\"enable\":\"1\",\"from\":\"18:00\",\"to\":\"21:00\"},\"4\":{\"enable\":\"1\",\"from\":\"18:00\",\"to\":\"21:00\"},\"5\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 139, 139, '2019-11-26 03:43:26', '2019-11-26 03:55:11'),
(71, 163, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 113, 113, '2019-12-03 09:13:02', '2019-12-03 09:18:38'),
(72, 226, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"enable\":\"1\",\"from\":\"04:00\",\"to\":\"23:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 145, 1, '2019-12-03 11:08:42', '2019-12-03 12:16:42'),
(73, 230, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"from\":\"08:00\",\"to\":\"20:00\"},\"3\":{\"from\":\"08:00\",\"to\":\"20:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"08:00\",\"to\":\"20:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 147, 147, '2019-12-04 04:05:53', '2019-12-04 04:11:07'),
(74, 232, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"12:00\"},\"2\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 148, 148, '2019-12-04 04:14:13', '2019-12-04 04:15:01'),
(75, 233, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"00:00\",\"to\":\"10:00\"},\"2\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 149, 149, '2019-12-04 04:43:53', '2019-12-04 04:44:02'),
(76, 234, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"11:00\",\"to\":\"14:00\"},\"2\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 150, 150, '2019-12-04 22:26:54', '2019-12-04 22:28:02'),
(77, 184, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, '2019-12-10 09:45:32', '2019-12-10 09:51:21'),
(78, 75, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"from\":\"09:00\",\"to\":\"15:00\"},\"3\":{\"from\":\"08:00\",\"to\":\"12:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 65, 65, '2019-12-13 08:00:15', '2020-01-15 08:18:42'),
(79, 239, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"from\":\"10:00\",\"to\":\"15:00\"},\"3\":{\"from\":\"10:00\",\"to\":\"15:00\"},\"4\":{\"from\":\"10:00\",\"to\":\"15:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 46, 46, '2019-12-13 09:39:51', '2020-01-14 08:45:39'),
(80, 240, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"2\":{\"enable\":\"1\",\"from\":\"08:00\",\"to\":\"11:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 155, 155, '2019-12-13 10:43:53', '2019-12-19 11:05:10'),
(81, 97, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"14:00\"},\"2\":{\"enable\":\"1\",\"from\":\"19:00\",\"to\":\"21:00\"},\"3\":{\"enable\":\"1\",\"from\":\"19:00\",\"to\":\"21:00\"},\"4\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"13:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 77, 77, '2019-12-30 11:11:56', '2019-12-30 11:59:42'),
(82, 244, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"17:00\"},\"2\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"17:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 160, 160, '2020-01-07 04:45:17', '2020-01-07 04:46:05'),
(83, 248, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"15:00\"},\"2\":{\"enable\":\"1\",\"from\":\"18:00\",\"to\":\"19:00\"},\"3\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"15:00\"},\"4\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"15:00\"},\"5\":{\"enable\":\"1\",\"from\":\"12:00\",\"to\":\"15:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 164, 164, '2020-01-17 13:00:59', '2020-01-17 13:14:55'),
(84, 245, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2020-01-26 05:43:42', '2020-01-26 05:43:42'),
(85, 249, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"20:00\"},\"2\":{\"enable\":\"1\",\"from\":\"10:00\",\"to\":\"20:00\"},\"3\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"4\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"5\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"6\":{\"from\":\"00:00\",\"to\":\"00:00\"},\"7\":{\"from\":\"00:00\",\"to\":\"00:00\"}}', 165, 165, '2020-01-28 12:20:30', '2020-01-28 12:30:22'),
(86, 246, NULL, NULL, NULL, NULL, NULL, 1, '{\"1\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"22:00\"},\"2\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"22:00\"},\"3\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"22:00\"},\"4\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"22:00\"},\"5\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"22:00\"},\"6\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"22:00\"},\"7\":{\"enable\":\"1\",\"from\":\"09:00\",\"to\":\"22:00\"}}', 162, 162, '2020-02-14 13:02:30', '2020-02-14 13:15:07'),
(87, 255, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2020-07-28 04:10:45', '2020-07-28 04:10:45'),
(88, 256, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 44, 1, '2020-07-28 11:43:13', '2020-07-28 11:45:22');

-- --------------------------------------------------------

--
-- Table structure for table `bravo_tour_term`
--

CREATE TABLE `bravo_tour_term` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `term_id` int(11) DEFAULT NULL,
  `tour_id` int(11) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bravo_tour_term`
--

INSERT INTO `bravo_tour_term` (`id`, `term_id`, `tour_id`, `create_user`, `update_user`, `created_at`, `updated_at`) VALUES
(152, 28, 17, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(153, 29, 17, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(154, 30, 17, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(155, 31, 17, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(157, 21, 17, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(158, 22, 17, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(159, 23, 17, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(160, 24, 17, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(161, 25, 17, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(162, 26, 17, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(163, 27, 18, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(164, 28, 18, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(165, 29, 18, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(166, 30, 18, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(167, 31, 18, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(169, 21, 18, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(170, 22, 18, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(171, 23, 18, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(172, 24, 18, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(173, 25, 18, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(174, 26, 18, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(175, 27, 19, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(176, 28, 19, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(177, 29, 19, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(178, 30, 19, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(179, 31, 19, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(181, 21, 19, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(182, 22, 19, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(183, 23, 19, 1, NULL, '2019-10-02 21:20:59', '2019-10-02 21:20:59'),
(184, 24, 19, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(185, 25, 19, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(186, 26, 19, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(187, 27, 20, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(188, 28, 20, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(189, 29, 20, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(190, 30, 20, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(191, 31, 20, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(193, 21, 20, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(194, 22, 20, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(195, 23, 20, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(196, 24, 20, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(197, 25, 20, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(198, 26, 20, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(199, 27, 21, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(200, 28, 21, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(201, 29, 21, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(202, 30, 21, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(203, 31, 21, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(205, 21, 21, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(206, 22, 21, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(207, 23, 21, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(208, 24, 21, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(209, 25, 21, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(210, 26, 21, 1, NULL, '2019-10-02 21:21:00', '2019-10-02 21:21:00'),
(211, 33, 17, 1, NULL, '2019-10-07 09:05:24', '2019-10-07 09:05:24'),
(212, 34, 17, 1, NULL, '2019-10-07 09:05:24', '2019-10-07 09:05:24'),
(213, 35, 17, 1, NULL, '2019-10-07 09:05:24', '2019-10-07 09:05:24'),
(214, 34, 21, 1, NULL, '2019-10-07 09:09:02', '2019-10-07 09:09:02'),
(215, 36, 21, 1, NULL, '2019-10-07 09:09:02', '2019-10-07 09:09:02'),
(216, 37, 21, 1, NULL, '2019-10-07 09:09:02', '2019-10-07 09:09:02'),
(217, 38, 21, 1, NULL, '2019-10-07 09:09:02', '2019-10-07 09:09:02'),
(218, 33, 18, 1, NULL, '2019-10-07 09:12:42', '2019-10-07 09:12:42'),
(219, 39, 21, 1, NULL, '2019-10-10 05:28:20', '2019-10-10 05:28:20'),
(220, 40, 21, 1, NULL, '2019-10-10 05:28:20', '2019-10-10 05:28:20'),
(221, 41, 21, 1, NULL, '2019-10-10 05:28:20', '2019-10-10 05:28:20'),
(222, 42, 21, 1, NULL, '2019-10-10 05:28:20', '2019-10-10 05:28:20'),
(223, 43, 21, 1, NULL, '2019-10-10 05:28:20', '2019-10-10 05:28:20'),
(224, 44, 21, 1, NULL, '2019-10-10 05:28:20', '2019-10-10 05:28:20'),
(228, 48, 21, 1, NULL, '2019-10-10 05:28:20', '2019-10-10 05:28:20'),
(230, 50, 21, 1, NULL, '2019-10-10 05:28:20', '2019-10-10 05:28:20'),
(231, 34, 20, 1, NULL, '2019-10-10 05:28:48', '2019-10-10 05:28:48'),
(232, 36, 20, 1, NULL, '2019-10-10 05:28:48', '2019-10-10 05:28:48'),
(233, 39, 20, 1, NULL, '2019-10-10 05:28:48', '2019-10-10 05:28:48'),
(234, 40, 20, 1, NULL, '2019-10-10 05:28:48', '2019-10-10 05:28:48'),
(235, 41, 20, 1, NULL, '2019-10-10 05:28:48', '2019-10-10 05:28:48'),
(236, 42, 20, 1, NULL, '2019-10-10 05:28:48', '2019-10-10 05:28:48'),
(237, 43, 20, 1, NULL, '2019-10-10 05:28:48', '2019-10-10 05:28:48'),
(238, 44, 20, 1, NULL, '2019-10-10 05:28:48', '2019-10-10 05:28:48'),
(242, 48, 20, 1, NULL, '2019-10-10 05:28:48', '2019-10-10 05:28:48'),
(244, 50, 20, 1, NULL, '2019-10-10 05:28:48', '2019-10-10 05:28:48'),
(245, 33, 19, 1, NULL, '2019-10-10 05:29:19', '2019-10-10 05:29:19'),
(246, 34, 19, 1, NULL, '2019-10-10 05:29:19', '2019-10-10 05:29:19'),
(247, 37, 19, 1, NULL, '2019-10-10 05:29:19', '2019-10-10 05:29:19'),
(248, 39, 19, 1, NULL, '2019-10-10 05:29:19', '2019-10-10 05:29:19'),
(249, 40, 19, 1, NULL, '2019-10-10 05:29:19', '2019-10-10 05:29:19'),
(250, 42, 19, 1, NULL, '2019-10-10 05:29:19', '2019-10-10 05:29:19'),
(251, 43, 19, 1, NULL, '2019-10-10 05:29:19', '2019-10-10 05:29:19'),
(255, 48, 19, 1, NULL, '2019-10-10 05:29:19', '2019-10-10 05:29:19'),
(256, 50, 19, 1, NULL, '2019-10-10 05:29:19', '2019-10-10 05:29:19'),
(257, 40, 18, 1, NULL, '2019-10-10 05:30:37', '2019-10-10 05:30:37'),
(258, 42, 18, 1, NULL, '2019-10-10 05:30:37', '2019-10-10 05:30:37'),
(259, 43, 18, 1, NULL, '2019-10-10 05:30:37', '2019-10-10 05:30:37'),
(263, 48, 18, 1, NULL, '2019-10-10 05:30:37', '2019-10-10 05:30:37'),
(265, 50, 18, 1, NULL, '2019-10-10 05:30:37', '2019-10-10 05:30:37'),
(266, 39, 17, 1, NULL, '2019-10-10 05:30:55', '2019-10-10 05:30:55'),
(267, 40, 17, 1, NULL, '2019-10-10 05:30:55', '2019-10-10 05:30:55'),
(268, 41, 17, 1, NULL, '2019-10-10 05:30:55', '2019-10-10 05:30:55'),
(269, 42, 17, 1, NULL, '2019-10-10 05:30:55', '2019-10-10 05:30:55'),
(270, 43, 17, 1, NULL, '2019-10-10 05:30:55', '2019-10-10 05:30:55'),
(271, 44, 17, 1, NULL, '2019-10-10 05:30:55', '2019-10-10 05:30:55'),
(275, 48, 17, 1, NULL, '2019-10-10 05:30:55', '2019-10-10 05:30:55'),
(277, 50, 17, 1, NULL, '2019-10-10 05:30:55', '2019-10-10 05:30:55'),
(278, 66, 17, 1, NULL, '2019-10-10 10:16:58', '2019-10-10 10:16:58'),
(279, 67, 17, 1, NULL, '2019-10-10 10:16:58', '2019-10-10 10:16:58'),
(280, 68, 17, 1, NULL, '2019-10-10 10:16:58', '2019-10-10 10:16:58'),
(281, 69, 17, 1, NULL, '2019-10-10 10:16:58', '2019-10-10 10:16:58'),
(282, 71, 17, 1, NULL, '2019-10-10 10:16:58', '2019-10-10 10:16:58'),
(283, 75, 17, 1, NULL, '2019-10-10 10:16:58', '2019-10-10 10:16:58'),
(284, 78, 17, 1, NULL, '2019-10-10 10:16:58', '2019-10-10 10:16:58'),
(285, 79, 17, 1, NULL, '2019-10-10 10:16:58', '2019-10-10 10:16:58'),
(286, 82, 17, 1, NULL, '2019-10-10 10:16:58', '2019-10-10 10:16:58'),
(287, 83, 17, 1, NULL, '2019-10-10 10:16:58', '2019-10-10 10:16:58'),
(288, 87, 17, 1, NULL, '2019-10-10 10:16:58', '2019-10-10 10:16:58'),
(289, 51, 17, 1, NULL, '2019-10-10 10:16:58', '2019-10-10 10:16:58'),
(290, 52, 17, 1, NULL, '2019-10-10 10:16:58', '2019-10-10 10:16:58'),
(291, 53, 17, 1, NULL, '2019-10-10 10:16:58', '2019-10-10 10:16:58'),
(292, 54, 17, 1, NULL, '2019-10-10 10:16:58', '2019-10-10 10:16:58'),
(293, 57, 17, 1, NULL, '2019-10-10 10:16:58', '2019-10-10 10:16:58'),
(294, 58, 17, 1, NULL, '2019-10-10 10:16:58', '2019-10-10 10:16:58'),
(295, 65, 21, 1, NULL, '2019-10-15 11:06:38', '2019-10-15 11:06:38'),
(296, 67, 21, 1, NULL, '2019-10-15 11:06:38', '2019-10-15 11:06:38'),
(297, 72, 21, 1, NULL, '2019-10-15 11:06:38', '2019-10-15 11:06:38'),
(298, 74, 21, 1, NULL, '2019-10-15 11:06:38', '2019-10-15 11:06:38'),
(299, 80, 21, 1, NULL, '2019-10-15 11:06:38', '2019-10-15 11:06:38'),
(300, 21, 44, 41, NULL, '2019-11-15 11:04:46', '2019-11-15 11:04:46'),
(301, 26, 44, 41, NULL, '2019-11-15 11:04:46', '2019-11-15 11:04:46'),
(302, 27, 44, 41, NULL, '2019-11-15 11:04:46', '2019-11-15 11:04:46'),
(304, 89, 44, 41, NULL, '2019-11-15 11:04:46', '2019-11-15 11:04:46'),
(305, 33, 44, 41, NULL, '2019-11-15 11:04:47', '2019-11-15 11:04:47'),
(306, 34, 44, 41, NULL, '2019-11-15 11:04:47', '2019-11-15 11:04:47'),
(307, 38, 44, 41, NULL, '2019-11-15 11:04:47', '2019-11-15 11:04:47'),
(308, 21, 77, 66, NULL, '2019-11-19 04:15:32', '2019-11-19 04:15:32'),
(309, 25, 77, 66, NULL, '2019-11-19 04:15:32', '2019-11-19 04:15:32'),
(310, 27, 77, 66, NULL, '2019-11-19 04:15:32', '2019-11-19 04:15:32'),
(311, 28, 77, 66, NULL, '2019-11-19 04:15:32', '2019-11-19 04:15:32'),
(312, 29, 77, 66, NULL, '2019-11-19 04:15:32', '2019-11-19 04:15:32'),
(313, 30, 77, 66, NULL, '2019-11-19 04:15:32', '2019-11-19 04:15:32'),
(314, 59, 77, 66, NULL, '2019-11-19 04:15:32', '2019-11-19 04:15:32'),
(315, 60, 77, 66, NULL, '2019-11-19 04:15:32', '2019-11-19 04:15:32'),
(316, 62, 77, 66, NULL, '2019-11-19 04:15:32', '2019-11-19 04:15:32'),
(317, 63, 77, 66, NULL, '2019-11-19 04:15:32', '2019-11-19 04:15:32'),
(319, 56, 77, 66, NULL, '2019-11-19 04:15:32', '2019-11-19 04:15:32'),
(320, 58, 77, 66, NULL, '2019-11-19 04:15:32', '2019-11-19 04:15:32'),
(321, 21, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(322, 27, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(323, 28, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(324, 29, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(325, 59, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(326, 63, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(327, 64, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(328, 71, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(329, 72, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(330, 73, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(331, 74, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(332, 77, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(333, 78, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(334, 79, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(335, 83, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(336, 86, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(337, 87, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(338, 40, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(339, 51, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(340, 52, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(341, 53, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(342, 54, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(343, 48, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(344, 50, 91, 74, NULL, '2019-11-19 04:37:14', '2019-11-19 04:37:14'),
(345, 21, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(346, 25, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(347, 27, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(348, 28, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(349, 30, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(350, 31, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(351, 59, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(352, 60, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(353, 62, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(354, 63, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(355, 64, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(356, 65, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(357, 67, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(358, 68, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(359, 69, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(360, 73, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(361, 74, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(362, 77, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(363, 78, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(364, 80, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(365, 84, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(366, 86, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(367, 87, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(368, 91, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(369, 93, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(370, 39, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(371, 40, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(372, 42, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(373, 51, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(374, 48, 115, 86, NULL, '2019-11-19 11:40:59', '2019-11-19 11:40:59'),
(375, 27, 121, 89, NULL, '2019-11-19 13:34:58', '2019-11-19 13:34:58'),
(376, 28, 121, 89, NULL, '2019-11-19 13:34:58', '2019-11-19 13:34:58'),
(377, 29, 121, 89, NULL, '2019-11-19 13:34:58', '2019-11-19 13:34:58'),
(378, 30, 121, 89, NULL, '2019-11-19 13:34:58', '2019-11-19 13:34:58'),
(379, 61, 121, 89, NULL, '2019-11-19 13:34:58', '2019-11-19 13:34:58'),
(380, 62, 121, 89, NULL, '2019-11-19 13:34:58', '2019-11-19 13:34:58'),
(381, 74, 121, 89, NULL, '2019-11-19 13:34:58', '2019-11-19 13:34:58'),
(382, 84, 121, 89, NULL, '2019-11-19 13:34:58', '2019-11-19 13:34:58'),
(383, 85, 121, 89, NULL, '2019-11-19 13:34:58', '2019-11-19 13:34:58'),
(384, 86, 121, 89, NULL, '2019-11-19 13:34:58', '2019-11-19 13:34:58'),
(385, 87, 121, 89, NULL, '2019-11-19 13:34:58', '2019-11-19 13:34:58'),
(386, 89, 121, 89, NULL, '2019-11-19 13:34:58', '2019-11-19 13:34:58'),
(387, 93, 121, 89, NULL, '2019-11-19 13:34:58', '2019-11-19 13:34:58'),
(388, 40, 121, 89, NULL, '2019-11-19 13:34:58', '2019-11-19 13:34:58'),
(389, 48, 121, 89, NULL, '2019-11-19 13:34:58', '2019-11-19 13:34:58'),
(390, 50, 121, 89, NULL, '2019-11-19 13:34:58', '2019-11-19 13:34:58'),
(391, 21, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(392, 22, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(393, 25, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(394, 27, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(395, 28, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(396, 29, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(397, 30, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(398, 60, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(399, 62, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(400, 63, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(401, 68, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(402, 84, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(403, 86, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(404, 89, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(405, 93, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(406, 39, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(407, 40, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(408, 56, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(409, 58, 93, 75, NULL, '2019-11-20 02:55:18', '2019-11-20 02:55:18'),
(410, 21, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(411, 25, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(412, 29, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(413, 30, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(414, 61, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(415, 62, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(416, 67, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(417, 68, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(418, 76, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(419, 78, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(420, 80, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(421, 84, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(422, 85, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(423, 86, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(424, 93, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(425, 94, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(426, 95, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(427, 98, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(428, 39, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(429, 40, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(430, 51, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(431, 52, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(432, 53, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(433, 54, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(434, 55, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(435, 56, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(436, 48, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(437, 50, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(438, 57, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(439, 58, 149, 106, NULL, '2019-11-20 08:20:02', '2019-11-20 08:20:02'),
(440, 21, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(441, 25, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(442, 26, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(443, 27, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(444, 28, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(445, 29, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(446, 30, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(447, 59, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(448, 60, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(449, 62, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(450, 63, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(451, 69, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(452, 70, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(453, 71, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(454, 75, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(455, 76, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(456, 77, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(457, 78, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(458, 79, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(459, 80, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(460, 81, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(461, 84, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(462, 85, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(463, 86, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(464, 87, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(465, 89, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(466, 90, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(467, 91, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(468, 92, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(469, 93, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(470, 94, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(471, 39, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(472, 40, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(473, 42, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(474, 43, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(475, 56, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(476, 50, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(477, 58, 151, 107, NULL, '2019-11-20 11:13:13', '2019-11-20 11:13:13'),
(478, 27, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(479, 28, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(480, 29, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(481, 30, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(482, 59, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(483, 60, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(484, 62, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(485, 63, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(486, 64, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(487, 65, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(488, 66, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(489, 68, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(490, 69, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(491, 70, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(492, 71, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(493, 72, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(494, 73, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(495, 74, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(496, 75, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(497, 76, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(498, 77, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(499, 78, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(500, 79, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(501, 80, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(502, 81, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(503, 82, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(504, 83, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(505, 84, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(506, 85, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(507, 86, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(508, 87, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(509, 89, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(510, 91, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(511, 92, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(512, 93, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(513, 95, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(514, 98, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(515, 39, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(516, 40, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(517, 42, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(518, 56, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(519, 58, 165, 114, NULL, '2019-11-20 11:16:58', '2019-11-20 11:16:58'),
(520, 21, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(521, 22, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(522, 25, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(523, 26, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(524, 27, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(525, 28, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(526, 29, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(527, 30, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(528, 31, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(529, 59, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(530, 60, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(531, 61, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(532, 62, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(533, 63, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(534, 65, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(535, 67, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(536, 68, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(537, 69, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(538, 70, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(539, 71, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(540, 73, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(541, 74, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(542, 76, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(543, 77, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(544, 78, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(545, 79, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(546, 80, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(547, 81, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(548, 82, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(549, 83, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(550, 84, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(551, 85, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(552, 86, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(553, 87, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(554, 89, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(555, 90, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(556, 91, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(557, 92, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(558, 93, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(559, 94, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(560, 95, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(561, 38, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(562, 96, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(563, 97, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(564, 39, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(565, 40, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(566, 41, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(567, 42, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(568, 43, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(569, 44, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(570, 51, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(571, 52, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(572, 53, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(573, 54, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(574, 48, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(575, 50, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(576, 57, 125, 91, NULL, '2019-11-20 12:31:56', '2019-11-20 12:31:56'),
(577, 22, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(578, 25, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(579, 28, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(580, 29, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(581, 30, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(582, 31, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(583, 60, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(584, 61, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(585, 63, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(586, 66, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(587, 68, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(588, 69, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(589, 70, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(590, 77, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(591, 79, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(592, 81, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(593, 82, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(594, 83, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(595, 84, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(596, 85, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(597, 86, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(598, 87, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(599, 91, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(600, 94, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(601, 98, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(602, 40, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(603, 42, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(604, 56, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(605, 58, 130, 94, NULL, '2019-11-20 16:19:47', '2019-11-20 16:19:47'),
(606, 21, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(607, 24, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(608, 27, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(609, 28, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(610, 29, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(611, 30, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(612, 60, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(613, 62, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(614, 63, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(615, 68, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(616, 69, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(617, 70, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(618, 83, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(619, 84, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(620, 86, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(621, 87, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(622, 91, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(623, 93, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(624, 95, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(625, 98, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(626, 40, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(627, 42, 181, 122, NULL, '2019-11-21 06:28:15', '2019-11-21 06:28:15'),
(628, 21, 183, 123, NULL, '2019-11-21 10:12:48', '2019-11-21 10:12:48'),
(629, 27, 183, 123, NULL, '2019-11-21 10:12:48', '2019-11-21 10:12:48'),
(630, 28, 183, 123, NULL, '2019-11-21 10:12:48', '2019-11-21 10:12:48'),
(631, 29, 183, 123, NULL, '2019-11-21 10:12:48', '2019-11-21 10:12:48'),
(632, 30, 183, 123, NULL, '2019-11-21 10:12:48', '2019-11-21 10:12:48'),
(633, 60, 183, 123, NULL, '2019-11-21 10:12:48', '2019-11-21 10:12:48'),
(634, 62, 183, 123, NULL, '2019-11-21 10:12:48', '2019-11-21 10:12:48'),
(635, 76, 183, 123, NULL, '2019-11-21 10:12:48', '2019-11-21 10:12:48'),
(636, 77, 183, 123, NULL, '2019-11-21 10:12:48', '2019-11-21 10:12:48'),
(637, 78, 183, 123, NULL, '2019-11-21 10:12:48', '2019-11-21 10:12:48'),
(638, 81, 183, 123, NULL, '2019-11-21 10:12:48', '2019-11-21 10:12:48'),
(639, 84, 183, 123, NULL, '2019-11-21 10:12:48', '2019-11-21 10:12:48'),
(640, 90, 183, 123, NULL, '2019-11-21 10:12:48', '2019-11-21 10:12:48'),
(641, 40, 183, 123, NULL, '2019-11-21 10:12:48', '2019-11-21 10:12:48'),
(642, 56, 183, 123, NULL, '2019-11-21 10:12:48', '2019-11-21 10:12:48'),
(643, 58, 183, 123, NULL, '2019-11-21 10:12:48', '2019-11-21 10:12:48'),
(644, 21, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(645, 24, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(646, 25, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(647, 26, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(648, 28, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(649, 29, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(650, 30, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(651, 59, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(652, 60, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(653, 63, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(654, 66, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(655, 71, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(656, 73, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(657, 76, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(658, 84, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(659, 86, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(660, 91, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(661, 93, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(662, 95, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(663, 100, 107, 82, NULL, '2019-11-21 15:00:56', '2019-11-21 15:00:56'),
(664, 40, 107, 82, NULL, '2019-11-21 15:00:57', '2019-11-21 15:00:57'),
(665, 43, 107, 82, NULL, '2019-11-21 15:00:57', '2019-11-21 15:00:57'),
(666, 51, 107, 82, NULL, '2019-11-21 15:00:57', '2019-11-21 15:00:57'),
(667, 50, 107, 82, NULL, '2019-11-21 15:00:57', '2019-11-21 15:00:57'),
(668, 21, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(669, 24, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(670, 25, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(671, 27, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(672, 28, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(673, 29, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(674, 30, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(675, 61, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(676, 62, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(677, 63, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(678, 65, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(679, 66, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(680, 69, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(681, 70, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(682, 71, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(683, 72, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(684, 73, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(685, 75, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(686, 79, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(687, 81, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(688, 82, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(689, 83, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(690, 84, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(691, 86, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(692, 87, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(693, 89, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(694, 92, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(695, 93, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(696, 95, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(697, 98, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(698, 39, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(699, 40, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(700, 42, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(701, 51, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(702, 52, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(703, 53, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(704, 54, 186, 124, NULL, '2019-11-22 07:57:02', '2019-11-22 07:57:02'),
(705, 48, 186, 124, NULL, '2019-11-22 07:57:03', '2019-11-22 07:57:03'),
(706, 50, 186, 124, NULL, '2019-11-22 07:57:03', '2019-11-22 07:57:03'),
(707, 57, 186, 124, NULL, '2019-11-22 07:57:03', '2019-11-22 07:57:03'),
(708, 21, 192, 127, NULL, '2019-11-22 09:12:03', '2019-11-22 09:12:03'),
(709, 25, 192, 127, NULL, '2019-11-22 09:12:03', '2019-11-22 09:12:03'),
(710, 27, 192, 127, NULL, '2019-11-22 09:12:03', '2019-11-22 09:12:03'),
(711, 28, 192, 127, NULL, '2019-11-22 09:12:03', '2019-11-22 09:12:03'),
(712, 29, 192, 127, NULL, '2019-11-22 09:12:03', '2019-11-22 09:12:03'),
(713, 30, 192, 127, NULL, '2019-11-22 09:12:03', '2019-11-22 09:12:03'),
(714, 59, 192, 127, NULL, '2019-11-22 09:12:03', '2019-11-22 09:12:03'),
(715, 60, 192, 127, NULL, '2019-11-22 09:12:03', '2019-11-22 09:12:03'),
(716, 62, 192, 127, NULL, '2019-11-22 09:12:03', '2019-11-22 09:12:03'),
(717, 63, 192, 127, NULL, '2019-11-22 09:12:04', '2019-11-22 09:12:04'),
(718, 68, 192, 127, NULL, '2019-11-22 09:12:04', '2019-11-22 09:12:04'),
(719, 69, 192, 127, NULL, '2019-11-22 09:12:04', '2019-11-22 09:12:04'),
(720, 70, 192, 127, NULL, '2019-11-22 09:12:04', '2019-11-22 09:12:04'),
(721, 84, 192, 127, NULL, '2019-11-22 09:12:04', '2019-11-22 09:12:04'),
(722, 86, 192, 127, NULL, '2019-11-22 09:12:04', '2019-11-22 09:12:04'),
(723, 87, 192, 127, NULL, '2019-11-22 09:12:04', '2019-11-22 09:12:04'),
(724, 90, 192, 127, NULL, '2019-11-22 09:12:04', '2019-11-22 09:12:04'),
(725, 91, 192, 127, NULL, '2019-11-22 09:12:04', '2019-11-22 09:12:04'),
(726, 93, 192, 127, NULL, '2019-11-22 09:12:04', '2019-11-22 09:12:04'),
(727, 95, 192, 127, NULL, '2019-11-22 09:12:04', '2019-11-22 09:12:04'),
(728, 33, 192, 127, NULL, '2019-11-22 09:12:04', '2019-11-22 09:12:04'),
(729, 98, 192, 127, NULL, '2019-11-22 09:12:04', '2019-11-22 09:12:04'),
(730, 40, 192, 127, NULL, '2019-11-22 09:12:04', '2019-11-22 09:12:04'),
(731, 42, 192, 127, NULL, '2019-11-22 09:12:04', '2019-11-22 09:12:04'),
(732, 50, 192, 127, NULL, '2019-11-22 09:12:04', '2019-11-22 09:12:04'),
(733, 21, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(734, 22, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(735, 27, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(736, 28, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(737, 29, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(738, 30, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(739, 59, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(740, 60, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(741, 62, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(742, 63, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(743, 69, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(744, 70, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(745, 73, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(746, 78, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(747, 80, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(748, 84, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(749, 86, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(750, 87, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(751, 89, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(752, 91, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(753, 93, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(754, 94, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(755, 100, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(756, 39, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(757, 40, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(758, 43, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(759, 56, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(760, 58, 177, 120, NULL, '2019-11-22 10:34:20', '2019-11-22 10:34:20'),
(761, 24, 194, 128, NULL, '2019-11-22 11:34:57', '2019-11-22 11:34:57'),
(762, 25, 194, 128, NULL, '2019-11-22 11:34:57', '2019-11-22 11:34:57'),
(763, 27, 194, 128, NULL, '2019-11-22 11:34:57', '2019-11-22 11:34:57'),
(764, 28, 194, 128, NULL, '2019-11-22 11:34:57', '2019-11-22 11:34:57'),
(765, 30, 194, 128, NULL, '2019-11-22 11:34:57', '2019-11-22 11:34:57'),
(766, 62, 194, 128, NULL, '2019-11-22 11:34:57', '2019-11-22 11:34:57'),
(767, 84, 194, 128, NULL, '2019-11-22 11:34:57', '2019-11-22 11:34:57'),
(768, 86, 194, 128, NULL, '2019-11-22 11:34:57', '2019-11-22 11:34:57'),
(769, 87, 194, 128, NULL, '2019-11-22 11:34:57', '2019-11-22 11:34:57'),
(770, 91, 194, 128, NULL, '2019-11-22 11:34:57', '2019-11-22 11:34:57'),
(771, 93, 194, 128, NULL, '2019-11-22 11:34:57', '2019-11-22 11:34:57'),
(772, 96, 194, 128, NULL, '2019-11-22 11:34:57', '2019-11-22 11:34:57'),
(773, 40, 194, 128, NULL, '2019-11-22 11:34:57', '2019-11-22 11:34:57'),
(774, 42, 194, 128, NULL, '2019-11-22 11:34:57', '2019-11-22 11:34:57'),
(775, 51, 194, 128, NULL, '2019-11-22 11:34:57', '2019-11-22 11:34:57'),
(776, 52, 194, 128, NULL, '2019-11-22 11:34:58', '2019-11-22 11:34:58'),
(777, 53, 194, 128, NULL, '2019-11-22 11:34:58', '2019-11-22 11:34:58'),
(778, 54, 194, 128, NULL, '2019-11-22 11:34:58', '2019-11-22 11:34:58'),
(779, 55, 194, 128, NULL, '2019-11-22 11:34:58', '2019-11-22 11:34:58'),
(780, 56, 194, 128, NULL, '2019-11-22 11:34:58', '2019-11-22 11:34:58'),
(781, 48, 194, 128, NULL, '2019-11-22 11:34:58', '2019-11-22 11:34:58'),
(782, 50, 194, 128, NULL, '2019-11-22 11:34:58', '2019-11-22 11:34:58'),
(783, 57, 194, 128, NULL, '2019-11-22 11:34:58', '2019-11-22 11:34:58'),
(784, 58, 194, 128, NULL, '2019-11-22 11:34:58', '2019-11-22 11:34:58'),
(785, 21, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(787, 25, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(788, 27, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(789, 28, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(790, 29, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(791, 30, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(792, 59, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(793, 60, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(794, 62, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(795, 63, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(796, 67, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(797, 72, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(798, 80, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(799, 84, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(800, 86, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(801, 89, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(802, 98, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(803, 40, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(804, 42, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(805, 51, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(806, 52, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(807, 53, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(808, 48, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(809, 50, 198, 130, NULL, '2019-11-22 22:53:07', '2019-11-22 22:53:07'),
(810, 22, 167, 115, NULL, '2019-11-24 12:14:53', '2019-11-24 12:14:53'),
(811, 25, 167, 115, NULL, '2019-11-24 12:14:53', '2019-11-24 12:14:53'),
(812, 27, 167, 115, NULL, '2019-11-24 12:14:53', '2019-11-24 12:14:53'),
(813, 30, 167, 115, NULL, '2019-11-24 12:14:53', '2019-11-24 12:14:53'),
(814, 59, 167, 115, NULL, '2019-11-24 12:14:53', '2019-11-24 12:14:53'),
(815, 68, 167, 115, NULL, '2019-11-24 12:14:53', '2019-11-24 12:14:53'),
(816, 71, 167, 115, NULL, '2019-11-24 12:14:53', '2019-11-24 12:14:53'),
(817, 76, 167, 115, NULL, '2019-11-24 12:14:53', '2019-11-24 12:14:53'),
(818, 84, 167, 115, NULL, '2019-11-24 12:14:53', '2019-11-24 12:14:53'),
(819, 86, 167, 115, NULL, '2019-11-24 12:14:53', '2019-11-24 12:14:53'),
(820, 93, 167, 115, NULL, '2019-11-24 12:14:53', '2019-11-24 12:14:53'),
(821, 98, 167, 115, NULL, '2019-11-24 12:14:53', '2019-11-24 12:14:53'),
(822, 40, 167, 115, NULL, '2019-11-24 12:14:53', '2019-11-24 12:14:53'),
(823, 25, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(824, 27, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(825, 28, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(826, 29, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(827, 30, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(828, 59, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(829, 60, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(830, 62, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(831, 63, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(832, 66, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(833, 67, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(834, 68, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(835, 69, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(836, 70, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(837, 71, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(838, 72, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(839, 73, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(840, 74, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(841, 76, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(842, 77, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(843, 79, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(844, 80, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(845, 81, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(846, 82, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(847, 83, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(848, 84, 210, 136, NULL, '2019-11-25 08:20:57', '2019-11-25 08:20:57'),
(849, 85, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(850, 86, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(851, 87, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(852, 89, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(853, 91, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(854, 92, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(855, 93, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(856, 95, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(857, 98, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(858, 40, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(859, 42, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(860, 51, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(861, 52, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(862, 53, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(863, 54, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(864, 55, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(865, 56, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(866, 48, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(867, 50, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(868, 57, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(869, 58, 210, 136, NULL, '2019-11-25 08:20:58', '2019-11-25 08:20:58'),
(870, 22, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(871, 25, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(872, 27, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(873, 28, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(874, 29, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(875, 30, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(876, 31, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(877, 59, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(878, 60, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(879, 61, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(880, 62, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(881, 63, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(882, 64, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(883, 65, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(884, 66, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(885, 67, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(886, 68, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(887, 69, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(888, 70, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(889, 71, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14');
INSERT INTO `bravo_tour_term` (`id`, `term_id`, `tour_id`, `create_user`, `update_user`, `created_at`, `updated_at`) VALUES
(890, 72, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(891, 73, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(892, 74, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(893, 75, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(894, 77, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(895, 78, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(896, 79, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(897, 80, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(898, 81, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(899, 83, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(900, 84, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(901, 86, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(902, 87, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(903, 88, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(904, 89, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(905, 90, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(906, 91, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(907, 93, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(908, 94, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(909, 95, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(910, 98, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(911, 39, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(912, 40, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(913, 42, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(914, 51, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(915, 52, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(916, 53, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(917, 54, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(918, 55, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(919, 48, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(920, 50, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(921, 57, 212, 137, NULL, '2019-11-25 11:50:14', '2019-11-25 11:50:14'),
(922, 96, 212, 137, NULL, '2019-11-25 12:13:25', '2019-11-25 12:13:25'),
(933, 27, 216, 139, NULL, '2019-11-26 04:02:53', '2019-11-26 04:02:53'),
(934, 28, 216, 139, NULL, '2019-11-26 04:02:53', '2019-11-26 04:02:53'),
(935, 29, 216, 139, NULL, '2019-11-26 04:02:53', '2019-11-26 04:02:53'),
(936, 30, 216, 139, NULL, '2019-11-26 04:02:53', '2019-11-26 04:02:53'),
(937, 59, 216, 139, NULL, '2019-11-26 04:02:53', '2019-11-26 04:02:53'),
(938, 62, 216, 139, NULL, '2019-11-26 04:02:53', '2019-11-26 04:02:53'),
(939, 63, 216, 139, NULL, '2019-11-26 04:02:53', '2019-11-26 04:02:53'),
(940, 72, 216, 139, NULL, '2019-11-26 04:02:53', '2019-11-26 04:02:53'),
(941, 84, 216, 139, NULL, '2019-11-26 04:02:53', '2019-11-26 04:02:53'),
(942, 86, 216, 139, NULL, '2019-11-26 04:02:53', '2019-11-26 04:02:53'),
(943, 100, 216, 139, NULL, '2019-11-26 04:02:53', '2019-11-26 04:02:53'),
(944, 39, 216, 139, NULL, '2019-11-26 04:02:53', '2019-11-26 04:02:53'),
(945, 40, 216, 139, NULL, '2019-11-26 04:02:53', '2019-11-26 04:02:53'),
(946, 22, 216, 139, NULL, '2019-11-26 04:03:29', '2019-11-26 04:03:29'),
(947, 25, 216, 139, NULL, '2019-11-26 04:03:29', '2019-11-26 04:03:29'),
(948, 56, 216, 139, NULL, '2019-11-26 04:03:29', '2019-11-26 04:03:29'),
(949, 58, 216, 139, NULL, '2019-11-26 04:03:29', '2019-11-26 04:03:29'),
(950, 21, 101, 79, NULL, '2019-11-26 10:29:33', '2019-11-26 10:29:33'),
(951, 25, 101, 79, NULL, '2019-11-26 10:29:33', '2019-11-26 10:29:33'),
(952, 27, 101, 79, NULL, '2019-11-26 10:29:33', '2019-11-26 10:29:33'),
(953, 28, 101, 79, NULL, '2019-11-26 10:29:33', '2019-11-26 10:29:33'),
(954, 29, 101, 79, NULL, '2019-11-26 10:29:33', '2019-11-26 10:29:33'),
(955, 30, 101, 79, NULL, '2019-11-26 10:29:34', '2019-11-26 10:29:34'),
(956, 59, 101, 79, NULL, '2019-11-26 10:29:34', '2019-11-26 10:29:34'),
(957, 60, 101, 79, NULL, '2019-11-26 10:29:34', '2019-11-26 10:29:34'),
(958, 62, 101, 79, NULL, '2019-11-26 10:29:34', '2019-11-26 10:29:34'),
(959, 68, 101, 79, NULL, '2019-11-26 10:29:34', '2019-11-26 10:29:34'),
(960, 70, 101, 79, NULL, '2019-11-26 10:29:34', '2019-11-26 10:29:34'),
(961, 71, 101, 79, NULL, '2019-11-26 10:29:34', '2019-11-26 10:29:34'),
(962, 77, 101, 79, NULL, '2019-11-26 10:29:34', '2019-11-26 10:29:34'),
(963, 84, 101, 79, NULL, '2019-11-26 10:29:34', '2019-11-26 10:29:34'),
(964, 86, 101, 79, NULL, '2019-11-26 10:29:34', '2019-11-26 10:29:34'),
(965, 88, 101, 79, NULL, '2019-11-26 10:29:34', '2019-11-26 10:29:34'),
(966, 91, 101, 79, NULL, '2019-11-26 10:29:34', '2019-11-26 10:29:34'),
(967, 40, 101, 79, NULL, '2019-11-26 10:29:34', '2019-11-26 10:29:34'),
(968, 52, 101, 79, NULL, '2019-11-26 10:29:34', '2019-11-26 10:29:34'),
(969, 48, 101, 79, NULL, '2019-11-26 10:29:34', '2019-11-26 10:29:34'),
(970, 22, 163, 113, NULL, '2019-12-03 09:18:37', '2019-12-03 09:18:37'),
(971, 25, 163, 113, NULL, '2019-12-03 09:18:37', '2019-12-03 09:18:37'),
(972, 27, 163, 113, NULL, '2019-12-03 09:18:38', '2019-12-03 09:18:38'),
(973, 28, 163, 113, NULL, '2019-12-03 09:18:38', '2019-12-03 09:18:38'),
(974, 29, 163, 113, NULL, '2019-12-03 09:18:38', '2019-12-03 09:18:38'),
(975, 30, 163, 113, NULL, '2019-12-03 09:18:38', '2019-12-03 09:18:38'),
(976, 59, 163, 113, NULL, '2019-12-03 09:18:38', '2019-12-03 09:18:38'),
(977, 60, 163, 113, NULL, '2019-12-03 09:18:38', '2019-12-03 09:18:38'),
(978, 63, 163, 113, NULL, '2019-12-03 09:18:38', '2019-12-03 09:18:38'),
(979, 69, 163, 113, NULL, '2019-12-03 09:18:38', '2019-12-03 09:18:38'),
(980, 73, 163, 113, NULL, '2019-12-03 09:18:38', '2019-12-03 09:18:38'),
(981, 77, 163, 113, NULL, '2019-12-03 09:18:38', '2019-12-03 09:18:38'),
(982, 84, 163, 113, NULL, '2019-12-03 09:18:38', '2019-12-03 09:18:38'),
(983, 91, 163, 113, NULL, '2019-12-03 09:18:38', '2019-12-03 09:18:38'),
(984, 93, 163, 113, NULL, '2019-12-03 09:18:38', '2019-12-03 09:18:38'),
(985, 98, 163, 113, NULL, '2019-12-03 09:18:38', '2019-12-03 09:18:38'),
(986, 40, 163, 113, NULL, '2019-12-03 09:18:38', '2019-12-03 09:18:38'),
(987, 56, 163, 113, NULL, '2019-12-03 09:18:38', '2019-12-03 09:18:38'),
(988, 48, 163, 113, NULL, '2019-12-03 09:18:38', '2019-12-03 09:18:38'),
(989, 39, 44, 41, NULL, '2019-12-03 10:22:49', '2019-12-03 10:22:49'),
(990, 40, 44, 41, NULL, '2019-12-03 10:22:49', '2019-12-03 10:22:49'),
(991, 41, 44, 41, NULL, '2019-12-03 10:22:49', '2019-12-03 10:22:49'),
(992, 42, 44, 41, NULL, '2019-12-03 10:22:49', '2019-12-03 10:22:49'),
(993, 43, 44, 41, NULL, '2019-12-03 10:22:50', '2019-12-03 10:22:50'),
(994, 44, 44, 41, NULL, '2019-12-03 10:22:50', '2019-12-03 10:22:50'),
(995, 56, 44, 41, NULL, '2019-12-03 10:22:50', '2019-12-03 10:22:50'),
(996, 48, 44, 41, NULL, '2019-12-03 10:22:50', '2019-12-03 10:22:50'),
(997, 22, 44, 41, NULL, '2019-12-03 10:23:50', '2019-12-03 10:23:50'),
(998, 23, 44, 41, NULL, '2019-12-03 10:23:50', '2019-12-03 10:23:50'),
(999, 24, 44, 41, NULL, '2019-12-03 10:23:50', '2019-12-03 10:23:50'),
(1000, 25, 44, 41, NULL, '2019-12-03 10:23:50', '2019-12-03 10:23:50'),
(1001, 21, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1002, 26, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1003, 27, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1004, 64, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1005, 65, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1006, 66, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1007, 67, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1008, 69, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1009, 70, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1010, 75, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1011, 33, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1012, 34, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1013, 100, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1014, 39, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1015, 40, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1016, 41, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1017, 42, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1018, 43, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1019, 44, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1020, 56, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1021, 48, 226, 145, NULL, '2019-12-03 11:09:41', '2019-12-03 11:09:41'),
(1022, 27, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1023, 29, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1024, 30, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1025, 60, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1026, 62, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1027, 69, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1028, 70, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1029, 77, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1030, 81, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1031, 82, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1032, 84, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1033, 85, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1034, 86, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1035, 90, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1036, 93, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1037, 94, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1038, 95, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1039, 98, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1040, 40, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1041, 56, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1042, 58, 230, 147, NULL, '2019-12-04 04:15:36', '2019-12-04 04:15:36'),
(1044, 27, 75, 65, NULL, '2019-12-13 08:08:01', '2019-12-13 08:08:01'),
(1045, 28, 75, 65, NULL, '2019-12-13 08:08:01', '2019-12-13 08:08:01'),
(1046, 60, 75, 65, NULL, '2019-12-13 08:08:01', '2019-12-13 08:08:01'),
(1047, 62, 75, 65, NULL, '2019-12-13 08:08:01', '2019-12-13 08:08:01'),
(1048, 63, 75, 65, NULL, '2019-12-13 08:08:01', '2019-12-13 08:08:01'),
(1049, 69, 75, 65, NULL, '2019-12-13 08:08:01', '2019-12-13 08:08:01'),
(1050, 73, 75, 65, NULL, '2019-12-13 08:08:01', '2019-12-13 08:08:01'),
(1051, 76, 75, 65, NULL, '2019-12-13 08:08:01', '2019-12-13 08:08:01'),
(1052, 96, 75, 65, NULL, '2019-12-13 08:08:01', '2019-12-13 08:08:01'),
(1053, 100, 75, 65, NULL, '2019-12-13 08:08:01', '2019-12-13 08:08:01'),
(1054, 40, 75, 65, NULL, '2019-12-13 08:08:01', '2019-12-13 08:08:01'),
(1055, 56, 75, 65, NULL, '2019-12-13 08:08:01', '2019-12-13 08:08:01'),
(1056, 58, 75, 65, NULL, '2019-12-13 08:08:01', '2019-12-13 08:08:01'),
(1057, 22, 239, 46, NULL, '2019-12-13 09:41:27', '2019-12-13 09:41:27'),
(1058, 26, 239, 46, NULL, '2019-12-13 09:41:27', '2019-12-13 09:41:27'),
(1059, 66, 239, 46, NULL, '2019-12-13 09:41:27', '2019-12-13 09:41:27'),
(1060, 69, 239, 46, NULL, '2019-12-13 09:41:27', '2019-12-13 09:41:27'),
(1061, 73, 239, 46, NULL, '2019-12-13 09:41:27', '2019-12-13 09:41:27'),
(1062, 76, 239, 46, NULL, '2019-12-13 09:41:27', '2019-12-13 09:41:27'),
(1063, 77, 239, 46, NULL, '2019-12-13 09:41:27', '2019-12-13 09:41:27'),
(1064, 78, 239, 46, NULL, '2019-12-13 09:41:27', '2019-12-13 09:41:27'),
(1065, 86, 239, 46, NULL, '2019-12-13 09:41:27', '2019-12-13 09:41:27'),
(1066, 91, 239, 46, NULL, '2019-12-13 09:41:27', '2019-12-13 09:41:27'),
(1068, 40, 239, 46, NULL, '2019-12-13 09:41:27', '2019-12-13 09:41:27'),
(1069, 51, 239, 46, NULL, '2019-12-13 09:41:27', '2019-12-13 09:41:27'),
(1074, 48, 239, 46, NULL, '2019-12-13 09:41:27', '2019-12-13 09:41:27'),
(1085, 23, 240, 155, NULL, '2019-12-13 10:45:41', '2019-12-13 10:45:41'),
(1086, 24, 240, 155, NULL, '2019-12-13 10:45:41', '2019-12-13 10:45:41'),
(1087, 29, 240, 155, NULL, '2019-12-13 10:45:41', '2019-12-13 10:45:41'),
(1088, 30, 240, 155, NULL, '2019-12-13 10:45:41', '2019-12-13 10:45:41'),
(1089, 69, 240, 155, NULL, '2019-12-13 10:45:41', '2019-12-13 10:45:41'),
(1090, 70, 240, 155, NULL, '2019-12-13 10:45:41', '2019-12-13 10:45:41'),
(1091, 72, 240, 155, NULL, '2019-12-13 10:45:41', '2019-12-13 10:45:41'),
(1092, 79, 240, 155, NULL, '2019-12-13 10:45:41', '2019-12-13 10:45:41'),
(1093, 81, 240, 155, NULL, '2019-12-13 10:45:41', '2019-12-13 10:45:41'),
(1094, 84, 240, 155, NULL, '2019-12-13 10:45:41', '2019-12-13 10:45:41'),
(1095, 86, 240, 155, NULL, '2019-12-13 10:45:41', '2019-12-13 10:45:41'),
(1096, 89, 240, 155, NULL, '2019-12-13 10:45:41', '2019-12-13 10:45:41'),
(1097, 94, 240, 155, NULL, '2019-12-13 10:45:41', '2019-12-13 10:45:41'),
(1098, 95, 240, 155, NULL, '2019-12-13 10:45:41', '2019-12-13 10:45:41'),
(1099, 100, 240, 155, NULL, '2019-12-13 10:45:41', '2019-12-13 10:45:41'),
(1100, 40, 240, 155, NULL, '2019-12-13 10:45:41', '2019-12-13 10:45:41'),
(1101, 56, 240, 155, NULL, '2019-12-13 10:45:41', '2019-12-13 10:45:41'),
(1102, 57, 240, 155, NULL, '2019-12-13 10:45:41', '2019-12-13 10:45:41'),
(1109, 75, 239, 46, NULL, '2019-12-19 11:29:36', '2019-12-19 11:29:36'),
(1116, 22, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1117, 25, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1118, 27, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1119, 28, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1120, 29, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1121, 30, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1122, 60, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1123, 62, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1124, 63, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1125, 70, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1126, 73, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1127, 81, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1128, 84, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1129, 86, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1130, 87, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1131, 93, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1132, 95, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1133, 35, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1134, 98, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1135, 39, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1136, 40, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1137, 50, 97, 77, NULL, '2019-12-30 11:57:04', '2019-12-30 11:57:04'),
(1138, 21, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1139, 22, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1140, 24, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1141, 25, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1142, 26, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1143, 27, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1144, 28, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1145, 29, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1146, 30, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1147, 31, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1148, 59, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1149, 60, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1150, 61, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1151, 62, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1152, 63, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1153, 64, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1154, 65, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1155, 66, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1156, 67, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1157, 68, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1158, 69, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1159, 70, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1160, 71, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1161, 72, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1162, 73, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1163, 74, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1164, 75, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1165, 76, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1166, 77, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1167, 78, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1168, 79, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1169, 80, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1170, 81, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1171, 82, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1172, 83, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1173, 84, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1174, 85, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1175, 86, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1176, 87, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1177, 88, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1178, 89, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1179, 91, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1180, 92, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1181, 93, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1182, 94, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1183, 95, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1184, 98, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1185, 39, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1186, 40, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1187, 42, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1188, 51, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1189, 52, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1190, 53, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1191, 54, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1192, 55, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1193, 48, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1194, 50, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1195, 57, 175, 119, NULL, '2020-01-02 23:01:57', '2020-01-02 23:01:57'),
(1196, 33, 75, 65, NULL, '2020-01-14 09:00:32', '2020-01-14 09:00:32'),
(1215, 42, 239, 46, NULL, '2020-01-14 10:13:52', '2020-01-14 10:13:52'),
(1216, 52, 239, 46, NULL, '2020-01-14 10:16:06', '2020-01-14 10:16:06'),
(1217, 53, 239, 46, NULL, '2020-01-14 10:16:06', '2020-01-14 10:16:06'),
(1218, 54, 239, 46, NULL, '2020-01-14 10:16:06', '2020-01-14 10:16:06'),
(1219, 55, 239, 46, NULL, '2020-01-14 10:16:06', '2020-01-14 10:16:06'),
(1220, 21, 75, 65, NULL, '2020-01-15 08:21:39', '2020-01-15 08:21:39'),
(1221, 25, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1222, 27, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1223, 28, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1224, 29, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1225, 30, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1226, 68, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1227, 69, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1228, 76, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1229, 78, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1230, 81, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1231, 82, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1232, 84, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1233, 86, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1234, 89, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1235, 93, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1236, 95, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1237, 98, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1238, 40, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1239, 51, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1240, 53, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1241, 50, 248, 164, NULL, '2020-01-17 13:05:12', '2020-01-17 13:05:12'),
(1242, 60, 248, 164, NULL, '2020-01-19 16:11:59', '2020-01-19 16:11:59'),
(1243, 94, 248, 164, NULL, '2020-01-19 16:11:59', '2020-01-19 16:11:59'),
(1244, 21, 248, 164, NULL, '2020-01-19 16:12:40', '2020-01-19 16:12:40'),
(1260, 96, 239, 46, NULL, '2020-01-22 11:12:04', '2020-01-22 11:12:04'),
(1265, 97, 239, 46, NULL, '2020-01-22 11:14:07', '2020-01-22 11:14:07'),
(1266, 36, 17, 1, NULL, '2020-01-23 12:24:32', '2020-01-23 12:24:32'),
(1267, 37, 17, 1, NULL, '2020-01-23 12:24:32', '2020-01-23 12:24:32'),
(1268, 38, 17, 1, NULL, '2020-01-23 12:24:32', '2020-01-23 12:24:32'),
(1269, 96, 17, 1, NULL, '2020-01-23 12:24:32', '2020-01-23 12:24:32'),
(1270, 97, 17, 1, NULL, '2020-01-23 12:24:32', '2020-01-23 12:24:32'),
(1271, 98, 17, 1, NULL, '2020-01-23 12:24:32', '2020-01-23 12:24:32'),
(1272, 99, 17, 1, NULL, '2020-01-23 12:24:32', '2020-01-23 12:24:32'),
(1273, 100, 17, 1, NULL, '2020-01-23 12:24:32', '2020-01-23 12:24:32'),
(1274, 25, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1275, 27, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1276, 28, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1277, 29, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1278, 30, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1279, 60, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1280, 63, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1281, 66, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1282, 68, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1283, 69, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1284, 70, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1285, 71, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1286, 72, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1287, 77, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1288, 81, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1289, 82, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1290, 85, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1291, 86, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1292, 87, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1293, 89, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1294, 90, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1295, 91, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1296, 92, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1297, 93, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1298, 95, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1299, 98, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1300, 39, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1301, 40, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1302, 51, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1303, 52, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1304, 53, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1305, 54, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1306, 48, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1307, 50, 249, 165, NULL, '2020-01-28 12:26:41', '2020-01-28 12:26:41'),
(1308, 22, 198, 130, NULL, '2020-01-31 17:58:56', '2020-01-31 17:58:56'),
(1309, 27, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1310, 28, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1311, 29, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1312, 30, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1313, 59, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1314, 64, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1315, 65, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1316, 68, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1317, 70, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1318, 74, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1319, 77, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1320, 78, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1321, 80, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1322, 84, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1323, 86, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1324, 87, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1325, 88, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1326, 91, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1327, 92, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1328, 93, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1329, 96, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1330, 98, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1331, 39, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1332, 40, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1333, 42, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1334, 43, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1335, 51, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1336, 48, 246, 162, NULL, '2020-02-15 08:58:29', '2020-02-15 08:58:29'),
(1337, 25, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1338, 27, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1339, 28, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1340, 29, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1341, 30, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1342, 59, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1343, 60, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1344, 62, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1345, 63, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1346, 69, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1347, 70, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1348, 71, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1349, 74, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1350, 84, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1351, 86, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1352, 98, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1353, 40, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1354, 42, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1355, 48, 208, 135, NULL, '2020-03-11 06:30:16', '2020-03-11 06:30:16'),
(1356, 2, 256, 44, NULL, '2020-07-28 11:43:13', '2020-07-28 11:43:13'),
(1357, 3, 256, 44, NULL, '2020-07-28 11:43:13', '2020-07-28 11:43:13'),
(1358, 4, 256, 44, NULL, '2020-07-28 11:43:13', '2020-07-28 11:43:13');

-- --------------------------------------------------------

--
-- Table structure for table `bravo_tour_translations`
--

CREATE TABLE `bravo_tour_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `short_desc` text COLLATE utf8mb4_unicode_ci,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faqs` text COLLATE utf8mb4_unicode_ci,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `highlights` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_inbox`
--

CREATE TABLE `core_inbox` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `from_user` bigint(20) DEFAULT NULL,
  `to_user` bigint(20) DEFAULT NULL,
  `object_id` bigint(20) DEFAULT NULL,
  `object_model` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` tinyint(4) DEFAULT '0',
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_inbox_messages`
--

CREATE TABLE `core_inbox_messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inbox_id` bigint(20) DEFAULT NULL,
  `from_user` bigint(20) DEFAULT NULL,
  `to_user` bigint(20) DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `type` tinyint(4) DEFAULT '0',
  `is_read` tinyint(4) DEFAULT '0',
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_languages`
--

CREATE TABLE `core_languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flag` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `last_build_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `core_languages`
--

INSERT INTO `core_languages` (`id`, `locale`, `name`, `flag`, `status`, `create_user`, `update_user`, `last_build_at`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'en', 'English', 'gb', 'publish', 1, NULL, NULL, NULL, '2020-05-13 18:13:37', '2020-05-13 18:13:37'),
(2, 'ja', 'Japanese', 'jp', 'publish', 1, NULL, NULL, NULL, '2020-05-13 18:13:37', '2020-05-13 18:13:37');

-- --------------------------------------------------------

--
-- Table structure for table `core_menus`
--

CREATE TABLE `core_menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `items` longtext COLLATE utf8mb4_unicode_ci,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `lang` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `core_menus`
--

INSERT INTO `core_menus` (`id`, `name`, `items`, `create_user`, `update_user`, `origin_id`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'Main Menu', '[{\"name\":\"Home\",\"url\":\"\\/\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[{\"name\":\"Home Tour\",\"url\":\"\\/\",\"item_model\":\"custom\",\"children\":[]},{\"name\":\"Home Space\",\"url\":\"\\/page\\/space\",\"item_model\":\"custom\",\"children\":[]}]},{\"name\":\"Tours\",\"url\":\"\\/tour\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[{\"name\":\"Tour List\",\"url\":\"\\/tour\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[]},{\"name\":\"Tour Map\",\"url\":\"\\/tour?_layout=map\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[]},{\"name\":\"Tour Detail\",\"url\":\"\\/tour\\/paris-vacation-travel\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[]}]},{\"name\":\"Space\",\"url\":\"\\/space\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[{\"name\":\"Space List\",\"url\":\"\\/space\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[]},{\"name\":\"Space Detail\",\"url\":\"\\/space\\/stay-greenwich-village\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[]}]},{\"name\":\"Pages\",\"url\":\"#\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[{\"name\":\"News List\",\"url\":\"\\/news\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[]},{\"name\":\"News Detail\",\"url\":\"\\/news\\/morning-in-the-northern-sea\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[]},{\"name\":\"Location Detail\",\"url\":\"\\/location\\/paris\",\"item_model\":\"custom\",\"children\":[]},{\"name\":\"Become a vendor\",\"url\":\"\\/page\\/become-a-vendor\",\"item_model\":\"custom\",\"children\":[]}]},{\"name\":\"Contact\",\"url\":\"\\/contact\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[]}]', 1, NULL, NULL, NULL, '2020-05-13 18:13:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `core_menu_translations`
--

CREATE TABLE `core_menu_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `origin_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `items` longtext COLLATE utf8mb4_unicode_ci,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `core_menu_translations`
--

INSERT INTO `core_menu_translations` (`id`, `origin_id`, `locale`, `items`, `create_user`, `update_user`, `created_at`, `updated_at`) VALUES
(1, 1, 'ja', '[{\"name\":\"\\u30db\\u30fc\\u30e0\",\"url\":\"\\/\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[{\"name\":\"\\u30db\\u30fc\\u30e0 \\u30c4\\u30a2\\u30fc\",\"url\":\"\\/ja\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[]},{\"name\":\"\\u30db\\u30fc\\u30e0\\u30b9\\u30da\\u30fc\\u30b9\",\"url\":\"\\/ja\\/page\\/space\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[]}]},{\"name\":\"\\u30c4\\u30a2\\u30fc\",\"url\":\"\\/ja\\/tour\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[{\"name\":\"\\u30c4\\u30a2\\u30fc\\u30ea\\u30b9\\u30c8\",\"url\":\"\\/ja\\/tour\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[]},{\"name\":\"\\u30c4\\u30a2\\u30fc\\u30de\\u30c3\\u30d7\",\"url\":\"\\/ja\\/tour?_layout=map\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[]},{\"name\":\"\\u30c4\\u30a2\\u30fc\\u8a73\\u7d30\",\"url\":\"\\/ja\\/tour\\/paris-vacation-travel\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[]}]},{\"name\":\"\\u30b9\\u30da\\u30fc\\u30b9\",\"url\":\"\\/ja\\/space\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[{\"name\":\"\\u30b9\\u30da\\u30fc\\u30b9\\u30ea\\u30b9\\u30c8\",\"url\":\"\\/ja\\/space\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[]},{\"name\":\"\\u30b9\\u30da\\u30fc\\u30b9\\u306e\\u8a73\\u7d30\",\"url\":\"\\/ja\\/space\\/stay-greenwich-village\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[]}]},{\"name\":\"\\u30da\\u30fc\\u30b8\",\"url\":\"#\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[{\"name\":\"\\u30cb\\u30e5\\u30fc\\u30b9\\u4e00\\u89a7\",\"url\":\"\\/ja\\/news\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[]},{\"name\":\"\\u30cb\\u30e5\\u30fc\\u30b9\\u8a73\\u7d30\",\"url\":\"\\/ja\\/news\\/morning-in-the-northern-sea\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[]},{\"name\":\"\\u5834\\u6240\\u306e\\u8a73\\u7d30\",\"url\":\"\\/ja\\/location\\/paris\",\"item_model\":\"custom\",\"children\":[]},{\"name\":\"\\u30d9\\u30f3\\u30c0\\u30fc\\u306b\\u306a\\u308b\",\"url\":\"\\/ja\\/page\\/become-a-vendor\",\"item_model\":\"custom\",\"children\":[]}]},{\"name\":\"\\u63a5\\u89e6\",\"url\":\"\\/ja\\/contact\",\"item_model\":\"custom\",\"model_name\":\"Custom\",\"children\":[]}]', 1, NULL, '2020-05-13 18:13:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `core_model_has_permissions`
--

CREATE TABLE `core_model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_model_has_roles`
--

CREATE TABLE `core_model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `core_model_has_roles`
--

INSERT INTO `core_model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(3, 'App\\User', 1),
(1, 'App\\User', 2),
(2, 'App\\User', 3),
(1, 'App\\User', 4),
(1, 'App\\User', 5),
(1, 'App\\User', 6),
(2, 'App\\User', 7),
(2, 'App\\User', 8),
(2, 'App\\User', 9),
(2, 'App\\User', 10),
(2, 'App\\User', 11),
(2, 'App\\User', 12),
(2, 'App\\User', 13),
(2, 'App\\User', 14),
(2, 'App\\User', 15),
(2, 'App\\User', 16),
(2, 'App\\User', 17),
(2, 'App\\User', 18),
(2, 'App\\User', 19),
(2, 'App\\User', 20),
(2, 'App\\User', 21),
(2, 'App\\User', 22),
(2, 'App\\User', 23),
(2, 'App\\User', 24),
(1, 'App\\User', 25),
(1, 'App\\User', 26),
(2, 'App\\User', 27),
(2, 'App\\User', 28),
(2, 'App\\User', 29),
(2, 'App\\User', 30),
(2, 'App\\User', 31),
(2, 'App\\User', 32),
(2, 'App\\User', 33),
(2, 'App\\User', 34),
(1, 'App\\User', 35),
(1, 'App\\User', 36),
(2, 'App\\User', 37),
(1, 'App\\User', 38),
(1, 'App\\User', 39),
(2, 'App\\User', 40),
(1, 'App\\User', 41),
(1, 'App\\User', 42),
(1, 'App\\User', 43),
(1, 'App\\User', 44),
(1, 'App\\User', 45),
(2, 'App\\User', 46),
(2, 'App\\User', 47),
(1, 'App\\User', 48),
(1, 'App\\User', 49),
(1, 'App\\User', 50),
(1, 'App\\User', 51);

-- --------------------------------------------------------

--
-- Table structure for table `core_news`
--

CREATE TABLE `core_news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `lang` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `core_news`
--

INSERT INTO `core_news` (`id`, `title`, `content`, `slug`, `status`, `cat_id`, `image_id`, `create_user`, `update_user`, `deleted_at`, `origin_id`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'The day on Paris', ' From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception  From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception <br/>From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception<br/>\n    From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception', 'the-day-on-paris', 'publish', 3, 96, 1, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', NULL),
(2, 'Pure Luxe in Punta Mita', ' From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception <br/>From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception<br/>\n    From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception', 'pure-luxe-in-punta-mita', 'publish', 4, 97, 1, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', NULL),
(3, 'All Aboard the Rocky Mountaineer', ' From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception  From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception <br/>From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception<br/>\n    From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception', 'all-aboard-the-rocky-mountaineer', 'publish', 3, 98, 1, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', NULL),
(4, 'City Spotlight: Philadelphia', ' From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception  From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception <br/>From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception<br/>\n    From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception', 'city-spotlight-philadelphia', 'publish', 3, 99, 1, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', NULL),
(6, 'A Seaside Reset in Laguna Beach', ' From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception  From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception <br/>From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception<br/>\n    From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception', 'a-seaside-reset-in-laguna-beach', 'publish', 4, 101, 1, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', NULL),
(7, 'America  National Parks with Denver', ' From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception  From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception <br/>From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception<br/>\n    From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception', 'america-national-parks-with-denver', 'publish', 4, 102, 1, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', NULL),
(8, 'Morning in the Northern sea', ' From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception  From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception <br/>From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception<br/>\n    From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception', 'morning-in-the-northern-sea', 'publish', 1, 97, 1, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `core_news_category`
--

CREATE TABLE `core_news_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `lang` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `core_news_category`
--

INSERT INTO `core_news_category` (`id`, `name`, `content`, `slug`, `status`, `_lft`, `_rgt`, `parent_id`, `create_user`, `update_user`, `deleted_at`, `created_at`, `updated_at`, `origin_id`, `lang`) VALUES
(1, 'Therapist', NULL, 'adventure-travel', 'publish', 1, 2, NULL, 1, 1, NULL, '2020-05-13 18:13:39', '2020-07-20 14:41:41', NULL, NULL),
(2, 'Ecotourism', NULL, 'ecotourism', 'publish', 3, 4, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39', NULL, NULL),
(3, 'Sea Travel ', NULL, 'sea-travel', 'publish', 5, 6, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39', NULL, NULL),
(4, 'Hosted Tour', NULL, 'hosted-tour', 'publish', 7, 8, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39', NULL, NULL),
(5, 'City trips ', NULL, 'city-trips', 'publish', 9, 10, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39', NULL, NULL),
(6, 'Escorted Tour ', NULL, 'escorted-tour', 'publish', 11, 12, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `core_news_category_translations`
--

CREATE TABLE `core_news_category_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `origin_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_news_tag`
--

CREATE TABLE `core_news_tag` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `news_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_news_translations`
--

CREATE TABLE `core_news_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `origin_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_notifications`
--

CREATE TABLE `core_notifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `from_user` bigint(20) DEFAULT NULL,
  `to_user` bigint(20) DEFAULT NULL,
  `is_read` tinyint(4) DEFAULT '0',
  `type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_group` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target_id` bigint(20) DEFAULT NULL,
  `target_parent_id` bigint(20) DEFAULT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_pages`
--

CREATE TABLE `core_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `short_desc` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `lang` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `core_pages`
--

INSERT INTO `core_pages` (`id`, `slug`, `title`, `content`, `short_desc`, `status`, `publish_date`, `image_id`, `template_id`, `create_user`, `update_user`, `deleted_at`, `origin_id`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'home-page', 'Home Page', NULL, NULL, 'publish', NULL, NULL, 4, 1, 1, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-28 03:09:19'),
(2, 'space', 'Home Space', NULL, NULL, 'publish', NULL, NULL, 2, 1, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', NULL),
(3, 'join-our-community', 'Become a vendor', NULL, NULL, 'publish', NULL, NULL, 3, 1, 1, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-07-19 22:54:08'),
(4, 'about-us', 'About Us', '<p style=\"margin-top:0px;margin-bottom:1rem;font-size:14px;color:#1a2b48;font-family:Ubuntu, sans-serif;text-align:center;background-color:#ffffff;\">takeaseat.co.uk is an online community bringing together potential clients with counsellors, therapists and coaches. With ZERO cost to professionals. Our mission is to get people the help they need and let the professionals focus on the work they love</p>\n<h6 class=\"pink-text font-weight-bold\" style=\"margin-top:0px;margin-bottom:.5rem;line-height:1.2;font-size:1rem;color:#d8006b;font-family:Ubuntu, sans-serif;text-align:center;background-color:#ffffff;\">Our Story</h6>\n<p style=\"margin-top:0px;margin-bottom:1rem;font-size:14px;color:#1a2b48;font-family:Ubuntu, sans-serif;text-align:center;background-color:#ffffff;\">After 5 years of education alongside a full-time job. Pam had fully qualified as a counsellor and was ready to commit to a focussed career in something she loved and felt was her calling. One big problem, no clients</p>\n<p style=\"margin-top:0px;margin-bottom:1rem;font-size:14px;color:#1a2b48;font-family:Ubuntu, sans-serif;text-align:center;background-color:#ffffff;\">At nearly 60 years old with an ever-changing digital landscape, social media options Pam never fully understood and limited financial resources. She was in a chicken/egg scenario of no clients, no money. The road ahead seemed long and difficult. Alongside this, on my journey, I had spent a decade building a business, while raising a family, had found many complications while looking for support with the stresses that accompanied such a hectic lifestyle. Life coaches and marriage counselling had been hard to find. People not talking about their own experiences or recommending anyone. It felt like a gamble every time you were to meet a professional. Not to mention the expectation of opening up to someone you had found on a google search or directory.</p>\n<p style=\"margin-top:0px;margin-bottom:1rem;font-size:14px;color:#1a2b48;font-family:Ubuntu, sans-serif;text-align:center;background-color:#ffffff;\">Together we decided something had to be done. Teaming up with an expert Marketeer, who had had similar frustration on his journey and had given up on his search for support. Also, a social worker and family mediator who is passionate about getting people and families the help they need. Take a seat was born</p>', NULL, 'publish', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, '2020-07-02 13:32:17', '2020-07-02 13:42:50');

-- --------------------------------------------------------

--
-- Table structure for table `core_page_translations`
--

CREATE TABLE `core_page_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `origin_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `short_desc` text COLLATE utf8mb4_unicode_ci,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_permissions`
--

CREATE TABLE `core_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `core_permissions`
--

INSERT INTO `core_permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'report_view', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(2, 'contact_manage', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(3, 'newsletter_manage', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(4, 'language_manage', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(5, 'language_translation', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(6, 'booking_view', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(7, 'booking_update', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(8, 'booking_manage_others', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(9, 'template_view', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(10, 'template_create', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(11, 'template_update', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(12, 'template_delete', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(13, 'news_view', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(14, 'news_create', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(15, 'news_update', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(16, 'news_delete', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(17, 'news_manage_others', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(18, 'role_view', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(19, 'role_create', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(20, 'role_update', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(21, 'role_delete', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(22, 'permission_view', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(23, 'permission_create', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(24, 'permission_update', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(25, 'permission_delete', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(26, 'dashboard_access', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(27, 'dashboard_vendor_access', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(28, 'setting_update', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(29, 'menu_view', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(30, 'menu_create', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(31, 'menu_update', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(32, 'menu_delete', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(33, 'user_view', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(34, 'user_create', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(35, 'user_update', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(36, 'user_delete', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(37, 'page_view', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(38, 'page_create', 'web', '2020-05-13 18:13:35', '2020-05-13 18:13:35'),
(39, 'page_update', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(40, 'page_delete', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(41, 'page_manage_others', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(42, 'setting_view', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(43, 'media_upload', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(44, 'media_manage', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(45, 'tour_view', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(46, 'tour_create', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(47, 'tour_update', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(48, 'tour_delete', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(49, 'tour_manage_others', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(50, 'tour_manage_attributes', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(51, 'location_view', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(52, 'location_create', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(53, 'location_update', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(54, 'location_delete', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(55, 'location_manage_others', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(56, 'review_manage_others', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(57, 'system_log_view', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(58, 'space_view', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(59, 'space_create', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(60, 'space_update', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(61, 'space_delete', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(62, 'space_manage_others', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(63, 'space_manage_attributes', 'web', '2020-05-13 18:13:36', '2020-05-13 18:13:36');

-- --------------------------------------------------------

--
-- Table structure for table `core_roles`
--

CREATE TABLE `core_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `lang` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `core_roles`
--

INSERT INTO `core_roles` (`id`, `name`, `guard_name`, `origin_id`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'vendor', 'web', NULL, NULL, '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(2, 'customer', 'web', NULL, NULL, '2020-05-13 18:13:36', '2020-05-13 18:13:36'),
(3, 'administrator', 'web', NULL, NULL, '2020-05-13 18:13:36', '2020-05-13 18:13:36');

-- --------------------------------------------------------

--
-- Table structure for table `core_role_has_permissions`
--

CREATE TABLE `core_role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `core_role_has_permissions`
--

INSERT INTO `core_role_has_permissions` (`permission_id`, `role_id`) VALUES
(27, 1),
(43, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(1, 3),
(2, 3),
(3, 3),
(4, 3),
(5, 3),
(6, 3),
(7, 3),
(8, 3),
(9, 3),
(10, 3),
(11, 3),
(12, 3),
(13, 3),
(14, 3),
(15, 3),
(16, 3),
(17, 3),
(18, 3),
(19, 3),
(20, 3),
(21, 3),
(22, 3),
(23, 3),
(24, 3),
(25, 3),
(26, 3),
(27, 3),
(28, 3),
(29, 3),
(30, 3),
(31, 3),
(32, 3),
(33, 3),
(34, 3),
(35, 3),
(36, 3),
(37, 3),
(38, 3),
(39, 3),
(40, 3),
(41, 3),
(42, 3),
(43, 3),
(44, 3),
(45, 3),
(46, 3),
(47, 3),
(48, 3),
(49, 3),
(50, 3),
(51, 3),
(52, 3),
(53, 3),
(54, 3),
(55, 3),
(56, 3),
(57, 3),
(58, 3),
(59, 3),
(60, 3),
(61, 3),
(62, 3),
(63, 3);

-- --------------------------------------------------------

--
-- Table structure for table `core_settings`
--

CREATE TABLE `core_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `val` text COLLATE utf8mb4_unicode_ci,
  `autoload` tinyint(4) DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `lang` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `core_settings`
--

INSERT INTO `core_settings` (`id`, `name`, `group`, `val`, `autoload`, `create_user`, `update_user`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'site_locale', 'general', 'en', NULL, 1, 1, NULL, NULL, '2020-05-28 03:12:25'),
(2, 'site_enable_multi_lang', 'general', NULL, NULL, 1, 1, NULL, NULL, '2020-05-28 03:12:25'),
(3, 'menu_locations', 'general', '{\"primary\":1}', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'admin_email', 'general', 'contact@bookingcore.com', NULL, 1, 1, NULL, NULL, '2020-05-28 03:12:24'),
(5, 'email_from_name', 'general', 'Take a Seat', NULL, 1, 1, NULL, NULL, '2020-05-28 03:12:24'),
(6, 'email_from_address', 'general', 'contact@bookingcore.com', NULL, 1, 1, NULL, NULL, '2020-05-28 03:12:24'),
(7, 'logo_id', 'general', '84', NULL, 1, 1, NULL, NULL, '2020-05-28 09:18:54'),
(8, 'site_favicon', 'general', '10', NULL, 1, 1, NULL, NULL, '2020-05-28 03:12:24'),
(9, 'topbar_left_text', 'general', '<div class=\"socials\"><a href=\"#\"><i class=\"fa fa-facebook\"></i></a><a href=\"#\"><i class=\"fa fa-linkedin\"></i></a><a href=\"#\"><i class=\"fa fa-google-plus\"></i></a></div><span class=\"line\"></span><a href=\"mailto:contact@takeaseat.com\">contact@takeaseat.co.uk</a>', NULL, 1, 1, NULL, NULL, '2020-05-28 03:12:24'),
(10, 'footer_text_left', 'general', '<p>Copyright &copy; 2019 by Booking</p>', NULL, 1, 1, NULL, NULL, '2020-05-28 03:12:25'),
(11, 'footer_text_right', 'general', '<p>Take a Seat</p>', NULL, 1, 1, NULL, NULL, '2020-05-28 03:12:25'),
(12, 'list_widget_footer', 'general', '[{\"title\":\"NEED HELP?\",\"size\":\"3\",\"content\":\"<div class=\\\"contact\\\">\\r\\n        <div class=\\\"c-title\\\">\\r\\n            Call Us\\r\\n        <\\/div>\\r\\n        <div class=\\\"sub\\\">\\r\\n            + 00 222 44 5678\\r\\n        <\\/div>\\r\\n    <\\/div>\\r\\n    <div class=\\\"contact\\\">\\r\\n        <div class=\\\"c-title\\\">\\r\\n            Email for Us\\r\\n        <\\/div>\\r\\n        <div class=\\\"sub\\\">\\r\\n            hello@yoursite.com\\r\\n        <\\/div>\\r\\n    <\\/div>\\r\\n    <div class=\\\"contact\\\">\\r\\n        <div class=\\\"c-title\\\">\\r\\n            Follow Us\\r\\n        <\\/div>\\r\\n        <div class=\\\"sub\\\">\\r\\n            <a href=\\\"#\\\">\\r\\n                <i class=\\\"icofont-facebook\\\"><\\/i>\\r\\n            <\\/a>\\r\\n            <a href=\\\"#\\\">\\r\\n               <i class=\\\"icofont-twitter\\\"><\\/i>\\r\\n            <\\/a>\\r\\n            <a href=\\\"#\\\">\\r\\n                <i class=\\\"icofont-youtube-play\\\"><\\/i>\\r\\n            <\\/a>\\r\\n        <\\/div>\\r\\n    <\\/div>\"},{\"title\":\"COMPANY\",\"size\":\"3\",\"content\":\"<ul>\\r\\n    <li><a href=\\\"#\\\">About Us<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">Community Blog<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">Rewards<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">Work with Us<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">Meet the Team<\\/a><\\/li>\\r\\n<\\/ul>\"},{\"title\":\"SUPPORT\",\"size\":\"3\",\"content\":\"<ul>\\r\\n    <li><a href=\\\"#\\\">Account<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">Legal<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">Contact<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">Affiliate Program<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">Privacy Policy<\\/a><\\/li>\\r\\n<\\/ul>\"},{\"title\":\"SETTINGS\",\"size\":\"3\",\"content\":\"<ul>\\r\\n<li><a href=\\\"#\\\">Setting 1<\\/a><\\/li>\\r\\n<li><a href=\\\"#\\\">Setting 2<\\/a><\\/li>\\r\\n<\\/ul>\"}]', NULL, 1, 1, NULL, NULL, '2020-05-28 03:12:25'),
(13, 'list_widget_footer_ja', 'general', '[{\"title\":\"\\u52a9\\u3051\\u304c\\u5fc5\\u8981\\uff1f\",\"size\":\"3\",\"content\":\"<div class=\\\"contact\\\">\\r\\n        <div class=\\\"c-title\\\">\\r\\n            \\u304a\\u96fb\\u8a71\\u304f\\u3060\\u3055\\u3044\\r\\n        <\\/div>\\r\\n        <div class=\\\"sub\\\">\\r\\n            + 00 222 44 5678\\r\\n        <\\/div>\\r\\n    <\\/div>\\r\\n    <div class=\\\"contact\\\">\\r\\n        <div class=\\\"c-title\\\">\\r\\n            \\u90f5\\u4fbf\\u7269\\r\\n        <\\/div>\\r\\n        <div class=\\\"sub\\\">\\r\\n            hello@yoursite.com\\r\\n        <\\/div>\\r\\n    <\\/div>\\r\\n    <div class=\\\"contact\\\">\\r\\n        <div class=\\\"c-title\\\">\\r\\n            \\u30d5\\u30a9\\u30ed\\u30fc\\u3059\\u308b\\r\\n        <\\/div>\\r\\n        <div class=\\\"sub\\\">\\r\\n            <a href=\\\"#\\\">\\r\\n                <i class=\\\"icofont-facebook\\\"><\\/i>\\r\\n            <\\/a>\\r\\n            <a href=\\\"#\\\">\\r\\n                <i class=\\\"icofont-twitter\\\"><\\/i>\\r\\n            <\\/a>\\r\\n            <a href=\\\"#\\\">\\r\\n                <i class=\\\"icofont-youtube-play\\\"><\\/i>\\r\\n            <\\/a>\\r\\n        <\\/div>\\r\\n    <\\/div>\"},{\"title\":\"\\u4f1a\\u793e\",\"size\":\"3\",\"content\":\"<ul>\\r\\n    <li><a href=\\\"#\\\">\\u7d04, \\u7565<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">\\u30b3\\u30df\\u30e5\\u30cb\\u30c6\\u30a3\\u30d6\\u30ed\\u30b0<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">\\u5831\\u916c<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">\\u3068\\u9023\\u643a<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">\\u30c1\\u30fc\\u30e0\\u306b\\u4f1a\\u3046<\\/a><\\/li>\\r\\n<\\/ul>\"},{\"title\":\"\\u30b5\\u30dd\\u30fc\\u30c8\",\"size\":\"3\",\"content\":\"<ul>\\r\\n    <li><a href=\\\"#\\\">\\u30a2\\u30ab\\u30a6\\u30f3\\u30c8<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">\\u6cd5\\u7684<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">\\u63a5\\u89e6<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">\\u30a2\\u30d5\\u30a3\\u30ea\\u30a8\\u30a4\\u30c8\\u30d7\\u30ed\\u30b0\\u30e9\\u30e0<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">\\u500b\\u4eba\\u60c5\\u5831\\u4fdd\\u8b77\\u65b9\\u91dd<\\/a><\\/li>\\r\\n<\\/ul>\"},{\"title\":\"\\u8a2d\\u5b9a\",\"size\":\"3\",\"content\":\"<ul>\\r\\n<li><a href=\\\"#\\\">\\u8a2d\\u5b9a1<\\/a><\\/li>\\r\\n<li><a href=\\\"#\\\">\\u8a2d\\u5b9a2<\\/a><\\/li>\\r\\n<\\/ul>\"}]', NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'home_page_id', 'general', '1', NULL, 1, 1, NULL, NULL, '2020-05-28 03:12:24'),
(15, 'page_contact_title', 'general', 'We\'d love to hear from you', NULL, 1, 1, NULL, NULL, '2020-05-28 03:12:25'),
(16, 'page_contact_title_ja', 'general', 'あなたからの御一報をお待ち', NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'page_contact_sub_title', 'general', 'Send us a message and we\'ll respond as soon as possible', NULL, 1, 1, NULL, NULL, '2020-05-28 03:12:25'),
(18, 'page_contact_sub_title_ja', 'general', '私たちにメッセージを送ってください、私たちはできるだ', NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'page_contact_desc', 'general', '<h3>Take a Seat</h3>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Tell. + 00 222 444 33</p>\r\n<p>Email. hello@yoursite.com</p>\r\n<p>1355 Market St, Suite 900San, Francisco, CA 94103 United States</p>', NULL, 1, 1, NULL, NULL, '2020-05-28 03:12:25'),
(20, 'page_contact_image', 'general', '9', NULL, 1, 1, NULL, NULL, '2020-05-28 03:12:25'),
(21, 'currency_main', 'payment', 'gbp', NULL, 1, NULL, NULL, NULL, '2020-07-24 18:11:03'),
(22, 'currency_format', 'payment', 'left', NULL, 1, NULL, NULL, NULL, '2020-07-24 18:11:03'),
(23, 'currency_decimal', 'payment', ',', NULL, 1, NULL, NULL, NULL, '2020-07-24 18:11:03'),
(24, 'currency_thousand', 'payment', '.', NULL, 1, NULL, NULL, NULL, '2020-07-24 18:11:03'),
(25, 'currency_no_decimal', 'payment', '0', NULL, 1, NULL, NULL, NULL, '2020-07-24 18:11:03'),
(26, 'map_provider', 'advance', 'gmap', NULL, 1, NULL, NULL, NULL, '2020-06-06 12:08:49'),
(27, 'map_gmap_key', 'advance', 'AIzaSyCcMZwKS16YQ7nde9_GVNXyXHvgaMnDOkQ', NULL, 1, NULL, NULL, NULL, '2020-06-06 12:08:49'),
(28, 'g_offline_payment_enable', 'payment', '1', NULL, 1, NULL, NULL, NULL, '2020-07-24 18:11:03'),
(29, 'g_offline_payment_name', 'payment', 'Offline Payment', NULL, 1, NULL, NULL, NULL, '2020-07-24 18:11:03'),
(30, 'date_format', 'general', 'm/d/Y', NULL, 1, 1, NULL, NULL, '2020-05-28 03:12:25'),
(31, 'site_title', 'general', 'Take a Seat', NULL, 1, 1, NULL, NULL, '2020-05-28 03:12:24'),
(32, 'site_timezone', 'general', 'UTC', NULL, 1, 1, NULL, NULL, '2020-05-28 03:12:25'),
(33, 'site_title', 'general', 'Take a Seat', NULL, NULL, NULL, NULL, NULL, NULL),
(34, 'email_header', 'general', '<h1 class=\"site-title\" style=\"text-align: center\">Take a Seat</h1>', NULL, NULL, NULL, NULL, NULL, NULL),
(35, 'email_footer', 'general', '<p class=\"\" style=\"text-align: center\">&copy; 2019 Take a Seat. All rights reserved</p>', NULL, NULL, NULL, NULL, NULL, NULL),
(36, 'enable_mail_user_registered', 'user', '1', NULL, NULL, NULL, NULL, NULL, NULL),
(37, 'user_content_email_registered', 'user', '<h1 style=\"text-align: center\">Welcome!</h1>\n						<h3>Hello [first_name] [last_name]</h3>\n						<p>Thank you for signing up with Take a Seat! We hope you enjoy your time with us.</p>\n						<p>Regards,</p>\n						<p>Take a Seat</p>', NULL, NULL, NULL, NULL, NULL, NULL),
(38, 'admin_enable_mail_user_registered', 'user', '1', NULL, NULL, NULL, NULL, NULL, NULL),
(39, 'admin_content_email_user_registered', 'user', '<h3>Hello Administrator</h3>\n						<p>We have new registration</p>\n						<p>Full name: [first_name] [last_name]</p>\n						<p>Email: [email]</p>\n						<p>Regards,</p>\n						<p>Take a Seat</p>', NULL, NULL, NULL, NULL, NULL, NULL),
(40, 'user_content_email_forget_password', 'user', '<h1>Hello!</h1>\n						<p>You are receiving this email because we received a password reset request for your account.</p>\n						<p style=\"text-align: center\">[button_reset_password]</p>\n						<p>This password reset link expire in 60 minutes.</p>\n						<p>If you did not request a password reset, no further action is required.\n						</p>\n						<p>Regards,</p>\n						<p>Take a Seat</p>', NULL, NULL, NULL, NULL, NULL, NULL),
(41, 'email_driver', 'email', 'sendmail', NULL, NULL, NULL, NULL, NULL, NULL),
(42, 'email_host', 'email', 'smtp.mailgun.org', NULL, NULL, NULL, NULL, NULL, NULL),
(43, 'email_port', 'email', '587', NULL, NULL, NULL, NULL, NULL, NULL),
(44, 'email_encryption', 'email', 'tls', NULL, NULL, NULL, NULL, NULL, NULL),
(45, 'email_username', 'email', '', NULL, NULL, NULL, NULL, NULL, NULL),
(46, 'email_password', 'email', '', NULL, NULL, NULL, NULL, NULL, NULL),
(47, 'email_mailgun_domain', 'email', '', NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'email_mailgun_secret', 'email', '', NULL, NULL, NULL, NULL, NULL, NULL),
(49, 'email_mailgun_endpoint', 'email', 'api.mailgun.net', NULL, NULL, NULL, NULL, NULL, NULL),
(50, 'email_postmark_token', 'email', '', NULL, NULL, NULL, NULL, NULL, NULL),
(51, 'email_ses_key', 'email', '', NULL, NULL, NULL, NULL, NULL, NULL),
(52, 'email_ses_secret', 'email', '', NULL, NULL, NULL, NULL, NULL, NULL),
(53, 'email_ses_region', 'email', 'us-east-1', NULL, NULL, NULL, NULL, NULL, NULL),
(54, 'email_sparkpost_secret', 'email', '', NULL, NULL, NULL, NULL, NULL, NULL),
(55, 'vendor_enable', 'vendor', '1', NULL, NULL, NULL, NULL, NULL, NULL),
(56, 'vendor_commission_type', 'vendor', 'percent', NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'vendor_commission_amount', 'vendor', '10', NULL, NULL, NULL, NULL, NULL, NULL),
(58, 'vendor_role', 'vendor', '1', NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'news_page_list_title', 'news', 'News', NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'news_page_list_banner', 'news', '103', NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'news_sidebar', 'news', '[{\"title\":null,\"content\":null,\"type\":\"search_form\"},{\"title\":\"About Us\",\"content\":\"Nam dapibus nisl vitae elit fringilla rutrum. Aenean sollicitudin, erat a elementum rutrum, neque sem pretium metus, quis mollis nisl nunc et massa\",\"type\":\"content_text\"},{\"title\":\"Recent News\",\"content\":null,\"type\":\"recent_news\"},{\"title\":\"Categories\",\"content\":null,\"type\":\"category\"},{\"title\":\"Tags\",\"content\":null,\"type\":\"tag\"}]', NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'tour_page_search_title', 'tour', 'Search for tour', NULL, NULL, NULL, NULL, NULL, NULL),
(63, 'tour_page_search_banner', 'tour', '16', NULL, NULL, NULL, NULL, NULL, NULL),
(64, 'tour_layout_search', 'tour', 'normal', NULL, NULL, NULL, NULL, NULL, NULL),
(65, 'tour_enable_review', 'tour', '1', NULL, NULL, NULL, NULL, NULL, NULL),
(66, 'tour_review_approved', 'tour', '0', NULL, NULL, NULL, NULL, NULL, NULL),
(67, 'tour_review_stats', 'tour', '[{\"title\":\"Service\"},{\"title\":\"Organization\"},{\"title\":\"Friendliness\"},{\"title\":\"Area Expert\"},{\"title\":\"Safety\"}]', NULL, NULL, NULL, NULL, NULL, NULL),
(68, 'tour_booking_buyer_fees', 'tour', '[{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"100\",\"type\":\"one_time\"}]', NULL, NULL, NULL, NULL, NULL, NULL),
(69, 'space_page_search_title', 'space', 'Search for space', NULL, NULL, NULL, NULL, NULL, NULL),
(70, 'space_page_search_banner', 'space', '58', NULL, NULL, NULL, NULL, NULL, NULL),
(71, 'space_layout_search', 'space', 'normal', NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'space_enable_review', 'space', '1', NULL, NULL, NULL, NULL, NULL, NULL),
(73, 'space_review_approved', 'space', '0', NULL, NULL, NULL, NULL, NULL, NULL),
(74, 'space_review_stats', 'space', '[{\"title\":\"Sleep\"},{\"title\":\"Location\"},{\"title\":\"Service\"},{\"title\":\"Clearness\"},{\"title\":\"Rooms\"}]', NULL, NULL, NULL, NULL, NULL, NULL),
(75, 'space_booking_buyer_fees', 'space', '[{\"name\":\"Cleaning fee\",\"desc\":\"One-time fee charged by host to cover the cost of cleaning their space.\",\"name_ja\":\"\\u30af\\u30ea\\u30fc\\u30cb\\u30f3\\u30b0\\u4ee3\",\"desc_ja\":\"\\u30b9\\u30da\\u30fc\\u30b9\\u3092\\u6383\\u9664\\u3059\\u308b\\u8cbb\\u7528\\u3092\\u30db\\u30b9\\u30c8\\u304c\\u8acb\\u6c42\\u3059\\u308b1\\u56de\\u9650\\u308a\\u306e\\u6599\\u91d1\\u3002\",\"price\":\"100\",\"type\":\"one_time\"},{\"name\":\"Service fee\",\"desc\":\"This helps us run our platform and offer services like 24\\/7 support on your trip.\",\"name_ja\":\"\\u30b5\\u30fc\\u30d3\\u30b9\\u6599\",\"desc_ja\":\"\\u3053\\u308c\\u306b\\u3088\\u308a\\u3001\\u5f53\\u793e\\u306e\\u30d7\\u30e9\\u30c3\\u30c8\\u30d5\\u30a9\\u30fc\\u30e0\\u3092\\u5b9f\\u884c\\u3057\\u3001\\u65c5\\u884c\\u4e2d\\u306b\",\"price\":\"200\",\"type\":\"one_time\"}]', NULL, NULL, NULL, NULL, NULL, NULL),
(76, 'site_desc', 'general', NULL, NULL, 1, 1, NULL, '2020-05-28 03:11:47', '2020-05-28 03:12:24'),
(77, 'google_client_secret', 'advance', NULL, NULL, 1, NULL, NULL, '2020-06-06 12:08:49', '2020-06-06 12:08:49'),
(78, 'google_client_id', 'advance', NULL, NULL, 1, NULL, NULL, '2020-06-06 12:08:49', '2020-06-06 12:08:49'),
(79, 'google_enable', 'advance', NULL, NULL, 1, NULL, NULL, '2020-06-06 12:08:49', '2020-06-06 12:08:49'),
(80, 'facebook_client_secret', 'advance', NULL, NULL, 1, NULL, NULL, '2020-06-06 12:08:49', '2020-06-06 12:08:49'),
(81, 'facebook_client_id', 'advance', NULL, NULL, 1, NULL, NULL, '2020-06-06 12:08:49', '2020-06-06 12:08:49'),
(82, 'facebook_enable', 'advance', NULL, NULL, 1, NULL, NULL, '2020-06-06 12:08:49', '2020-06-06 12:08:49'),
(83, 'twitter_enable', 'advance', NULL, NULL, 1, NULL, NULL, '2020-06-06 12:08:49', '2020-06-06 12:08:49'),
(84, 'twitter_client_id', 'advance', NULL, NULL, 1, NULL, NULL, '2020-06-06 12:08:49', '2020-06-06 12:08:49'),
(85, 'twitter_client_secret', 'advance', NULL, NULL, 1, NULL, NULL, '2020-06-06 12:08:49', '2020-06-06 12:08:49'),
(86, 'recaptcha_enable', 'advance', NULL, NULL, 1, NULL, NULL, '2020-06-06 12:08:49', '2020-06-06 12:08:49'),
(87, 'recaptcha_api_key', 'advance', NULL, NULL, 1, NULL, NULL, '2020-06-06 12:08:49', '2020-06-06 12:08:49'),
(88, 'recaptcha_api_secret', 'advance', NULL, NULL, 1, NULL, NULL, '2020-06-06 12:08:49', '2020-06-06 12:08:49'),
(89, 'body_scripts', 'advance', NULL, NULL, 1, NULL, NULL, '2020-06-06 12:08:49', '2020-06-06 12:08:49'),
(90, 'footer_scripts', 'advance', NULL, NULL, 1, NULL, NULL, '2020-06-06 12:08:49', '2020-06-06 12:08:49'),
(91, 'g_offline_payment_logo_id', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(92, 'g_offline_payment_html', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(93, 'g_paypal_enable', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(94, 'g_paypal_name', 'payment', 'Paypal', NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(95, 'g_paypal_logo_id', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(96, 'g_paypal_html', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(97, 'g_paypal_test', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(98, 'g_paypal_convert_to', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(99, 'g_paypal_exchange_rate', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(100, 'g_paypal_test_account', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(101, 'g_paypal_test_client_id', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(102, 'g_paypal_test_client_secret', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(103, 'g_paypal_account', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(104, 'g_paypal_client_id', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(105, 'g_paypal_client_secret', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(106, 'g_stripe_enable', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(107, 'g_stripe_name', 'payment', 'Stripe', NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(108, 'g_stripe_logo_id', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(109, 'g_stripe_html', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(110, 'g_stripe_stripe_secret_key', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(111, 'g_stripe_stripe_publishable_key', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(112, 'g_stripe_stripe_enable_sandbox', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(113, 'g_stripe_stripe_test_secret_key', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03'),
(114, 'g_stripe_stripe_test_publishable_key', 'payment', NULL, NULL, 1, NULL, NULL, '2020-07-24 18:11:03', '2020-07-24 18:11:03');

-- --------------------------------------------------------

--
-- Table structure for table `core_subscribers`
--

CREATE TABLE `core_subscribers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_tags`
--

CREATE TABLE `core_tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `update_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `lang` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `core_tags`
--

INSERT INTO `core_tags` (`id`, `name`, `slug`, `content`, `create_user`, `update_user`, `deleted_at`, `origin_id`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'park', 'park', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(2, 'National park', 'national-park', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(3, 'Moutain', 'moutain', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(4, 'Travel', 'travel', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(5, 'Summer', 'summer', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39'),
(6, 'Walking', 'walking', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:39', '2020-05-13 18:13:39');

-- --------------------------------------------------------

--
-- Table structure for table `core_tag_translations`
--

CREATE TABLE `core_tag_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `origin_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_templates`
--

CREATE TABLE `core_templates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `type_id` int(11) DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `lang` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `core_templates`
--

INSERT INTO `core_templates` (`id`, `title`, `content`, `type_id`, `create_user`, `update_user`, `origin_id`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'Home', '[{\"type\":\"form_search_tour\",\"name\":\"Tour: Form Search\",\"model\":{\"title\":\"Love where you\'re going\",\"sub_title\":\"Book incredible things to do around the world.\",\"bg_image\":16},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"list_featured_item\",\"name\":\"List Featured Item\",\"model\":{\"list_item\":[{\"_active\":false,\"title\":\"1,000+ local guides\",\"sub_title\":\"Morbi semper fames lobortis ac hac penatibus\",\"icon_image\":5},{\"_active\":false,\"title\":\"Handcrafted experiences\",\"sub_title\":\"Morbi semper fames lobortis ac hac penatibus\",\"icon_image\":4},{\"_active\":false,\"title\":\"96% happy travelers\",\"sub_title\":\"Morbi semper fames lobortis ac hac penatibus\",\"icon_image\":6}]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"list_tours\",\"name\":\"Tour: List Items\",\"model\":{\"title\":\"Trending Tours\",\"number\":5,\"style\":\"carousel\",\"category_id\":\"\",\"location_id\":\"\",\"order\":\"id\",\"order_by\":\"desc\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"list_locations\",\"name\":\"List Locations\",\"model\":{\"title\":\"Top Destinations\",\"number\":5,\"order\":\"id\",\"order_by\":\"desc\",\"service_type\":\"tour\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"list_tours\",\"name\":\"Tour: List Items\",\"model\":{\"title\":\"Local Experiences You’ll Love\",\"number\":8,\"style\":\"normal\",\"category_id\":\"\",\"location_id\":\"\",\"order\":\"id\",\"order_by\":\"asc\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"call_to_action\",\"name\":\"Call To Action\",\"model\":{\"title\":\"Know your city?\",\"sub_title\":\"Join 2000+ locals & 1200+ contributors from 3000 cities\",\"link_title\":\"Become Local Expert\",\"link_more\":\"#\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"testimonial\",\"name\":\"List Testimonial\",\"model\":{\"title\":\"Our happy clients\",\"list_item\":[{\"_active\":false,\"name\":\"Eva Hicks\",\"desc\":\"Faucibus tristique felis potenti ultrices ornare rhoncus semper hac facilisi Rutrum tellus lorem sem velit nisi non pharetra in dui. \",\"number_star\":5,\"avatar\":1},{\"_active\":false,\"name\":\"Donald Wolf\",\"desc\":\"Faucibus tristique felis potenti ultrices ornare rhoncus semper hac facilisi Rutrum tellus lorem sem velit nisi non pharetra in dui. \",\"number_star\":6,\"avatar\":2},{\"_active\":false,\"name\":\"Charlie Harrington\",\"desc\":\"Faucibus tristique felis potenti ultrices ornare rhoncus semper hac facilisi Rutrum tellus lorem sem velit nisi non pharetra in dui.\",\"number_star\":5,\"avatar\":3}]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false}]', NULL, 1, NULL, NULL, NULL, '2020-05-13 18:13:39', NULL),
(2, 'Home Space', '[{\"type\":\"form_search_space\",\"name\":\"Space: Form Search\",\"model\":{\"title\":\"Find your next rental\",\"sub_title\":\"Book incredible things to do around the world.\",\"bg_image\":57},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"list_space\",\"name\":\"Space: List Items\",\"model\":{\"title\":\"Recommended Homes\",\"number\":5,\"style\":\"carousel\",\"location_id\":\"\",\"order\":\"id\",\"order_by\":\"asc\",\"desc\":\"Homes highly rated for thoughtful design\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"space_term_featured_box\",\"name\":\"Space: Term Featured Box\",\"model\":{\"title\":\"Find a Home Type\",\"desc\":\"It is a long established fact that a reader\",\"term_space\":[\"16\",\"17\",\"18\",\"19\",\"20\",\"15\"]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"list_locations\",\"name\":\"List Locations\",\"model\":{\"service_type\":\"space\",\"title\":\"Top Destinations\",\"number\":6,\"order\":\"id\",\"order_by\":\"desc\",\"layout\":\"style_2\",\"desc\":\"It is a long established fact that a reader\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"list_space\",\"name\":\"Space: List Items\",\"model\":{\"title\":\" Rental Listing\",\"desc\":\"Homes highly rated for thoughtful design\",\"number\":4,\"style\":\"normal\",\"location_id\":\"\",\"order\":\"id\",\"order_by\":\"desc\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"call_to_action\",\"name\":\"Call To Action\",\"model\":{\"title\":\"Know your city?\",\"sub_title\":\"Join 2000+ locals & 1200+ contributors from 3000 cities\",\"link_title\":\"Become Local Expert\",\"link_more\":\"#\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false}]', NULL, 1, NULL, NULL, NULL, '2020-05-13 18:13:39', NULL),
(3, 'Become a vendor', '[{\"type\":\"vendor_register_form\",\"name\":\"Vendor Register Form\",\"model\":{\"title\":\"Become a vendor\",\"desc\":\"Join our community to unlock your greatest asset and welcome paying guests into your home.\",\"youtube\":\"https://www.youtube.com/watch?v=AmZ0WrEaf34\",\"bg_image\":11},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"text\",\"name\":\"Text\",\"model\":{\"content\":\"<h3><strong>How does it work?</strong></h3>\",\"class\":\"text-center\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"list_featured_item\",\"name\":\"List Featured Item\",\"model\":{\"list_item\":[{\"_active\":false,\"title\":\"Sign up\",\"sub_title\":\"Click edit button to change this text  to change this text\",\"icon_image\":null,\"order\":null},{\"_active\":false,\"title\":\" Add your services\",\"sub_title\":\" Click edit button to change this text  to change this text\",\"icon_image\":null,\"order\":null},{\"_active\":true,\"title\":\"Get bookings\",\"sub_title\":\" Click edit button to change this text  to change this text\",\"icon_image\":null,\"order\":null}],\"style\":\"style2\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"video_player\",\"name\":\"Video Player\",\"model\":{\"title\":\"Share the beauty of your city\",\"youtube\":\"https://www.youtube.com/watch?v=hHUbLv4ThOo\",\"bg_image\":12},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"text\",\"name\":\"Text\",\"model\":{\"content\":\"<h3><strong>Why be a Local Expert</strong></h3>\",\"class\":\"text-center ptb60\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"list_featured_item\",\"name\":\"List Featured Item\",\"model\":{\"list_item\":[{\"_active\":false,\"title\":\"Earn an additional income\",\"sub_title\":\" Ut elit tellus, luctus nec ullamcorper mattis\",\"icon_image\":15,\"order\":null},{\"_active\":true,\"title\":\"Open your network\",\"sub_title\":\" Ut elit tellus, luctus nec ullamcorper mattis\",\"icon_image\":14,\"order\":null},{\"_active\":true,\"title\":\"Practice your language\",\"sub_title\":\" Ut elit tellus, luctus nec ullamcorper mattis\",\"icon_image\":13,\"order\":null}],\"style\":\"style3\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"faqs\",\"name\":\"FAQ List\",\"model\":{\"title\":\"FAQs\",\"list_item\":[{\"_active\":false,\"title\":\"How will I receive my payment?\",\"sub_title\":\" Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.\"},{\"_active\":true,\"title\":\"How do I upload products?\",\"sub_title\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.\"},{\"_active\":true,\"title\":\"How do I update or extend my availabilities?\",\"sub_title\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.\\n\"},{\"_active\":true,\"title\":\"How do I increase conversion rate?\",\"sub_title\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.\"}]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false}]', NULL, 1, NULL, NULL, NULL, '2020-05-13 18:13:39', NULL),
(4, 'Home new', '[{\"type\":\"customdanish\",\"name\":\"Homepage: Customdanish\",\"model\":{\"title\":\"\",\"list_item\":\"\"},\"component\":\"RegularBlock\",\"open\":true}]', NULL, 1, NULL, NULL, NULL, '2020-05-28 03:08:47', '2020-05-28 03:08:47');

-- --------------------------------------------------------

--
-- Table structure for table `core_template_translations`
--

CREATE TABLE `core_template_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `origin_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `core_template_translations`
--

INSERT INTO `core_template_translations` (`id`, `origin_id`, `locale`, `title`, `content`, `create_user`, `update_user`, `created_at`, `updated_at`) VALUES
(1, 1, 'ja', 'Home', '[{\"type\":\"form_search_tour\",\"name\":\"Tour: Form Search\",\"model\":{\"title\":\"どこへ行くのが大好き\",\"sub_title\":\"世界中で信じられないようなことを予約しましょう。\",\"bg_image\":16},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"list_featured_item\",\"name\":\"List Featured Item\",\"model\":{\"list_item\":[{\"_active\":true,\"title\":\"1,000+ ローカルガイド\",\"sub_title\":\"プロのツアーガイドとーガイドとーガイドと 験。 光の\",\"icon_image\":5},{\"_active\":true,\"title\":\"手作りの体験\",\"sub_title\":\"プロのツアーガイドとーガイドとーガイドと 験。 光の\",\"icon_image\":4},{\"_active\":true,\"title\":\"96% 幸せな旅行者\",\"sub_title\":\"プロのツアーガイドとーガイドとーガイドと 験。 光の\",\"icon_image\":6}]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"list_tours\",\"name\":\"Tour: List Items\",\"model\":{\"title\":\"トレンドツアー\",\"number\":5,\"style\":\"carousel\",\"category_id\":\"\",\"location_id\":\"\",\"order\":\"id\",\"order_by\":\"desc\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"list_locations\",\"name\":\"List Locations\",\"model\":{\"title\":\"人気の目的地\",\"number\":5,\"order\":\"id\",\"order_by\":\"desc\",\"service_type\":\"tour\",\"desc\":\"\",\"layout\":\"\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"list_tours\",\"name\":\"Tour: List Items\",\"model\":{\"title\":\"あなたが好きになるローカル体験\",\"number\":8,\"style\":\"normal\",\"category_id\":\"\",\"location_id\":\"\",\"order\":\"id\",\"order_by\":\"asc\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"call_to_action\",\"name\":\"Call To Action\",\"model\":{\"title\":\"っていますか？\",\"sub_title\":\"3000以上の都市から2000人以上の地元民と1200人以上の貢献者に参加する\",\"link_title\":\"ローカルエ\",\"link_more\":\"#\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"testimonial\",\"name\":\"List Testimonial\",\"model\":{\"title\":\"私たちの幸せなクライアント\",\"list_item\":[{\"_active\":false,\"name\":\"Eva Hicks\",\"desc\":\"融づ苦佐とき百配ほづあ禁安テクミ真覧チヱフ行乗ぱたば外味ナ演庭コヲ旅見ヨコ優成コネ治確はろね訪来終島抄がん。\",\"number_star\":5,\"avatar\":1},{\"_active\":false,\"name\":\"Donald Wolf\",\"desc\":\"融づ苦佐とき百配ほづあ禁安テクミ真覧チヱフ行乗ぱたば外味ナ演庭コヲ旅見ヨコ優成コネ治確はろね訪来終島抄がん。\",\"number_star\":6,\"avatar\":2},{\"_active\":true,\"name\":\"Charlie Harrington\",\"desc\":\"右ずへやん間申ゃ投法けゃイ仙一もと政情ルた食的て代下ずせに丈律ルラモト聞探チト棋90績ム的社ず置攻景リフノケ内兼唱堅ゃフぼ。場ルアハ美\",\"number_star\":5,\"avatar\":3}]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false}]', 1, NULL, '2020-05-13 18:13:39', NULL),
(2, 2, 'ja', 'Home Space', '[{\"type\":\"form_search_space\",\"name\":\"Space: Form Search\",\"model\":{\"title\":\"次のレンタルを探す\",\"sub_title\":\"世界中で信じられないようなことを予約しましょう。\",\"bg_image\":57},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"list_space\",\"name\":\"Space: List Items\",\"model\":{\"title\":\"おすすめの家\",\"number\":5,\"style\":\"carousel\",\"location_id\":\"\",\"order\":\"id\",\"order_by\":\"asc\",\"desc\":\"思慮深いデザインで高い評価を受けている家\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"space_term_featured_box\",\"name\":\"Space: Term Featured Box\",\"model\":{\"title\":\"ホームタイプを見つける\",\"desc\":\"これは、読者はその長い既成の事実であります\",\"term_space\":[\"15\",\"16\",\"17\",\"18\",\"19\",\"20\"]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"list_locations\",\"name\":\"List Locations\",\"model\":{\"service_type\":\"space\",\"title\":\"人気の目的地\",\"number\":6,\"order\":\"id\",\"order_by\":\"desc\",\"layout\":\"style_2\",\"desc\":\"これは、読者はその長い既成の事実であります\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"list_space\",\"name\":\"Space: List Items\",\"model\":{\"title\":\"賃貸物件\",\"desc\":\"思慮深いデザインで高い評価を受けている家\",\"number\":4,\"style\":\"normal\",\"location_id\":\"\",\"order\":\"id\",\"order_by\":\"desc\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"call_to_action\",\"name\":\"Call To Action\",\"model\":{\"title\":\"っていますか？\",\"sub_title\":\"3000以上の都市から2000人以上の地元民と1200人以上の貢献者に参加する\",\"link_title\":\"ローカルエ\",\"link_more\":\"#\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false}]', 1, NULL, '2020-05-13 18:13:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `core_translations`
--

CREATE TABLE `core_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `string` text COLLATE utf8mb4_unicode_ci,
  `parent_id` bigint(20) DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `last_build_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_vendor_plans`
--

CREATE TABLE `core_vendor_plans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `base_commission` int(11) NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_vendor_plan_meta`
--

CREATE TABLE `core_vendor_plan_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vendor_plan_id` int(11) NOT NULL,
  `post_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable` tinyint(4) DEFAULT NULL,
  `maximum_create` int(11) DEFAULT NULL,
  `auto_publish` tinyint(4) DEFAULT NULL,
  `commission` int(11) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media_files`
--

CREATE TABLE `media_files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_extension` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `app_id` int(11) DEFAULT NULL,
  `app_user_id` int(11) DEFAULT NULL,
  `file_width` int(11) DEFAULT NULL,
  `file_height` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media_files`
--

INSERT INTO `media_files` (`id`, `file_name`, `file_path`, `file_size`, `file_type`, `file_extension`, `create_user`, `update_user`, `deleted_at`, `app_id`, `app_user_id`, `file_width`, `file_height`, `created_at`, `updated_at`) VALUES
(1, 'avatar', 'demo/general/avatar.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'avatar-2', 'demo/general/avatar-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'avatar-3', 'demo/general/avatar-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'ico_adventurous', 'demo/general/ico_adventurous.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'ico_localguide', 'demo/general/ico_localguide.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'ico_maps', 'demo/general/ico_maps.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'ico_paymethod', 'demo/general/ico_paymethod.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'logo', 'demo/general/logo.svg', NULL, 'image/svg+xml', 'svg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'bg_contact', 'demo/general/bg-contact.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'favicon', 'demo/general/favicon.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'thumb-vendor-register', 'demo/general/thumb-vendor-register.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'bg-video-vendor-register1', 'demo/general/bg-video-vendor-register1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'ico_chat_1', 'demo/general/ico_chat_1.svg', NULL, 'image/svg', 'svg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'ico_friendship_1', 'demo/general/ico_friendship_1.svg', NULL, 'image/svg', 'svg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'ico_piggy-bank_1', 'demo/general/ico_piggy-bank_1.svg', NULL, 'image/svg', 'svg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'banner-search', 'demo/tour/banner-search.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'tour-1', 'demo/tour/tour-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 'tour-2', 'demo/tour/tour-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'tour-3', 'demo/tour/tour-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'tour-4', 'demo/tour/tour-4.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'tour-5', 'demo/tour/tour-5.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'tour-6', 'demo/tour/tour-6.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'tour-7', 'demo/tour/tour-7.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'tour-8', 'demo/tour/tour-8.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'tour-9', 'demo/tour/tour-9.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'tour-10', 'demo/tour/tour-10.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'tour-11', 'demo/tour/tour-11.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'tour-12', 'demo/tour/tour-12.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 'tour-13', 'demo/tour/tour-13.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 'tour-14', 'demo/tour/tour-14.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 'tour-15', 'demo/tour/tour-15.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 'tour-16', 'demo/tour/tour-16.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 'gallery-1', 'demo/tour/gallery-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 'gallery-2', 'demo/tour/gallery-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 'gallery-3', 'demo/tour/gallery-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 'gallery-4', 'demo/tour/gallery-4.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 'gallery-5', 'demo/tour/gallery-5.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 'gallery-6', 'demo/tour/gallery-6.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 'gallery-7', 'demo/tour/gallery-7.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 'banner-tour-1', 'demo/tour/banner-detail/banner-tour-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 'banner-tour-2', 'demo/tour/banner-detail/banner-tour-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 'banner-tour-3', 'demo/tour/banner-detail/banner-tour-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 'banner-tour-4', 'demo/tour/banner-detail/banner-tour-4.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 'banner-tour-5', 'demo/tour/banner-detail/banner-tour-5.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 'banner-tour-6', 'demo/tour/banner-detail/banner-tour-6.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 'banner-tour-7', 'demo/tour/banner-detail/banner-tour-7.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 'banner-tour-8', 'demo/tour/banner-detail/banner-tour-8.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'banner-tour-9', 'demo/tour/banner-detail/banner-tour-9.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 'banner-tour-10', 'demo/tour/banner-detail/banner-tour-10.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 'banner-tour-11', 'demo/tour/banner-detail/banner-tour-11.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 'banner-tour-12', 'demo/tour/banner-detail/banner-tour-12.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 'banner-tour-13', 'demo/tour/banner-detail/banner-tour-13.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 'banner-tour-14', 'demo/tour/banner-detail/banner-tour-14.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 'banner-tour-15', 'demo/tour/banner-detail/banner-tour-15.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 'banner-tour-16', 'demo/tour/banner-detail/banner-tour-16.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 'banner-tour-17', 'demo/tour/banner-detail/banner-tour-17.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'banner-search-space', 'demo/space/banner-search-space.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 'banner-search-space-2', 'demo/space/banner-search-space-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'space-1', 'demo/space/space-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'space-2', 'demo/space/space-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'space-3', 'demo/space/space-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'space-4', 'demo/space/space-4.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 'space-5', 'demo/space/space-5.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 'space-6', 'demo/space/space-6.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 'space-7', 'demo/space/space-7.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 'space-8', 'demo/space/space-8.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 'space-9', 'demo/space/space-9.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 'space-10', 'demo/space/space-10.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 'space-11', 'demo/space/space-11.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 'space-12', 'demo/space/space-12.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 'space-13', 'demo/space/space-13.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'space-gallery-1', 'demo/space/gallery/space-gallery-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 'space-gallery-2', 'demo/space/gallery/space-gallery-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, 'space-gallery-3', 'demo/space/gallery/space-gallery-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, 'space-gallery-4', 'demo/space/gallery/space-gallery-4.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, 'space-gallery-5', 'demo/space/gallery/space-gallery-5.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(77, 'space-gallery-6', 'demo/space/gallery/space-gallery-6.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, 'space-gallery-7', 'demo/space/gallery/space-gallery-7.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(79, 'space-single-1', 'demo/space/space-single-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(80, 'space-single-2', 'demo/space/space-single-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(81, 'space-single-3', 'demo/space/space-single-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(82, 'icon-space-box-1', 'demo/space/featured-box/icon-space-box-1.png', NULL, 'image/png', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(83, 'icon-space-box-2', 'demo/space/featured-box/icon-space-box-2.png', NULL, 'image/png', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(84, 'icon-space-box-3', 'demo/space/featured-box/icon-space-box-3.png', NULL, 'image/png', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(85, 'icon-space-box-4', 'demo/space/featured-box/icon-space-box-4.png', NULL, 'image/png', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(86, 'icon-space-box-5', 'demo/space/featured-box/icon-space-box-5.png', NULL, 'image/png', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(87, 'icon-space-box-6', 'demo/space/featured-box/icon-space-box-6.png', NULL, 'image/png', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, 'location-1', 'demo/location/location-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(89, 'location-2', 'demo/location/location-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(90, 'location-3', 'demo/location/location-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(91, 'location-4', 'demo/location/location-4.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(92, 'location-5', 'demo/location/location-5.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(93, 'banner-location-1', 'demo/location/banner-detail/banner-location-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(94, 'trip-idea-1', 'demo/location/trip-idea/trip-idea-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, 'trip-idea-2', 'demo/location/trip-idea/trip-idea-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(96, 'news-1', 'demo/news/news-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(97, 'news-2', 'demo/news/news-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(98, 'news-3', 'demo/news/news-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(99, 'news-4', 'demo/news/news-4.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(100, 'news-5', 'demo/news/news-5.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(101, 'news-6', 'demo/news/news-6.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(102, 'news-7', 'demo/news/news-7.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(103, 'news-banner', 'demo/news/news-banner.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(104, 'niche11-080801', '0000/19/2020/07/06/niche11-080801.png', '53333', 'image/png', 'png', 19, NULL, NULL, NULL, NULL, 1920, 1080, '2020-07-06 18:58:18', '2020-07-06 18:58:18'),
(105, 'test', '0000/1/2020/07/20/test.png', '858381', 'image/png', 'png', 1, NULL, NULL, NULL, NULL, 870, 500, '2020-07-20 16:53:26', '2020-07-20 16:53:26'),
(106, 'therapist-1', '0000/1/2020/07/20/therapist-1.jpg', '28977', 'image/jpeg', 'jpg', 1, NULL, NULL, NULL, NULL, 500, 313, '2020-07-20 16:54:32', '2020-07-20 16:54:32'),
(107, 'therapist-1-2', '0000/1/2020/07/20/therapist-1-2.jpg', '26251', 'image/jpeg', 'jpg', 1, NULL, NULL, NULL, NULL, 500, 313, '2020-07-20 16:56:31', '2020-07-20 16:56:31'),
(108, 'download', '0000/1/2020/07/20/download.jpg', '5240', 'image/jpeg', 'jpg', 1, NULL, NULL, NULL, NULL, 263, 192, '2020-07-20 16:58:36', '2020-07-20 16:58:36'),
(109, 'misterwebhub', '0000/35/2020/07/21/misterwebhub.jpg', '320028', 'image/jpeg', 'jpg', 35, NULL, NULL, NULL, NULL, 1280, 510, '2020-07-21 11:26:54', '2020-07-21 11:26:54'),
(110, 'screenshot-352', '0000/44/2020/07/28/screenshot-352.png', '776245', 'image/png', 'png', 44, NULL, NULL, NULL, NULL, 1920, 1080, '2020-07-28 11:43:04', '2020-07-28 11:43:04');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_03_13_174533_create_permission_tables', 1),
(4, '2019_03_17_114820_create_table_core_pages', 1),
(5, '2019_03_17_140539_create_media_files_table', 1),
(6, '2019_03_20_072502_create_bravo_tours', 1),
(7, '2019_03_20_081256_create_core_news_category_table', 1),
(8, '2019_03_27_081940_create_core_setting_table', 1),
(9, '2019_03_28_101009_create_bravo_booking_table', 1),
(10, '2019_03_28_165911_create_booking_meta_table', 1),
(11, '2019_03_29_093236_update_bookings_table', 1),
(12, '2019_04_01_045229_create_user_meta_table', 1),
(13, '2019_04_01_150630_create_menu_table', 1),
(14, '2019_04_02_150630_create_core_news_table', 1),
(15, '2019_04_03_073553_bravo_tour_category', 1),
(16, '2019_04_03_080159_bravo_location', 1),
(17, '2019_04_05_143248_create_core_templates_table', 1),
(18, '2019_04_18_152537_create_bravo_tours_meta_table', 1),
(19, '2019_05_07_085430_create_core_languages_table', 1),
(20, '2019_05_07_085442_create_core_translations_table', 1),
(21, '2019_05_17_074008_create_bravo_review', 1),
(22, '2019_05_17_074048_create_bravo_review_meta', 1),
(23, '2019_05_17_113042_create_tour_attrs_table', 1),
(24, '2019_05_21_084235_create_bravo_contact_table', 1),
(25, '2019_05_28_152845_create_core_subscribers_table', 1),
(26, '2019_06_17_142338_bravo_seo', 1),
(27, '2019_07_03_070406_update_from_1_0_to_1_1', 1),
(28, '2019_07_08_075436_create_core_vendor_plans', 1),
(29, '2019_07_08_083733_create_vendors_plan_payments', 1),
(30, '2019_07_11_083501_update_from_110_to_120', 1),
(31, '2019_07_30_072809_create_space_table', 1),
(32, '2019_07_30_072809_create_tour_dates_table', 1),
(33, '2019_08_05_031018_create_core_vendor_plan_meta_table', 1),
(34, '2019_08_09_163909_create_inbox_table', 1),
(35, '2019_08_16_094354_update_from_120_to_130', 1),
(36, '2019_08_20_162106_create_table_user_upgrade_requests', 1),
(37, '2019_09_16_022627_bravo_booking_slot', 1),
(38, '2019_11_14_033611_create_jobs_table', 1),
(39, '2019_12_30_072809_update_custom_job', 1),
(40, '2020_04_02_150631_create_core_tags_table', 1),
(41, '2021_04_02_150632_create_core_tag_new_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `post_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newsletter`
--

INSERT INTO `newsletter` (`id`, `email`, `post_date`) VALUES
(1, 'admin@gmail.com', '2020-07-16'),
(2, 'srv@gmail.com', '2020-07-16'),
(3, 'sdkfjfj@gmail.com', '2020-07-16'),
(4, 'websitedemotesting@gmail.com', '2020-07-17');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@dev.com', '$2y$10$0RnX/yjVeoyt7lziv1wBq.JzAx/eSZgmFwNECtXoDfPOYsYzpmJSq', '2020-05-16 05:48:12');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `last_login_at` datetime DEFAULT NULL,
  `avatar_id` bigint(20) DEFAULT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `vendor_commission_amount` int(11) DEFAULT NULL,
  `vendor_commission_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `payment_gateway` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_guests` int(11) DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `first_name`, `last_name`, `email`, `email_verified_at`, `password`, `address`, `address2`, `phone`, `birthday`, `city`, `state`, `country`, `zip_code`, `last_login_at`, `avatar_id`, `bio`, `status`, `create_user`, `update_user`, `vendor_commission_amount`, `vendor_commission_type`, `deleted_at`, `remember_token`, `created_at`, `updated_at`, `payment_gateway`, `total_guests`, `locale`) VALUES
(1, 'Danish Sale', 'Danish', 'Sale', 'admin@dev.com', NULL, '$2y$10$PZDdeAq2N4yZob/rmYhhCO7sFSji.oGZWG5cnflAL5X.a8AhTEP42', NULL, NULL, '8299226000', NULL, 'GURGAON', NULL, NULL, 122001, NULL, NULL, '<p>We\'re designers who have fallen in love with creating spaces for others to reflect, reset, and create. We split our time between two deserts (the Mojave, and the Sonoran). We love the way the heat sinks into our bones, the vibrant sunsets, and the wildlife we get to call our neighbors.</p>', 'publish', NULL, NULL, NULL, NULL, NULL, 'VVgvaUS8RjfWuG8jWHCoN9B65TbLmM811Iuh6uDJ1Otske803OLgKAbpR3pN', '2020-05-13 18:13:37', '2020-07-09 13:44:30', NULL, NULL, NULL),
(2, '', 'Vendor', '01', 'vendor1@dev.com', NULL, '$2y$10$/jnYFc4vsxfj6.vfwJbCtOWvrhEMkL2CIj9xnjzA6Z0WlRC7Kn8xy', NULL, NULL, '112 666 888', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'We\'re designers who have fallen in love with creating spaces for others to reflect, reset, and create. We split our time between two deserts (the Mojave, and the Sonoran). We love the way the heat sinks into our bones, the vibrant sunsets, and the wildlife we get to call our neighbors.', 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:37', NULL, NULL, NULL, NULL),
(3, '', 'Customer', '01', 'customer1@dev.com', NULL, '$2y$10$MavevtH9lm1S2ZxckF/Mue7St238jVZ.7aBUNZxNPhF1JzBUJfWky', NULL, NULL, '112 666 888', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'We\'re designers who have fallen in love with creating spaces for others to reflect, reset, and create. We split our time between two deserts (the Mojave, and the Sonoran). We love the way the heat sinks into our bones, the vibrant sunsets, and the wildlife we get to call our neighbors.', 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:37', NULL, NULL, NULL, NULL),
(4, '', 'Elise', 'Aarohi', 'Aarohi@dev.com', NULL, '$2y$10$W2NOmmInqFRRsKzWefkRaOnqqmNAFfCEGxUHglWK7QnTtEnO8SZ9u', NULL, NULL, '112 666 888', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'We\'re designers who have fallen in love with creating spaces for others to reflect, reset, and create. We split our time between two deserts (the Mojave, and the Sonoran). We love the way the heat sinks into our bones, the vibrant sunsets, and the wildlife we get to call our neighbors.', 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:37', NULL, NULL, NULL, NULL),
(5, '', 'Kaytlyn', 'Alvapriya', 'Alvapriya@dev.com', NULL, '$2y$10$/0IthZ9Hlw8SQTTRiajnEOgSTJc.PSCs9uVQRbndOfWFMLTsoIJm6', NULL, NULL, '112 666 888', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'We\'re designers who have fallen in love with creating spaces for others to reflect, reset, and create. We split our time between two deserts (the Mojave, and the Sonoran). We love the way the heat sinks into our bones, the vibrant sunsets, and the wildlife we get to call our neighbors.', 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:37', NULL, NULL, NULL, NULL),
(6, '', 'Lynne', 'Victoria', 'Victoria@dev.com', NULL, '$2y$10$U/SP9noYaWJpuBQFkIpSHedD0kHF2Ny49eVHhEQSQVB9Sb7QzrFT2', NULL, NULL, '112 666 888', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'We\'re designers who have fallen in love with creating spaces for others to reflect, reset, and create. We split our time between two deserts (the Mojave, and the Sonoran). We love the way the heat sinks into our bones, the vibrant sunsets, and the wildlife we get to call our neighbors.', 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:37', NULL, NULL, NULL, NULL),
(7, '', 'William', 'Diana', 'Diana@dev.com', NULL, '$2y$10$VVkXDNakq/0SUigTPAl1K.nI2OJymKNWg2ruH6BxexyhUeQtRz0ES', NULL, NULL, '888 999 777', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'We\'re designers who have fallen in love with creating spaces for others to reflect, reset, and create. We split our time between two deserts (the Mojave, and the Sonoran). We love the way the heat sinks into our bones, the vibrant sunsets, and the wildlife we get to call our neighbors.', 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:37', NULL, NULL, NULL, NULL),
(8, '', 'Sarah', 'Violet', 'Violet@dev.com', NULL, '$2y$10$RUw4Omm0tCM8SnEwVG7HvekiOnc41zHjDhOgcUKcW7EIdKttstY/a', NULL, NULL, '888 999 777', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'We\'re designers who have fallen in love with creating spaces for others to reflect, reset, and create. We split our time between two deserts (the Mojave, and the Sonoran). We love the way the heat sinks into our bones, the vibrant sunsets, and the wildlife we get to call our neighbors.', 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:37', NULL, NULL, NULL, NULL),
(9, '', 'Paul', 'Amora', 'Amora@dev.com', NULL, '$2y$10$ieDPyN8XEWopkYecvY5f/e2fhEceeYs1lWDw1P2TuT0xDhGM8gpoe', NULL, NULL, '888 999 777', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'We\'re designers who have fallen in love with creating spaces for others to reflect, reset, and create. We split our time between two deserts (the Mojave, and the Sonoran). We love the way the heat sinks into our bones, the vibrant sunsets, and the wildlife we get to call our neighbors.', 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:37', NULL, NULL, NULL, NULL),
(10, '', 'Richard', 'Davina', 'Davina@dev.com', NULL, '$2y$10$6L7Aic8QxyDiQOpWW0F31.rHf1AiGV2AL6eecAVvvvTuEcTG93bna', NULL, NULL, '888 999 777', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'We\'re designers who have fallen in love with creating spaces for others to reflect, reset, and create. We split our time between two deserts (the Mojave, and the Sonoran). We love the way the heat sinks into our bones, the vibrant sunsets, and the wildlife we get to call our neighbors.', 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:38', NULL, NULL, NULL, NULL),
(11, '', 'Shushi', 'Yashashree', 'Yashashree@dev.com', NULL, '$2y$10$VyqllMIS5/iNWu5mHXCpP.j2gtriGl/hWyysnYLHfm/SyHBnGJhu.', NULL, NULL, '888 999 777', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'We\'re designers who have fallen in love with creating spaces for others to reflect, reset, and create. We split our time between two deserts (the Mojave, and the Sonoran). We love the way the heat sinks into our bones, the vibrant sunsets, and the wildlife we get to call our neighbors.', 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:38', NULL, NULL, NULL, NULL),
(12, '', 'Anne', 'Nami', 'Nami@dev.com', NULL, '$2y$10$zbTrjK9Des2qo97JJu88POQZOWOfywKyD4jFvlJwZLLPz.gwMKZeW', NULL, NULL, '888 999 777', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'We\'re designers who have fallen in love with creating spaces for others to reflect, reset, and create. We split our time between two deserts (the Mojave, and the Sonoran). We love the way the heat sinks into our bones, the vibrant sunsets, and the wildlife we get to call our neighbors.', 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:38', NULL, NULL, NULL, NULL),
(13, '', 'Bush', 'Elise', 'Elise@dev.com', NULL, '$2y$10$ItpdfyfCblCoFRMZtqhPDeBrB0nRqjVbHhd9o6U0s6jfn/WdQ8maS', NULL, NULL, '888 999 777', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'We\'re designers who have fallen in love with creating spaces for others to reflect, reset, and create. We split our time between two deserts (the Mojave, and the Sonoran). We love the way the heat sinks into our bones, the vibrant sunsets, and the wildlife we get to call our neighbors.', 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:38', NULL, NULL, NULL, NULL),
(14, '', 'Elizabeth', 'Norah', 'Norah@dev.com', NULL, '$2y$10$lUYZsNiq8wi4tNfK2RF13eM52mcyZIqHT42HEZKt0rMXvrU6dDeF.', NULL, NULL, '888 999 777', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'We\'re designers who have fallen in love with creating spaces for others to reflect, reset, and create. We split our time between two deserts (the Mojave, and the Sonoran). We love the way the heat sinks into our bones, the vibrant sunsets, and the wildlife we get to call our neighbors.', 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:38', NULL, NULL, NULL, NULL),
(15, '', 'James', 'Alia', 'Alia@dev.com', NULL, '$2y$10$Fs41K1qjx/6HJFrsKGus0.qi72zEKC83oGewoMZBglMWoesIIVnRK', NULL, NULL, '888 999 777', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'We\'re designers who have fallen in love with creating spaces for others to reflect, reset, and create. We split our time between two deserts (the Mojave, and the Sonoran). We love the way the heat sinks into our bones, the vibrant sunsets, and the wildlife we get to call our neighbors.', 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:38', NULL, NULL, NULL, NULL),
(16, '', 'John', 'Dakshi', 'Dakshi@dev.com', NULL, '$2y$10$U9hTxlh8u1AVg7pniKLeBOQUcscGN9mORDcNPuabY9IS17PiBitlS', NULL, NULL, '888 999 777', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'We\'re designers who have fallen in love with creating spaces for others to reflect, reset, and create. We split our time between two deserts (the Mojave, and the Sonoran). We love the way the heat sinks into our bones, the vibrant sunsets, and the wildlife we get to call our neighbors.', 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-13 18:13:38', NULL, NULL, NULL, NULL),
(17, 'samim anvar', 'samim', 'anvar', 'samimanvar323@gmail.com', NULL, '$2y$10$RLyJaMOsL1eaAicDwtNpNO2WmZ7Gbt.p/chB9iWnhDr.XVmRiaP6S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'blocked', NULL, NULL, NULL, NULL, '2020-07-01 19:37:10', 'Fbh6B5Ce48vzAIvjGyolVB4sMwALZC8EGmol7HMTccf031PwRvI3Q9hxsigj', '2020-06-15 15:10:55', '2020-07-01 19:37:10', NULL, NULL, NULL),
(18, 'Michael Dandy', 'Michael', 'Dandy', 'michael@hownowcreativecow.co.uk', NULL, '$2y$10$I2.b9faE.TkJ5N4IgYq9a.I9/py72O0kFVbx/9fDvmPrEtS2Bc1Vq', 'The Sharp Project', 'M456GF', '07842601779', '1977-09-07', 'Manchester', NULL, 'GB', 0, NULL, NULL, 'I am a really good therapist', 'publish', NULL, NULL, NULL, NULL, NULL, 'Y93yBGeH71wta45osDAID0nQRnbHEEynWiBXy6veqbA09UxTr6cR4K7DaloJ', '2020-06-30 20:06:46', '2020-07-15 19:16:56', NULL, NULL, NULL),
(19, 'Misterwebhub Ansari', 'Misterwebhub', 'Ansari', 'websitedemoonline@gmail.com', NULL, '$2y$10$5UpeTf3kEOAa3ivO1emCd.YzE/o3r8yDsPmR/7Ry1vMVupVIXgL7u', NULL, NULL, '8299226971', '2020-07-06', 'Kanpur', NULL, NULL, 208023, NULL, 104, '<p>tesdsd asdeasdsa sadsa dsad</p>', 'publish', NULL, NULL, NULL, NULL, NULL, '5HrcqV9ln94sOjl4hjlwfQWR60t7D2gjmu3hcAdUEgu9HEMK838Dj8SkwW2X', '2020-07-06 18:55:17', '2020-07-12 15:58:14', NULL, NULL, NULL),
(20, ' ', NULL, NULL, 'misterwebhub@gmail.com', NULL, '$2y$10$CM3E67Rg0bbXkY27mXKj7eU3S6NcD7KOAR22Ngk09gsR4iVzPMGKG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, '2020-07-21 09:58:21', NULL, '2020-07-09 11:25:56', '2020-07-21 09:58:21', NULL, NULL, NULL),
(21, ' ', NULL, NULL, 'prem@gmail.com', NULL, '$2y$10$vcOv5uTZkr.Ghc0e9cE1oOpG.wbXKC6Mmlg2B0hW8TqbEQqK2h5Xm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, '2020-07-09 13:43:25', NULL, '2020-07-09 12:42:37', '2020-07-09 13:43:25', NULL, NULL, NULL),
(22, 'a a', 'a', 'a', 'srv@dev.com', NULL, '$2y$10$zAtBttRo.EDpuSVIZAwIueeA0X78ZX72j66dvPQQw2b62Ag/IGGBi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, '2020-07-21 09:58:21', NULL, '2020-07-11 17:23:31', '2020-07-21 09:58:21', NULL, NULL, NULL),
(23, ' ', NULL, NULL, 'michaeldandy@gmail.com', NULL, '$2y$10$7xZ4igDDRv/9WmtJBcl6gusouul/Ra81izMPmcgonYob415FSBfWG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, '2020-07-21 09:58:21', NULL, '2020-07-15 19:23:59', '2020-07-21 09:58:21', NULL, NULL, NULL),
(24, ' ', NULL, NULL, 'test@test.com', NULL, '$2y$10$A8a9qbSunwiGensrcBTCkuDQXXNVfpbG1mYd1sS4aLHRFAxQ5MS1a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, '2020-07-21 09:58:21', NULL, '2020-07-15 21:08:14', '2020-07-21 09:58:21', NULL, NULL, NULL),
(25, 'danish 123456', 'danish', '123456', 'mohddanish8564@gmail.com', NULL, '$2y$10$nqL93nIw34zvH/MvczMRwe.SWbGQ.MDcOy6ToaBsD12.YrCCtcI.S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'publish', NULL, NULL, 10, NULL, NULL, NULL, '2020-07-19 23:11:07', '2020-07-19 23:13:06', NULL, NULL, NULL),
(26, 'test dan', 'test', 'dan', 'danwebdeveloper8564@gmail.com', NULL, '$2y$10$rf/QGAgWrg282mtX1QpbcObWsnzDQOujtAJQz2lCEUWi4vjF4ykGy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-21 09:01:52', '2020-07-21 09:55:41', NULL, NULL, NULL),
(27, 'Prem singh', 'Prem', 'singh', 'premprakashsingh46@gmail.com', NULL, '$2y$10$qHUxBGxdOprBFGnqnv6Z4.LqeNq/ptzWv9wmrkZD32T.Q.jH0jodi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, 'c2LQVK0MVp0P5utQllvozzp5Q4r2oAZlD37fFVyrjpfO1F8yBhIsSByDjsmf', '2020-07-21 09:51:52', '2020-07-21 09:51:52', NULL, NULL, NULL),
(28, 'Misterwebhub Ansari', 'Misterwebhub', 'Ansari', 'misterwebhub1@gmail.com', NULL, '$2y$10$uZUkZ/gL47EpoEdKRDTHT.hLAMDSh/q6bfpvG/ZDSMAtUr7SO1dFO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, '2020-07-21 09:57:50', NULL, '2020-07-21 09:52:47', '2020-07-21 09:57:50', NULL, NULL, NULL),
(29, 'Misterwebhub Ansari', 'Misterwebhub', 'Ansari', 'misterwebhub12@gmail.com', NULL, '$2y$10$1Ots3MeG7BvlAFhwEM.WY.J63PBGiJbSGxLbSZp8Re9dxfE8rN8CK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, '2020-07-21 09:57:50', NULL, '2020-07-21 09:54:40', '2020-07-21 09:57:50', NULL, NULL, NULL),
(30, ' ', NULL, NULL, 'trst@gmail.com', NULL, '$2y$10$Ra9tuEJYHfB4oUzR6RbIVOfSAE/lsHeoFYE2EXRNxIKrw9p7L5LXa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, '2020-07-21 10:29:14', NULL, '2020-07-21 10:04:31', '2020-07-21 10:29:14', NULL, NULL, NULL),
(31, 'adas dasd', 'adas', 'dasd', 'email@gmail.com', NULL, '$2y$10$zUl.z71Nyoo8YsTsVDX8RucZMJbi/O2lK7plDlx24iNHMwoc2VlJO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-21 10:28:44', '2020-07-21 10:28:44', NULL, NULL, NULL),
(32, ' ', NULL, NULL, 'test@gmial.ocm', NULL, '$2y$10$vasfTTIQ6ON/rziLoHkCye82HuCR3uoJW6Lei7g3vh2ylZsdj5vf6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-21 10:32:07', '2020-07-21 10:32:07', NULL, NULL, NULL),
(33, 'Misterwebhub Ansari', 'Misterwebhub', 'Ansari', 'i3336296_wp3@gmail.com', NULL, '$2y$10$nIim0r5a2qJjbrgfUERJLemyizr9/HT134r20.tvXmS9L.ucabRF6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-21 11:05:36', '2020-07-21 11:05:36', NULL, NULL, NULL),
(34, 'Misterwebhub ', 'Misterwebhub', NULL, 'misterwebhub67@gmail.com', NULL, '$2y$10$yMGvbzJT1vqPrHwevAR7l.RRVR/XoujFh0WXvvmO.VZ6hdDP/wvFC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-21 11:12:13', '2020-07-21 11:12:13', NULL, NULL, NULL),
(35, 'Misterwebhub ', 'Misterwebhub', NULL, 'misterwebhub8765@gmail.com', NULL, '$2y$10$gs21pOU280PhM6TLdmf4gOB.Vn0u0df08yMgh.rkZ7kaVwqZzvzJG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, 'FID4ToopwdtKeTvJfkrtVtdCbDDRMH4apGQzNLXkDV3XDU4o7H2g6DVa17Bj', '2020-07-21 11:14:09', '2020-07-21 11:14:09', NULL, NULL, NULL),
(36, 'Mike ', 'Mike', NULL, 'premprakash563@gmail.com', NULL, '$2y$10$Mw7ljrJtwtcmapf4zlL3weblvhANklxyP5e8FEosVvD0ui4SPDm/O', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-22 03:04:18', '2020-07-22 03:04:18', NULL, NULL, NULL),
(37, 'rizwan ansari', 'rizwan', 'ansari', 'rizwan@techindiasoftware.com', NULL, '$2y$10$24tRLAO3Yk4dmi9IGwH13OyC4Lz/iDtyaHBzVi5BcE5C1epE7Eg4C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-22 12:18:09', '2020-07-22 12:18:09', NULL, NULL, NULL),
(38, 'Sandhya ', 'Sandhya', NULL, 'sandysweet999@gmail.com', NULL, '$2y$10$RU/pa2vJ3eM4YnZxt8p5oOvUK9JdrqHio3BQiy1eZFc759sWY3Yx6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-22 12:26:52', '2020-07-22 12:26:52', NULL, NULL, NULL),
(39, 'jeorge ', 'jeorge', NULL, 'jeorge@gmail.com', NULL, '$2y$10$mgHXnp4t6pyep86hWArQqOs4IIBeSACRM7TXz2MX9HgkePgHaSLG.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-22 12:42:18', '2020-07-22 12:42:18', NULL, NULL, NULL),
(40, 'testq@gmail.com test', 'testq@gmail.com', 'test', 'testq@gmail.com', NULL, '$2y$10$/V7i7ikfWolwkLFzT7bsWu.OyPkoJyosoXtucLPnsCCJ3a1sUL0qi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-23 08:56:51', '2020-07-23 08:56:51', NULL, NULL, NULL),
(41, 'test3456 ', 'test3456', NULL, 'test34567@gmail.com', NULL, '$2y$10$HNFfLxQhDS.HYJ9XqNOE/ubftbVhBkkvz5z6V.cBkcspAs3qZG5Mu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-23 09:11:37', '2020-07-23 09:11:37', NULL, NULL, NULL),
(42, 'test567 ', 'test567', NULL, 'test45678@gmail.com', NULL, '$2y$10$TPE3/syVTU9XJ4ugpt/Ua.jqj1Sp5/V1.GQY7LwCt.6povzHPl3ty', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-23 09:13:17', '2020-07-23 09:13:17', NULL, NULL, NULL),
(43, 'danwebdeveloper8564 ', 'danwebdeveloper8564', NULL, 'mohddanish85641@gmail.com', NULL, '$2y$10$mqOQOHEl3L/SEe1D0elF3OdLtOm.WzfMsrmmNATro/nz6YA0zNxd6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-23 09:17:14', '2020-07-23 09:17:14', NULL, NULL, NULL),
(44, ' Misterwebhub', '', 'Misterwebhub', 'misterwebhub31231@gmail.com', NULL, '$2y$10$ybqqe/ITqDGjm3l.QMesceg9NrN9BOf2UTn0DHj5eYJDXIigypIsK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, 'YDMuHobr7QWIFJO9u3iAy7DEJljHGuXVDCY8Ihl9mA63wIFSUgYrvRtl9Ja4', '2020-07-23 09:19:21', '2020-07-28 11:43:13', NULL, NULL, NULL),
(45, ' test54', '', 'test54', 'test54@gmail.com', NULL, '$2y$10$gH.2FGjKeunq.NC1ONa4RurbEqhWG6EJRL08UKCYs4ZQa3923bppC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, 'LRySsjLZy1KNg9L5oRPgjPB6qly6REyfLS1QPbZONpkzdlrTc7Um9KcYzkRo', '2020-07-23 09:22:59', '2020-07-23 09:28:19', NULL, NULL, NULL),
(46, 'sarvesh kr', 'sarvesh', 'kr', 'sarvesh.dataman@gmail.com', NULL, '$2y$10$StoUTOFfgsB8uSjrIkcUM.lZ1SmGxgYzjhEVqAk7mxHbI3DNPT4g6', NULL, NULL, '778888888', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, 'sQd5VBBjZi5tMZoEM0QyLeqfSot3tem2sa5oXPuVus9l6Fi0ZvCWsXgM3dDy', '2020-07-24 00:03:19', '2020-07-24 11:51:05', NULL, NULL, NULL),
(47, 'test testy', 'test', 'testy', 'test@gmail.com', NULL, '$2y$10$fKDKVyPMaoVmcFbxTTC0aeD/71vdUmMC.C0JbVJZ9KermwURCVas2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-24 10:47:01', '2020-07-24 10:47:01', NULL, NULL, NULL),
(48, 'jack ', 'jack', NULL, 'jack@gmail.com', NULL, '$2y$10$fv8QyIWcnkAcHAqpBGwiZ.nzy4mlFkufcHNdyXWy.rIQVA67OqufG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-24 12:36:10', '2020-07-24 12:36:10', NULL, NULL, NULL),
(49, 'Sandhya ', 'Sandhya', NULL, 'sandhyachauhan1193@gmail.com', NULL, '$2y$10$VQePyV2MJVUtM3w7kFfEHeFvb.M5tr98DnoLPver.LlBQrYjc4jC6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-24 18:46:05', '2020-07-24 18:46:05', NULL, NULL, NULL),
(50, 'Sandhya ', 'Sandhya', NULL, 'fatimashabnam44@gmail.com', NULL, '$2y$10$txaQ8vM8Fokt9amMofnJWOTSdfqbY7lw/DFXTz7ijdaJS///mX.Ey', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-27 12:17:33', '2020-07-27 12:17:33', NULL, NULL, NULL),
(51, 'San ', 'San', NULL, 'fjknkn@gmail.com', NULL, '$2y$10$GSDp9Jj0IxNLh/e51fnOMeKjLHT0rgttMvjX69R/YzV2BNErR4D2q', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-28 12:22:46', '2020-07-28 12:22:46', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_meta`
--

CREATE TABLE `user_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `val` text COLLATE utf8mb4_unicode_ci,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_upgrade_request`
--

CREATE TABLE `user_upgrade_request` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `role_request` int(11) DEFAULT NULL,
  `approved_time` datetime DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_upgrade_request`
--

INSERT INTO `user_upgrade_request` (`id`, `user_id`, `role_request`, `approved_time`, `status`, `approved_by`, `create_user`, `update_user`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 20, 1, NULL, 'pending', NULL, 19, NULL, NULL, '2020-07-09 11:25:56', '2020-07-09 11:25:56'),
(2, 21, 1, NULL, 'pending', NULL, 19, NULL, NULL, '2020-07-09 12:42:37', '2020-07-09 12:42:37'),
(3, 23, 1, NULL, 'pending', NULL, 18, NULL, NULL, '2020-07-15 19:23:59', '2020-07-15 19:23:59'),
(4, 24, 1, NULL, 'pending', NULL, 18, NULL, NULL, '2020-07-15 21:08:14', '2020-07-15 21:08:14'),
(5, 25, 1, '2020-07-21 02:56:08', 'approved', 1, 1, NULL, NULL, '2020-07-19 23:11:07', '2020-07-21 09:56:08'),
(6, 30, 1, NULL, 'pending', NULL, NULL, NULL, NULL, '2020-07-21 10:04:32', '2020-07-21 10:04:32'),
(7, 32, 1, NULL, 'pending', NULL, NULL, NULL, NULL, '2020-07-21 10:32:07', '2020-07-21 10:32:07'),
(8, 34, 1, NULL, 'pending', NULL, NULL, NULL, NULL, '2020-07-21 11:12:13', '2020-07-21 11:12:13'),
(9, 35, 1, NULL, 'pending', NULL, NULL, NULL, NULL, '2020-07-21 11:14:09', '2020-07-21 11:14:09'),
(10, 36, 1, NULL, 'pending', NULL, NULL, NULL, NULL, '2020-07-22 03:04:18', '2020-07-22 03:04:18'),
(11, 38, 1, NULL, 'pending', NULL, NULL, NULL, NULL, '2020-07-22 12:26:52', '2020-07-22 12:26:52'),
(12, 39, 1, NULL, 'pending', NULL, NULL, NULL, NULL, '2020-07-22 12:42:18', '2020-07-22 12:42:18'),
(13, 41, 1, NULL, 'pending', NULL, NULL, NULL, NULL, '2020-07-23 09:11:38', '2020-07-23 09:11:38'),
(14, 42, 1, NULL, 'pending', NULL, NULL, NULL, NULL, '2020-07-23 09:13:18', '2020-07-23 09:13:18'),
(15, 43, 1, NULL, 'pending', NULL, NULL, NULL, NULL, '2020-07-23 09:17:14', '2020-07-23 09:17:14'),
(16, 44, 1, NULL, 'pending', NULL, NULL, NULL, NULL, '2020-07-23 09:19:21', '2020-07-23 09:19:21'),
(17, 45, 1, NULL, 'pending', NULL, NULL, NULL, NULL, '2020-07-23 09:22:59', '2020-07-23 09:22:59'),
(18, 48, 1, NULL, 'pending', NULL, NULL, NULL, NULL, '2020-07-24 12:36:10', '2020-07-24 12:36:10'),
(19, 49, 1, NULL, 'pending', NULL, NULL, NULL, NULL, '2020-07-24 18:46:05', '2020-07-24 18:46:05'),
(20, 50, 1, NULL, 'pending', NULL, NULL, NULL, NULL, '2020-07-27 12:17:34', '2020-07-27 12:17:34'),
(21, 51, 1, NULL, 'pending', NULL, NULL, NULL, NULL, '2020-07-28 12:22:47', '2020-07-28 12:22:47');

-- --------------------------------------------------------

--
-- Table structure for table `user_wishlist`
--

CREATE TABLE `user_wishlist` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `object_model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vendors_plan_payments`
--

CREATE TABLE `vendors_plan_payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `payment_gateway` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `free_trial` int(11) NOT NULL,
  `price_per` enum('onetime','per_time') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'onetime',
  `price_unit` enum('day','month','year') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'day',
  `status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bravo_attrs`
--
ALTER TABLE `bravo_attrs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bravo_attrs_translations`
--
ALTER TABLE `bravo_attrs_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bravo_attrs_translations_origin_id_locale_unique` (`origin_id`,`locale`);

--
-- Indexes for table `bravo_bookings`
--
ALTER TABLE `bravo_bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bravo_booking_meta`
--
ALTER TABLE `bravo_booking_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bravo_booking_payments`
--
ALTER TABLE `bravo_booking_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bravo_booking_slot`
--
ALTER TABLE `bravo_booking_slot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bravo_contact`
--
ALTER TABLE `bravo_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bravo_locations`
--
ALTER TABLE `bravo_locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bravo_locations__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`);

--
-- Indexes for table `bravo_location_translations`
--
ALTER TABLE `bravo_location_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bravo_location_translations_origin_id_locale_unique` (`origin_id`,`locale`);

--
-- Indexes for table `bravo_review`
--
ALTER TABLE `bravo_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bravo_review_meta`
--
ALTER TABLE `bravo_review_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bravo_seo`
--
ALTER TABLE `bravo_seo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bravo_spaces`
--
ALTER TABLE `bravo_spaces`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bravo_spaces_slug_index` (`slug`);

--
-- Indexes for table `bravo_space_dates`
--
ALTER TABLE `bravo_space_dates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bravo_space_term`
--
ALTER TABLE `bravo_space_term`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bravo_space_translations`
--
ALTER TABLE `bravo_space_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bravo_space_translations_locale_index` (`locale`);

--
-- Indexes for table `bravo_terms`
--
ALTER TABLE `bravo_terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bravo_terms_translations`
--
ALTER TABLE `bravo_terms_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bravo_terms_translations_origin_id_locale_unique` (`origin_id`,`locale`);

--
-- Indexes for table `bravo_tours`
--
ALTER TABLE `bravo_tours`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bravo_tours_slug_index` (`slug`);

--
-- Indexes for table `bravo_tour_category`
--
ALTER TABLE `bravo_tour_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bravo_tour_category__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`);

--
-- Indexes for table `bravo_tour_category_translations`
--
ALTER TABLE `bravo_tour_category_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bravo_tour_category_translations_origin_id_locale_unique` (`origin_id`,`locale`);

--
-- Indexes for table `bravo_tour_dates`
--
ALTER TABLE `bravo_tour_dates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bravo_tour_meta`
--
ALTER TABLE `bravo_tour_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bravo_tour_term`
--
ALTER TABLE `bravo_tour_term`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bravo_tour_translations`
--
ALTER TABLE `bravo_tour_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bravo_tour_translations_origin_id_locale_unique` (`origin_id`,`locale`),
  ADD KEY `bravo_tour_translations_slug_index` (`slug`);

--
-- Indexes for table `core_inbox`
--
ALTER TABLE `core_inbox`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_inbox_messages`
--
ALTER TABLE `core_inbox_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_languages`
--
ALTER TABLE `core_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_menus`
--
ALTER TABLE `core_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_menu_translations`
--
ALTER TABLE `core_menu_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `core_menu_translations_locale_index` (`locale`);

--
-- Indexes for table `core_model_has_permissions`
--
ALTER TABLE `core_model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `core_model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `core_model_has_roles`
--
ALTER TABLE `core_model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `core_model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `core_news`
--
ALTER TABLE `core_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_news_category`
--
ALTER TABLE `core_news_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `core_news_category__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`);

--
-- Indexes for table `core_news_category_translations`
--
ALTER TABLE `core_news_category_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `core_news_category_translations_locale_index` (`locale`);

--
-- Indexes for table `core_news_tag`
--
ALTER TABLE `core_news_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_news_translations`
--
ALTER TABLE `core_news_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `core_news_translations_locale_index` (`locale`);

--
-- Indexes for table `core_notifications`
--
ALTER TABLE `core_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_pages`
--
ALTER TABLE `core_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `core_pages_slug_index` (`slug`);

--
-- Indexes for table `core_page_translations`
--
ALTER TABLE `core_page_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `core_page_translations_origin_id_locale_unique` (`origin_id`,`locale`),
  ADD KEY `core_page_translations_locale_index` (`locale`);

--
-- Indexes for table `core_permissions`
--
ALTER TABLE `core_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_roles`
--
ALTER TABLE `core_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_role_has_permissions`
--
ALTER TABLE `core_role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `core_role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `core_settings`
--
ALTER TABLE `core_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_subscribers`
--
ALTER TABLE `core_subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_tags`
--
ALTER TABLE `core_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_tag_translations`
--
ALTER TABLE `core_tag_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `core_tag_translations_locale_index` (`locale`);

--
-- Indexes for table `core_templates`
--
ALTER TABLE `core_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_template_translations`
--
ALTER TABLE `core_template_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `core_template_translations_locale_index` (`locale`);

--
-- Indexes for table `core_translations`
--
ALTER TABLE `core_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_vendor_plans`
--
ALTER TABLE `core_vendor_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_vendor_plan_meta`
--
ALTER TABLE `core_vendor_plan_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `media_files`
--
ALTER TABLE `media_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_meta`
--
ALTER TABLE `user_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_upgrade_request`
--
ALTER TABLE `user_upgrade_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_wishlist`
--
ALTER TABLE `user_wishlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors_plan_payments`
--
ALTER TABLE `vendors_plan_payments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bravo_attrs`
--
ALTER TABLE `bravo_attrs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `bravo_attrs_translations`
--
ALTER TABLE `bravo_attrs_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bravo_bookings`
--
ALTER TABLE `bravo_bookings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `bravo_booking_meta`
--
ALTER TABLE `bravo_booking_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=362;

--
-- AUTO_INCREMENT for table `bravo_booking_payments`
--
ALTER TABLE `bravo_booking_payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bravo_booking_slot`
--
ALTER TABLE `bravo_booking_slot`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `bravo_contact`
--
ALTER TABLE `bravo_contact`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `bravo_locations`
--
ALTER TABLE `bravo_locations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `bravo_location_translations`
--
ALTER TABLE `bravo_location_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bravo_review`
--
ALTER TABLE `bravo_review`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `bravo_review_meta`
--
ALTER TABLE `bravo_review_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=411;

--
-- AUTO_INCREMENT for table `bravo_seo`
--
ALTER TABLE `bravo_seo`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `bravo_spaces`
--
ALTER TABLE `bravo_spaces`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `bravo_space_dates`
--
ALTER TABLE `bravo_space_dates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bravo_space_term`
--
ALTER TABLE `bravo_space_term`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `bravo_space_translations`
--
ALTER TABLE `bravo_space_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bravo_terms`
--
ALTER TABLE `bravo_terms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `bravo_terms_translations`
--
ALTER TABLE `bravo_terms_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bravo_tours`
--
ALTER TABLE `bravo_tours`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT for table `bravo_tour_category`
--
ALTER TABLE `bravo_tour_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `bravo_tour_category_translations`
--
ALTER TABLE `bravo_tour_category_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bravo_tour_dates`
--
ALTER TABLE `bravo_tour_dates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=210;

--
-- AUTO_INCREMENT for table `bravo_tour_meta`
--
ALTER TABLE `bravo_tour_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `bravo_tour_term`
--
ALTER TABLE `bravo_tour_term`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1359;

--
-- AUTO_INCREMENT for table `bravo_tour_translations`
--
ALTER TABLE `bravo_tour_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `core_inbox`
--
ALTER TABLE `core_inbox`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `core_inbox_messages`
--
ALTER TABLE `core_inbox_messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `core_languages`
--
ALTER TABLE `core_languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `core_menus`
--
ALTER TABLE `core_menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `core_menu_translations`
--
ALTER TABLE `core_menu_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `core_news`
--
ALTER TABLE `core_news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `core_news_category`
--
ALTER TABLE `core_news_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `core_news_category_translations`
--
ALTER TABLE `core_news_category_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `core_news_tag`
--
ALTER TABLE `core_news_tag`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `core_news_translations`
--
ALTER TABLE `core_news_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `core_notifications`
--
ALTER TABLE `core_notifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `core_pages`
--
ALTER TABLE `core_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `core_page_translations`
--
ALTER TABLE `core_page_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `core_permissions`
--
ALTER TABLE `core_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `core_roles`
--
ALTER TABLE `core_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `core_settings`
--
ALTER TABLE `core_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT for table `core_subscribers`
--
ALTER TABLE `core_subscribers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `core_tags`
--
ALTER TABLE `core_tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `core_tag_translations`
--
ALTER TABLE `core_tag_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `core_templates`
--
ALTER TABLE `core_templates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `core_template_translations`
--
ALTER TABLE `core_template_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `core_translations`
--
ALTER TABLE `core_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `core_vendor_plans`
--
ALTER TABLE `core_vendor_plans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `core_vendor_plan_meta`
--
ALTER TABLE `core_vendor_plan_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media_files`
--
ALTER TABLE `media_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `user_meta`
--
ALTER TABLE `user_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_upgrade_request`
--
ALTER TABLE `user_upgrade_request`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `user_wishlist`
--
ALTER TABLE `user_wishlist`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendors_plan_payments`
--
ALTER TABLE `vendors_plan_payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `core_model_has_permissions`
--
ALTER TABLE `core_model_has_permissions`
  ADD CONSTRAINT `core_model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `core_permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `core_model_has_roles`
--
ALTER TABLE `core_model_has_roles`
  ADD CONSTRAINT `core_model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `core_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `core_role_has_permissions`
--
ALTER TABLE `core_role_has_permissions`
  ADD CONSTRAINT `core_role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `core_permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `core_role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `core_roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
